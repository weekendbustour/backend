# WeekendBusTour API

## Requirements

* PHP >= 7.4
* MySQL >= 8.0
* Xdebug >= 2.9 *(for debug)*

## Setup (ENG, current)

1. Build and run Docker containers with _docker-compose_
   Go to *laradock* folder:
    ```shell
    cd laradock
    ```

   Copy *.env-example* into *.env* file (dev and production env files are for corresponding environments):
    ```shell
    cp .env-example .env
    ```

   Build containers (first time only):
    ```shell
    docker-compose build
    ```

   Wait until containers are built.

   Run containers (*-d* stands for *"run as daemon"*):
    ```shell
    docker-compose up -d
    ```

   Get in *workspace* container:
    ```shell
    docker-compose exec --user=laradock workspace bash
    ```
2. Packages setup and changing Laravel settings
   Inside *workspace* container run the command:
    ```shell
    composer install
    ``` 

   Copy .env.example into .env (dev and production are for corresponding environments):
    ```shell
    cp .env.example .env
    ```

   Generating Laravel app key (if it's **not** inside .env file):
    ```shell
    php artisan key:generate
    ```

   To make `storage` folder accessible:
    ```shell
    php artisan storage:link
    ```

   Run migrations (still inside *workspace* container) and seeds:
    ```shell
    // local & dev envs only
    php artisan migrate --seed
   
    // on production env
    php artisan migrate
    php artisan db:seed
    ```

3. Laravel .env file changes

   Check following constants, read the .env.example for info:
    ```dotenv
    APP_URL
    WEB_URL
    API_URL
    IMAGE_URL
    CMF_URL
    All Database-related constants
    REDIS_HOST
    API_DOMAIN
    ```

#### Useful commands

* To stop containers run the following command in *laradock* folder:
    ```shell
    docker-compose stop
    ```

* To re-build containers (run in *laradock* folder):
    ```shell
    docker-compose up -d --build
    // or
    docker-compose build --no-cache
    ```

* Before any new seeder run:
    ```shell
    composer dump-autoload
    ```

* Optimize configurations and routes (if API don't response 200 or seen error like a 'The version given was unknown or has no registered routes.'):
    ```shell
    make optimize
    ```

* Build styles and scripts for admin
    ```shell
    npm install
    npm run cmf-watch
    npm run cmf-dev
    npm run cmf-prod
    ```
  
#### Basic commands
- cp .env.example .env
- php artisan storage:link
- php artisan key:generate
- cd ./public git clone ... frontend
- composer install
- npm install
- cd ./public/frontend yarn install
- build-npm-frontend
