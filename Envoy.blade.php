@setup
    require __DIR__.'/vendor/autoload.php';
    $dotenv = Dotenv\Dotenv::create(__DIR__);
    try {
        $dotenv->load();
        $dotenv->required(['DEPLOY_USER', 'DEPLOY_SERVER', 'DEPLOY_BASE_DIR', 'DEPLOY_REPO'])->notEmpty();
    } catch ( Exception $e )  {
        echo $e->getMessage();
    }

    $user = env('DEPLOY_USER');
    $server = env('DEPLOY_SERVER');

    $repo = env('DEPLOY_REPO');

    if (!isset($baseDir)) {
        $baseDir = env('DEPLOY_BASE_DIR');
    }

    if (!isset($branch)) {
        throw new Exception('--branch must be specified');
    }

    $releaseDir = $baseDir . '/releases';
    $currentDir = $baseDir . '/current';
    $release = date('YmdHis');
    $currentReleaseDir = $releaseDir . '/' . $release;
    $currentReleaseDirFrontend = $currentReleaseDir . '/public/frontend';

    function logMessage($message) {
        return "echo '\033[32m" .$message. "\033[0m';\n";
    }
@endsetup

@servers(['prod' => [$user . '@' . $server], 'local' => '127.0.0.1'])

@story('deploy', ['on' => 'prod'])
    git
    composer
    npm_frontend_run_prod
    npm_backend_run_prod
@endstory

@task('upload', ['on' => 'local'])
    {{ logMessage('Upload files') }}
    rsync -av -e ssh \
    --exclude='.git' \
    . {{ $user }}@{{ $server }}:/var/www/api
@endtask

@story('deploy-r', ['on' => 'prod'])
    git
    composer
    npm_backend_install
    npm_backend_run_prod
    npm_frontend_install
    npm_frontend_run_prod
    update_symlinks
    cache
    migrate
    finish
    clean_old_releases
@endstory

@story('rollback')
    rollback
@endstory

@task('git')
    {{ logMessage("Cloning repository") }}

    cd {{ $currentDir }}
    git pull -q
    git submodule update -q
@endtask

@task('build')
    {{ logMessage("Build Docker") }}

    cd {{ $currentDir }}/laradock
    docker-compose down
    docker-compose build workspace
    docker-compose up -d workspace
    docker-compose exec --user=root workspace apt install php7.4-redis -y
@endtask

@task('composer')
    {{ logMessage("Running composer") }}

    cd {{ $currentDir }}
    composer install
@endtask

@task('cache')
    {{ logMessage("Building cache") }}

    php {{ $currentReleaseDir }}/artisan route:cache --quiet

    php {{ $currentReleaseDir }}/artisan config:cache --quiet
@endtask

@task('update_symlinks')
    {{ logMessage("Updating symlinks") }}

    # Remove the storage directory and replace with persistent data
    {{ logMessage("Linking storage directory") }}
    rm -rf {{ $currentReleaseDir }}/storage
    ln -nfs {{ $baseDir }}/storage {{ $currentReleaseDir }}/storage

    # Import the environment config
    {{ logMessage("Linking .env file") }}
    ln -nfs {{ $baseDir }}/.env {{ $currentReleaseDir }}/.env
@endtask

@task('finish')
    {{ logMessage("Linking current release") }}
    ln -nfs {{ $currentReleaseDir }} {{ $currentDir }}
@endtask

@task('migrate')
    {{ logMessage("Running migrations") }}

    php {{ $currentReleaseDir }}/artisan migrate --force
@endtask

@task('npm_backend_install')
    {{ logMessage("NPM install") }}

    cd {{ $currentDir }}
    docker-compose exec --user=root --workdir /var/www workspace npm install
@endtask

@task('npm_frontend_install')
    {{ logMessage("YARN install") }}

    cd {{ $currentDir }}
    docker-compose exec --user=root --workdir /var/www/public/frontend workspace yarn install
@endtask

@task('npm_backend_run_prod')
    {{ logMessage("NPM run prod") }}

    cd {{ $currentDir }}
    make build-npm-backend
@endtask

@task('npm_frontend_run_prod')
    {{ logMessage("YARN run prod") }}

    cd {{ $currentDir }}
    make build-npm-frontend
@endtask

@task('clean_old_releases')
    # Delete all but the 5 most recent releases
    {{ logMessage("Cleaning old releases") }}
    cd {{ $releaseDir }}
    ls -dt {{ $releaseDir }}/* | tail -n +6 | xargs -d "\n" rm -rf;
@endtask

@task('init')
    if [ ! -d {{ $baseDir }}/current ]; then
    cd {{ $baseDir }}
    git clone {{ $repo }} --branch={{ $branch }} --depth=1 -q {{ $release }}
    {{ logMessage("Repository cloned") }}
    mv {{ $release }}/storage {{ $baseDir }}/storage
    ln -s {{ $baseDir }}/storage {{ $release }}/storage
    ln -s {{ $baseDir }}/storage/public {{ $release }}/public/storage
    {{ logMessage("Storage directory set up") }}
    cp {{ $release }}/.env.example {{ $baseDir }}/.env
    ln -s {{ $baseDir }}/.env {{ $release }}/.env
    {{ logMessage("Environment file set up") }}
    rm -rf {{ $release }}
    {{ logMessage("Deployment path initialised. Run 'envoy run deploy' now.") }}
    else
    {{ logMessage("Deployment path already initialised (current symlink exists)!") }}
    fi
@endtask

@task('rollback')
    cd {{ $releaseDir }}
    ln -nfs {{ $releaseDir }}/$(find . -maxdepth 1 -name "20*" | sort  | tail -n 2 | head -n1) {{ $baseDir }}/current
    echo "Rolled back to $(find . -maxdepth 1 -name "20*" | sort  | tail -n 2 | head -n1)"
@endtask

@finished
    echo "Envoy deployment complete.\r\n";
@endfinished
