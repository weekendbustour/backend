<?php

namespace Tests\Browser;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;
use Laravel\Dusk\Concerns\ProvidesBrowser;

class DuskCommon extends DuskTestCase
{
    use ProvidesBrowser;

    /**
     * Куки для брайзера, если они есть то в bootstrap/app.php подключается другой .env
     */
    const COOKIE_NAME = 'dusk-cookie';

    /**
     * Название метода в котором происходит тест
     *
     * @var null|string
     */
    protected $method = null;

    /**
     *
     */
    public function setUp(): void
    {
        parent::setUp();
        //$this->artisan('migrate:fresh');
    }

    /**
     * Закрыть брайзер после теста
     *
     * @throws \Throwable
     */
    public function tearDown(): void
    {
        parent::tearDown();
        static::closeAll();
    }

    /**
     * @param Browser $browser
     */
    private function setCookie(Browser $browser): void
    {
        $appUrl = config('app.url');
        $aUrl = parse_url($appUrl);
        $domain = $aUrl['host'];
        $protocol = $aUrl['scheme'];
        if (config('dusk.auth_enabled')) {
            $browserUser = config('dusk.auth.user');
            $browserPassword = config('dusk.auth.password');
            $browser
                ->visit($protocol . '://' . $browserUser . ':' . $browserPassword . '@' . $domain . '/');
        } else {
            $browser
                ->visit('/');
        }
        $browser
            ->cookie(DuskCommon::COOKIE_NAME, Str::random(16), time() + 86400 * 2, [
                'path' => '/',
                'domain' => $domain,
                'secure' => false,
                'httpOnly' => true,
            ]);
    }

    /**
     * Делает тоже самое что и browse, но перед коллбеком добавляет куки
     * Чтобы не писать каждй раз $this->secCookie()
     *
     * @param callable $callback
     * @return Browser|void
     * @throws \Throwable
     */
    public function browseCommon(callable $callback)
    {
        return $this->browse(function (Browser $browser) use ($callback) {
            $this->setCookie($browser);
            $callback($browser);
            $this->method = null;
        });
    }

    /**
     * @param string $method
     * @param int|null $number
     * @return string
     */
    protected function screenName(string $method, ?int $number = null): string
    {
        $method = str_replace('test', '', $method);
        $method = Str::kebab($method);
        $path = property_exists($this, 'screenPath') ? $this->screenPath : 'common';
        return $path . '/' . $method . '-' . $number;
    }

    /**
     * Удаление скриншотов метода перед
     */
    private function clearScreenshots()
    {
        if (property_exists($this, 'screenPath') && $this->method !== null) {
            $dir = base_path() . '/tests/Browser/screenshots/' . $this->screenPath;
            $fileNeedles = str_replace($this->screenPath . '/', '', $this->screenName($this->method));
            if (File::isDirectory($dir)) {
                $files = File::files($dir);
                foreach ($files as $file) {
                    if (Str::startsWith($file->getBasename(), $fileNeedles)) {
                        File::delete($file->getPathname());
                    }
                }
            }
        }
    }

    /**
     * @param string $method
     */
    public function setMethod(string $method): void
    {
        $this->method = $method;
        $this->clearScreenshots();
    }
}
