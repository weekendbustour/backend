<?php

namespace Tests\Browser\Tests\Common;

use Laravel\Dusk\Browser;
use Tests\Browser\DuskCommon;
use Tests\FactoryTrait;
use Tests\SnipeMigrations;

class StartTest extends DuskCommon
{
    use FactoryTrait;
    //use SnipeMigrations;

    /**
     * @var string
     */
    public $screenPath = 'common';

    /**
     * Для того чтобы запустить Snipe и просто создать что-то
     *
     * ~ 108с на тест
     * @group snipe
     *
     * @throws \Throwable
     */
    public function testStart()
    {
        $this->setMethod(__FUNCTION__);

        info('-------------------------------------------------------');
        info('-------------------------------------------------------');
        info('----------------------' . 'START DUSK' . '-----------------------');
        info('-------------------------------------------------------');
        info('-------------------------------------------------------');
        $this->factoryUser();
        $this->assertTrue(true);

        $this->browseCommon(function (Browser $browser) {
            $browser
                ->visit('/')
                ->pause(2000)
                ->screenshot($this->screenName($this->method, __LINE__))
                ->visit('/tours')
                ->pause(2000)
                ->screenshot($this->screenName($this->method, __LINE__))
                ->visit('/dev/tour/47/card')
                ->pause(2000)
                ->screenshot($this->screenName($this->method, __LINE__));
        });
    }
}
