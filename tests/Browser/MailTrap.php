<?php

namespace Tests\Browser;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Config;
use Exception;
use RuntimeException;

trait MailTrap
{

    /**
     * The MailTrap configuration.
     *
     * @var int
     */
    protected $mailTrapInboxId;

    /**
     * The MailTrap API Key.
     *
     * @var string
     */
    protected $mailTrapApiKey;

    /**
     * The Guzzle client.
     *
     * @var Client
     */
    protected $client;

    /**
     * Get the configuration for MailTrap.
     *
     * @param int|null $inboxId
     * @throws Exception
     */
    protected function applyMailTrapConfiguration($inboxId = null)
    {
        if (null === $config = Config::get('services.mailtrap')) {
            throw new Exception(
                'Set "secret" and "default_inbox" keys for "mailtrap" in "config/services.php."'
            );
        }

        $this->mailTrapInboxId = $inboxId ?: $config['default_inbox'];
        $this->mailTrapApiKey = $config['api_token'];
    }

    /**
     * Fetch a MailTrap inbox.
     *
     * @param int|null $inboxId
     * @return array|mixed
     * @throws Exception
     */
    protected function fetchInbox(int $inboxId = null)
    {
        if (!$this->alreadyConfigured()) {
            $this->applyMailTrapConfiguration($inboxId);
        }

        $body = $this->requestClient()
            ->get($this->getMailTrapMessagesUrl())
            ->getBody();

        return $this->parseJson($body);
    }

    /**
     *
     * Empty the MailTrap inbox.
     *
     */
    public function emptyInbox()
    {
        $this->requestClient()->patch($this->getMailTrapCleanUrl());
    }

    /**
     * Get the MailTrap messages endpoint.
     *
     * @return string
     */
    protected function getMailTrapMessagesUrl(): string
    {
        return "/api/v1/inboxes/{$this->mailTrapInboxId}/messages";
    }

    /**
     * Get the MailTrap "empty inbox" endpoint.
     *
     * @return string
     */
    protected function getMailTrapCleanUrl(): string
    {
        return "/api/v1/inboxes/{$this->mailTrapInboxId}/clean";
    }

    /**
     * Determine if MailTrap config has been retrieved yet.
     *
     * @return bool
     */
    protected function alreadyConfigured(): bool
    {
        return !empty($this->mailTrapApiKey);
    }

    /**
     * Request a new Guzzle client.
     *
     * @return Client
     */
    protected function requestClient(): Client
    {
        $this->client = new Client([
            'base_uri' => 'https://mailtrap.io',
            'headers' => ['Api-Token' => $this->mailTrapApiKey],
        ]);

        return $this->client;
    }

    /**
     * @param string $body
     * @return array|mixed
     */
    protected function parseJson(string $body)
    {
        $data = json_decode((string)$body, true);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new RuntimeException('Unable to parse response body into JSON: ' . json_last_error());
        }

        return $data === null ? [] : $data;
    }

    /**
     * Search Messages Url
     *
     * @param string $query Search query
     * @return string
     */
    protected function searchInboxMessagesUrl($query): string
    {
        return "/api/v1/inboxes/{$this->mailTrapInboxId}/messages?search=" . $query;
    }

    /**
     * Find and fetch a Message By Query.
     *
     * @param string $query
     * @return array|mixed
     * @throws Exception
     */
    protected function findMessage(string $query)
    {
        if (!$this->alreadyConfigured()) {
            $this->applyMailTrapConfiguration();
        }

        $body = $this->requestClient()
            ->get($this->searchInboxMessagesUrl($query))
            ->getBody();

        return $this->parseJson($body);
    }

    /**
     * Check if a message exists based on a string query.
     *
     * @param string $query
     * @return bool
     * @throws Exception
     */
    protected function messageExists(string $query): bool
    {
        $messages = $this->findMessage($query);

        return count($messages) > 0;
    }

    /**
     * Check if a message exists based on a string query.
     *
     * @param string $query
     * @param string $email
     * @return bool
     * @throws Exception
     */
    protected function messageExistsByEmail(string $query, string $email): bool
    {
        $messages = $this->findMessage($query);
        if (count($messages) === 0) {
            return false;
        }
        foreach ($messages as $message) {
            if (isset($message['to_email']) && $message['to_email'] === $email) {
                return true;
            }
        }
        return false;
    }
}
