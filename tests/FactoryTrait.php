<?php

namespace Tests;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Departure;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\User;

trait FactoryTrait
{
    /**
     * @param string $class
     * @param array $data
     * @return mixed
     */
    protected function factoryMake(string $class, array $data = [])
    {
        return factory($class)->make($data);
    }

    /**
     * @param string $class
     * @param array $data
     * @return array
     */
    protected function factoryRaw(string $class, array $data = []): array
    {
        return array_merge(factory($class)->raw(), $data);
    }

    /**
     * @param array $data
     * @return User
     */
    protected function factoryUser(array $data = []): User
    {
        return factory(User::class)->create($data);
    }

    /**
     * @param array $data
     * @return Organization
     */
    protected function factoryOrganization(array $data = []): Organization
    {
        return factory(Organization::class)->create($data);
    }

    /**
     * @param array $data
     * @return Tour
     */
    protected function factoryTour(array $data = []): Tour
    {
        return factory(Tour::class)->create($data);
    }

    /**
     * @param array $data
     * @return Category
     */
    protected function factoryCategory(array $data = []): Category
    {
        return factory(Category::class)->create($data);
    }

    /**
     * @param array $data
     * @return Direction
     */
    protected function factoryDirection(array $data = []): Direction
    {
        return factory(Direction::class)->create($data);
    }

    /**
     * @param array $data
     * @return Departure
     */
    protected function factoryDeparture(array $data = []): Departure
    {
        return factory(Departure::class)->create($data);
    }

    /**
     * @param array $data
     * @return Comment
     */
    protected function factoryComment(array $data = []): Comment
    {
        return factory(Comment::class)->create($data);
    }

    /**
     * @param array $data
     * @return User
     */
    protected function factoryAdmin(array $data = []): User
    {
        return factory(User::class)->create($data);//->assignRole(User::ROLE_SUPERADMIN);
    }
}
