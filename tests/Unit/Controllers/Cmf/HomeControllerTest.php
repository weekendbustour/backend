<?php

namespace Tests\Unit\Controllers\Cmf;

use App\Cmf\Project\HomeController;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\FactoryTrait;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    use DatabaseTransactions;
    use FactoryTrait;

    /**
     * @var User
     */
    private $user;

    /**
     * Создание сущностей
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->user = $this->factoryAdmin();
    }

    /**
     * @return HomeController
     */
    private function controller(): HomeController
    {
        return new HomeController();
    }

    /**
     * @see HomeController::index()
     */
    public function testIndex()
    {
        $response = $this->controller()->index();
        $this->assertTrue($response instanceof \Illuminate\View\View);
    }

    /**
     * @see HomeController::unknown()
     */
    public function testUnknown()
    {
        $response = $this->controller()->unknown('wrong-name');
        $this->assertTrue($response instanceof \Illuminate\View\View);
    }
}
