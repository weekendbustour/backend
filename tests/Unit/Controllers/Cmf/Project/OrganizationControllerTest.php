<?php

namespace Tests\Unit\Controllers\Cmf\Project;

use App\Cmf\Project\Organization\OrganizationController;
use App\Cmf\Project\User\UserController;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\FactoryTrait;
use Tests\TestCase;

class OrganizationControllerTest extends TestCase
{
    //use DatabaseTransactions;
    use FactoryTrait;

    /**
     * @return OrganizationController
     */
    private function controller(): OrganizationController
    {
        return new OrganizationController();
    }

    /**
     * @see UserController::index()
     */
    public function testIndex()
    {
        $response = $this->controller()->index($this->request());
        $this->assertInstanceOf(\Illuminate\View\View::class, $response);
    }

    public function testCreate()
    {
        $oOrganization = $this->factoryOrganization();
        $oCategory = $this->factoryCategory();
        $oOrganization->categories()->attach($oCategory->id);
        $oCategory = $this->factoryCategory();
        $oOrganization->categories()->attach($oCategory->id);

        $oDirection = $this->factoryDirection();
        $oDeparture = $this->factoryDeparture([
            'organization_id' => $oOrganization->id,
        ]);

        $oTour = $this->factoryTour([
            'organization_id' => $oOrganization->id,
            'direction_id' => $oDirection->id,
        ]);
        $oTour->departures()->attach($oDeparture->id, [
            'departure_at' => now(),
        ]);
        $oTour = $this->factoryTour([
            'organization_id' => $oOrganization->id,
            'direction_id' => $oDirection->id,
        ]);
        $oTour->departures()->attach($oDeparture->id, [
            'departure_at' => now(),
        ]);
        $oTour = $this->factoryTour([
            'organization_id' => $oOrganization->id,
            'direction_id' => $oDirection->id,
        ]);
        $oTour->departures()->attach($oDeparture->id, [
            'departure_at' => now(),
        ]);
        $oTour = $this->factoryTour([
            'organization_id' => $oOrganization->id,
            'direction_id' => $oDirection->id,
        ]);
        $oTour->departures()->attach($oDeparture->id, [
            'departure_at' => now(),
        ]);


        $oUser = $this->factoryUser();

        $oUser->rate($oOrganization, 2);


        $this->assertTrue(true);
    }
}
