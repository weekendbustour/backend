<?php

namespace Tests\Unit\Controllers\Cmf\Project;

use App\Cmf\Project\Direction\DirectionController;
use App\Cmf\Project\Organization\OrganizationController;
use App\Cmf\Project\User\UserController;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\FactoryTrait;
use Tests\TestCase;

class DirectionControllerTest extends TestCase
{
    //use DatabaseTransactions;
    use FactoryTrait;

    /**
     * @return DirectionController
     */
    private function controller(): DirectionController
    {
        return new DirectionController();
    }

    /**
     * @see UserController::index()
     */
    public function testIndex()
    {
        $response = $this->controller()->index($this->request());
        $this->assertInstanceOf(\Illuminate\View\View::class, $response);
    }

    public function testCreate()
    {
        $oDirection = $this->factoryDirection();

        $oDirection->comments()->saveMany([
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
            $this->factoryComment(),
        ]);

        $this->assertTrue(true);
    }
}
