<?php

namespace Tests\Unit\Services\Image\Upload;

use App\Models\Image;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use App\Services\Image\Path\ImagePathService;
use App\Services\Image\Upload\ImageUploadService;
use Tests\TestCase;
use App\Models\User;
use Tests\FactoryTrait;
use Illuminate\Support\Facades\File;
use App\Cmf\Project\User\UserController;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImageUploadServiceTest extends TestCase
{
    use DatabaseTransactions;
    use FactoryTrait;

    /**
     * @return ImageUploadService
     */
    public function service(): ImageUploadService
    {
        return new ImageUploadService();
    }

    /**
     * @return ImagePathService
     */
    public function path(): ImagePathService
    {
        return new ImagePathService();
    }

    /**
     * @param string $type
     * @param string $key
     * @return array
     */
    private function getOptions(string $type, string $key): array
    {
        return [
            'key' => $key,
            'type' => $type,
            'with_main' => true,
            'unique' => false,
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
                ImageSize::CARD_DEFAULT => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 350,
                        'height' => 235,
                    ],
                ],
            ],
        ];
    }

    /**
     *
     */
    public function testUpload()
    {
        $file = $this->getFile('file.jpg', __DIR__);
        $key = 'model';
        $type = 'type';
        $id = 1;
        $aOptions = $this->getOptions($type, $key);
        $filename = $this->service()->uploadFile($file, $key, $id, $aOptions['filters'], $type);

        $this->assertTrue(File::exists(public_path('storage/testing/images/' . $key . '/' . $id . '/' . $type . '/original/' . $filename)));
    }

    public function testUploadModelImage()
    {
        $oUser = $this->factoryUser();
        $file = $this->getFile('file.jpg', __DIR__);
        $key = 'user';
        $type = ImageType::MODEL;
        $id = $oUser->id;
        $aOptions = $this->getOptions($type, $key);
        $filename = $this->service()->uploadFile($file, $key, $id, $aOptions['filters'], $type);
        $this->assertTrue(File::exists(public_path('storage/testing/images/' . $key . '/' . $id . '/' . $type . '/original/' . $filename)));
        $oUser->images()->create([
            'type' => $type,
            'filename' => $filename,
            'is_main' => 1,
        ]);
        $file = $oUser->image;
        $this->assertTrue(File::exists(public_path($file)));
    }

    public function testUploadGalleryModelImage()
    {
        $oUser = $this->factoryUser();
        $file = $this->getFile('file.jpg', __DIR__);
        $key = 'user';
        $type = ImageType::GALLERY;
        $id = $oUser->id;
        $aOptions = $this->getOptions($type, $key);
        $filename = $this->service()->uploadFile($file, $key, $id, $aOptions['filters'], $type);

        $this->assertTrue(File::exists(public_path('storage/testing/images/' . $key . '/' . $id . '/' . $type . '/original/' . $filename)));
        $oUser->images()->create([
            'type' => $type,
            'filename' => $filename,
        ]);
        $files = $oUser->gallery;
        $this->assertTrue(File::exists(public_path($files[0])));
    }
}
