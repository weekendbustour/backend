<?php

namespace Tests\Unit\Services\Image\Path;

use App\Services\Image\Path\ImagePathService;
use Tests\TestCase;
use App\Models\User;
use Tests\FactoryTrait;
use Illuminate\Support\Facades\File;
use App\Cmf\Project\User\UserController;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImagePathServiceTest extends TestCase
{
    use DatabaseTransactions;
    use FactoryTrait;

    /**
     * @return ImagePathService
     */
    public function service(): ImagePathService
    {
        return new ImagePathService();
    }

    /**
     *
     */
    public function testUpload()
    {
        $this->assertTrue(true);
    }
}
