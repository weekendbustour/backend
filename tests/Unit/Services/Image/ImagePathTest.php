<?php

namespace Tests\Unit\Services\Image;

use Tests\TestCase;
use App\Models\User;
use Tests\FactoryTrait;
use Illuminate\Support\Facades\File;
use App\Cmf\Project\User\UserController;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ImagePathTest extends TestCase
{
    use DatabaseTransactions;
    use FactoryTrait;

    /**
     * @var User
     */
    private $user;

    /**
     * Создание сущностей.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->user = $this->factoryUser();
    }

    /**
     * @return UserController
     */
    private function controller(): UserController
    {
        return new UserController();
    }

    /**
     * @return User
     */
    private function model(): User
    {
        return User::find($this->user->id);
    }

    /**
     * @see \App\Services\Image\ImagePath::default()
     */
    public function testDefault()
    {
        $fileDefault = ImagePath::default();
        $this->assertTrue(File::exists(public_path($fileDefault)));

        $file = ImagePath::default('WRONG_KEY');
        $this->assertEquals($fileDefault, $file);
    }

    /**
     * @see \App\Services\Image\ImagePath::image()
     *
     * @throws \Throwable
     */
    public function testImage()
    {
        $fileDefault = ImagePath::default('user');
        $this->assertTrue(File::exists(public_path($fileDefault)));

        $oItem = $this->user;
        $this->uploadImage($this->model());

        $file = ImagePath::main('user', 'square', $oItem);
        $this->assertTrue(File::exists(public_path($file)));

        $oItem = $this->model();
        foreach ($oItem->images as $image) {
            $fileImage = ImagePath::image('user', 'square');
            $this->assertEquals($fileDefault, $fileImage);

            $fileImage = ImagePath::image('user', 'square', $image);
            $this->assertTrue(File::exists(public_path($fileImage)));

            $publicPath = ImagePath::publicPath('user', 'square', $image);
            $this->assertEquals(public_path($fileImage), $publicPath);

            $filename = $image->filename;
            $image->update([
                'filename' => null,
            ]);
            $fileImage = ImagePath::image('user', 'square', $image);
            $this->assertEquals($fileDefault, $fileImage);

            $publicPath = ImagePath::publicPath('user', 'square', $image);
            $this->assertEquals(public_path($fileDefault), $publicPath);

            $image->update([
                'filename' => $filename,
            ]);
        }

        // удаление всех, потому что тестирование
        $this->destroyImages($this->model());
    }

    /**
     * @see \App\Services\Image\ImagePath::main()
     *
     * @throws \Throwable
     */
    public function testMain()
    {
        $oItem = $this->user;
        $fileDefault = ImagePath::default('user');

        // дефолтное изображение
        $file = ImagePath::main('user', 'square', null);
        $this->assertTrue(File::exists(public_path($file)));
        $this->assertEquals($fileDefault, $file);

        // дефолтное изображение
        $file = ImagePath::main('user', 'square', $oItem);
        $this->assertTrue(File::exists(public_path($file)));
        $this->assertEquals($fileDefault, $file);

        $this->uploadImage($this->model());

        $file = ImagePath::main('user', 'square', $oItem);
        $this->assertTrue(File::exists(public_path($file)));

        // удаление всех, потому что тестирование
        $this->destroyImages($this->model());
    }

    /**
     * @see \App\Services\Image\ImagePath::checkMain()
     *
     * @throws \Throwable
     */
    public function testCheckMain()
    {
        // без изображений
        $file = ImagePath::checkMain('user', 'square', $this->model());
        $this->assertEquals(false, $file);

        // с изображениями
        $this->uploadImage($this->model());
        $file = ImagePath::checkMain('user', 'square', $this->model());
        $this->assertEquals(true, $file);

        // без главного изображения
        $oModel = $this->model();
        foreach ($oModel->images as $image) {
            $image->update([
                'is_main' => 0,
            ]);
        }
        $file = ImagePath::checkMain('user', 'square', $this->model());
        $this->assertEquals(false, $file);

        // с главным
        $oImage = $this->model()->images()->first();
        $oImage->update([
            'is_main' => 1,
        ]);
        $file = ImagePath::checkMain('user', 'square', $this->model());
        $this->assertEquals(true, $file);

        // с удаленным изображением
        $file = ImagePath::publicPath('user', 'square', $oImage);
        File::delete($file);
        $file = ImagePath::checkMain('user', 'square', $this->model());
        $this->assertEquals(false, $file);

        // удаление всех, потому что тестирование
        $this->destroyImages($this->model());
    }

    /**
     * Загрузить файл.
     *
     * @param object $oItem
     * @throws \Throwable
     */
    private function uploadImage($oItem)
    {
        // успешная загрузка
        $file = $this->getFile('accounting.jpg', __DIR__);
        $response = $this->controller()->imageUpload($this->request([
            'images' => [
                $file,
            ],
            'validate' => false,
        ]), $oItem->id);
    }

    /**
     * Удалить все изображения.
     *
     * @param object $oItem
     */
    private function destroyImages($oItem)
    {
        $this->controller()->beforeDeleteForImages($oItem);
    }
}
