<?php

namespace Tests;

use Drfraker\SnipeMigrations\Snipe;
use Illuminate\Foundation\Testing\RefreshDatabaseState;

trait SnipeMigrations
{
    /**
     * @see \Drfraker\SnipeMigrations\SnipeMigrations
     *
     * Отключена проверка на другие трейты, т.е. запускать принудительно если есть
     */
    public function setUpTraits()
    {
        (new Snipe())->importSnapshot();
        RefreshDatabaseState::$migrated = true;
        parent::setUpTraits();
    }
}
