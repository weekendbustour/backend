<?php

use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\SyslogUdpHandler;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Log Channel
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */

    'default' => env('LOG_CHANNEL', 'stack'),

    /*
    |--------------------------------------------------------------------------
    | Log Channels
    |--------------------------------------------------------------------------
    |
    | Here you may configure the log channels for your application. Out of
    | the box, Laravel uses the Monolog PHP logging library. This gives
    | you a variety of powerful log handlers / formatters to utilize.
    |
    | Available Drivers: "single", "daily", "slack", "syslog",
    |                    "errorlog", "monolog",
    |                    "custom", "stack"
    |
    */

    'channels' => [
        'stack' => [
            'driver' => 'stack',
            'channels' => ['daily', 'slack'],
            'ignore_exceptions' => false,
        ],
        'stack-dusk' => [
            'driver' => 'stack',
            'channels' => ['daily-dusk'],
        ],
        'daily-dusk' => [
            'driver' => 'daily',
            'path' => storage_path('logs/dusk/laravel.log'),
            'level' => 'debug',
            'days' => 14,
        ],

        'single' => [
            'driver' => 'single',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
        ],

        'daily' => [
            'driver' => 'daily',
            'path' => storage_path('logs/laravel.log'),
            'level' => 'debug',
            'days' => 14,
            'permission' => 0775,
        ],
        'slack' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEBHOOK_URL'),
            'username' => 'weekendbustour.ru',
            'emoji' => 'https://drive.google.com/open?id=139VGj7GIP3F2Y7RH7p5A2OcfpRbmlTPb',
            'level' => 'debug',
        ],
        // http://prntscr.com/sgkwom
        'slack-debug' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_DEBUG_WEBHOOK_URL'),
            'username' => 'weekendbustour.ru',
            'emoji' => 'https://drive.google.com/open?id=139VGj7GIP3F2Y7RH7p5A2OcfpRbmlTPb',
            'level' => 'debug',
            'short' => true,
            'env' => '*',
        ],
        'slack-critical' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_CRITICAL_WEBHOOK_URL'),
            'username' => 'weekendbustour.ru',
            'emoji' => 'https://drive.google.com/open?id=139VGj7GIP3F2Y7RH7p5A2OcfpRbmlTPb',
            'level' => 'debug',
            'short' => true,
            'env' => '*',
            'exception_enabled' => env('LOG_SLACK_CRITICAL_EXCEPTION_ENABLED', false),
        ],
        'slack-sitemap' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_SITEMAP_WEBHOOK_URL'),
            'username' => 'weekendbustour.ru',
            'emoji' => 'https://drive.google.com/open?id=139VGj7GIP3F2Y7RH7p5A2OcfpRbmlTPb',
            'level' => 'debug',
            'short' => true,
            'env' => '*',
        ],
        'slack-queue' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_QUEUE_WEBHOOK_URL'),
            'username' => 'weekendbustour.ru',
            'emoji' => 'https://drive.google.com/open?id=139VGj7GIP3F2Y7RH7p5A2OcfpRbmlTPb',
            'level' => 'debug',
            'short' => true,
            'env' => '*',
        ],
        'slack-weather' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_WEATHER_WEBHOOK_URL'),
            'username' => 'weekendbustour.ru',
            'emoji' => 'https://drive.google.com/open?id=139VGj7GIP3F2Y7RH7p5A2OcfpRbmlTPb',
            'level' => 'debug',
            'short' => true,
            'env' => '*',
        ],
        'slack-yandex-cloud' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_YANDEX_CLOUD_WEBHOOK_URL'),
            'username' => 'weekendbustour.ru',
            'emoji' => 'https://drive.google.com/open?id=139VGj7GIP3F2Y7RH7p5A2OcfpRbmlTPb',
            'level' => 'debug',
            'short' => true,
            'env' => '*',
        ],
        'slack-reservations' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_RESERVATIONS_WEBHOOK_URL'),
            'username' => 'weekendbustour.ru',
            'emoji' => 'https://drive.google.com/open?id=139VGj7GIP3F2Y7RH7p5A2OcfpRbmlTPb',
            'level' => 'debug',
            'short' => true,
            'env' => '*',
        ],
        'slack-feedback-rating' => [
            'driver' => 'slack',
            'url' => env('LOG_SLACK_FEEDBACK_RATING_WEBHOOK_URL'),
            'username' => 'weekendbustour.ru',
            'emoji' => 'https://drive.google.com/open?id=139VGj7GIP3F2Y7RH7p5A2OcfpRbmlTPb',
            'level' => 'debug',
            'short' => true,
            'env' => '*',
        ],
        'papertrail' => [
            'driver' => 'monolog',
            'level' => 'debug',
            'handler' => SyslogUdpHandler::class,
            'handler_with' => [
                'host' => env('PAPERTRAIL_URL'),
                'port' => env('PAPERTRAIL_PORT'),
            ],
        ],

        'stderr' => [
            'driver' => 'monolog',
            'handler' => StreamHandler::class,
            'formatter' => env('LOG_STDERR_FORMATTER'),
            'with' => [
                'stream' => 'php://stderr',
            ],
        ],

        'syslog' => [
            'driver' => 'syslog',
            'level' => 'debug',
        ],

        'errorlog' => [
            'driver' => 'errorlog',
            'level' => 'debug',
        ],

        'null' => [
            'driver' => 'monolog',
            'handler' => NullHandler::class,
        ],

        'emergency' => [
            'path' => storage_path('logs/laravel.log'),
        ],

        'test' => [
            'driver' => 'daily',
            'path' => storage_path('logs/testing/laravel.log'),
            'level' => 'debug',
            'days' => 14,
        ],
    ],

];
