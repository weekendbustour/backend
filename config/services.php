<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'yandex_map' => [
        'enabled' => env('YANDEX_MAP_ENABLED', true),
    ],

    'yandex_webmaster' => [
        'enabled' => env('YANDEX_WEBMASTER_ENABLED', true),
        'key' => env('YANDEX_WEBMASTER_KEY', null),
    ],

    'yandex' => [
        'enabled' => env('YANDEX_ENABLED', true),
        'client_id' => env('YANDEX_KEY'),
        'client_secret' => env('YANDEX_SECRET'),
        'redirect' => env('YANDEX_REDIRECT_URI')
    ],

    'google_analytics' => [
        'enabled' => env('GOOGLE_ANALYTICS_ENABLED', true),
        'key' => env('GOOGLE_ANALYTICS_KEY', null),
    ],
    'yandex_metrika' => [
        'enabled' => env('YANDEX_METRIKA_ENABLED', true),
    ],

    'google_tag_manager' => [
        'enabled' => env('GOOGLE_TAG_MANAGER_ENABLED', true),
        'key' => env('GOOGLE_TAG_MANAGER_KEY', null),
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_CLIENT_ID'),
        'client_secret' => env('FACEBOOK_CLIENT_SECRET'),
        'redirect' => env('FACEBOOK_REDIRECT_URI')
    ],

    'google' => [
        'enabled' => env('GOOGLE_ENABLED', true),
        'client_id' => env('GOOGLE_CLIENT_ID'),
        'client_secret' => env('GOOGLE_CLIENT_SECRET'),
        'redirect' => env('GOOGLE_CALLBACK_URL'),
    ],

    'vkontakte' => [
        'app_id' => env('VKONTAKTE_APP_ID'),
        'enabled' => env('VKONTAKTE_ENABLED', true),
        'client_id' => env('VKONTAKTE_KEY'),
        'client_secret' => env('VKONTAKTE_SECRET'),
        'redirect' => env('VKONTAKTE_REDIRECT_URI'),

        'group_key' => env('VKONTAKTE_GROUP_KEY'),
        'group_id' => env('VKONTAKTE_GROUP_ID'),
        'market_access_token' => env('VK_STANDALONE_MARKET_ACCESS_TOKEN'),
        'wall_access_token' => env('VK_STANDALONE_WALL_ACCESS_TOKEN'),
    ],

];
