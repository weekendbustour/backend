<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    'webp_enabled' => env('WEBP_ENABLED', true),
    'jpeg_enabled' => env('JPEG_ENABLED', true),

    'url' => [
        'remote' => env('IMAGE_URL', ''),
        'hash' => env('IMAGE_HASH', 'dOiKcAeXIPhgPCZb'),
    ],

    'cloud' => env('IMAGE_CLOUD', false),
];
