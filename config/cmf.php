<?php

use App\Cmf\Project\User\UserController;
use App\Cmf\Project\Organization\OrganizationController;
use App\Cmf\Project\Direction\DirectionController;
use App\Cmf\Project\Departure\DepartureController;
use App\Cmf\Project\Category\CategoryController;
use App\Cmf\Project\Tag\TagController;
use App\Cmf\Project\Tour\TourController;
use App\Cmf\Project\TourTimeline\TourTimelineController;
use App\Cmf\Project\Role\RoleController;
use App\Cmf\Project\Activation\ActivationController;
use App\Cmf\Project\Option\OptionController;
use App\Cmf\Project\Entertainment\EntertainmentController;
use App\Cmf\Project\Promotion\PromotionController;
use App\Cmf\Project\News\NewsController;
use App\Cmf\Project\Payment\PaymentController;
use App\Cmf\Project\TourReservation\TourReservationController;
use App\Cmf\Project\Attraction\AttractionController;
use App\Cmf\Project\Article\ArticleController;
use App\Cmf\Project\Question\QuestionController;
use App\Cmf\Project\Outfit\OutfitController;
use App\Models\User;

return [

    'version' => '0.0.1',

    'as' => env('CMF_AS', ''),

    'url' => env('CMF_URL', env('APP_URL', '')),

    'prefix' => env('CMF_PREFIX', ''),

    'public_path' => env('CMF_PUBLIC_PATH', ''),

    'image_public_directory' => env('IMAGE_PUBLIC_DIRECTORY', 'storage/images'),

    'image_testing_directory' => env('IMAGE_TESTING_DIRECTORY', 'storage/testing/images'),

    'files_public_directory' => env('FILES_PUBLIC_DIRECTORY', 'storage/files'),

    'files_testing_directory' => env('FILES_TESTING_DIRECTORY', 'storage/testing/files'),

    'sidebar' => [
        'admin' => [
            '' => [
                'title' => 'Главная',
                'iconCls' => 'icon-puzzle',
            ],
            OrganizationController::NAME => [
                'title' => OrganizationController::TITLE,
                'iconCls' => OrganizationController::ICON,
            ],
            DirectionController::NAME => [
                'title' => DirectionController::TITLE,
                'iconCls' => DirectionController::ICON,
                'sub' => [
                    DirectionController::NAME => [
                        'title' => 'Общее',
                        'iconCls' => DirectionController::ICON,
                    ],
                    AttractionController::NAME => [
                        'title' => AttractionController::TITLE,
                        'iconCls' => AttractionController::ICON,
                    ],
                ]
            ],
//            TourController::NAME => [
//                'title' => TourController::TITLE,
//                'iconCls' => TourController::ICON,
//            ],
            TourController::NAME => [
                'title' => TourController::TITLE,
                'iconCls' => TourController::ICON,
                'sub' => [
                    TourController::NAME => [
                        'title' => 'Общее',
                        'iconCls' => TourController::ICON,
                    ],
                    TourTimelineController::NAME => [
                        'title' => TourTimelineController::TITLE,
                        'iconCls' => TourTimelineController::ICON,
                    ],
                    TourReservationController::NAME => [
                        'title' => 'Бронирования',
                        'iconCls' => TourReservationController::ICON,
                    ],
                ]
            ],
            NewsController::NAME => [
                'title' => NewsController::TITLE,
                'iconCls' => NewsController::ICON,
            ],
            ArticleController::NAME => [
                'title' => ArticleController::TITLE,
                'iconCls' => ArticleController::ICON,
            ],
            UserController::NAME => [
                'title' => UserController::TITLE,
                'iconCls' => UserController::ICON,
                'roles' => [
                    User::ROLE_SUPER_ADMIN,
                ],
            ],
            PromotionController::NAME => [
                'title' => PromotionController::TITLE,
                'iconCls' => PromotionController::ICON,
            ],
            PaymentController::NAME => [
                'title' => PaymentController::TITLE,
                'iconCls' => PaymentController::ICON,
            ],
            'options' => [
                'title' => 'Справочники',
                'iconCls' => 'icon-grid',
                'sub' => [
                    DepartureController::NAME => [
                        'title' => DepartureController::TITLE,
                        'iconCls' => DepartureController::ICON,
                    ],
                    EntertainmentController::NAME => [
                        'title' => EntertainmentController::TITLE,
                        'iconCls' => EntertainmentController::ICON,
                    ],
                    OptionController::NAME => [
                        'title' => OptionController::TITLE,
                        'iconCls' => OptionController::ICON,
                    ],
                    CategoryController::NAME => [
                        'title' => CategoryController::TITLE,
                        'iconCls' => CategoryController::ICON,
                    ],
                    TagController::NAME => [
                        'title' => TagController::TITLE,
                        'iconCls' => TagController::ICON,
                    ],
                    RoleController::NAME => [
                        'title' => RoleController::TITLE,
                        'iconCls' => RoleController::ICON,
                    ],
                    QuestionController::NAME => [
                        'title' => QuestionController::TITLE,
                        'iconCls' => QuestionController::ICON,
                    ],
                    OutfitController::NAME => [
                        'title' => OutfitController::TITLE,
                        'iconCls' => OutfitController::ICON,
                    ],
                ]
            ],
            'info' => [
                'title' => 'Информация',
                'iconCls' => 'icon-notebook',
                'sub' => [
                    ActivationController::NAME => [
                        'title' => ActivationController::TITLE,
                        'iconCls' => ActivationController::ICON,
                    ],
                ]
            ],
            'statistic' => [
                'title' => 'Статистика',
                'iconCls' => 'icon-chart',
            ],
        ],
    ],

    'options' => [
        'app' => [
            'title' => 'Административная панель',
            'title_short' => env('APP_NAME'),
            'description' => null,
            'keywords' => null,
            'favicon' => [
                'main' => '/img/logo.png',
            ],
            'powered' => env('APP_NAME'),
            'copyright' => ' ' . env('APP_NAME'),
        ],
    ],

    'cache' => [
        'fields' => true,
        'member' => true,

        'name' => 'user_',
        'tag' => 'members',
    ]
];
