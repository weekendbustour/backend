<?php

declare(strict_types=1);

namespace App\Console\Commands;

use App\Console\Commands\Common\CommandTrait;
use App\Models\Location;
use App\Services\Logger\Logger;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class WeatherSyncCommand extends Command
{
    use CommandTrait;

    /**
     *
     */
    const SIGNATURE = 'weather:sync';

    /**
     * The name and signature of the console command.
     *
     * php artisan weather:sync
     * php artisan weather:sync --testing
     *
     * @var string
     */
    protected $signature = self::SIGNATURE . '
        {--testing : включить режим тестирования}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Синхронизация погоды для направлений';

    /**
     * @var Logger|null
     */
    private $logger = null;

    /**
     *  Не будет записывать в обычные логи, только по Logger
     *
     * @var bool
     */
    private $log = false;

    /**
     * CheckQueuesCommand constructor.
     */
    public function __construct()
    {
        @parent::__construct();

        $this->logger = (new Logger())->setName('weather')->log();
    }

    /**
     * Execute the console command.
     *
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->start();

        $oLocations = Location::active()->directions()->get();

        $bar = $this->bar(count($oLocations));

        foreach ($oLocations as $oLocation) {
            $oWeather = $oLocation->weather;

            $result = $this->request($oLocation);

            dd($result);

            if (is_null($result)) {
                dd($result);
                continue;
            }

            $fact = $result['fact'];

            if (!is_null($oWeather)) {
                $oWeather->update([
                    //'icon' => $fact['icon'],
                    'icon' => 'https://yastatic.net/weather/i/icons/blueye/color/svg/' . $fact['icon'] . '.svg',
                    'condition' => $fact['condition'],
                    'temp' => $fact['temp'],
                    'feels_like' => $fact['feels_like'],
                    'humidity' => $fact['humidity'],
                    'pressure' => $fact['pressure_mm'],
                    'wind' => $fact['wind_speed'],
                ]);
            } else {
                $oLocation->weather()->create([
                    'icon' => $fact['icon'],
                    'condition' => $fact['condition'],
                    'temp' => $fact['temp'],
                    'feels_like' => $fact['feels_like'],
                    'humidity' => $fact['humidity'],
                    'pressure' => $fact['pressure_mm'],
                    'wind' => $fact['wind_speed'],
                ]);
            }
            $bar->advance();
        }
        $bar->finish();

        $this->finish();
    }

    /**
     * @param Location $oLocation
     * @return \Psr\Http\Message\ResponseInterface|null|array
     */
    private function request(Location $oLocation)
    {
        try {
            $client = new Client([
                'headers' => [
                    'X-Yandex-API-Key' => '7fbeed24-961b-4a2a-bbc5-837b805d3933',
                    //'X-Yandex-API-Key' => 'c964309d-febc-4f13-a9a1-40dcaeceaa57',
                ],
            ]);
            $result = $client->request('GET', 'https://api.weather.yandex.ru/v1/informers', [
                'lat' => $oLocation->latitude,
                'lon' => $oLocation->longitude,
                'lang' => 'ru_RU',
            ]);
            return json_decode($result->getBody()->getContents(), true);
        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            return null;
        }
    }
}
