<?php

declare(strict_types=1);

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;

class ScheduleCommon
{
    /**
     * Название очереди для задач пользователя
     */
    const CACHE_NAME = 'schedule';

    const CACHE_TIME = 100;

    /**
     * @return bool
     */
    public function isActive()
    {
        return Cache::has(self::CACHE_NAME);
    }

    /**
     * @return string|null
     */
    public function lastTime(): ?string
    {
        if (Cache::has(self::CACHE_NAME)) {
            return Cache::get(self::CACHE_NAME)['time'];
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function putTime()
    {
        Cache::put(self::CACHE_NAME, [
            'time' => now()->format('d.m.Y H:i:s'),
        ], 100);
        return Cache::get(self::CACHE_NAME);
    }
}
