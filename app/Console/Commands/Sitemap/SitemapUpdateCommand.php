<?php

declare(strict_types=1);

namespace App\Console\Commands\Sitemap;

use App\Console\Commands\Common\CommandTrait;
use App\Http\Controllers\Api\IndexController;
use App\Jobs\UpdateSitemapJob;
use App\Services\Seo\SeoSitemapService;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class SitemapUpdateCommand extends Command
{
    use CommandTrait;

    /**
     *
     */
    const SIGNATURE = 'sitemap:update';

    /**
     * The name and signature of the console command.
     *
     * php artisan sitemap:update
     * php artisan sitemap:update --queue
     *
     * @var string
     */
    protected $signature = self::SIGNATURE . '
        {--queue : отправить в очередь}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновить sitemap.';

    /**
     *  Не будет записывать в обычные логи, только по Logger
     *
     * @var bool
     */
    private $log = false;

    /**
     *  Не будет записывать в обычные логи, только по Logger
     *
     * @var bool
     */
    private $queue = false;

    /**
     * CacheDataUpdateCommand constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->start();

        if ($this->queue) {
            (new SeoSitemapService())->dispatch();
        } else {
            (new UpdateSitemapJob())->handle();
        }
        $this->log('Sitemap was updated.');
        $this->finish();
    }
}
