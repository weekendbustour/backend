<?php

declare(strict_types=1);

namespace App\Console\Commands\Image;

use App\Cmf\Project\Tour\TourController;
use App\Console\Commands\Common\CommandTrait;
use App\Console\Commands\Common\CommonImageCommandTrait;
use App\Models\Image;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use App\Services\Image\Path\ImagePathBannerService;
use App\Services\Image\Path\ImagePathGalleryService;
use App\Services\Image\Path\ImagePathModelService;
use App\Services\ImageService;
use App\Services\Logger\Logger;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as ImageStatic;

class ImageUpdateNumbersCommand extends Command
{
    use CommandTrait;
    use CommonImageCommandTrait;

    /**
     * The name and signature of the console command.
     *
     * php artisan image:update-numbers --model=journal
     * php artisan image:update-numbers --model=tour
     *
     * @var string
     */
    protected $signature = 'image:update-numbers
                            {--model= : название модели}
                            {--testing : включить режим тестирования}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Заполнить картинкам number';

    /**
     * @var Logger|null
     */
    private $logger = null;

    /**
     *  Не будет записывать в обычные логи, только по Logger
     *
     * @var bool
     */
    private $log = false;

    /**
     * Ключ, например journal
     *
     * @var string|null
     */
    private $model = null;

    /**
     * CheckQueuesCommand constructor.
     */
    public function __construct()
    {
        @parent::__construct();

        $this->logger = (new Logger())->setName('image')->log();
    }

    /**
     * Execute the console command.
     *
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->start();

        if (is_null($this->model)) {
            $this->exit('Options --model= was not found.');
            return;
        }

        $model = $this->getModelByModel($this->model);
        if (is_string($model)) {
            $this->exit('Model ' . $model . ' not found.');
            return;
        }
        /** @var TourController $controller */
        $controller = $this->getControllerByModel($this->model);
        if (is_string($controller)) {
            $this->exit('Controller ' . $controller . ' not found.');
            return;
        }
        if (!method_exists($controller, 'imageUpload')) {
            $this->exit('Model is not Imageable.');
            return;
        }

        $images = $controller->image;

        $bar = $this->bar($model::count());
        $model::chunk(100, function ($oItems) use ($images, $bar) {
            $oImageService = (new ImageService());
            foreach ($oItems as $oItem) {
                foreach ($images as $type => $options) {
                    switch ($type) {
                        case ImageType::MODEL:
                            $oImageService->updateNumbers($oItem, ImageType::MODEL);
                            break;
                        case ImageType::BANNER:
                            $oImageService->updateNumbers($oItem, ImageType::BANNER);
                            break;
                        case ImageType::GALLERY:
                            $oImageService->updateNumbers($oItem, ImageType::GALLERY);
                            break;
                    }
                }
                $bar->advance();
            }
        });
        $bar->finish();
        $this->finish();
    }

    /**
     * Выход из приложения
     *
     * @param string|array $message
     */
    private function exit($message)
    {
        $this->errorLog($message);
        $this->finish();
    }
}
