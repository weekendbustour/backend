<?php

declare(strict_types=1);

namespace App\Console\Commands\Image;

use App\Cmf\Project\Tour\TourController;
use App\Console\Commands\Common\CommandTrait;
use App\Models\Image;
use App\Services\Image\ImageType;
use App\Services\Image\Path\ImagePathBannerService;
use App\Services\Image\Path\ImagePathGalleryService;
use App\Services\Image\Path\ImagePathModelService;
use App\Services\ImageService;
use App\Services\Logger\Logger;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class ImageResizeCommand extends Command
{
    use CommandTrait;

    /**
     * The name and signature of the console command.
     *
     * php artisan image:resize --model=journal
     * php artisan image:resize --model=tour
     *
     * @var string
     */
    protected $signature = 'image:resize
                            {--model= : название модели}
                            {--testing : включить режим тестирования}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обрабатывает все изображения';

    /**
     * @var Logger|null
     */
    private $logger = null;

    /**
     *  Не будет записывать в обычные логи, только по Logger
     *
     * @var bool
     */
    private $log = false;

    /**
     * Ключ, например journal
     *
     * @var string|null
     */
    private $model = null;

    /**
     * @var int
     */
    private $dump = 0;

    /**
     * @var array
     */
    private $aId = [];

    /**
     * @var array
     */
    private $aInfo = [];

    /**
     * CheckQueuesCommand constructor.
     */
    public function __construct()
    {
        @parent::__construct();

        $this->logger = (new Logger())->setName('image')->log();
    }

    /**
     * Execute the console command.
     *
     * @throws \Throwable
     */
    public function handle(): void
    {
        $this->start();

        if (is_null($this->model)) {
            $this->exit('Options --model= was not found.');
            return;
        }

        $model = $this->getModelByModel($this->model);
        if (is_string($model)) {
            $this->exit('Model ' . $model . ' not found.');
            return;
        }
        /** @var TourController $controller */
        $controller = $this->getControllerByModel($this->model);
        if (is_string($controller)) {
            $this->exit('Controller ' . $controller . ' not found.');
            return;
        }
        if (!method_exists($controller, 'imageUpload')) {
            $this->exit('Model is not Imageable.');
            return;
        }

        $images = $controller->image;

        $counter = 0;
        $model::chunk(100, function ($oItems) use ($images, &$counter) {
            foreach ($oItems as $oItem) {
                foreach ($images as $type => $options) {
                    $oService = null;
                    $oImages = [];
                    $oImageService = (new ImageService());
                    switch ($type) {
                        case ImageType::MODEL:
                            $oService = $oImageService->getImagePathServiceByType(ImageType::MODEL);
                            $oImages = $oItem->modelImages;
                            break;
                        case ImageType::BANNER:
                            $oService = $oImageService->getImagePathServiceByType(ImageType::BANNER);
                            $oImages = $oItem->bannerImages;
                            break;
                        case ImageType::GALLERY:
                            $oService = $oImageService->getImagePathServiceByType(ImageType::GALLERY);
                            $oImages = $oItem->galleryImages;
                            break;
                    }
                    foreach ($oImages as $oImage) {
                        $success = $this->checkImage($oService, 'original', $oItem, $oImage);
                        if (!$success) {
                            $this->exit('Original file was not found. Model: ' . $this->model);
                        }
                        $counter++;
                    }
                }
            }
        });
        $bar = $this->bar($model::count());
        $model::chunk(100, function ($oItems) use ($images, $bar) {
            foreach ($oItems as $oItem) {
                foreach ($images as $type => $options) {
                    $oService = null;
                    $oImages = [];
                    $oImageService = (new ImageService());
                    switch ($type) {
                        case ImageType::MODEL:
                            $oService = $oImageService->getServiceByType(ImageType::MODEL);
                            $oImages = $oItem->modelImages;
                            break;
                        case ImageType::BANNER:
                            $oService = $oImageService->getServiceByType(ImageType::BANNER);
                            $oImages = $oItem->bannerImages;
                            break;
                        case ImageType::GALLERY:
                            $oService = $oImageService->getServiceByType(ImageType::GALLERY);
                            $oImages = $oItem->galleryImages;
                            break;
                    }
                    foreach ($oImages as $oImage) {
                        //dd($oImage, $this->model, $oItem->id, $options['filters']);
                        $oService->resizeByOptions($oImage, $this->model, $oItem->id, $options['filters']);
                    }
                }
                $bar->advance();
            }
        });
        $bar->finish();
        $this->finish();
    }

    /**
     * @param ImagePathModelService|ImagePathGalleryService|ImagePathBannerService|null $oImagePath
     * @param string $defaultSize
     * @param object $oItem
     * @param Image $oImage
     * @return bool
     */
    private function checkImage($oImagePath, string $defaultSize, object $oItem, Image $oImage): bool
    {
        if (!$oImagePath->checkOriginalImage($oImage, $this->model, $defaultSize, $oItem)) {
            return false;
        }
        return true;
    }

    /**
     * Вывод строки в обычном формате и запись в лог
     *
     * @param string $info
     */
    private function logLine(string $info): void
    {
        $this->line($info);
        $this->log($info, true, false);
    }

    /**
     * @param string $model
     * @return string
     */
    private function getControllerByModel(string $model)
    {
        $tValue = $this->getTitleCase($model);
        $sClass = $tValue . Str::studly('_controller');
        $sClass = 'App\Cmf\Project\\' . $tValue . '\\' . $sClass;
        return class_exists($sClass) ? new $sClass() : $sClass;
    }

    /**
     * @param string $model
     * @return string
     */
    private function getModelByModel(string $model)
    {
        $tValue = $this->getTitleCase($model);
        $sModel = 'App\Models\\' . $tValue;
        return class_exists($sModel) ? new $sModel() : $sModel;
    }

    /**
     * Обычный title_case с возможностью принимать значение вида key_name и транформирую в KeyName
     *
     * @param string $value
     * @return mixed
     */
    protected function getTitleCase($value)
    {
        return str_replace(' ', '', Str::title(str_replace('_', ' ', $value)));
    }

    /**
     * Выход из приложения
     *
     * @param string|array $message
     */
    private function exit($message)
    {
        $this->errorLog($message);
        $this->finish();
    }
}
