<?php

declare(strict_types=1);

namespace App\Console\Commands\Check;

use App\Console\Commands\Common\CommandTrait;
use App\Jobs\QueueCommon;
use App\Services\Logger\Logger;
use App\Services\Notification\Slack\SlackQueueNotification;
use Illuminate\Console\Command;

class CheckQueuesCommand extends Command
{
    use CommandTrait;

    /**
     *
     */
    const SIGNATURE = 'check:queues';
    const PARAMETER_NOTIFICATION = '--notification';

    /**
     * The name and signature of the console command.
     *
     * php artisan check:queues
     * php artisan check:queues --notification
     *
     * @var string
     */
    protected $signature = self::SIGNATURE . '
        {' . self::PARAMETER_NOTIFICATION . ' : только проверка, без включения}
        {--down : выключить прослушивания}
        {--queue= : название конкретной очереди}
        {--testing : включить режим тестирования, без запуска команд}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверяет, запущены ли необходимые очереди и запускает новые, если нужно';

    /**
     *  Не будет записывать в обычные логи, только по Logger
     *
     * @var bool
     */
    private $log = false;

    /**
     * @var string|null
     */
    private $queue = null;

    /**
     * @var bool
     */
    private $notification = false;

    /**
     * @var Logger|null
     */
    private $logger = null;

    /**
     * Команда php для сервера, может быть php72 и т.п.
     *
     * @var string|null
     */
    private $php_alias = null;

    /**
     * CheckQueuesCommand constructor.
     */
    public function __construct()
    {
        @parent::__construct();

        $this->logger = (new Logger())->setName('queues')->log();
        $this->php_alias = config('app.php_alias');
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->start();

        if ($this->notification) {
            $this->notification();
            $this->finish();
            return;
        }

        $this->option('down')
            ? $this->down()
            : $this->up();

        $this->finish();
    }

    /**
     *
     */
    private function notification(): void
    {
        $queues = $this->queuesNotification();
        foreach ($queues as $queue) {
            if (!checkQueue($queue)) {
                sleep(3);
                if (!checkQueue($queue)) {
                    //(new SlackQueueNotification())->log('Queue not working: ' . $queue);
                    $this->log('Queue not working: ' . $queue);
                }
            }
        }
    }

    /**
     *
     */
    private function up(): void
    {
        $queues = $this->queuesUp();
        foreach ($queues as $queue) {
            if (!checkQueue($queue)) {
                // на всякий случай удалить прослушивание
                $this->downQueue($queue);
                $this->upQueue($queue);
            }
        }
    }

    /**
     *
     */
    private function down(): void
    {
        $queues = $this->queuesDown();
        foreach ($queues as $queue) {
            if (checkQueue($queue)) {
                $this->downQueue($queue);
            }
        }
    }

    /**
     * @param string $queue
     */
    private function downQueue(string $queue): void
    {
        $aCommands = (new QueueCommon())->commands();
        $id = checkQueueGetId($queue);
        $command = $this->commandForExecute($aCommands[$queue]);
        if ($id !== null) {
            if (!$this->testing) {
                try {
                    exec('kill -9 ' . $id);
                } catch (\Exception $e) {
                    dd($e);
                }
            }
            // Если была уничтожена команда прослушивания, то notification не будет работать, поэтому посылаем LOG
            if ($queue === QueueCommon::QUEUE_NAME_NOTIFICATION) {
                (new SlackQueueNotification())->log('Command was kill: ' . $command);
            } else {
                (new SlackQueueNotification())->send('Command was kill: ' . $command, $queue . ':down');
            }
            $this->log('Command was kill: ' . $command);
        }
    }

    /**
     * @param string $queue
     */
    private function upQueue(string $queue)
    {
        $aCommands = (new QueueCommon())->commands();
        $command = $this->commandForExecute($aCommands[$queue]);
        if (!$this->testing) {
            exec($command);
        }
        (new SlackQueueNotification())->send('Command was execute: ' . $command, $queue . ':up');
        $this->log('Command was execute: ' . $command);
    }

    /**
     * @param array $queues
     * @param string $queue
     * @return array
     */
    private function rejectByQueue(array $queues, string $queue): array
    {
        return collect($queues)->filter(function ($value, $key) use ($queue) {
            return $value === $queue;
        })->toArray();
    }

    /**
     * @param string $command
     * @return string
     */
    private function commandForExecute(string $command)
    {
        return $this->php_alias . ' artisan ' . $command . ' > /dev/null &';
    }

    /**
     * @return array
     */
    private function queuesUp(): array
    {
        $queues = [
            //QueueCommon::QUEUE_NAME_MAIL,
            QueueCommon::QUEUE_NAME_SUBSCRIPTION,
            QueueCommon::QUEUE_NAME_NOTIFICATION,
            QueueCommon::QUEUE_NAME_SEO,
        ];
        if (!is_null($this->queue)) {
            $queues = $this->rejectByQueue($queues, $this->queue);
        }
        return $queues;
    }

    /**
     * @return array
     */
    private function queuesNotification(): array
    {
        $queues = [
            QueueCommon::QUEUE_NAME_NOTIFICATION,
        ];
        if (!is_null($this->queue)) {
            $queues = $this->rejectByQueue($queues, $this->queue);
        }
        return $queues;
    }

    /**
     * @return array
     */
    private function queuesDown(): array
    {
        $queues = [
            QueueCommon::QUEUE_NAME_MAIL,
            QueueCommon::QUEUE_NAME_SUBSCRIPTION,
            QueueCommon::QUEUE_NAME_NOTIFICATION,
            QueueCommon::QUEUE_NAME_SEO,
        ];
        if (!is_null($this->queue)) {
            $queues = $this->rejectByQueue($queues, $this->queue);
        }
        return $queues;
    }
}
