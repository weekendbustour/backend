<?php

declare(strict_types=1);

namespace App\Console\Commands\Check;

use App\Console\Commands\Common\CommandTrait;
use App\Console\Commands\ScheduleCommon;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class CheckLogsCreateCommand extends Command
{
    use CommandTrait;

    /**
     *
     */
    const SIGNATURE = 'check:logs-create';

    /**
     * The name and signature of the console command.
     *
     * php artisan check:logs-create
     *
     * @var string
     */
    protected $signature = self::SIGNATURE . '
        {--testing : включить режим тестирования}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создать запись в лог';

    /**
     *  Не будет записывать в обычные логи, только по Logger
     *
     * @var bool
     */
    private $log = false;

    /**
     * ExportConverterUsersCommand constructor.
     */
    public function __construct()
    {
        @parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Throwable
     */
    public function handle(): void
    {
        Log::info('------------- DAILY LOG 0777 -------------');
        File::chmod(storage_path('logs/laravel-' . now()->format('Y-m-d') . '.log'), 0777);
    }
}
