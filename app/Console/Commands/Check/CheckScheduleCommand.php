<?php

declare(strict_types=1);

namespace App\Console\Commands\Check;

use App\Console\Commands\Common\CommandTrait;
use App\Console\Commands\ScheduleCommon;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;

class CheckScheduleCommand extends Command
{
    use CommandTrait;

    /**
     *
     */
    const SIGNATURE = 'check:schedule';

    /**
     * The name and signature of the console command.
     *
     * php artisan check:schedule
     *
     * @var string
     */
    protected $signature = self::SIGNATURE . '
        {--testing : включить режим тестирования}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверка крона';

    /**
     *  Не будет записывать в обычные логи, только по Logger
     *
     * @var bool
     */
    private $log = false;

    /**
     * ExportConverterUsersCommand constructor.
     */
    public function __construct()
    {
        @parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @throws \Throwable
     */
    public function handle(): void
    {
        // Чтобы избежать погрешности
        // Ставлю на 100 секунд и потом перезаписываю
        $oScheduleCommon = (new ScheduleCommon());
        if (Cache::has(ScheduleCommon::CACHE_NAME)) {
            $cacheSchedule = Cache::get(ScheduleCommon::CACHE_NAME);
            if (now()->diffInMinutes(Carbon::parse($cacheSchedule['time'])) === 1) {
                $oScheduleCommon->putTime();
            }
        } else {
            $oScheduleCommon->putTime();
        }
    }
}
