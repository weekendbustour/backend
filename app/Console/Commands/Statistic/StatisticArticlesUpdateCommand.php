<?php

declare(strict_types=1);

namespace App\Console\Commands\Statistic;

use App\Console\Commands\Common\CommandTrait;
use App\Jobs\QueueCommon;
use App\Models\Article;
use App\Models\Attraction;
use App\Services\Logger\Logger;
use App\Services\Notification\Slack\SlackQueueNotification;
use Illuminate\Console\Command;

class StatisticArticlesUpdateCommand extends Command
{
    use CommandTrait;

    /**
     *
     */
    const SIGNATURE = 'statistic:articles-update';

    /**
     * The name and signature of the console command.
     *
     * php artisan statistic:articles-update
     *
     * @var string
     */
    protected $signature = self::SIGNATURE . '
        {--testing : включить режим тестирования, без запуска команд}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновляет статистику просмотров в статье';

    /**
     *  Не будет записывать в обычные логи, только по Logger
     *
     * @var bool
     */
    private $log = false;

    /**
     * CheckQueuesCommand constructor.
     */
    public function __construct()
    {
        @parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->start();

        $oArticles = Article::all();

        $bar = $this->bar(count($oArticles));
        foreach ($oArticles as $oArticle) {
            $oStatistic = $oArticle->statistic;
            if (is_null($oStatistic)) {
                $oStatistic = $oArticle->statistic()->create([]);
            }
            $countViews = visits($oArticle)->count();
            $countComments = $oArticle->comments()->count();
            if ($oStatistic->views !== $countViews || $oStatistic->comments !== $countComments) {
                $oStatistic->update([
                    'views' => visits($oArticle)->count(),
                    'comments' => $oArticle->comments()->count(),
                ]);
            }
            $bar->advance();
        }
        $bar->finish();

        $oAttractions = Attraction::all();

        $bar = $this->bar(count($oAttractions));
        foreach ($oAttractions as $oAttraction) {
            $oStatistic = $oAttraction->statistic;
            if (is_null($oStatistic)) {
                $oStatistic = $oAttraction->statistic()->create([]);
            }
            $countViews = visits($oAttraction)->count();
            $countComments = $oAttraction->comments()->count();
            if ($oStatistic->views !== $countViews || $oStatistic->comments !== $countComments) {
                $oStatistic->update([
                    'views' => visits($oAttraction)->count(),
                    'comments' => $oAttraction->comments()->count(),
                ]);
            }
            $bar->advance();
        }
        $bar->finish();

        $this->finish();
    }
}
