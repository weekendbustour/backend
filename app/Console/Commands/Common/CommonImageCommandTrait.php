<?php

declare(strict_types=1);

namespace App\Console\Commands\Common;

use Illuminate\Support\Str;

trait CommonImageCommandTrait
{
    /**
     * @param string $model
     * @return string
     */
    private function getControllerByModel(string $model)
    {
        $tValue = $this->getTitleCase($model);
        $sClass = $tValue . Str::studly('_controller');
        $sClass = 'App\Cmf\Project\\' . $tValue . '\\' . $sClass;
        return class_exists($sClass) ? new $sClass() : $sClass;
    }

    /**
     * @param string $model
     * @return string
     */
    private function getModelByModel(string $model)
    {
        $tValue = $this->getTitleCase($model);
        $sModel = 'App\Models\\' . $tValue;
        return class_exists($sModel) ? new $sModel() : $sModel;
    }

    /**
     * Обычный title_case с возможностью принимать значение вида key_name и транформирую в KeyName
     *
     * @param string $value
     * @return mixed
     */
    protected function getTitleCase($value)
    {
        return str_replace(' ', '', Str::title(str_replace('_', ' ', $value)));
    }
}
