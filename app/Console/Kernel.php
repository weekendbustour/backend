<?php

declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\Cache\CacheDataUpdateCommand;
use App\Console\Commands\Check\CheckLogsCreateCommand;
use App\Console\Commands\Check\CheckQueuesCommand;
use App\Console\Commands\Check\CheckScheduleCommand;
use App\Console\Commands\Statistic\StatisticArticlesUpdateCommand;
use App\Console\Commands\Image\ImageResizeCommand;
use App\Console\Commands\Image\ImageUpdateNumbersCommand;
use App\Console\Commands\Image\ImageWebpCommand;
use App\Console\Commands\Sitemap\SitemapUpdateCommand;
use App\Console\Commands\Version\VersionGitCommand;
use App\Console\Commands\WeatherSyncCommand;
use App\Services\Environment;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ImageResizeCommand::class,
        ImageWebpCommand::class,
        ImageUpdateNumbersCommand::class,
        WeatherSyncCommand::class,
        CheckScheduleCommand::class,
        CheckQueuesCommand::class,
        VersionGitCommand::class,
        StatisticArticlesUpdateCommand::class,
        CacheDataUpdateCommand::class,
        SitemapUpdateCommand::class,
        CheckLogsCreateCommand::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        byEnv('*', function () use ($schedule) {
            /**
             * Проверка крона
             */
            $schedule->command(CheckScheduleCommand::SIGNATURE)->everyMinute();
            $schedule->command(CheckLogsCreateCommand::SIGNATURE)->dailyAt('00:01');

            $schedule->command(StatisticArticlesUpdateCommand::SIGNATURE)->everyTenMinutes();
            //$schedule->command(CheckQueuesCommand::SIGNATURE)->everyMinute();

            //$schedule->command(WeatherSyncCommand::SIGNATURE)->dailyAt('08:00');

            // $schedule->command('horizon:snapshot')->everyFiveMinutes();
        });
        byEnv(Environment::PRODUCTION, function () use ($schedule) {
            /**
             * Проверка очереди уведомлений
             */
            $schedule->command(CheckQueuesCommand::SIGNATURE, [
                CheckQueuesCommand::PARAMETER_NOTIFICATION,
            ])->everyMinute();
        });
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
