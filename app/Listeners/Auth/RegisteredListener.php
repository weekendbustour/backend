<?php

declare(strict_types=1);

namespace App\Listeners\Auth;

use App\Events\Auth\RegisteredEvent;
use App\Jobs\QueueCommon;
use App\Jobs\Mail\Auth\SendMailAuthRegisteredJob;
use App\Mail\Auth\RegisteredMail;
use Illuminate\Support\Facades\Mail;

class RegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param RegisteredEvent $event
     * @return void
     */
    public function handle(RegisteredEvent $event)
    {
        $oUser = $event->user;

        if (QueueCommon::commandMailIsEnabled()) {
            SendMailAuthRegisteredJob::dispatch($oUser)->onQueue(QueueCommon::QUEUE_NAME_MAIL);
        } else {
            SendMailAuthRegisteredJob::dispatchSync($oUser);
        }
    }
}
