<?php

declare(strict_types=1);

namespace App\Listeners\Auth;

use App\Events\Auth\ResetPasswordEvent;
use App\Jobs\Mail\Auth\SendMailAuthResetPasswordJob;
use App\Jobs\QueueCommon;

class ResetPasswordListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ResetPasswordEvent $event
     * @return void
     */
    public function handle(ResetPasswordEvent $event)
    {
        $oUser = $event->user;

        if (QueueCommon::commandMailIsEnabled()) {
            SendMailAuthResetPasswordJob::dispatch($oUser)->onQueue(QueueCommon::QUEUE_NAME_MAIL);
        } else {
            SendMailAuthResetPasswordJob::dispatchSync($oUser);
        }
    }
}
