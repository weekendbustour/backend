<?php

declare(strict_types=1);

namespace App\Services\Transformers;

use App\Cmf\Project\Tour\TourController;
use App\Models\Tour;
use App\Services\Image\Upload\ImageUploadService;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class TourServiceTransformer
{
    /**
     * @param Tour $oItem
     * @return string
     */
    public function startAtDays(Tour $oItem)
    {
        $start = $oItem->start_at->translatedFormat('j');
        $finish = $oItem->finish_at->translatedFormat('j');
        $startMonth = $oItem->start_at->format('m');
        $finishMonth = $oItem->finish_at->format('m');
        if ($start === $finish) {
            return $start;
        }
        if ($startMonth === $finishMonth) {
            return $start . '-' . $finish;
        }
        return $start . '-' . $finish . '.' . $finishMonth;
    }

    /**
     * @param Tour $oItem
     * @return string
     */
    public function startAtMonth(Tour $oItem)
    {
        return Str::ucfirst(__('months.' . Str::lower($oItem->start_at->format('F'))));
    }

    public function queryMultiple($oQuery)
    {
        $multiples = [];
        $oItems = $oQuery->get()->filter(function ($item) use (&$multiples) {
            if ($item->multi) {
                if (in_array($item->multi_code, $multiples)) {
                    return false;
                }
                $multiples[] = $item->multi_code;
            }
            return true;
        });
        return $oItems;
    }

    /**
     * @param Collection $oItems
     * @param Request $request
     * @param int|null $count
     * @param int $limit
     * @return LengthAwarePaginator
     */
    public function returnPaginate(Collection $oItems, Request $request, int $limit = 12, ?int $count = null): LengthAwarePaginator
    {
        $page = $request->exists('page') ? $request->get('page') - 1 : 0;
        if (is_null($count)) {
            $count = $oItems->count();
        }
        $oItems = $oItems->slice($page * $limit)->take($limit);
        return new LengthAwarePaginator($oItems, $count, $limit);
    }

    /**
     * @param Tour $oItem
     * @param Tour $oNew
     */
    public function duplicate(Tour $oItem, Tour $oNew)
    {
        // цена
        $oItemPrice = $oItem->price;
        if (!is_null($oItemPrice)) {
            $oPrice = $oItemPrice->replicate();
            $oPrice->priceable_id = $oNew->id;
            $oPrice->save();
        }
        // отмены
        $oItemCancellation = $oItem->cancellation;
        if (!is_null($oItemCancellation)) {
            $oCancellation = $oItemCancellation->replicate();
            $oCancellation->tour_id = $oNew->id;
            $oCancellation->save();
        }
        // копировать категории
        foreach ($oItem->categories as $oCategory) {
            $oNew->categories()->attach($oCategory);
        }
        // копировать отправления
        foreach ($oItem->departures as $oDeparture) {
            $oNew->departures()->attach($oDeparture, [
                'departure_at' => $oDeparture->pivot->departure_at,
            ]);
        }
        // копировать отправления
        foreach ($oItem->outfits as $oOutfit) {
            $oNew->outfits()->attach($oOutfit, [
                'outfit_id' => $oOutfit->id,
                'rent' => $oOutfit->pivot->rent,
                'rent_price' => $oOutfit->pivot->rent_price,
                'rent_description' => $oOutfit->pivot->rent_description,
            ]);
        }
        // копировать достопримечательности
        foreach ($oItem->attractions as $oAttraction) {
            $oNew->attractions()->attach($oAttraction);
        }
        // копировать развлечения
        foreach ($oItem->entertainments as $oEntertainment) {
            $oNew->entertainments()->attach($oEntertainment);
        }
        // копировать расписание
        foreach ($oItem->timelines as $oTimeline) {
            $oNewTimeline = $oTimeline->replicate();
            $oNewTimeline->tour_id = $oNew->id;
            $oNewTimeline->save();
        }
        // копировать параметры
        foreach ($oItem->values as $oValue) {
            $oNewValue = $oValue->replicate();
            $oNewValue->tour_id = $oNew->id;
            $oNewValue->save();
        }
        // копировать изображения
        foreach ($oItem->images as $oValue) {
            $oNewValue = $oValue->replicate();
            $oNewValue->imageable_id = $oNew->id;
            $oNewValue->save();
        }
        // копировать изображения
        $from = (new ImageUploadService())->getDirectory(TourController::NAME, $oItem->id);
        $to = (new ImageUploadService())->getDirectory(TourController::NAME, $oNew->id);
        File::copyDirectory($from, $to);
    }

    /**
     * @param array $data
     * @return array
     */
    public function mergeMultiDates(array $data): array
    {
        $dates = [];
        foreach ($data as $key => $value) {
            if (Str::startsWith($key, 'multi_')) {
                $sKey = $key;
                $sKey = str_replace('multi_start_at_', '', $sKey);
                $sKey = str_replace('multi_finish_at_', '', $sKey);

                $startFinishKey = $key;
                $startFinishKey = str_replace('_' . $sKey, '', $startFinishKey);
                $startFinishKey = str_replace('multi_', '', $startFinishKey);
                $dates[$sKey][$startFinishKey] = $value;
            }
        }
        if (isset($dates['multi_save'])) {
            unset($dates['multi_save']);
        }
        return $dates;
    }

    /**
     * @return string
     */
    public function getMulticode()
    {
        return Str::random(10);
    }
}
