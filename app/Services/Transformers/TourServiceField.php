<?php

declare(strict_types=1);

namespace App\Services\Transformers;

use App\Cmf\Core\FieldParameter;
use App\Cmf\Project\Tour\TourController;
use App\Models\Tour;
use Illuminate\Support\Str;

class TourServiceField
{
    /**
     * @param string $name
     * @return array|mixed|null
     */
    public function get(string $name)
    {
        return (new FieldParameter())->getField(TourController::NAME, $name);
    }
}
