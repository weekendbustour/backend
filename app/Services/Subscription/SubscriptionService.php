<?php

declare(strict_types=1);

namespace App\Services\Subscription;

use App\Jobs\Mail\Subscription\SendMailSubscriptionDirectionJob;
use App\Jobs\Mail\Subscription\SendMailSubscriptionOrganizationJob;
use App\Jobs\QueueCommon;
use App\Models\Organization;
use App\Models\Subscription;
use App\Models\User;

class SubscriptionService
{
    private $oUser = null;
    private $oModel = null;
    private $email = null;

    /**
     * SubscriptionService constructor.
     * @param User|null $oUser
     * @param object|null $oModel
     */
    public function __construct(?User $oUser = null, object $oModel = null)
    {
        $this->oUser = $oUser;
        $this->oModel = $oModel;
    }

    /**
     * @param string $email
     * @return $this
     */
    public function email(string $email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return Subscription|null
     */
    public function find(): ?Subscription
    {
        $oQuery = Subscription::where('user_id', $this->oUser->id);

        if (!is_null($this->oModel)) {
            $oQuery
                ->where('subscriptionable_id', $this->oModel->id)
                ->where('subscriptionable_type', get_class($this->oModel));
        }
        $oSubscription = $oQuery->first();
        return $oSubscription;
    }

    /**
     * @return Subscription|null
     */
    public function findByEmail(): ?Subscription
    {
        return $oSubscription = Subscription::where('email', $this->email)->first();
    }

    /**
     * @return bool
     */
    public function isActiveByEmail()
    {
        $oSubscription = $this->findByEmail();
        if (is_null($oSubscription)) {
            return false;
        }
        return $oSubscription->status === Subscription::STATUS_ACTIVE;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        $oSubscription = $this->find();
        if (is_null($oSubscription)) {
            return false;
        }
        return $oSubscription->status === Subscription::STATUS_ACTIVE;
    }

    /**
     * @return Subscription|null
     */
    public function createByType(): ?Subscription
    {
        $data = [];
        if (!is_null($this->oModel)) {
            $data['subscriptionable_id'] = $this->oModel->id;
            $data['subscriptionable_type'] = get_class($this->oModel);
        }
        $oModel = Subscription::create(array_merge($data, [
            'user_id' => $this->oUser->id ?? null,
            'email' => $this->oUser->email ?? $this->email,
            'status' => 1,
        ]));
        return $oModel;
    }

    /**
     * @return Subscription|null
     */
    public function subscribe(): ?Subscription
    {
        $oSubscription = !is_null($this->oUser)
            ? $this->find()
            : $this->findByEmail();
        if (!is_null($oSubscription)) {
            $oSubscription->update([
                'status' => Subscription::STATUS_ACTIVE,
            ]);
        } else {
            $oSubscription = $this->createByType();
        }
        return $oSubscription;
    }

    /**
     * @return Subscription|null
     */
    public function unsubscribe(): ?Subscription
    {
        $oSubscription = !is_null($this->oUser)
            ? $this->find()
            : $this->findByEmail();
        if (!is_null($oSubscription)) {
            $oSubscription->update([
                'status' => Subscription::STATUS_UNSUBSCRIBE,
            ]);
        }
        return $oSubscription;
    }

    public function mailing()
    {
        $oTour = $this->oModel;
        $oOrganization = $oTour->organization;
        $oDirection = $oTour->direction;

        $oSubscriptions = Subscription::where('subscriptionable_type', get_class($oOrganization))
            ->where('subscriptionable_id', $oOrganization->id)
            ->where('status', Subscription::STATUS_ACTIVE)
            ->get();
        if (count($oSubscriptions) !== 0) {
            foreach ($oSubscriptions as $oSubscription) {
                $oUser = $oSubscription->user;
                dispatch(new SendMailSubscriptionOrganizationJob($oUser, $oOrganization))->onQueue(QueueCommon::QUEUE_NAME_SUBSCRIPTION);
            }
        }

        $oSubscriptions = Subscription::where('subscriptionable_type', get_class($oDirection))
            ->where('subscriptionable_id', $oDirection->id)
            ->where('status', Subscription::STATUS_ACTIVE)
            ->get();
        if (count($oSubscriptions) !== 0) {
            foreach ($oSubscriptions as $oSubscription) {
                $oUser = $oSubscription->user;
                dispatch(new SendMailSubscriptionDirectionJob($oUser, $oDirection))->onQueue(QueueCommon::QUEUE_NAME_SUBSCRIPTION);
            }
        }
    }
}
