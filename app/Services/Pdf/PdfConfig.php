<?php

declare(strict_types=1);

namespace App\Services\Pdf;

class PdfConfig
{
    /**
     * Для 64х битных процессоров
     */
    public const PROCESSOR_AMD64 = 'amd64';

    /**
     * Для 32х битных процессоров
     */
    public const PROCESSOR_I386 = 'i386';

    /**
     * Для маковских
     */
    public const PROCESSOR_MACOS = 'macos';

    /**
     * Для centos7
     */
    public const PROCESSOR_CENTOS7 = 'centos7';

    /**
     * Для centos7
     */
    public const PROCESSOR_DEFAULT = 'default';

    /**
     * @param string $processor
     * @return string|null
     * @throws \Exception
     */
    public static function getBinPdfByProcessor(string $processor): ?string
    {
        if (!in_array($processor, self::getProcessors())) {
            throw new \Exception('Processor ' . $processor . ' not found. Please set correct value SNAPPY_PDF_PROCESSOR on .env');
        }
        $amd64 = base_path('vendor/michael-schaefer-eu/wkhtmltox/bin/wkhtmltopdf-bionic-amd64');
        //$amd64 = storage_path('bin/wkhtmltopdf-bionic-amd64');
        $i386 = base_path('vendor/michael-schaefer-eu/wkhtmltox/bin/wkhtmltopdf-bionic-i386');
        $macos = base_path('vendor/profburial/wkhtmltopdf-binaries-osx/bin/wkhtmltopdf-amd64-osx');
        $centos7 = base_path('vendor/rvanlaak/wkhtmltopdf-amd64-centos7/bin/wkhtmltopdf-amd64');
        $default = '/usr/local/bin/wkhtmltopdf';

        switch ($processor) {
            case self::PROCESSOR_AMD64:
                return $amd64;
            case self::PROCESSOR_I386:
                return $i386;
            case self::PROCESSOR_MACOS:
                return $macos;
            case self::PROCESSOR_CENTOS7:
                return $centos7;
            case self::PROCESSOR_DEFAULT:
                return $default;
            default:
                return null;
        }
    }

    /**
     * @param string $processor
     * @return string|null
     * @throws \Exception
     */
    public static function getBinImageByProcessor(string $processor): ?string
    {
        if (!in_array($processor, self::getProcessors())) {
            throw new \Exception('Processor ' . $processor . ' not found. Please set correct value SNAPPY_PDF_PROCESSOR on .env');
        }
        $amd64 = base_path('vendor/michael-schaefer-eu/wkhtmltox/bin/wkhtmltoimage-bionic-amd64');
        //$amd64 = storage_path('bin/wkhtmltoimage-bionic-amd64');
        $i386 = base_path('vendor/michael-schaefer-eu/wkhtmltox/bin/wkhtmltoimage-bionic-i386');
        $macos = base_path('vendor/profburial/wkhtmltopdf-binaries-osx/bin/wkhtmltopdf-amd64-osx');
        $centos7 = base_path('vendor/rvanlaak/wkhtmltopdf-amd64-centos7/bin/wkhtmltopdf-amd64');
        $default = '/usr/local/bin/wkhtmltoimage';
        switch ($processor) {
            case self::PROCESSOR_AMD64:
                return $amd64;
            case self::PROCESSOR_I386:
                return $i386;
            case self::PROCESSOR_MACOS:
                return $macos;
            case self::PROCESSOR_CENTOS7:
                return $centos7;
            case self::PROCESSOR_DEFAULT:
                return $default;
            default:
                return null;
        }
    }

    /**
     * Массив доступных процессоров
     *
     * @return array
     */
    public static function getProcessors(): array
    {
        return [
            self::PROCESSOR_AMD64,
            self::PROCESSOR_I386,
            self::PROCESSOR_MACOS,
            self::PROCESSOR_CENTOS7,
            self::PROCESSOR_DEFAULT,
        ];
    }
}
