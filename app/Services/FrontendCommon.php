<?php

declare(strict_types=1);

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class FrontendCommon
{
    /**
     * @var mixed
     */
    private $manifest = [];

    /**
     * @var string
     */
    private $dirToFrontend = '';

    /**
     * @var string
     */
    private $pathToFrontend = '';

    /**
     * @var string
     */
    private $publicToFrontend = '';
    private $fonts = '';

    /**
     * @var string
     */
    private $version = '';

    /**
     * FrontendCommon constructor.
     * @param bool $parse
     */
    public function __construct($parse = false)
    {
        $this->dirToFrontend = public_path('frontend/build/production');
        $this->pathToFrontend = 'frontend/build/production';
        $this->publicToFrontend = 'frontend/public';
        if ($parse) {
            $this->manifest = Cache::remember('frontend-' . config('frontend.version'), 3600, function () {
                return json_decode(File::get($this->dirToFrontend . '/asset-manifest.json'), true);
            });
        }
        $this->fonts = remember('frontend:media', function () {
            $files = File::allFiles($this->dirToFrontend . '/media');
            $media = [];
            foreach ($files as $file) {
                $media[] = $file->getFilename();
            }
            return $media;
        });
        $this->version = GitVersion::getVersion();
    }

    /**
     * @param string $name
     * @return string
     */
    public function asset(string $name): string
    {
        if (isset($this->manifest[$name])) {
            return asset($this->pathToFrontend . '/' . $this->manifest[$name] . '?v=' . $this->version);
        }
        return asset($this->pathToFrontend . '/' . $name . '?v=' . $this->version);
    }

    /**
     * @return string
     */
    public function manifest()
    {
        return asset($this->publicToFrontend . '/manifest.json');
    }

    /**
     * @param string $name
     * @return string
     */
    public function font(string $name): string
    {
        $searchName = str_replace('.woff2', '', $name);
        $files = $this->fonts;
        foreach ($files as $filename) {
            if (Str::startsWith($filename, $searchName) && Str::endsWith($filename, 'woff2')) {
                $name = $filename;
                break;
            }
        }
        return '/frontend/build/production/media/' . $name;
    }
}
