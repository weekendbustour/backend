<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Tour;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Barryvdh\Snappy\Facades\SnappyImage;
use Spatie\Browsershot\Browsershot;

class Preview
{
    /**
     * @param Tour $oTour
     * @return string
     */
    public function createTourCard(Tour $oTour)
    {
        $file = $oTour->previewCardPath();
        $dir = str_replace('/' . $oTour->id . '.png', '', $file);
        if (!File::exists($dir)) {
            File::makeDirectory($dir, 0777, true);
        }
        if (File::exists($file)) {
            File::delete($file);
        }
        $oImage = SnappyImage::loadView('cmf.components.dev.cards.pdf.tour', [
            'oTour' => $oTour,
        ])
            ->setOption('quality', 100)
            ->setOption('format', 'png')
            ->setOption('transparent', true)
            ->setOption('width', 800)
            ->setOption('height', 800);

        $oImage->save($file);
        return $oTour->previewCardUrl();
    }

    public function browserShot(int $id)
    {
        $oTour = Tour::find($id);

        $dir = public_path('storage/images/card/tour');
        if (!File::exists($dir)) {
            File::makeDirectory($dir, 0777, true);
        }

        $file = public_path('storage/images/card/tour/' . $id . '.png');
        if (File::exists($file)) {
            File::delete($file);
        }


        Browsershot::html(view('cmf.components.dev.cards.pdf.tour', [
            'oTour' => $oTour,
        ])->render())
            ->setChromePath('/usr/bin/chromium-browser')
            ->windowSize(350, 420)
            //->setIncludePath('$PATH:/usr/bin')
            //->setNodeBinary('/usr/bin/node')
            //->setNpmBinary('/usr/bin/npm')
            //->setNodeModulePath('/usr/lib/node_modules')
            //->setNodeModulePath(base_path('node_modules/'))
            ->save($file);
    }
}
