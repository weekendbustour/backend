<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Image;
use App\Models\Tour;
use App\Services\Image\ImageType;
use App\Services\Image\Path\ImagePathBannerService;
use App\Services\Image\Path\ImagePathGalleryService;
use App\Services\Image\Path\ImagePathModelService;
use App\Services\Image\Upload\ImageUploadBannerService;
use App\Services\Image\Upload\ImageUploadGalleryService;
use App\Services\Image\Upload\ImageUploadModelService;

class ImageService
{
    /**
     * @param object $oItem
     * @param array $aFiles
     * @param array $options
     * @param string $type
     * @return bool
     * @throws \Exception
     */
    public function upload($oItem, array $aFiles, array $options, string $type): bool
    {
        $result = false;
        $files = [];
        switch ($type) {
            case ImageType::MODEL:
                $oService = $this->getServiceByType(ImageType::MODEL);
                foreach ($aFiles as $oFile) {
                    $files[] = $oService->upload($oFile, $oItem, $options);
                }
                $result = true;
                break;
            case ImageType::GALLERY:
                $oService = $this->getServiceByType(ImageType::GALLERY);
                foreach ($aFiles as $oFile) {
                    $files[] = $oService->upload($oFile, $oItem, $options);
                }
                $result = true;
                break;
            case ImageType::BANNER:
                $oService = $this->getServiceByType(ImageType::BANNER);
                foreach ($aFiles as $oFile) {
                    $files[] = $oService->upload($oFile, $oItem, $options);
                }
                $result = true;
                break;
        }
        if (isset($options['unique']) && $options['unique']) {
            $oImages = Image::where('type', $type)
                ->where('imageable_type', get_class($oItem))
                ->where('imageable_id', $oItem->id)
                ->get();
            if (count($oImages) !== 0) {
                foreach ($oImages as $oImage) {
                    if ($oImage->filename === $files[0]) {
                        continue;
                    }
                    $this->delete($oItem, $oImage, $options, $type);
                }
            }
        }
        if (isset($options['with_main']) && $options['with_main']) {
            $oMainImage = Image::where('type', $type)
                ->where('imageable_type', get_class($oItem))
                ->where('imageable_id', $oItem->id)
                ->where('is_main', 1)
                ->first();
            if (is_null($oMainImage)) {
                $oImage = Image::where('type', $type)
                    ->where('imageable_type', get_class($oItem))
                    ->where('imageable_id', $oItem->id)
                    ->first();
                $oImage->update([
                    'is_main' => 1,
                ]);
                $this->thisAfterChangeEvent($oItem, $oImage, $options, $type);
            }
        }
        $this->updateNumbers($oItem, $type);
        return $result;
    }

    /**
     * @param object|Tour $oItem
     * @param Image $oImage
     * @param array $options
     * @param string $type
     * @return bool
     * @throws \Exception
     */
    public function delete(object $oItem, Image $oImage, array $options, string $type): bool
    {
        $result = false;
        switch ($type) {
            case ImageType::MODEL:
                $oService = $this->getServiceByType(ImageType::MODEL);
                $result = $oService->delete($oItem, $oImage, $options);
                // После удаления
                $oMainImage = $oItem->modelImagesOrdered()->where('is_main', 1)->first();
                if (is_null($oMainImage)) {
                    $oMainImage = $oItem->modelImagesOrdered()->first();
                    if (!is_null($oMainImage)) {
                        $oMainImage->update([
                            'is_main' => 1,
                        ]);
                    }
                }
                break;
            case ImageType::GALLERY:
                $oService = $this->getServiceByType(ImageType::GALLERY);
                $result = $oService->delete($oItem, $oImage, $options);
                break;
            case ImageType::BANNER:
                $oService = $this->getServiceByType(ImageType::BANNER);
                $result = $oService->delete($oItem, $oImage, $options);
                break;
        }
        $this->updateNumbers($oItem, $type);
        $this->thisAfterChangeEvent($oItem, $oImage, $options, $type);
        return $result;
    }

    /**
     * @param object $oItem
     * @param Image $oImage
     * @param array $options
     * @param string $type
     * @return bool
     * @throws \Exception
     */
    public function main(object $oItem, Image $oImage, array $options, string $type): bool
    {
        $result = false;
        switch ($type) {
            case ImageType::MODEL:
                $oService = $this->getServiceByType(ImageType::MODEL);
                $result = $oService->clearMain($oItem);
                break;
            case ImageType::GALLERY:
                $oService = $this->getServiceByType(ImageType::GALLERY);
                $result = $oService->clearMain($oItem);
                break;
            case ImageType::BANNER:
                $oService = $this->getServiceByType(ImageType::BANNER);
                $result = $oService->clearMain($oItem);
                break;
        }
        // была проблема после мультисохранения
        // бралось изображение с is_main 1, стирались is_main везде и потом ему ставилось опять 1
        // и наверно т.к. значение одинаковое, то обновление не происходило
        // поэтому стоит перевыборка этого изображения
        /** @var Image $oImage */
        $oImage = Image::find($oImage->id);
        $oImage->update([
            'is_main' => 1,
        ]);
        //$this->updateNumbers($oItem, $type);
        $this->thisAfterChangeEvent($oItem, $oImage, $options, $type);
        return $result;
    }

    /**
     * @param string $type
     * @return ImageUploadBannerService|ImageUploadGalleryService|ImageUploadModelService|null
     */
    public function getServiceByType(string $type)
    {
        $oService = null;
        switch ($type) {
            case ImageType::MODEL:
                $oService = new ImageUploadModelService();
                break;
            case ImageType::GALLERY:
                $oService = new ImageUploadGalleryService();
                break;
            case ImageType::BANNER:
                $oService = new ImageUploadBannerService();
                break;
        }
        return $oService;
    }

    /**
     * @param string $type
     * @return ImagePathModelService|ImagePathGalleryService|ImagePathBannerService|null
     */
    public function getImagePathServiceByType(string $type)
    {
        $oService = null;
        switch ($type) {
            case ImageType::MODEL:
                $oService = new ImagePathModelService();
                break;
            case ImageType::GALLERY:
                $oService = new ImagePathGalleryService();
                break;
            case ImageType::BANNER:
                $oService = new ImagePathBannerService();
                break;
        }
        return $oService;
    }

    /**
     * @param object $oItem
     * @param Image $oImage
     * @param array $options
     * @param string $type
     */
    private function thisAfterChangeEvent(object $oItem, Image $oImage, array $options, string $type)
    {
        if (isset($options['clear_cache']) && $options['clear_cache']) {
            if (method_exists($this, 'thisAfterChange')) {
                $this->thisAfterChange($oItem);
            }
        }
    }

    /**
     * @param object $oItem
     * @param string $type
     */
    public function updateNumbers(object $oItem, string $type)
    {
        $oImages = $this->getOrderedImagesByType($oItem, $type);
        foreach ($oImages as $key => $oImage) {
            $oImage->update([
                'number' => $key + 1,
            ]);
        }
    }

    /**
     * @param object|Tour $oItem
     * @param string $type
     * @return mixed
     */
    public function getOrderedImagesByType(object $oItem, string $type)
    {
        $oImages = [];
        switch ($type) {
            case ImageType::MODEL:
                $oImages = $oItem->modelImagesOrdered;
                break;
            case ImageType::GALLERY:
                $oImages = $oItem->galleryImagesOrdered;
                break;
            case ImageType::BANNER:
                $oImages = $oItem->bannerImagesOrdered;
                break;
        }
        return $oImages;
    }
}
