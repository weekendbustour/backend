<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use App\Models\Source;

/**
 * Trait Sourceable
 * @package App\Services\Sourceable
 *
 * @property mixed source
 *
 */
trait Sourceable
{
    /**
     * @return mixed
     */
    public function source()
    {
        return $this->morphOne(Source::class, 'sourceable');
    }
}
