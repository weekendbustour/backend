<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use Illuminate\Support\Facades\File;

trait Zipable
{
    /**
     * Разархивировать в директорию
     *
     * @param object $model
     * @param string $directory
     * @return bool
     */
    public function extract($model, string $directory)
    {
        $file = $model->getFilePath();
        $zip = new \ZipArchive();
        try {
            if (!File::exists($directory)) {
                File::makeDirectory($directory, 0777, true);
            }
            if ($zip->open($file) === true) {
                $zip->extractTo($directory);
                $zip->close();
            }
        } catch (\Exception $e) {
            return false;
        }
        return true;
    }
}
