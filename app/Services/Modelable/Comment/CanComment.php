<?php

declare(strict_types=1);

namespace App\Services\Modelable\Comment;

use App\Models\Comment;

trait CanComment
{
    /**
     * @param $commentable
     * @param string $commentText
     * @param int $rate
     * @param $parent_id
     * @param $anon
     * @param $reply_id
     * @return $this
     */
    public function comment($commentable, $commentText = '', $rate = 0, $anon = 0, $parent_id = null, $reply_id = null)
    {
        return Comment::create([
            'commented_id' => $this->id,
            'commented_type' => get_class(),
            'commentable_id' => $commentable->id,
            'commentable_type' => $commentable->getShortName(),
            'text' => $commentText,
            'rate' => $rate,
            'approved' => true,
            'anon' => $anon,
            'parent_id' => $parent_id,
            'reply_id' => $reply_id,
        ]);
    }

    public function commentGetMain($commentable)
    {
        return $this->comments()
            ->where('commented_id', $this->id)
            ->where('commented_type', get_class())
            ->where('commentable_id', $commentable->id)
            ->where('commentable_type', $commentable->getShortName())
            ->where('parent_id', null)
            ->where('reply_id', null)
            ->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commented');
    }
}
