<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use App\Models\MetaTag;

/**
 * Trait MetaTagable
 * @package App\Services\Modelable
 *
 * @property mixed|MetaTag|null metaTag
 *
 */
trait MetaTagable
{
    /**
     * Get the post's image.
     */
    public function metaTag()
    {
        return $this->morphOne(MetaTag::class, 'meta_tagable');
    }
}
