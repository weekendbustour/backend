<?php

declare(strict_types=1);

namespace App\Services\Modelable\Word;

use App\Http\Transformers\TourParameterTransformer;
use App\Http\Transformers\TourTransformer;
use App\Models\ExternalVkParameter;
use App\Models\Option;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\WordDeclension;
use Illuminate\Support\Str;

/**
 * Trait WordDeclensionable
 * @package App\Services\Modelable
 *
 * @property string|null|WordDeclension $declensionWhere
 * @property string|null|WordDeclension $declensionFrom
 */
trait WordDeclensionable
{
    /**
     * @return mixed
     */
    public function wordDeclensions()
    {
        return $this->morphMany(WordDeclension::class, 'word_declensionable');
    }

    /**
     * @return mixed
     */
    public function wordDeclensionWhere()
    {
        return $this->morphOne(WordDeclension::class, 'word_declensionable')->where('type', WordDeclension::TYPE_WHERE);
    }

    /**
     * @return mixed
     */
    public function wordDeclensionFrom()
    {
        return $this->morphOne(WordDeclension::class, 'word_declensionable')->where('type', WordDeclension::TYPE_FROM);
    }

    /**
     * @return mixed
     */
    public function getDeclensionWhereAttribute()
    {
        return $this->wordDeclensionWhere()->first();
    }

    /**
     * @return mixed
     */
    public function getDeclensionFromAttribute()
    {
        return $this->wordDeclensionFrom()->first();
    }

    /**
     * @param string $type
     * @return WordDeclension|null
     */
    public function getDeclensionItemByType(string $type)
    {
        $method = 'getDeclension' . Str::ucfirst($type) . 'Attribute';
        if (method_exists($this, $method)) {
            return $this->{$method}();
        }
        return null;
    }

    /**
     * @param string $type
     * @return mixed
     */
    public function getDeclensionMorphByType(string $type)
    {
        $method = 'wordDeclension' . Str::ucfirst($type);
        if (method_exists($this, $method)) {
            return $this->{$method}();
        }
        return null;
    }
}
