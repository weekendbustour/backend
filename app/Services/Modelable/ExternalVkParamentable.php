<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use App\Http\Transformers\TourParameterTransformer;
use App\Http\Transformers\TourTransformer;
use App\Models\ExternalVkParameter;
use App\Models\Option;
use App\Models\Organization;
use App\Models\Tour;

trait ExternalVkParamentable
{
    /**
     * @return mixed
     */
    public function externalVkParameters()
    {
        return $this->morphMany(ExternalVkParameter::class, 'external_vk_parametrable');
    }

    /**
     * @return mixed
     */
    public function vkPost()
    {
        return $this->morphOne(ExternalVkParameter::class, 'external_vk_parametrable')->where('type', ExternalVkParameter::TYPE_POST);
    }

    /**
     * @return mixed
     */
    public function vkMarket()
    {
        return $this->morphOne(ExternalVkParameter::class, 'external_vk_parametrable')->where('type', ExternalVkParameter::TYPE_MARKET);
    }

    /**
     * @return mixed
     */
    public function vkAlbum()
    {
        return $this->morphOne(ExternalVkParameter::class, 'external_vk_parametrable')->where('type', ExternalVkParameter::TYPE_ALBUM);
    }

    /**
     * @return mixed
     */
    public function vkMarketAlbum()
    {
        $oMarket = $this->morphOne(ExternalVkParameter::class, 'external_vk_parametrable')->where('type', ExternalVkParameter::TYPE_MARKET)->getResults();
        return $this->vkAlbum()->where('item_id', $oMarket->album_id);
    }

    /**
     * @return string
     */
    public function vkPostTextTour()
    {
        /** @var Tour $oItem */
        $oItem = $this;
        $text = '';
        $text .= $oItem->metaTag->title . "\n\n";
        $text .= $oItem->preview_description . "\n\n";

        $text = $this->commonTourText($oItem, $text);

        $text .= 'Полное описание по ссылке: ' . $oItem->getUrl();
        return $text;
    }

    /**
     * @return string
     */
    public function vkMarketTextTour()
    {
        /** @var Tour $oItem */
        $oItem = $this;
        $text = '';
        //$text .= $oItem->metaTag->title . "\n\n";
        $text .= $oItem->preview_description . "\n\n";

        $text = $this->commonTourText($oItem, $text);

        $text .= 'Полное описание по ссылке: ' . $oItem->getUrl();
        return $text;
    }

    /**
     * @param Tour $oItem
     * @param string $text
     * @return string
     */
    private function commonTourText(Tour $oItem, string $text)
    {
        //$text .= $oItem->description . "\n\n";
        $text .= 'Цена: ' . $oItem->price->value . 'р.' . "\n";
        if (!is_null($oItem->price->discount)) {
            $text .= 'Цена со скидкой: ' . $oItem->price->discount . 'р.' . "\n";
        }
        $text .= "\n";
        $text .= '🚙 Дата: ' . $oItem->start_at->format('d.m.Y') . "\n";
        $text .= '👩‍🔧 Организатор: ' . $oItem->organization->title . "\n";
        $text .= '🚏 Отравление из ' . natural_language_join($oItem->departures->pluck('title')->toArray()) . '' . "\n";
        $text .= '🏔 Направление: ' . $oItem->direction->title . "\n\n";
        // &nbsp;
//
//        $aParameters = (new TourParameterTransformer())->transformTourParameters($oItem);
//        $aParametersView = (new TourParameterTransformer())->transformTourParametersForView($oItem);
//
//        if (isset($aParametersView[Option::NAME_DAYS])) {
//            $text .= $aParametersView[Option::NAME_DAYS]['title'] . ': ' . $aParametersView[Option::NAME_DAYS]['value'] . "\n";
//        }
//        if (isset($aParametersView[Option::NAME_TIME_ROAD])) {
//            $text .= $aParametersView[Option::NAME_TIME_ROAD]['title'] . ': ' . $aParametersView[Option::NAME_TIME_ROAD]['value'] . "\n";
//        }
//        if (isset($aParametersView[Option::NAME_GID])) {
//            $text .= $aParametersView[Option::NAME_GID]['title'] . ': ' . $aParametersView[Option::NAME_GID]['value'] . "\n";
//        }
//        if (isset($aParametersView[Option::NAME_PHONE_INFO])) {
//            $text .= $aParametersView[Option::NAME_PHONE_INFO]['title'] . ': ' . $aParametersView[Option::NAME_PHONE_INFO]['value'] . "\n";
//        }
//        if (isset($aParametersView[Option::NAME_PHOTO_SESSION])) {
//            $text .= $aParametersView[Option::NAME_PHOTO_SESSION]['title'] . ': ' . $aParametersView[Option::NAME_PHOTO_SESSION]['value'] . "\n";
//        }
//        $text .= "\n";
//
//        if (isset($aParameters[Option::NAME_INCLUDE])) {
//            $text .= 'Что входит? ' . "\n" . $aParameters[Option::NAME_INCLUDE]['value'] . "\n\n";
//        }
//        if (isset($aParameters[Option::NAME_NOT_INCLUDE])) {
//            $text .= 'Что не входит? ' . "\n" . $aParameters[Option::NAME_NOT_INCLUDE]['value'] . "\n\n";
//        }
//        if (isset($aParameters[Option::NAME_MORE_INCLUDE])) {
//            $text .= 'Дополнительные расходы ' . "\n" . $aParameters[Option::NAME_MORE_INCLUDE]['value'] . "\n\n";
//        }
//        if (isset($aParameters[Option::NAME_RESERVATION])) {
//            $text .= 'Как забронировать? ' . "\n" . $aParameters[Option::NAME_RESERVATION]['value'] . "\n\n";
//        }
        return $text;
    }
}
