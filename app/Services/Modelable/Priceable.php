<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use App\Models\Price;

trait Priceable
{
    /**
     * @return mixed
     */
    public function price()
    {
        return $this->morphOne(Price::class, 'priceable');
    }
}
