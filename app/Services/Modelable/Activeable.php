<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use Illuminate\Database\Eloquent\Builder;

trait Activeable
{
    /**
     * @param Builder $query
     * @return Builder|\Illuminate\Database\Query\Builder
     */
    public function scopeActive(Builder $query)
    {
        return $query->where('active', 1);
    }
}
