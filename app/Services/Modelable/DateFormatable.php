<?php

declare(strict_types=1);

namespace App\Services\Modelable;

trait DateFormatable
{
    /**
     * данный метод написан на коленке (нужно переписать)
     */
    public function post($time)
    {
        $timestamp = strtotime($time);
        $published = date('d.m.Y', $timestamp);

        if ($published === date('d.m.Y')) {
            return trans('date.today', ['time' => date('H:i', $timestamp)]);
        } elseif ($published === date('d.m.Y', strtotime('-1 day'))) {
            return trans('date.yesterday', ['time' => date('H:i', $timestamp)]);
        } else {
            $formatted = trans('date.later', [
                'time' => date('H:i', $timestamp),
                'date' => date('d F' . (date('Y', $timestamp) === date('Y') ? null : ' Y'), $timestamp)
            ]);

            return strtr($formatted, trans('date.month_declensions'));
        }
    }

    /**
     * Вывод в формате 29 августа 2017г. в 20:00
     *
     * @param $time
     * @return string
     */
    public function dmYHi($time)
    {
        $timestamp = strtotime($time);
        $published = date('d.m.Y', $timestamp);

        $formatted = trans('date.later', [
            'time' => date('H:i', $timestamp),
            'date' => date('d F ' . date('Y', $timestamp) . 'г.', $timestamp)
        ]);

        return strtr($formatted, trans('date.month_declensions'));
    }
}
