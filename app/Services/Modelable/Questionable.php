<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use App\Models\Question;

trait Questionable
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable')->ordered();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activeQuestions()
    {
        return $this->morphMany(Question::class, 'questionable')->active()->ordered();
    }
}
