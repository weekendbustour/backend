<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use App\Models\Tour;
use App\Services\Preview;

trait Previewable
{
    /**
     * @return string
     */
    public function previewCardUrl()
    {
        $model = $this->getImageTypeModel();
        return asset('storage/images/card/' . $model . '/' . $this->id . '.png');
    }

    /**
     * @return string
     */
    public function previewCardPath()
    {
        $model = $this->getImageTypeModel();
        return public_path('storage/images/card/' . $model . '/' . $this->id . '.png');
    }

    /**
     * @return string
     */
    public function previewCardCreatePath()
    {
        /** @var Tour $oTour */
        $oTour = self::find($this->id);
        (new Preview())->createTourCard($oTour);
        return $this->previewCardPath();
    }
}
