<?php

declare(strict_types=1);

namespace App\Services\Modelable;

trait Parentable
{
    public function children()
    {
        return $this->hasMany(__CLASS__, 'parent_id', 'id');
    }

    public function activeChildren()
    {
        return $this->hasMany(__CLASS__, 'parent_id', 'id')->where('status', 1);
    }

    public function approvedChildren()
    {
        return $this->hasMany(__CLASS__, 'parent_id', 'id')->where('approved', 1);
    }

    public function parent()
    {
        return $this->belongsTo(__CLASS__);
    }

    public function hasChildren()
    {
        return count($this->children) !== 0;
    }

    public function hasApprovedChildren()
    {
        return count($this->approvedChildren) !== 0;
    }

    public function hasActiveChildren()
    {
        return count($this->activeChildren) !== 0;
    }

    public function hasSubChildren()
    {
        $oChildren = $this->children;
        if (count($oChildren) !== 0) {
            foreach ($oChildren as $oChild) {
                if (count($oChild->children) !== 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public function hasSubActiveChildren($oItems = null)
    {
        if (!is_null($oItems)) {
            $oChildren = $oItems->get($this->id)->activeChildren;
        } else {
            $oChildren = $this->activeChildren;
        }
        if (count($oChildren) !== 0) {
            if (!is_null($oItems)) {
                foreach ($oChildren as $oChild) {
                    if (count($oItems->get($oChild->id)->activeChildren) !== 0) {
                        return true;
                    }
                }
            } else {
                foreach ($oChildren as $oChild) {
                    if (count($oChild->activeChildren) !== 0) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public function tree()
    {
        $oTree = collect();
        if (!is_null($this->parent_id)) {
            $oParent = $this->parent;
            if (!is_null($oParent)) {
                $oTree->prepend($oParent);
            }
            if (!is_null($oParent->parent_id)) {
                $oParent = $oParent->parent;
                if (!is_null($oParent)) {
                    $oTree->prepend($oParent);
                }
            }
        }
        return $oTree;
    }

    public function treeChildren()
    {
        $oTree = collect();
        if ($this->hasChildren()) {
            $oChildren = $this->children;
            $oTree = $oChildren;
            foreach ($oChildren as $tree) {
                if ($tree->hasChildren()) {
                    $oTree = $oTree->merge($tree->children);
                }
            }
        }
        //$oTree = $oTree->merge($this);
        $oTree = $oTree->unique();
        return $oTree;
    }
}
