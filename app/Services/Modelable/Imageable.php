<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use App\Models\Image;
use App\Services\Image\Facades\ImagePath;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use App\Services\Image\Path\ImagePathBannerService;
use App\Services\Image\Path\ImagePathGalleryService;
use App\Services\Image\Path\ImagePathModelService;
use App\Services\Image\Path\ImagePathService;
use Illuminate\Support\Str;

/**
 * Trait Imageable
 * @package App\Services\Modelable
 *
 * @property mixed $textImages
 */
trait Imageable
{
    /**
     * @return string
     */
    public function getImageModel(): string
    {
        return $this->getImageModelPath(ImageSize::XL);
    }

    /**
     * @return array
     */
    public function getImagesModel(): array
    {
        return $this->getImageModelGalleryPath(ImageSize::XL);
    }

    /**
     * @return string
     */
    public function getImageBanner(): string
    {
        return $this->getImageBannerPath(ImageSize::XL);
    }

    /**
     * @return string
     */
    public function getImageAttribute(): string
    {
        return $this->getImageModelPath(ImageSize::ORIGINAL);
    }

    /**
     * @return array
     */
    public function getImageGalleryAttribute(): array
    {
        return $this->getImageModelGalleryPath(ImageSize::XL);
    }

    /**
     * @return array
     */
    public function getImageGalleryCardMdAttribute(): array
    {
        return $this->getImageModelGalleryPath(ImageSize::CARD_MD);
    }

    /**
     * @return string
     */
    public function getImageOriginalAttribute(): string
    {
        return $this->getImageModelPath(ImageSize::ORIGINAL);
    }

    /**
     * @return string
     */
    public function getImageDefaultAttribute(): string
    {
        return $this->getImageModelPath(ImageSize::XL);
    }

    /**
     * @return string
     */
    public function getImageSquareAttribute(): string
    {
        return $this->getImageModelPath(ImageSize::SQUARE);
    }

    /**
     * @return string
     */
    public function getImageXsAttribute(): string
    {
        return $this->getImageModelPath(ImageSize::XS);
    }

    /**
     * @return string
     */
    public function getImageCardAttribute(): string
    {
        return $this->getImageModelPath(ImageSize::CARD_DEFAULT);
    }

    /**
     * @return array
     */
    public function getGalleryAttribute(): array
    {
        return $this->getImageGalleryPath(ImageSize::XL);
    }

    /**
     * @return string
     */
    public function getImageBannerAttribute(): string
    {
        return $this->getImageBannerPath(ImageSize::XL);
    }
//
//
//    /**
//     * Магический метод, для получения пути картинок
//     * @param  string $name             формат картинки
//     * @return string | null | object   путь до изображения
//     */
//    public function __get($name)
//    {
//        switch ($name) {
//            case 'image':
//                return $this->getImagePath('original');
//            case 'image_default':
//                return $this->getImagePath('xl');
//            case 'image_card':
//                return $this->getImagePath('card-default');
//            case 'image_square':
//                return $this->getImagePath('square');
//            case 'image_rectangle_height':
//                return $this->getImagePath('rectangle_height');
//            default:
//                return parent::__get($name);
//        }
//    }


    /**
     * @return mixed
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable');
    }

    /**
     * @return mixed
     */
    public function modelImages()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', ImageType::MODEL);
    }

    /**
     * @return mixed
     */
    public function modelImagesOrdered()
    {
        return $this->modelImages()->orderBy('id');
    }

    /**
     * @return mixed
     */
    public function galleryImages()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', ImageType::GALLERY);
    }

    /**
     * @return mixed
     */
    public function galleryImagesOrdered()
    {
        return $this->galleryImages()->orderBy('id');
    }

    /**
     * @return mixed
     */
    public function bannerImages()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', ImageType::BANNER);
    }

    /**
     * @return mixed
     */
    public function bannerImagesOrdered()
    {
        return $this->bannerImages()->orderBy('id');
    }

    /**
     * @return mixed
     */
    public function textImages()
    {
        return $this->morphMany(Image::class, 'imageable')
            ->where('type', ImageType::TEXT);
    }

    /**
     * @param string $size
     * @return string
     */
    public function getImageModelPath($size = 'square'): string
    {
        $model = $this->getImageTypeModel();
        return (new ImagePathModelService())->getMain($model, $size, $this);
    }

    /**
     * @param Image $oImage
     * @return string
     */
    public function getImageModelOriginalStorage(Image $oImage): string
    {
        $model = $this->getImageTypeModel();
        return (new ImagePathModelService())->getPathByImage($model, $oImage);
    }

    /**
     * @param string $size
     * @return array
     */
    public function getImageGalleryPath($size = 'square'): array
    {
        $model = $this->getImageTypeModel();
        return (new ImagePathGalleryService())->getImages($model, $size, $this);
    }

    /**
     * @param string $size
     * @return array
     */
    public function getImageModelGalleryPath($size = 'square'): array
    {
        $model = $this->getImageTypeModel();
        return (new ImagePathModelService())->getImagesWithDefault($model, $size, $this);
    }

    /**
     * @param string $size
     * @return string
     */
    public function getImageBannerPath($size = 'square'): string
    {
        $model = $this->getImageTypeModel();
        return (new ImagePathBannerService())->getMain($model, $size, $this);
    }

    /**
     * @param string $size
     * @return bool
     */
    public function hasImagePath($size = 'square'): bool
    {
        $model = $this->getImageTypeModel();
        return ImagePath::checkMain($model, $size, $this);
    }

    /**
     * @return bool
     */
    public function imageHasBanner(): bool
    {
        $model = $this->getImageTypeModel();
        return (new ImagePathBannerService())->checkMainImage($model, ImageSize::XL, $this);
    }

    /**
     * @return string
     */
    public function getImageTypeModel(): string
    {
        $sPath = get_class($this);
        $sPath = substr($sPath, strrpos($sPath, '\\') + 1);
        $sClass = $sPath . Str::studly('_controller');
        $sClass = 'App\Cmf\Project\\' . $sPath . '\\' . $sClass;
        if (class_exists($sClass)) {
            $oClass = new $sClass();
            $model = $oClass::NAME;
        } else {
            $model = get_class($this);
            $model = substr($model, strrpos($model, '\\') + 1);
            $model = strtolower($model);
        }
        return $model;
    }

    /**
     * @return string
     */
    public function getImageOriginalModelAttribute(): string
    {
        return $this->getImageModelPath('original');
    }

    /**
     * @return array
     */
    public function getImageOriginalGalleryAttribute(): array
    {
        return $this->getImageGalleryPath('original');
    }

    /**
     * @return string
     */
    public function getImageOriginalBannerAttribute(): string
    {
        return $this->getImageBannerPath('original');
    }

    /**
     * @return array
     */
    public function getImageBannerCaption(): array
    {
        $oImage = $this->bannerImages()->first();
        $data = [];
        if (is_null($oImage)) {
            return $data;
        }
        $data = $this->generateCaption($oImage);
        return $data;
    }

    /**
     * @return array
     */
    public function getImageModelCaption(): array
    {
        $oImage = $this->modelImages()->first();
        $data = [];
        if (is_null($oImage)) {
            return $data;
        }
        $data = $this->generateCaption($oImage);
        return $data;
    }

    /**
     * @return array
     */
    public function getImageGalleryCaption(): array
    {
        $oImages = $this->modelImages()->get();
        $data = [];
        foreach ($oImages as $oImage) {
            if (is_null($oImage)) {
                continue;
            }
            $data[] = $this->generateCaption($oImage);
        }
        return $data;
    }

    /**
     * @return array
     */
    public function getGalleryCaption(): array
    {
        $oImages = $this->galleryImages()->get();
        $data = [];
        foreach ($oImages as $oImage) {
            if (is_null($oImage)) {
                continue;
            }
            $data[] = $this->generateCaption($oImage);
        }
        return $data;
    }

    /**
     * @param Image $oImage
     * @return array
     */
    private function generateCaption(Image $oImage)
    {
        $data = [];
        if (!is_null($oImage->info)) {
            if (isset($oImage->info['title'])) {
                $data['title'] = $oImage->info['title'];
            }
            if (isset($oImage->info['description'])) {
                $data['description'] = $oImage->info['description'];
            }
        }
        if (!is_null($oImage->source)) {
            $data['source'] = $oImage->source;
        }
        return $data;
    }
}
