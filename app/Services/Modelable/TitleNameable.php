<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use Illuminate\Support\Str;

trait TitleNameable
{
    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        //info($this->attributes);
        if (!isset($this->attributes['name']) || empty($this->attributes['name'])) {
            $this->attributes['name'] = Str::slug($value, '-');
        }
    }
//
//    /**
//     * @param string|null $value
//     */
//    public function setNameAttribute(?string $value)
//    {
//        info('setNameAttribute');
//        info($this->attributes);
//        if (!empty($value)) {
//            $this->attributes['name'] = $value;
//        }
//    }
}
