<?php

declare(strict_types=1);

namespace App\Services\Modelable\Rate;

use App\Models\Rating;

trait CanRate
{
    public function ratings()
    {
        return $this->hasMany(Rating::class, 'user_id');
    }

    public function rate($model, $value)
    {
        return (new LaravelRating())->rate($this, $model, $value);
    }

    public function rateByComment($model, $value)
    {
        return (new LaravelRating())->rateByComment($this, $model, $value);
    }

    public function getRatingValue($model)
    {
        return (new LaravelRating())->getRatingValue($this, $model);
    }

    public function isRated($model)
    {
        return (new LaravelRating())->isRated($this, $model);
    }

    public function rated()
    {
        return (new LaravelRating())->resolveRatedItems($this->ratings);
    }
}
