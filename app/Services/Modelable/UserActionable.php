<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use App\Models\UserAction;

trait UserActionable
{
    /**
     * @return mixed
     */
    public function userAction()
    {
        return $this->morphOne(UserAction::class, 'user_actionable');
    }

    /**
     * @return mixed
     */
    public function userActions()
    {
        return $this->morphMany(UserAction::class, 'user_actionable');
    }

    /**
     * @return mixed
     */
    public function userActionsActive()
    {
        return $this->morphMany(UserAction::class, 'user_actionable')->active()->ordered();
    }
}
