<?php

declare(strict_types=1);

namespace App\Services\Modelable;

use App\Models\File;
use App\Services\File\Facades\FilePath;

trait Fileable
{
    /**
     * @return mixed
     */
    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return FilePath::get($this->getFilePathModel(), $this);
    }

    /**
     * Название модели
     *
     * @return string
     */
    protected function getFilePathModel()
    {
        $model = get_class($this);
        $model = strtolower(substr($model, strrpos($model, '\\') + 1));
        return $model;
    }
}
