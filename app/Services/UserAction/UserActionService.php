<?php

declare(strict_types=1);

namespace App\Services\UserAction;

use App\Models\Tour;
use App\Models\User;
use App\Models\UserAction;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class UserActionService
{
    /**
     * @var User|Authenticatable|null
     */
    private $oUser = null;

    /**
     * @var null
     */
    private $type = null;

    /**
     * UserActionService constructor.
     * @param User|Authenticatable|null $oUser
     */
    public function __construct($oUser = null)
    {
        $this->oUser = !is_null($oUser) ? $oUser : Auth::user();
    }

    /**
     * @param Tour $oItem
     */
    public function tourPublished(Tour $oItem)
    {
        $this->type = UserAction::TYPE_PUBLISHED;
        $this->tour($oItem);
    }

    /**
     * @param Tour $oItem
     */
    public function tourUnpublished(Tour $oItem)
    {
        $this->type = UserAction::TYPE_UNPUBLISHED;
        $this->tour($oItem);
    }

    /**
     * @param Tour $oItem
     */
    public function tourCreated(Tour $oItem)
    {
        $this->type = UserAction::TYPE_CREATED;
        $this->tour($oItem);
    }

    /**
     * @param Tour $oItem
     */
    public function tourUpdated(Tour $oItem)
    {
        $this->type = UserAction::TYPE_UPDATED;
        $this->tour($oItem);
    }

    /**
     * @param Tour $oItem
     */
    public function tour(Tour $oItem)
    {
        $oItem->userAction()->create([
            'type' => $this->type,
            'user_id' => $this->oUser->id,
            'start_at' => now(),
        ]);
    }
}
