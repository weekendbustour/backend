<?php

declare(strict_types=1);

namespace App\Services\Notification\Slack;

use App\Jobs\QueueCommon;
use App\Notifications\Debug\FeedbackRatingNotification;
use App\Notifications\Debug\WeatherNotification;
use App\Notifications\Debug\YandexCloudNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class SlackFeedbackRatingNotification extends SlackCommonNotification implements SlackNotificationInterface
{
    /**
     *
     */
    private const STACK = 'slack-feedback-rating';

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function config(): array
    {
        return config('logging.channels.' . self::STACK);
    }

    /**
     * @param string $message
     */
    public function send(string $message): void
    {
        if (!checkEnv($this->env())) {
            return;
        }
        Notification::route('slack', $this->channel())->notify((new FeedbackRatingNotification($message))->onQueue(QueueCommon::QUEUE_NAME_NOTIFICATION));
    }

    /**
     * @param string $message
     */
    public function log(string $message): void
    {
        if (!checkEnv($this->env())) {
            return;
        }
        Log::channel(self::STACK)->debug($message);
    }
}
