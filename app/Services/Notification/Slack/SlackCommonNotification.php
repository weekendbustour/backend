<?php

declare(strict_types=1);

namespace App\Services\Notification\Slack;

use App\Jobs\QueueCommon;
use App\Notifications\Debug\CriticalNotification;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Log;

class SlackCommonNotification
{
    /**
     * @return string|array|null
     */
    protected function env()
    {
        return $this->config()['env'];
    }

    /**
     * @return string|array|null
     */
    protected function channel()
    {
        return $this->config()['url'];
    }
}
