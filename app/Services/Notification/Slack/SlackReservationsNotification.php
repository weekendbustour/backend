<?php

declare(strict_types=1);

namespace App\Services\Notification\Slack;

use App\Jobs\QueueCommon;
use App\Notifications\Debug\ReservationsNotification;
use App\Notifications\Debug\WeatherNotification;
use App\Notifications\Debug\YandexCloudNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class SlackReservationsNotification extends SlackCommonNotification implements SlackNotificationInterface
{
    /**
     *
     */
    private const STACK = 'slack-reservations';

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function config(): array
    {
        return config('logging.channels.' . self::STACK);
    }

    /**
     * @param string $message
     * @param string $type
     */
    public function send(string $message): void
    {
        if (!checkEnv($this->env())) {
            return;
        }
        Notification::route('slack', $this->channel())->notify((new ReservationsNotification($message))->onQueue(QueueCommon::QUEUE_NAME_NOTIFICATION));
    }

    /**
     * @param string $message
     */
    public function log(string $message): void
    {
        if (!checkEnv($this->env())) {
            return;
        }
        Log::channel(self::STACK)->debug($message);
    }
}
