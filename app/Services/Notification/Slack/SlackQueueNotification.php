<?php

declare(strict_types=1);

namespace App\Services\Notification\Slack;

use App\Jobs\QueueCommon;
use App\Notifications\Debug\QueueNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class SlackQueueNotification extends SlackCommonNotification
{
    /**
     *
     */
    private const STACK = 'slack-queue';

    /**
     * @return \Illuminate\Config\Repository|mixed
     */
    public function config(): array
    {
        return config('logging.channels.' . self::STACK);
    }

    /**
     * @param string $message
     * @param string $queue
     */
    public function send(string $message, string $queue): void
    {
        if (!checkEnv($this->env())) {
            return;
        }
        Notification::route('slack', $this->channel())->notify((new QueueNotification($message, $queue))->onQueue(QueueCommon::QUEUE_NAME_NOTIFICATION));
    }

    /**
     * @param string $message
     */
    public function log(string $message): void
    {
        if (!checkEnv($this->env())) {
            return;
        }
        Log::channel(self::STACK)->error($message);
    }
}
