<?php

declare(strict_types=1);

namespace App\Services\Search;

class SearchService
{
    const ORDER_BY_NEAREST = 'nearest';
    const ORDER_BY_NEW = 'new';
    const ORDER_BY_PRICE_DOWN = 'price_down';
    const ORDER_BY_PRICE_UP = 'price_up';
    //const ORDER_BY_RATING = 'rating';

    /**
     * @return array
     */
    public function orders()
    {
        return [
            self::ORDER_BY_NEAREST => 'Ближайшие',
            self::ORDER_BY_NEW => 'Новинки',
            self::ORDER_BY_PRICE_DOWN => 'Сначала дешевле',
            self::ORDER_BY_PRICE_UP => 'Сначала дороже',
            //self::ORDER_BY_RATING => 'По рейтингу',
        ];
    }
}
