<?php

declare(strict_types=1);

namespace App\Services\Image\Upload;

use App\Models\Image;
use App\Services\Image\ImageType;

class ImageUploadGalleryService extends ImageUploadService implements ImageUploadInterface
{
    /**
     * @param array $options
     * @return bool
     */
    private function checkOptions(array $options = []): bool
    {
        return true;
    }

    /**
     * @param object $oItem
     * @return bool
     */
    public function clearMain(object $oItem): bool
    {
        $oMainImages = $oItem->galleryImages()->where('is_main', 1)->get();
        foreach ($oMainImages as $oMainImage) {
            $oMainImage->update([
                'is_main' => 0,
            ]);
        }
        return true;
    }

    /**
     * @param object $oItem
     * @param Image $oImage
     * @param array $options
     * @return bool
     * @throws \Exception
     */
    public function delete(object $oItem, Image $oImage, array $options): bool
    {
        $key = $oItem->getImageTypeModel();
        $filters = isset($options['filters']) ? $options['filters'] : [];
        $this->deleteImages($oImage->filename, $key, $oItem->id, $filters, ImageType::GALLERY);
        $oImage->delete();
        return true;
    }

    /**
     * @param object $oFile
     * @param object $oItem
     * @param array $options
     * @return string
     * @throws \Exception
     */
    public function upload(object $oFile, object $oItem, array $options): string
    {
        $key = $oItem->getImageTypeModel();
        if (!$this->checkOptions($options)) {
            throw new \Exception('Options not correct.');
        }
        $filters = isset($options['filters']) ? $options['filters'] : [];
        $filename = $this->uploadFile($oFile, $key, $oItem->id, $filters, ImageType::GALLERY);
        $oItem->galleryImages()->create([
            'type' => ImageType::GALLERY,
            'filename' => $filename,
        ]);
        return $filename;
    }
}
