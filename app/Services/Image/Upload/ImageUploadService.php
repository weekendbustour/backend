<?php

declare(strict_types=1);

namespace App\Services\Image\Upload;

use App\Models\Image;
use App\Services\Notification\Slack\SlackYandexCloudNotification;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Services\Image\Filters\FilterInterface;
use Intervention\Image\ImageManagerStatic as ImageStatic;

class ImageUploadService
{
    /**
     * Ключ с оригиналами
     *
     * @var string
     */
    private $originalKey = 'original';

    /**
     * images
     *
     * @var string
     */
    protected $imagePublicDirectory;

    /**
     * @var string
     */
    protected $imageStorageTmpDirectory;

    /**
     * ImageService constructor.
     */
    public function __construct()
    {
        $this->imagePublicDirectory = config('cmf.image_public_directory');

        if (config('app.env') === 'testing') {
            $this->imagePublicDirectory = config('cmf.image_testing_directory');
        }
        $this->imageStorageTmpDirectory = 'storage/tmp';
    }

    /**
     * @param object $file
     * @param string $key
     * @param int $id
     * @param array $filters
     * @param string|null $type
     * @return string
     */
    public function uploadFile(object $file, string $key, int $id, array $filters = [], ?string $type = null): string
    {
        $path = is_null($type)
            ? $this->getPath($key, $id)
            : $this->getPath($key, $id, $type);

        $originalDir = public_path($path . '/' . $this->originalKey . '/');
        $this->checkDirectory($originalDir);

        $filename = $this->uploadOriginalFile($file, $originalDir);
        $original = $originalDir . $filename;

        foreach ($filters as $size => $filter) {
            /**
             * @var $oObject FilterInterface
             */
            $oObject = new $filter['filter']($original, $filter['options']);
            $oObject->resize($path, $size, $filename);
        }
//        if (config('image.webp_enabled') && config('image.jpeg_enabled')) {
//            $jpgFilename = str_replace('.webp', '.jpg', $filename);
//            foreach ($filters as $size => $filter) {
//                /**
//                 * @var $oObject FilterInterface
//                 */
//                $oObject = new $filter['filter']($original, $filter['options']);
//                $oObject->resize($path, $size, $jpgFilename);
//            }
//        }
        return $filename;
    }

    /**
     * Загрузить оригинальное изображение
     * @param object $file
     * @param string $path
     * @return string
     */
    private function uploadOriginalFile($file, $path): string
    {
        $sFileName = $file->getClientOriginalName();
        $extension = $this->getExtension($sFileName);
        $sFileName = Str::random(12) . '' . $extension;
        $file instanceof \Illuminate\Http\UploadedFile ? $this->moveUploadedFile($file, $path, $sFileName) : $file->move($path, $sFileName);
        // webp
        $image = ImageStatic::make($path . $sFileName);
        $webpFileName = str_replace($extension, '.webp', $sFileName);
        $image->save($path . $webpFileName);
        // jpg
        $image = ImageStatic::make($path . $sFileName);
        $jpgFileName = str_replace('.webp', '.jpg', $sFileName);
        $image->save($path . $jpgFileName);
        if (config('image.webp_enabled')) {
            return $webpFileName;
        }
        if (config('image.jpeg_enabled')) {
            return $jpgFileName;
        }
        return $sFileName;
    }

    /**
     * @param string $fileName
     * @return false|string
     */
    private function getExtension($fileName)
    {
        return substr(strrchr($fileName, '.'), 0);
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $path
     * @param string $sFileName
     */
    private function moveUploadedFile(\Illuminate\Http\UploadedFile $file, $path, $sFileName): void
    {
        File::copy($file->getPathName(), $path . $sFileName);

        if (config('image.cloud')) {
            $pathCloud = str_replace(public_path('storage'), '', $path);
            $fileCloud = Storage::disk('yandex')->put($pathCloud . $sFileName, File::get($file));
            if (!$fileCloud) {
                (new SlackYandexCloudNotification())->error('Error Upload: ' . $pathCloud . $sFileName);
            }
        }
    }

    /**
     * @param string $key
     * @param int $id
     * @param string|null $type
     * @return string
     */
    private function getPath(string $key, int $id, ?string $type = null): string
    {
        return is_null($type)
            ? $this->path() . '/' . $key . '/' . $id
            : $this->path() . '/' . $key . '/' . $id . '/' . $type;
    }

    /**
     * @return string
     */
    protected function path(): string
    {
        return $this->imagePublicDirectory;
        //return $this->isTmp ? $this->imageStorageTmpDirectory : $this->imagePublicDirectory;
    }

    /**
     * @param string $originalDir
     */
    private function checkDirectory(string $originalDir): void
    {
        if (!File::exists($originalDir)) {
            File::makeDirectory($originalDir, 0777, true);
        }
    }


    /**
     * Удалить изображение со всех папок
     * @param string $filename
     * @param string $key
     * @param int $id
     * @param array $filters
     * @param string|null $type
     */
    public function deleteImages($filename, string $key, int $id, array $filters = [], ?string $type = null)
    {
        $path = is_null($type)
            ? $this->getPath($key, $id)
            : $this->getPath($key, $id, $type);

        $aSizes[] = $this->originalKey;

        foreach ($filters as $k => $option) {
            $aSizes[] = $k;
        }
        foreach ($aSizes as $size) {
            $dir = public_path($path . '/' . $size);
            $file = public_path($path . '/' . $size . '/' . $filename);
            if (File::exists($file)) {
                File::Delete($file);
                if (config('image.webp_enabled') && config('image.jpeg_enabled')) {
                    $jpgFile = str_replace('.webp', '.jpg', $file);
                    if (File::exists($jpgFile)) {
                        File::Delete($jpgFile);
                    }
                }
                if (config('image.cloud')) {
                    $pathCloud = str_replace(public_path('storage'), '', $dir);
                    $fileCloud = $pathCloud . '/' . $filename;
                    if (Storage::disk('yandex')->exists($fileCloud)) {
                        $result = Storage::disk('yandex')->delete($fileCloud);
                        if (!$result) {
                            (new SlackYandexCloudNotification())->error('Error Delete: ' . $fileCloud);
                        }
                    } else {
                        (new SlackYandexCloudNotification())->error('Error Delete: ' . $fileCloud);
                    }
                    //$this->deleteDirectory($dir);
                }
            }
        }
        $dir = public_path($path);
        if (File::exists($dir)) {
            $this->deleteDirectory($dir);
        }
    }

    /**
     * Удалить директорию, если в ней нет файлов
     *
     * @param string $dir
     */
    private function deleteDirectory(string $dir): void
    {
        if (count(File::allFiles($dir)) === 0) {
            File::deleteDirectory($dir);
        }
    }

    /**
     * @param Image $oImage
     * @param string $key
     * @param int $id
     * @param array $filters
     */
    public function resizeByOptions(Image $oImage, string $key, int $id, array $filters = []): void
    {
        $path = is_null($oImage->type)
            ? $this->getPath($key, $id)
            : $this->getPath($key, $id, $oImage->type);

        $originalDir = public_path($path . '/' . $this->originalKey . '/');

        $filename = $oImage->filename;
        $original = $originalDir . $filename;

        foreach ($filters as $size => $filter) {
            /**
             * @var $oObject FilterInterface
             */
            $oObject = new $filter['filter']($original, $filter['options']);
            $oObject->resize($path, $size, $filename);
        }
//        if (config('image.webp_enabled') && config('image.jpeg_enabled')) {
//            $jpgFilename = str_replace('.webp', '.jpg', $filename);
//            foreach ($filters as $size => $filter) {
//                /**
//                 * @var $oObject FilterInterface
//                 */
//                $oObject = new $filter['filter']($original, $filter['options']);
//                $oObject->resize($path, $size, $jpgFilename);
//            }
//        }
    }

    /**
     * @param string $key
     * @param int $id
     * @return string
     */
    public function getDirectory(string $key, int $id): string
    {
        return public_path($this->path() . '/' . $key . '/' . $id);
    }
}
