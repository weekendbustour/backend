<?php

declare(strict_types=1);

namespace App\Services\Image;

class ImageType
{
    const MODEL = 'model';
    const GALLERY = 'gallery';
    const BANNER = 'banner';
    const TEXT = 'text';
    const TMP = 'tmp';
}
