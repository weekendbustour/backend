<?php

declare(strict_types=1);

namespace App\Services\Image\Path;

use App\Models\Image;
use App\Services\Image\ImageType;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ImagePathGalleryService extends ImagePathService
{
    /**
     * @param string $key
     * @param string $size
     * @param object|null $model
     * @return array
     */
    public function getImages(string $key, string $size, ?object $model = null): array
    {
        $oImages = $model->galleryImages;
        $aImages = [];
        foreach ($oImages as $oImage) {
            $aImages[] = $this->image($key, $size, $oImage, ImageType::GALLERY);
        }
        return $aImages;
    }

    /**
     * @param string $key
     * @param string $size
     * @param Image|null $model
     * @return string
     */
    public function getImage(string $key, string $size, Image $model = null): string
    {
        return $this->image($key, $size, $model, ImageType::GALLERY);
    }

    /**
     * @param string $key
     * @param string $size
     * @param object|null $model
     * @return bool
     */
    public function checkMainImage(string $key, string $size, ?object $model = null): bool
    {
        return $this->checkMain($key, $size, $model, ImageType::GALLERY);
    }

    /**
     * @param Image $oImage
     * @param string $key
     * @param string $size
     * @param object|null $model
     * @return bool
     */
    public function checkOriginalImage(Image $oImage, string $key, string $size, ?object $model = null): bool
    {
        return $this->checkOriginal($oImage, $key, $size, $model, ImageType::GALLERY);
    }
}
