<?php

declare(strict_types=1);

namespace App\Services\Image;

class ImageSize
{
    const ORIGINAL = 'original';

    const SQUARE = 'square';

    const XL = 'xl';
    const LG = 'lg';
    const MD = 'md';
    const SM = 'sm';
    const XS = 'xs';

    const CARD_DEFAULT = 'card-default';
    const CARD_MD = 'card-md';

    const BANNER_DEFAULT = 'banner-default';
    const BANNER_XL = 'xl';
}
