<?php

declare(strict_types=1);

namespace App\Services\Vk;

use VK\Client\VKApiClient;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;

class VkApi
{
    public function __constructor()
    {
    }

    public function run()
    {
        $vk = new VKApiClient();

        $oauth = new VKOAuth();
        $client_id = config('services.vkontakte.client_id');
        $redirect_uri = env('VKONTAKTE_REDIRECT_URI_REDIRECT');
        $display = VKOAuthDisplay::PAGE;
        $scope = [VKOAuthUserScope::WALL, VKOAuthUserScope::GROUPS];
        $state = config('services.vkontakte.client_secret');

        $browser_url = $oauth->getAuthorizeUrl(VKOAuthResponseType::CODE, $client_id, $redirect_uri, $display, $scope, $state);
        dd($browser_url);
    }
}
