<?php

declare(strict_types=1);

namespace App\Services\Vk;

use App\Models\ExternalVkParameter;
use App\Models\Tour;

class VkWall extends VkBase
{
    /**
     * @var array
     */
    private $attachments = [];

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var null|Tour
     */
    private $oItem = null;

    /**
     * @var ExternalVkParameter|null
     */
    private $oVkParameter = null;

    public function __construct($oItem = null)
    {
        parent::__construct();
        $this->oItem = $oItem;
        $this->accessToken = config('services.vkontakte.wall_access_token');
        if (!is_null($this->oItem)) {
            $this->oVkParameter = $this->oItem->vkPost;
            if (is_null($this->oVkParameter)) {
                $this->oVkParameter = $this->oItem->externalVkParameters()->create([
                    'type' => ExternalVkParameter::TYPE_POST,
                ]);
            }
        }
    }

    /**
     * @param string $image
     * @return $this
     * @throws \VK\Exceptions\Api\VKApiParamAlbumIdException
     * @throws \VK\Exceptions\Api\VKApiParamHashException
     * @throws \VK\Exceptions\Api\VKApiParamServerException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function attachPhoto(string $image)
    {
        $server = $this->vkApiClient->photos()->getWallUploadServer($this->accessToken, [
            'group_id' => $this->group_id,
        ]);
        if (isset($server['upload_url'])) {
            // Отправка изображения на сервер.
            if (function_exists('curl_file_create')) {
                $curl_file = curl_file_create($image);
            } else {
                $curl_file = '@' . $image;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $server['upload_url']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['photo' => $curl_file]);
            $upload = curl_exec($ch);
            curl_close($ch);

            $upload = json_decode($upload);
            if (!empty($upload->server)) {
                $save = $this->vkApiClient->photos()->saveWallPhoto($this->accessToken, [
                    'group_id' => $this->group_id,
                    'server' => $upload->server,
                    'photo' => stripslashes($upload->photo),
                    'hash' => $upload->hash,
                ]);
                $response = $save[0];
                $this->attachments[] = 'photo' . $response['owner_id'] . '_' . $response['id'];
                $this->oVkParameter->update([
                    'photo_id' => $response['id'],
                    'images' => $upload->photo,
                ]);
            }
        }
        return $this;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function attachUrl(string $url)
    {
        $this->attachments[] = $url;
        return $this;
    }

    /**
     * @param string $message
     * @return $this
     */
    public function post(string $message)
    {
        $this->params = [
            'owner_id' => -$this->group_id,
            'from_group' => 1,
            'message' => $message,
        ];
        return $this;
    }


    /**
     * (new VkWall())->post('Сообщение')->send();
     * (new VkWall())->post('Сообщение')->attachUrl('http')->send();
     * (new VkWall())->post('Сообщение')->attachPhoto(public_path('/'))->send();
     */
    /**
     * @throws \VK\Exceptions\Api\VKApiWallAddPostException
     * @throws \VK\Exceptions\Api\VKApiWallAdsPostLimitReachedException
     * @throws \VK\Exceptions\Api\VKApiWallAdsPublishedException
     * @throws \VK\Exceptions\Api\VKApiWallLinksForbiddenException
     * @throws \VK\Exceptions\Api\VKApiWallTooManyRecipientsException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function send()
    {
        if (!empty($this->attachments)) {
            $this->params['attachments'] = implode(',', $this->attachments);
        }
        $response = $this->vkApiClient->wall()->post($this->accessToken, $this->params);
        $this->oVkParameter->update([
            'item_id' => $response['post_id'],
            'published_at' => now(),
        ]);
    }

    /**
     * @return bool
     * @throws \VK\Exceptions\Api\VKApiWallAccessPostException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function delete()
    {
        if (!is_null($this->oVkParameter) && !is_null($this->oVkParameter->item_id)) {
            $response = $this->vkApiClient->wall()->delete($this->accessToken, [
                'owner_id' => -$this->group_id,
                'post_id' => $this->oVkParameter->item_id,
            ]);
            return $response === 1;
        }
        return false;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function attachMarket(int $id)
    {
        $this->attachments[] = 'market' . - $this->group_id . '_' . $id;
        return $this;
    }
}
