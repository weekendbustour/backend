<?php

declare(strict_types=1);

namespace App\Services\Vk;

use VK\Client\VKApiClient;

class VkBase
{
    /**
     * @var null|VKApiClient
     */
    public $vkApiClient = null;

    public $accessToken = null;

    public $group_id = null;

    public function __construct()
    {
        $this->vkApiClient = new VKApiClient('5.110');
        $this->group_id = config('services.vkontakte.group_id');
    }
}
