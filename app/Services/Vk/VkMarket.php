<?php

declare(strict_types=1);

namespace App\Services\Vk;

use App\Models\ExternalVkParameter;
use App\Models\Tour;

class VkMarket extends VkBase
{
    /**
     *
     * @var array
     */
    private $attachments = [];

    /**
     * @var array
     */
    private $params = [];

    /**
     * @var null|Tour
     */
    private $oItem = null;

    /**
     * @var ExternalVkParameter|null
     */
    private $oVkParameter = null;

    public function __construct($oItem = null)
    {
        parent::__construct();
        $this->oItem = $oItem;
        $this->accessToken = config('services.vkontakte.market_access_token');
        if (!is_null($this->oItem)) {
            $this->oVkParameter = $this->oItem->vkMarket;
            if (is_null($this->oVkParameter)) {
                $this->oVkParameter = $this->oItem->externalVkParameters()->create([
                    'type' => ExternalVkParameter::TYPE_MARKET,
                ]);
            }
        }
    }

    /**
     * Первое фото - главное
     *
     * @param string $image
     * @return $this
     * @throws \VK\Exceptions\Api\VKApiParamAlbumIdException
     * @throws \VK\Exceptions\Api\VKApiParamHashException
     * @throws \VK\Exceptions\Api\VKApiParamServerException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function attachPhoto(string $image)
    {
        $isMain = !isset($this->attachments['main_photo']) ? 1 : 0;
        $server = $this->vkApiClient->photos()->getMarketUploadServer($this->accessToken, [
            'group_id' => $this->group_id,
            'main_photo' => $isMain,
        ]);
        if (isset($server['upload_url'])) {
            // Отправка изображения на сервер.
            if (function_exists('curl_file_create')) {
                $curl_file = curl_file_create($image);
            } else {
                $curl_file = '@' . $image;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $server['upload_url']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['photo' => $curl_file]);
            $upload = curl_exec($ch);
            curl_close($ch);

            $upload = json_decode($upload);
            if (!empty($upload->server)) {
                $save = $this->vkApiClient->photos()->saveMarketPhoto($this->accessToken, [
                    'group_id' => $this->group_id,
                    'photo' => stripslashes($upload->photo),
                    'server' => $upload->server,
                    'hash' => $upload->hash,
                    'crop_hash' => $upload->crop_hash,
                    'crop_data' => $upload->crop_data,
                ]);
                $response = $save[0];
                if ($isMain) {
                    $this->attachments['main_photo'] = $response['id'];
                    $this->oVkParameter->update([
                        'photo_id' => $response['id'],
                        'images' => $upload->photo,
                    ]);
                } else {
                    $this->attachments['photo'][] = $response['id'];
                }
            }
        }
        return $this;
    }

    /**
     * @param string $image
     * @return mixed
     * @throws \VK\Exceptions\Api\VKApiParamAlbumIdException
     * @throws \VK\Exceptions\Api\VKApiParamHashException
     * @throws \VK\Exceptions\Api\VKApiParamServerException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    private function attachPhotoAlbum(string $image)
    {
        $server = $this->vkApiClient->photos()->getMarketAlbumUploadServer($this->accessToken, [
            'group_id' => $this->group_id,
        ]);
        if (isset($server['upload_url'])) {
            // Отправка изображения на сервер.
            if (function_exists('curl_file_create')) {
                $curl_file = curl_file_create($image);
            } else {
                $curl_file = '@' . $image;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $server['upload_url']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['photo' => $curl_file]);
            $upload = curl_exec($ch);
            curl_close($ch);

            $upload = json_decode($upload);
            if (!empty($upload->server)) {
                $save = $this->vkApiClient->photos()->saveMarketAlbumPhoto($this->accessToken, [
                    'group_id' => $this->group_id,
                    'photo' => stripslashes($upload->photo),
                    'server' => $upload->server,
                    'hash' => $upload->hash,
                ]);
                $response = $save[0];
                return $response['id'];
            }
        }
        return null;
    }

    /**
     * @param string $url
     * @return $this
     */
    public function attachUrl(string $url)
    {
        $this->attachments['url'] = $url;
        return $this;
    }

    /**
     * @param string $name
     * @param string $description
     * @param int $category_id
     * @param int|string $price
     * @return $this
     */
    public function product(string $name, string $description, int $category_id, $price)
    {
        $this->params = [
            'owner_id' => -$this->group_id,
            'name' => $name,
            'description' => $description,
            'category_id' => $category_id,
            'price' => $price,
        ];
        $this->oVkParameter->update([
            'category_id' => $category_id,
        ]);
        return $this;
    }

    /**
     * @param int|string $price
     * @return $this
     */
    public function attachDiscount($price)
    {
        $this->params['old_price'] = $this->params['price'];
        $this->params['price'] = $price;
        return $this;
    }


    /**
     * (new VkMarket())->product('Название товара', 'Описание товара', 3, 120)->send();
     * (new VkMarket())->product('Название товара', 'Описание товара', 3, 120)->attachUrl('http')->send();
     * (new VkMarket())->product('Название товара', 'Описание товара', 3, 120)->attachPhoto(public_path('/'))->send();
     */
    /**
     * @throws \VK\Exceptions\Api\VKApiWallAddPostException
     * @throws \VK\Exceptions\Api\VKApiWallAdsPostLimitReachedException
     * @throws \VK\Exceptions\Api\VKApiWallAdsPublishedException
     * @throws \VK\Exceptions\Api\VKApiWallLinksForbiddenException
     * @throws \VK\Exceptions\Api\VKApiWallTooManyRecipientsException
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function send()
    {
        $albums = [];
        if (isset($this->attachments['album'])) {
            $albums = $this->checkAlbums($this->attachments['album']);
        }
        if (isset($this->attachments['main_photo'])) {
            $this->params['main_photo_id'] = $this->attachments['main_photo'];
        }
        if (isset($this->attachments['url'])) {
            $this->params['url'] = $this->attachments['url'];
        }
        if (isset($this->attachments['photo']) && !empty($this->attachments['photo'])) {
            $this->params['photo_ids'] = $this->attachments['photo'];
        }

        $response = $this->vkApiClient->market()->add($this->accessToken, $this->params);
        $this->oVkParameter->update([
            'item_id' => $response['market_item_id'],
            'published_at' => now(),
        ]);

        if (isset($this->attachments['album'])) {
            $this->toAlbums($response['market_item_id'], $albums);
        }
    }

    public function attachAlbum(string $title, ?string $image = null)
    {
        $this->attachments['album'][] = [
            'title' => $title,
            'image' => $image ?? null,
        ];
        return $this;
    }

    public function checkAlbums(array $data)
    {
        $albums = [];
        foreach ($data as $item) {
            $response = $this->vkApiClient->market()->getAlbums($this->accessToken, [
                'owner_id' => -$this->group_id,
            ]);
            $isset = false;
            if ($response['count'] !== 0) {
                foreach ($response['items'] as $album) {
                    if ($album['title'] === $item['title']) {
                        $albums[] = $album['id'];
                        $isset = true;
                        break;
                    }
                }
            }
            if (!$isset) {
                $paramsAlbum = [
                    'owner_id' => -$this->group_id,
                    'title' => $item['title'],
                ];
                if (!is_null($item['image'])) {
                    $id = $this->attachPhotoAlbum($item['image']);
                    if (!is_null($id)) {
                        $paramsAlbum['photo_id'] = $id;
                    }
                }
                $response = $this->vkApiClient->market()->addAlbum($this->accessToken, $paramsAlbum);
                $albums[] = $response['market_album_id'];
            }
        }
        return $albums;
    }

    public function toAlbums($item_id, $albums)
    {
        $response = $this->vkApiClient->market()->addToAlbum($this->accessToken, [
            'owner_id' => -$this->group_id,
            'item_id' => $item_id,
            'album_ids' => $albums,
        ]);
    }

    /**
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function delete()
    {
        if (!is_null($this->oVkParameter) && !is_null($this->oVkParameter->item_id)) {
            $response = $this->vkApiClient->market()->delete($this->accessToken, [
                'owner_id' => -$this->group_id,
                'item_id' => $this->oVkParameter->item_id,
            ]);
            return $response === 1;
        }
        return false;
    }
}
