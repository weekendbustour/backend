<?php

declare(strict_types=1);

namespace App\Services\Seo;

use App\Http\Transformers\MetaTagTransformer;
use App\Models\Article;
use App\Models\Category;
use App\Models\Direction;
use App\Models\News;
use App\Models\Organization;
use App\Models\Tour;
use App\Services\Environment;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Traits\SEOTools;
use Butschster\Head\Facades\Meta;
use Butschster\Head\MetaTags\Entities\Tag;

class SeoService
{
    use SEOTools;

    /**
     * SeoService constructor.
     */
    public function __construct()
    {
        $this->common();
    }

    /**
     *
     */
    public function common()
    {
        SEOMeta::addMeta('image', asset('favicon152x152.png'));
        SEOMeta::addMeta('api', config('api.url'));
        if (checkEnv(Environment::PRODUCTION) && config('services.yandex_webmaster.enabled')) {
            SEOMeta::addMeta('yandex-verification', config('services.yandex_webmaster.key'));
        }
    }

    /**
     *
     */
    public function index()
    {
        $this->seo()->setTitle(config('app.name') . ' | Туры выходного дня', false);
        $this->seo()->setDescription('Платформа поиска туров выходного дня');
    }

    /**
     * @param Tour $oItem
     */
    public function tour(Tour $oItem)
    {
        $oMetaTag = $oItem->metaTag;
        if (!is_null($oMetaTag)) {
            $meta = (new MetaTagTransformer())->transform($oMetaTag);
        } else {
            $meta = [
                'title' => $oItem->title,
                'description' => $oItem->preview_description,
                'keywords' => null,
            ];
        }

        $image = $oItem->image_card;
        $title = $meta['title'];
        $description = $meta['description'];

        SEOMeta::addMeta('image', $image);

        $this->seo()->setTitle($title);
        $this->seo()->setDescription($description);
        $this->seo()->addImages([
            $image,
        ]);

        $this->seo()->opengraph()->setTitle($title);
        $this->seo()->opengraph()->setDescription($description);
        $this->seo()->opengraph()->addProperty('url', route('app.tour', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'ru-RU');
        $this->seo()->opengraph()->addProperty('image', $image);
        $this->seo()->opengraph()->addProperty('site_name', config('app.name'));

        $this->seo()->twitter()->setTitle($title);
        $this->seo()->twitter()->setDescription($description);
        $this->seo()->twitter()->setUrl(route('app.tour', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->twitter()->setImage($image);
        $this->seo()->twitter()->setSite('@weekendbustour');
        $this->seo()->twitter()->addValue('card', 'summary');
        $this->seo()->twitter()->addValue('domain', 'weekendbustour.ru');

        SEOMeta::addMeta('vk:image', $image, 'property');
    }

    /**
     * @param Direction $oItem
     */
    public function direction(Direction $oItem)
    {
        $oMetaTag = $oItem->metaTag;
        if (!is_null($oMetaTag)) {
            $meta = (new MetaTagTransformer())->transform($oMetaTag);
        } else {
            $meta = [
                'title' => $oItem->title,
                'description' => $oItem->preview_description,
                'keywords' => null,
            ];
        }
        $title = $meta['title'];
        $description = $meta['description'];
        $image = $oItem->image_default;
        $url = $oItem->getUrlShort();

        SEOMeta::addMeta('image', $image);
        SEOMeta::setCanonical($url);

        $this->seo()->setTitle($title);
        $this->seo()->setDescription($description);
        $this->seo()->addImages([
            $image,
        ]);

        $this->seo()->opengraph()->setTitle($title);
        $this->seo()->opengraph()->setDescription($description);
        $this->seo()->opengraph()->addProperty('url', $url);
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'ru-RU');
        $this->seo()->opengraph()->addProperty('image', $image);
        $this->seo()->opengraph()->addProperty('site_name', config('app.name'));

        $this->seo()->twitter()->setTitle($title);
        $this->seo()->twitter()->setDescription($description);
        $this->seo()->twitter()->setUrl($url);
        $this->seo()->twitter()->setImage($image);
        $this->seo()->twitter()->setSite('@weekendbustour');
        $this->seo()->twitter()->addValue('card', 'summary');
        $this->seo()->twitter()->addValue('domain', 'weekendbustour.ru');

        SEOMeta::addMeta('vk:image', $image, 'property');
    }

    /**
     * @param Direction $oItem
     */
    public function directionAttractions(Direction $oItem)
    {
        $oMetaTag = $oItem->metaTag;
        if (!is_null($oMetaTag)) {
            $meta = (new MetaTagTransformer())->transform($oMetaTag);
        } else {
            $meta = [
                'title' => $oItem->title,
                'description' => $oItem->preview_description,
                'keywords' => null,
            ];
        }
        $title = 'Достопримечательности по направлению ' . $oItem->title;
        $description = $meta['description'];

        SEOMeta::addMeta('image', $oItem->image_default);

        $this->seo()->setTitle($title);
        $this->seo()->setDescription($description);
        $this->seo()->addImages([
            $oItem->image_default,
        ]);

        $this->seo()->opengraph()->setTitle($title);
        $this->seo()->opengraph()->setDescription($description);
        $this->seo()->opengraph()->addProperty('url', route('app.direction.attractions', ['id' => $oItem->id]));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'ru-RU');
        $this->seo()->opengraph()->addProperty('image', $oItem->image_default);
        $this->seo()->opengraph()->addProperty('site_name', config('app.name'));

        $this->seo()->twitter()->setTitle($title);
        $this->seo()->twitter()->setDescription($description);
        $this->seo()->twitter()->setUrl(route('app.direction.attractions', ['id' => $oItem->id]));
        $this->seo()->twitter()->setImage($oItem->image_default);
        $this->seo()->twitter()->setSite('@weekendbustour');
        $this->seo()->twitter()->addValue('card', 'summary');
        $this->seo()->twitter()->addValue('domain', 'weekendbustour.ru');
    }

    /**
     * @param Organization $oItem
     */
    public function organization(Organization $oItem)
    {
        $oMetaTag = $oItem->metaTag;
        if (!is_null($oMetaTag)) {
            $meta = (new MetaTagTransformer())->transform($oMetaTag);
        } else {
            $meta = [
                'title' => $oItem->title,
                'description' => $oItem->preview_description,
                'keywords' => null,
            ];
        }
        $title = $meta['title'];
        $description = $meta['description'];
        $image = $oItem->image_default;
        $url = $oItem->getUrlShort();

        SEOMeta::addMeta('image', $image);
        SEOMeta::setCanonical($url);

        $this->seo()->setTitle($title);
        $this->seo()->setDescription($description);
        $this->seo()->addImages([
            $image,
        ]);

        $this->seo()->opengraph()->setTitle($title);
        $this->seo()->opengraph()->setDescription($description);
        $this->seo()->opengraph()->addProperty('url', $url);
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'ru-RU');
        $this->seo()->opengraph()->addProperty('image', $image);
        $this->seo()->opengraph()->addProperty('site_name', config('app.name'));

        $this->seo()->twitter()->setTitle($title);
        $this->seo()->twitter()->setDescription($description);
        $this->seo()->twitter()->setUrl($url);
        $this->seo()->twitter()->setImage($image);
        $this->seo()->twitter()->setSite('@weekendbustour');
        $this->seo()->twitter()->addValue('card', 'summary');
        $this->seo()->twitter()->addValue('domain', 'weekendbustour.ru');

        SEOMeta::addMeta('vk:image', $image, 'property');
    }

    /**
     * @param Organization $oItem
     */
    public function organizationArticles(Organization $oItem)
    {
        $oMetaTag = $oItem->metaTag;
        if (!is_null($oMetaTag)) {
            $meta = (new MetaTagTransformer())->transform($oMetaTag);
        } else {
            $meta = [
                'title' => $oItem->title,
                'description' => $oItem->preview_description,
                'keywords' => null,
            ];
        }
        $title = 'Статьи написанные организатором ' . $meta['title'];
        $description = $meta['description'];

        SEOMeta::addMeta('image', $oItem->image_default);

        $this->seo()->setTitle($title);
        $this->seo()->setDescription($description);
        $this->seo()->addImages([
            $oItem->image_default,
        ]);

        $this->seo()->opengraph()->setTitle($title);
        $this->seo()->opengraph()->setDescription($description);
        $this->seo()->opengraph()->addProperty('url', route('app.organization.articles', ['id' => $oItem->id]));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'ru-RU');
        $this->seo()->opengraph()->addProperty('image', $oItem->image_default);
        $this->seo()->opengraph()->addProperty('site_name', config('app.name'));

        $this->seo()->twitter()->setTitle($title);
        $this->seo()->twitter()->setDescription($description);
        $this->seo()->twitter()->setUrl(route('app.organization.articles', ['id' => $oItem->id]));
        $this->seo()->twitter()->setImage($oItem->image_default);
        $this->seo()->twitter()->setSite('@weekendbustour');
        $this->seo()->twitter()->addValue('card', 'summary');
        $this->seo()->twitter()->addValue('domain', 'weekendbustour.ru');
    }

    /**
     * @param News $oItem
     */
    public function newsPage(News $oItem)
    {
        SEOMeta::addMeta('image', $oItem->image_default);

        $this->seo()->setTitle($oItem->title);
        $this->seo()->setDescription($oItem->preview_description);
        $this->seo()->addImages([
            $oItem->image_default,
        ]);

        $this->seo()->opengraph()->setTitle($oItem->title);
        $this->seo()->opengraph()->setDescription($oItem->preview_description);
        $this->seo()->opengraph()->addProperty('url', route('app.organization', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'ru-RU');
        $this->seo()->opengraph()->addProperty('image', $oItem->image_default);
        $this->seo()->opengraph()->addProperty('site_name', config('app.name'));

        $this->seo()->twitter()->setTitle($oItem->title);
        $this->seo()->twitter()->setDescription($oItem->preview_description);
        $this->seo()->twitter()->setUrl(route('app.organization', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->twitter()->setImage($oItem->image_default);
        $this->seo()->twitter()->setSite('@weekendbustour');
        $this->seo()->twitter()->addValue('card', 'summary');
        $this->seo()->twitter()->addValue('domain', 'weekendbustour.ru');
    }

    /**
     * @param Article $oItem
     */
    public function article(Article $oItem)
    {
        SEOMeta::addMeta('image', $oItem->image_default);

        $this->seo()->setTitle($oItem->title);
        $this->seo()->setDescription($oItem->preview_description);
        $this->seo()->addImages([
            $oItem->image_default,
        ]);

        $this->seo()->opengraph()->setTitle($oItem->title);
        $this->seo()->opengraph()->setDescription($oItem->preview_description);
        $this->seo()->opengraph()->addProperty('url', route('app.article', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'ru-RU');
        $this->seo()->opengraph()->addProperty('image', $oItem->image_default);
        $this->seo()->opengraph()->addProperty('site_name', config('app.name'));

        $this->seo()->twitter()->setTitle($oItem->title);
        $this->seo()->twitter()->setDescription($oItem->preview_description);
        $this->seo()->twitter()->setUrl(route('app.article', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->twitter()->setImage($oItem->image_default);
        $this->seo()->twitter()->setSite('@weekendbustour');
        $this->seo()->twitter()->addValue('card', 'summary');
        $this->seo()->twitter()->addValue('domain', 'weekendbustour.ru');
    }

    /**
     * @param Category $oItem
     */
    public function category(Category $oItem)
    {
        SEOMeta::addMeta('image', $oItem->image_default);

        $this->seo()->setTitle($oItem->title);
        $this->seo()->setDescription($oItem->description);
        $this->seo()->addImages([
            $oItem->image_default,
        ]);

        $this->seo()->opengraph()->setTitle($oItem->title);
        $this->seo()->opengraph()->setDescription($oItem->description);
        $this->seo()->opengraph()->addProperty('url', route('app.category', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'ru-RU');
        $this->seo()->opengraph()->addProperty('image', $oItem->image_default);
        $this->seo()->opengraph()->addProperty('site_name', config('app.name'));

        $this->seo()->twitter()->setTitle($oItem->title);
        $this->seo()->twitter()->setDescription($oItem->description);
        $this->seo()->twitter()->setUrl(route('app.article', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->twitter()->setImage($oItem->image_default);
        $this->seo()->twitter()->setSite('@weekendbustour');
        $this->seo()->twitter()->addValue('card', 'summary');
        $this->seo()->twitter()->addValue('domain', 'weekendbustour.ru');
    }

    /**
     * @param News $oItem
     */
    public function newsItem(News $oItem)
    {
        SEOMeta::addMeta('image', $oItem->image_default);

        $this->seo()->setTitle($oItem->title);
        $this->seo()->setDescription($oItem->preview_description);
        $this->seo()->addImages([
            $oItem->image_default,
        ]);

        $this->seo()->opengraph()->setTitle($oItem->title);
        $this->seo()->opengraph()->setDescription($oItem->preview_description);
        $this->seo()->opengraph()->addProperty('url', route('app.news.item', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->opengraph()->addProperty('type', 'article');
        $this->seo()->opengraph()->addProperty('locale', 'ru-RU');
        $this->seo()->opengraph()->addProperty('image', $oItem->image_default);
        $this->seo()->opengraph()->addProperty('site_name', config('app.name'));

        $this->seo()->twitter()->setTitle($oItem->title);
        $this->seo()->twitter()->setDescription($oItem->preview_description);
        $this->seo()->twitter()->setUrl(route('app.news.item', ['id' => $oItem->id, 'name' => $oItem->name]));
        $this->seo()->twitter()->setImage($oItem->image_default);
        $this->seo()->twitter()->setSite('@weekendbustour');
        $this->seo()->twitter()->addValue('card', 'summary');
        $this->seo()->twitter()->addValue('domain', 'weekendbustour.ru');
    }

    /**
     *
     */
    public function news(): void
    {
        $this->seo()->setTitle('Новости');
        $this->seo()->setDescription('');
    }

    /**
     *
     */
    public function help(): void
    {
        $this->seo()->setTitle('Помощь');
    }

    /**
     *
     */
    public function terms(): void
    {
        $this->seo()->setTitle('Условия использования');
    }

    /**
     *
     */
    public function privacy(): void
    {
        $this->seo()->setTitle('Положение о конфиденциальностия');
    }

    /**
     *
     */
    public function about(): void
    {
        $this->seo()->setTitle('О нас');
    }

    /**
     *
     */
    public function contacts(): void
    {
        $this->seo()->setTitle('Контакты');
    }

    /**
     *
     */
    public function search(): void
    {
        $this->seo()->setTitle('Поиск');
    }

    /**
     *
     */
    public function tours(): void
    {
        $this->seo()->setTitle('Туры');
    }

    /**
     *
     */
    public function organizations(): void
    {
        $this->seo()->setTitle('Организаторы');
    }

    /**
     *
     */
    public function directions(): void
    {
        $this->seo()->setTitle('Направления');
    }

    /**
     *
     */
    public function articles(): void
    {
        $this->seo()->setTitle('Статьи');
    }
}
