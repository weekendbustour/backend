<?php

declare(strict_types=1);

namespace App\Services\Seo;

use App\Http\Transformers\MetaTagTransformer;
use App\Models\Article;
use App\Models\Attraction;
use App\Models\Category;
use App\Models\Direction;
use App\Models\News;
use App\Models\Organization;
use App\Models\Tour;
use App\Services\Environment;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Traits\SEOTools;
use Butschster\Head\Facades\Meta;
use Butschster\Head\MetaTags\Entities\Tag;
use Illuminate\Http\Request;
use Irazasyed\LaravelGAMP\Facades\GAMP;

class SeoGoogleAnalyticsApiService
{
    /**
     * @var null|GAMP
     */
    private $ga = null;
    private $enabled = false;
    private $path = null;

    /**
     * SeoService constructor.
     */
    public function __construct()
    {
        $this->enabled = config('services.google_analytics.enabled');
        if ($this->enabled) {
            $this->ga = GAMP::setClientId(config('gamp.tracking_id'));
        }
    }

    /**
     *
     */
    public function run()
    {
        if (!$this->enabled) {
            return;
        }
        if (!is_null($this->path)) {
            $this->ga->setDocumentPath($this->path);
            $this->ga->setDataSource('api');
            $this->ga->sendPageview();
        }
    }

    /**
     * @param Tour $oItem
     * @return $this
     */
    public function tour(Tour $oItem)
    {
        $this->path = route('app.tour', ['id' => $oItem->id, 'name' => $oItem->name], false);
        return $this;
    }

    /**
     * @param Direction $oItem
     * @return $this
     */
    public function direction(Direction $oItem)
    {
        $this->path = route('app.direction', ['id' => $oItem->id, 'name' => $oItem->name], false);
        return $this;
    }

    /**
     * @param Organization $oItem
     * @return $this
     */
    public function organization(Organization $oItem)
    {
        $this->path = route('app.organization', ['id' => $oItem->id, 'name' => $oItem->name], false);
        return $this;
    }

    /**
     * @param Article $oItem
     * @return $this
     */
    public function article(Article $oItem)
    {
        $this->path = route('app.article', ['id' => $oItem->id, 'name' => $oItem->name], false);
        return $this;
    }

    /**
     * @param Attraction $oItem
     * @return $this
     */
    public function attraction(Attraction $oItem)
    {
        $this->path = route('app.attraction', ['id' => $oItem->id, 'name' => $oItem->name], false);
        return $this;
    }

    /**
     * @param Category $oItem
     * @return $this
     */
    public function category(Category $oItem)
    {
        $this->path = route('app.category', ['id' => $oItem->id, 'name' => $oItem->name], false);
        return $this;
    }

    /**
     * @param News $oItem
     * @return $this
     */
    public function newsItem(News $oItem)
    {
        $this->path = route('app.news.item', ['id' => $oItem->id, 'name' => $oItem->name], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function news()
    {
        $this->path = route('app.news', [], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function help()
    {
        $this->path = route('app.help', [], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function terms()
    {
        $this->path = route('app.terms', [], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function privacy()
    {
        $this->path = route('app.privacy', [], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function about()
    {
        $this->path = route('app.about', [], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function contacts()
    {
        $this->path = route('app.contacts', [], false);
        return $this;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function search(Request $request)
    {
        $data = $request->all();
        $this->path = route('app.search', $data, false);
        return $this;
    }

    /**
     * @return $this
     */
    public function tours()
    {
        $this->path = route('app.tours', [], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function organizations()
    {
        $this->path = route('app.organizations', [], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function directions()
    {
        $this->path = route('app.directions', [], false);
        return $this;
    }

    /**
     * @return $this
     */
    public function articles()
    {
        $this->path = route('app.articles', [], false);
        return $this;
    }
}
