<?php

declare(strict_types=1);

namespace App\Services\Seo;

use App\Jobs\Mail\Auth\SendMailAuthRegisteredJob;
use App\Jobs\QueueCommon;
use App\Jobs\UpdateSitemapJob;
use App\Models\Article;
use App\Models\Direction;
use App\Models\News;
use App\Models\Organization;
use App\Models\Tour;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class SeoSitemapService
{
    /**
     * @return Sitemap
     */
    public function getSitemap()
    {
        $oSitemap = Sitemap::create();

        $oTours = Tour::active()->ordered()->get();

        $oSitemap->add(Url::create('/')->setLastModificationDate(now()));

        foreach ($oTours as $oTour) {
            /** @var Tour $oTour */
            if (!is_null($oTour->announcement)) {
                continue;
            }
            $oSitemap->add(Url::create($oTour->getUrl())->setLastModificationDate(now()));
        }

        $oArticles = Article::active()->ordered()->get();
        foreach ($oArticles as $oArticle) {
            /** @var Article $oArticle */
            $oSitemap->add(Url::create($oArticle->getUrl())->setLastModificationDate(now()));
        }

        $oNews = News::active()->ordered()->get();
        foreach ($oNews as $oNewItem) {
            /** @var News $oNewItem */
            $oSitemap->add(Url::create($oNewItem->getUrl())->setLastModificationDate(now()));
        }

        $oOrganizations = Organization::active()->ordered()->get();
        foreach ($oOrganizations as $oOrganization) {
            /** @var Organization $oOrganization */
            $oSitemap->add(Url::create($oOrganization->getUrl())->setLastModificationDate(now()));
            $oSitemap->add(Url::create($oOrganization->getUrlShort())->setLastModificationDate(now()));
        }

        $oDirections = Direction::active()->ordered()->get();
        foreach ($oDirections as $oDirection) {
            /** @var Direction $oDirection */
            $oSitemap->add(Url::create($oDirection->getUrl())->setLastModificationDate(now()));
            $oSitemap->add(Url::create($oDirection->getUrlShort())->setLastModificationDate(now()));
        }

        return $oSitemap;
    }

    /**
     * @return void
     */
    public function dispatch()
    {
        if (QueueCommon::commandSeoIsEnabled()) {
            UpdateSitemapJob::dispatch()->onQueue(QueueCommon::QUEUE_NAME_SEO);
        } else {
            UpdateSitemapJob::dispatchSync();
        }
    }
}
