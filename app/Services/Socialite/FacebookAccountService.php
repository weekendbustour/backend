<?php

declare(strict_types=1);

namespace App\Services\Socialite;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Project\User\UserController;
use App\Models\UserSocialAccount;
use App\Models\User;
use App\Services\Image\ImageType;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Laravel\Socialite\Contracts\User as ProviderUser;

class FacebookAccountService
{
    use ImageableTrait;

    private const NAME = 'facebook';

    /**
     * @param ProviderUser $providerUser
     * @return User|null
     */
    public function createOrGetUser(ProviderUser $providerUser): ?User
    {
        $account = UserSocialAccount::whereProvider(self::NAME)
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if (!is_null($account)) {
            return $account->user;
        } else {
            try {
                $account = new UserSocialAccount([
                    'provider_user_id' => $providerUser->getId(),
                    'provider' => self::NAME,
                ]);
                $fakeLogin = self::NAME . '-' . $providerUser->getId();
                /** @var User $user */
                $user = User::where('login', $fakeLogin)->first();
                if (is_null($user)) {
                    $user = User::create([
                        'login' => $fakeLogin,
                        'email' => $providerUser->getEmail(),
                        'phone' => null,
                        'first_name' => $providerUser->getName(),
                        'password' => Hash::make(md5(rand(1, 10000))),
                    ]);
                    $this->saveImage($providerUser->getAvatar(), $user);
                }
                $account->user()->associate($user);
                $account->save();
                $user->assignRole(User::ROLE_USER);
                return $user;
            } catch (\Exception $e) {
                Log::critical($e->getMessage());
                return null;
            }
        }
    }

    /**
     * @param string $url
     * @param User $oUser
     * @throws \Exception
     */
    public function saveImage(string $url, User $oUser)
    {
        $destinationPath = public_path('/storage/images/user/tmp/');
        $imageName = Str::random(10) . '.' . 'jpg';
        $pathFile = $destinationPath . $imageName;

        File::put($pathFile, file_get_contents($url));
        $oFile = uploadFileFromPath($pathFile, $imageName);
        (new \App\Services\ImageService())->upload($oUser, [$oFile], (new UserController())->image[ImageType::MODEL], ImageType::MODEL);
        //File::delete($pathFile);
    }
}
