<?php

declare(strict_types=1);

namespace App\Services\Socialite;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Project\User\UserController;
use App\Models\UserSocialAccount;
use App\Models\User;
use App\Services\Image\ImageType;
use App\Services\Image\Upload\ImageUploadModelService;
use App\Services\ImageService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class VkontakteAccountService
{
    use ImageableTrait;

    private const NAME = 'vkontakte';

    /**
     * @param Request $request
     * @return User|null
     */
    public function createOrGetUser(Request $request): ?User
    {
        $account = UserSocialAccount::whereProvider(self::NAME)
            ->whereProviderUserId($request->get('uid'))
            ->first();
        info($request);
        if (!is_null($account)) {
            return $account->user;
        } else {
            try {
                $account = new UserSocialAccount([
                    'provider_user_id' => $request->get('uid'),
                    'provider' => self::NAME,
                ]);
                $fakeLogin = self::NAME . '-' . $request->get('uid');
                /** @var User $user */
                $user = User::where('login', $fakeLogin)->first();
                if (is_null($user)) {
                    $user = User::create([
                        'login' => $fakeLogin,
                        'email' => null,
                        'phone' => null,
                        'first_name' => $request->get('first_name'),
                        'last_name' => $request->get('last_name'),
                        'password' => Hash::make(md5(rand(1, 10000))),
                    ]);
                    $this->saveImage($request->get('photo'), $user);
                    //$this->saveImage($request->get('photo'), $user);
                    //$this->activateUser($user);
                }
                $account->user()->associate($user);
                $account->save();
                $user->assignRole(User::ROLE_USER);
                return $user;
            } catch (\Exception $e) {
                Log::critical($e->getMessage());
                return null;
            }
        }
    }

    /**
     * @param string $url
     * @param User $oUser
     * @throws \Exception
     */
    public function saveImage(string $url, User $oUser)
    {
        $destinationPath = public_path('/storage/images/user/tmp/');
        $imageName = Str::random(10) . '.' . 'jpg';
        $pathFile = $destinationPath . $imageName;

        File::put($pathFile, file_get_contents($url));
        $oFile = uploadFileFromPath($pathFile, $imageName);
        (new ImageService())->upload($oUser, [$oFile], (new UserController())->image[ImageType::MODEL], ImageType::MODEL);
        //File::delete($pathFile);
    }
}
