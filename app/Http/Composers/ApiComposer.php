<?php

declare(strict_types=1);

namespace App\Http\Composers;

use App\Http\Controllers\Api\Index\Data\Index;
use App\Http\Controllers\Api\IndexController;
use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Http\Controllers\Api\UserController;
use App\Models\User;
use App\Services\Image\Facades\ImagePath;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;

class ApiComposer
{
    use CommonComposersTrait;
    use ImageTransformerTrait;

    private $data;
    private $dataOsx;
    private $user = null;

    public function __construct()
    {
        $this->setCommon();

        $this->data = remember('data', function () {
            $request = new Request();
            return json_encode((new Index())->__invoke($request));
        });
//        $this->dataOsx = $this->remember('data_osx', function () {
//            $data = json_decode($this->data, true)['data'];
//            $data['tours'] = $this->replaceOsx($data['tours']);
//            $data['organizations'] = $this->replaceOsx($data['organizations']);
//            $data['directions'] = $this->replaceOsx($data['directions']);
//            $data['news'] = $this->replaceOsx($data['news']);
//            $data['articles'] = $this->replaceOsx($data['articles']);
//            $data['promotions'] = $this->replaceOsx($data['promotions']);
//            $newData['data'] = $data;
//            return json_encode($newData);
//        });
//        info('isOsx');
//        if ($this->isOsx()) {
//            $this->data = $this->dataOsx;
//        }
//        $this->user = !Auth::guest() ? $this->remember('user', function () {
//            $result = (new UserController())->userData(Auth::user());
//            return json_encode($result);
//        }) : null;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $this->setCommonCompose($view);

        $view->with('aComposerData', $this->data);
        $view->with('aComposerUser', $this->user);
    }

    /**
     * @param array $data
     * @return array
     */
    private function replaceOsx(array $data): array
    {
        foreach ($data as $key => $value) {
            $data[$key]['image'] = $this->replaceExtension($value['image']);
        }
        return $data;
    }
}
