<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Direction;
use App\Models\Location;
use App\Models\Organization;
use App\Models\Departure;
use League\Fractal\TransformerAbstract;

class LocationTransformer extends TransformerAbstract
{
    /**
     * @param Location $oItem
     * @return array
     */
    public function transform(Location $oItem)
    {
        return [
            'id' => $oItem->id,
            'point' => [
                (float)$oItem->latitude,
                (float)$oItem->longitude,
            ],
            'zoom' => (int)$oItem->zoom,
            'title' => !is_null($oItem->title) ? trim($oItem->title) : null,
            'description' => !is_null($oItem->description) ? trim($oItem->description) : null,
            'placeholder' => !is_null($oItem->placeholder) ? trim($oItem->placeholder) : null,
        ];
    }
}
