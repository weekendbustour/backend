<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\Price;
use App\Models\Tag;
use App\Models\Tour;
use App\Models\TourAnnouncement;
use App\Models\TourCancellation;
use App\Services\Modelable\Priceable;
use League\Fractal\TransformerAbstract;

class TourAnnouncementTransformer extends TransformerAbstract
{
    /**
     * @param TourAnnouncement $oItem
     * @return array
     */
    public function transform(TourAnnouncement $oItem)
    {
        return [
            'type' => $oItem->type,
            'title' => $oItem->title_by_type,
            'tooltip' => $oItem->tooltip,
            'description' => $oItem->description,
        ];
    }
}
