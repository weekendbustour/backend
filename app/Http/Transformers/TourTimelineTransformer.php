<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Models\Category;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\TourTimeline;
use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class TourTimelineTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;

    /**
     * @param TourTimeline $oItem
     * @return array
     */
    public function transform(TourTimeline $oItem)
    {
        $start_at = null;
        if ($oItem->start_at_hourly) {
            $start_at = $oItem->start_at->format('d.m H:i');
        } else {
            $start_at = $oItem->start_at->format('d.m');
        }

        $to_at = null;
        if ($oItem->finish_at_hourly) {
            $to_at = !is_null($oItem->finish_at) ? $oItem->finish_at->format('d.m H:i') : null;
        } else {
            $to_at = !is_null($oItem->finish_at) ? $oItem->finish_at->format('d.m') : null;
        }

        $time_description = null;
        if ($oItem->start_at_hourly && $oItem->finish_at_hourly) {
            $count = $oItem->start_at->diffInHours($oItem->finish_at);
            if ($count !== 0) {
                $time_description = $count . ' ' . declensionWord($count, ['час', 'часа', 'часов']);
            } else {
                $count = $oItem->start_at->diffInMinutes($oItem->finish_at);
                $time_description = $count . ' ' . declensionWord($count, ['минута', 'минуты', 'минут']);
            }
        }
        if ($oItem->title === $start_at) {
            $time_description = Str::ucfirst($oItem->start_at->translatedFormat('l'));
        }
        return [
            'id' => (int)$oItem->id,
            'title' => trim($oItem->title),
            'description' => !is_null($oItem->description) ? trim($oItem->description) : null,
            'image' => $this->tourTimelineImage($oItem),
            'type' => $oItem->type,
            'time_description' => $time_description,
            'date' => $oItem->start_at->format('d.m'),
            'start_at' => $oItem->start_at->format('d.m.Y H:i:s'),
            'start_at_format' => $start_at,
            'start_at_time' => $oItem->start_at_hourly ? $oItem->start_at->format('H:i') : null,
            'finish_at' => !is_null($oItem->finish_at) ? $oItem->finish_at->format('d.m.Y H:i:s') : null,
            'finish_at_format' => $to_at,
            'finish_at_time' => $oItem->finish_at_hourly ? $oItem->finish_at->format('H:i') : null,
            'position' => 1,
        ];
    }
}
