<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Article;
use App\Models\Category;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\Question;
use App\Services\Image\ImageSize;
use League\Fractal\TransformerAbstract;

class QuestionTransformer extends TransformerAbstract
{
    /**
     * @param Question $oItem
     * @return array
     */
    public function transform(Question $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'question' => trim($oItem->question),
            'answer' => trim($oItem->answer),
        ];
    }
}
