<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Models\Article;
use App\Models\Category;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Departure;
use App\Services\Image\ImageSize;
use League\Fractal\TransformerAbstract;

class CategoryTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;

    /**
     * @param Category $oItem
     * @return array
     */
    public function transformSelect(Category $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'label' => trim($oItem->title),
            'name' => trim($oItem->name),
        ];
    }

    /**
     * @param Category $oItem
     * @return array
     */
    public function transformMention(Category $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'name' => trim($oItem->name),
            'title' => trim($oItem->title),
            'color' => trim($oItem->color),
        ];
    }

    /**
     * @param Category $oItem
     * @return array
     */
    public function transformDetail(Category $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'name' => trim($oItem->name),
            'title' => trim($oItem->title),
            'description' => trim($oItem->description),
            'color' => trim($oItem->color),
            'image' => $this->categoryImage($oItem),
            'articles' => $oItem->articlesActiveOrdered->transform(function (Article $item) {
                return (new ArticleTransformer())->transformCard($item);
            }),
        ];
    }
}
