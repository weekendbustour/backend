<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\Price;
use App\Models\Tag;
use App\Models\Tour;
use App\Models\TourCancellation;
use App\Services\Modelable\Priceable;
use League\Fractal\TransformerAbstract;

class TourCancellationTransformer extends TransformerAbstract
{
    /**
     * @param TourCancellation $oItem
     * @return array
     */
    public function transform(TourCancellation $oItem)
    {
        return [
            'type' => $oItem->type,
            'title' => $oItem->title_by_type,
            'tooltip' => $oItem->tooltip,
            'description' => $oItem->description,
            'start_at_format' => !is_null($oItem->start_at) ? $oItem->start_at->format('d.m.Y') : null,
            'finish_at_format' => !is_null($oItem->finish_at) ? $oItem->finish_at->format('d.m.Y') : null,
        ];
    }
}
