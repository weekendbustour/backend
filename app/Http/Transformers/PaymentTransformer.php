<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Organization;
use App\Models\Payment;
use App\Models\Tour;
use App\Models\TourReservation;
use App\Models\TourTimeline;
use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class PaymentTransformer extends TransformerAbstract
{
    /**
     * @param Payment $oItem
     * @return array
     */
    public function transform(Payment $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'amount' => $oItem->amount,
        ];
    }
}
