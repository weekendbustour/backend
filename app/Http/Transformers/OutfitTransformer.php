<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Direction;
use App\Models\MetaTag;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\Outfit;
use App\Models\Tag;
use League\Fractal\TransformerAbstract;

class OutfitTransformer extends TransformerAbstract
{
    /**
     * @param Outfit $oItem
     * @return array
     */
    public function transform(Outfit $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'title' => trim($oItem->title),
        ];
    }

    /**
     * @param Outfit $oItem
     * @return array
     */
    public function transformRent(Outfit $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'title' => trim($oItem->title),
            'tooltip' => $oItem->tooltip,
            'price' => $oItem->pivot->rent_price,
            'description' => $oItem->pivot->rent_description,
        ];
    }
}
