<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\Tag;
use League\Fractal\TransformerAbstract;

class TagTransformer extends TransformerAbstract
{
    /**
     * @param Tag $oItem
     * @return array
     */
    public function transform(Tag $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'title' => trim($oItem->title),
            'color' => trim($oItem->color),
        ];
    }
}
