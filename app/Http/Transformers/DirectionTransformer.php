<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Models\Attraction;
use App\Models\Direction;
use App\Models\Tour;
use League\Fractal\TransformerAbstract;

class DirectionTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;

    /**
     * @param Direction $oItem
     * @return array
     */
    public function transformSelect(Direction $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'label' => trim($oItem->title),
            'name' => trim($oItem->name),
        ];
    }

    /**
     * @param Direction $oItem
     * @return array
     */
    public function transformMention(Direction $oItem): array
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'location' => !is_null($oItem->location)
                ? (new LocationTransformer())->transform($oItem->location)
                : null,
        ];
    }

    /**
     * @param Direction $oItem
     * @return array
     */
    public function transformCard(Direction $oItem): array
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
            'image' => $this->directionImage($oItem),
            'tours' => $this->toursMention($oItem),
            'rating' => $oItem->averageRating() ?? 0,
            'statistic' => $this->statistic($oItem),
        ];
    }

    /**
     * @param Direction $oItem
     * @return array
     */
    public function transformDetail(Direction $oItem)
    {
        $aTours = $oItem->toursActive()->orderedNearest()->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transformCardWithoutMultiples($item);
        })->toArray();

        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
            'description' => $oItem->description,
            'meta_tag' => $this->metaTag($oItem),
            'image' => $this->directionImage($oItem),
            'tours' => $aTours,
            'attractions' => $this->attractions($oItem),
            'dates' => $this->dates($oItem),
            'rating' => $oItem->averageRating() ?? 0,
            'statistic' => $this->statistic($oItem),
            'location' => !is_null($oItem->location)
                ? (new LocationTransformer())->transform($oItem->location)
                : null,
        ];
    }

    /**
     * @param Direction $oItem
     * @return array
     */
    private function tours(Direction $oItem): array
    {
        return $oItem->toursActive()->orderedNearest()->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transformCard($item);
        })->toArray();
    }

    /**
     * @param Direction $oItem
     * @return array
     */
    private function toursMention(Direction $oItem): array
    {
        return $oItem->toursActive()->orderedNearest()->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transformMention($item);
        })->toArray();
    }

    /**
     * @param Direction $oItem
     * @return array
     */
    public function statistic(Direction $oItem)
    {
        $oStatistic = $oItem->statistic;
        if (!is_null($oStatistic)) {
            return [
                'comments' => (int)$oStatistic->comments,
                'views' => (int)$oStatistic->views,
                'likes' => (int)$oStatistic->likes,
            ];
        }
        return [
            'comments' => $oItem->comments()->count(),
            'views' => rand(1, 300),
            'likes' => rand(1, 100),
        ];
    }

    /**
     * @param Direction $oItem
     * @return array
     */
    public function dates(Direction $oItem): array
    {
        $oTours = $oItem->toursActive;

        $aDates = [];
        foreach ($oTours as $oTour) {
            $aDates[] = $oTour->start_at->format('d.m.Y');
            //$aDates[] = $oTour->finish_at->format('d.m.Y');
        }

        $aSales = [];
        foreach ($oTours as $oTour) {
            $aSales[] = $oTour->start_at->format('d.m.Y');
            //$aSales[] = $oTour->finish_at->format('d.m.Y');
        }

//        $aTours = $oItem->tours->transform(function ($item) {
//            return $item->start_at->format('d.m.Y');
//        })->toArray();
        return [
            'sales' => $aSales,
            'active' => $aDates,
        ];
    }

    /**
     * @param Direction $oItem
     * @return array
     */
    private function metaTag(Direction $oItem)
    {
        $oMetaTag = $oItem->metaTag;
        if (!is_null($oMetaTag)) {
            return (new MetaTagTransformer())->transform($oMetaTag);
        }
        return [
            'title' => $oItem->title,
            'description' => $oItem->preview_description,
            'keywords' => null,
        ];
    }

    /**
     * @param Direction $oItem
     * @return array
     */
    private function attractions(Direction $oItem): array
    {
        $data['data'] = $oItem->attractionsActive()->get()->transform(function (Attraction $item) {
            return (new AttractionTransformer())->transformCard($item);
        })->toArray();
        $data['total'] = $oItem->attractionsActive()->count();
        return $data;
    }
}
