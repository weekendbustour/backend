<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Comment;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\User;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    /**
     * @param Comment $oItem
     * @return array
     */
    public function transform(Comment $oItem)
    {
        if ($oItem->anon) {
            return [
                'anon' => (int)$oItem->anon,
                'rating' => $oItem->rating->value ?? 0,
            ];
        }
        return [
            'id' => (int)$oItem->id,
            'author' => $oItem->commented->first_name . ' ' . $oItem->commented->last_name,
            'image' => $oItem->commented->image_square,
            'text' => !empty(trim($oItem->text)) ? $oItem->text : null,
            'rate' => (int)$oItem->rate,
            'anon' => (int)$oItem->anon,
            'rating' => $oItem->rating->value ?? 0,
            'votes' => (int)$oItem->votes()->sum('value') ?? 0,
            'parent_id' => $oItem->parent_id,
            'children' => $oItem->hasApprovedChildren() ? $oItem->approvedChildren->transform(function ($item) {
                return $this->transform($item);
            }) : [],
            'created_at_format' => $oItem->created_at->format('d.m.Y H:i'),
        ];
    }
}
