<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Models\Article;
use App\Models\Organization;
use App\Models\Social;
use App\Models\Tour;
use League\Fractal\TransformerAbstract;

class OrganizationTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;

    /**
     * @param Organization $oItem
     * @return array
     */
    public function transformSelect(Organization $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'label' => trim($oItem->title),
        ];
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    public function transformMention(Organization $oItem): array
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'image' => $this->organizationImage($oItem),
        ];
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    public function transformCard(Organization $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'name' => trim($oItem->name),
            'title' => trim($oItem->title),
            'preview_description' => !is_null($oItem->preview_description) ? trim($oItem->preview_description) : null,
            'image' => $this->organizationImage($oItem, 'card'),
//            'categories' => $oItem->categories->transform(function ($item) {
//                return (new CategoryTransformer())->transformMention($item);
//            })->toArray(),
            'departures' => $oItem->departures->transform(function ($item) {
                return (new DepartureTransformer())->transformMention($item);
            })->toArray(),
            'tours' => $this->toursMention($oItem),
            'rating' => $oItem->averageRating() ?? 0,
            'statistic' => $this->statistic($oItem),
        ];
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    public function transformDetail(Organization $oItem)
    {
        $aTours = $oItem->toursActive()->orderedNearest()->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transformCardWithoutMultiples($item);
        })->toArray();

        return [
            'id' => (int)$oItem->id,
            'name' => trim($oItem->name),
            'title' => trim($oItem->title),
            'preview_description' => !is_null($oItem->preview_description) ? trim($oItem->preview_description) : null,
            'description' => !is_null($oItem->description) ? trim($oItem->description) : null,
            'meta_tag' => $this->metaTag($oItem),
            'image' => $this->organizationImage($oItem),
            'parameters' => (new OrganizationParameterTransformer())->transformOrganizationParameters($oItem),
            'parameters_view' => (new OrganizationParameterTransformer())->transformOrganizationParametersForView($oItem),
            'categories' => $oItem->categories->transform(function ($item) {
                return (new CategoryTransformer())->transformMention($item);
            })->toArray(),
            'departures' => $oItem->departures->transform(function ($item) {
                return (new DepartureTransformer())->transformMention($item);
            })->toArray(),
//            'departures_title' => $oItem->departures_title,
            'text' => $oItem->description,
            'tours' => $aTours,
            'articles' => $this->articles($oItem),
            'dates' => $this->dates($oItem),
            'rating' => $oItem->averageRating() ?? 0,
            'statistic' => $this->statistic($oItem),
            'socials' => $this->socials($oItem),
        ];
    }

    /**
     * @param Organization $oItem
     * @return \Illuminate\Support\Collection|mixed
     */
    private function socials(Organization $oItem)
    {
        $oSocial = new \App\Models\Social();
        $aSocialTypes = $oSocial->typeIcons;
        return $oItem->socials->mapWithKeys(function ($item) use ($aSocialTypes) {
            return $this->social($item, $aSocialTypes);
        });
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    private function tours(Organization $oItem): array
    {
        return $oItem->toursActive()->orderedNearest()->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transformCard($item);
        })->toArray();
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    private function toursMention(Organization $oItem): array
    {
        return $oItem->toursActive()->orderedNearest()->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transformMention($item);
        })->toArray();
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    private function articles(Organization $oItem): array
    {
        $data['data'] = $oItem->articlesActive()->take(3)->get()->transform(function (Article $item) {
            return (new ArticleTransformer())->transformCard($item);
        })->toArray();
        $data['total'] = $oItem->articlesActive()->count();
        return $data;
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    public function statistic(Organization $oItem)
    {
        $oStatistic = $oItem->statistic;
        if (!is_null($oStatistic)) {
            return [
                'comments' => (int)$oStatistic->comments,
                'views' => (int)$oStatistic->views,
                'likes' => (int)$oStatistic->likes,
            ];
        }
        return [
            'comments' => $oItem->comments()->where('anon', 0)->count(),
            'views' => rand(1, 300),
            'likes' => rand(1, 100),
        ];
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    public function dates(Organization $oItem): array
    {
        $oTours = $oItem->toursActive;

        $aDates = [];
        foreach ($oTours as $oTour) {
            $aDates[] = $oTour->start_at->format('d.m.Y');
            //$aDates[] = $oTour->finish_at->format('d.m.Y');
        }

        $aSales = [];
        foreach ($oTours as $oTour) {
            $aSales[] = $oTour->start_at->format('d.m.Y');
            //$aSales[] = $oTour->finish_at->format('d.m.Y');
        }

//        $aTours = $oItem->tours->transform(function ($item) {
//            return $item->start_at->format('d.m.Y');
//        })->toArray();
        return [
            'sales' => $aSales,
            'active' => $aDates,
        ];
    }

    private function social($item, $aSocialTypes)
    {
        if ($item['type'] === Social::TYPE_WHATSAPP) {
            $item['url'] = preg_replace('/[^0-9]/', '', $item['url']);
        }
        return [$item['type'] => [
            'icon' => $aSocialTypes[$item['type']]['class'],
            'url' => $item['url'],
        ]];
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    private function metaTag(Organization $oItem)
    {
        $oMetaTag = $oItem->metaTag;
        if (!is_null($oMetaTag)) {
            return (new MetaTagTransformer())->transform($oMetaTag);
        }
        return [
            'title' => $oItem->title,
            'description' => $oItem->preview_description,
            'keywords' => null,
        ];
    }
}
