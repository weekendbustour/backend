<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\TourReservation;
use App\Models\TourTimeline;
use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class TourReservationTransformer extends TransformerAbstract
{
    /**
     * @param TourReservation $oItem
     * @return array
     */
    public function transform(TourReservation $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'amount' => $oItem->amount,
            'code' => $oItem->code,
            'places' => $oItem->places,
            'payment' => $this->payment($oItem),
            'source' => $oItem->source,
            'can_pay' => false,
            'status' => $this->status($oItem),
        ];
    }

    /**
     * @param TourReservation $oItem
     * @return array
     */
    private function payment(TourReservation $oItem)
    {
        $oPayment = $oItem->payment;
        if (is_null($oPayment)) {
            return null;
        }
        return (new PaymentTransformer())->transform($oPayment);
    }

    /**
     * @param TourReservation $oItem
     * @return array
     */
    private function status(TourReservation $oItem)
    {
        return [
            'title' => $oItem->getStatusTextAttribute(),
            'class' => $oItem->getStatusIconAttribute()['class'],
            'type' => $oItem->getStatusIconAttribute()['type'],
            'description' => $oItem->getStatusIconAttribute()['description'] ?? null,
        ];
    }
}
