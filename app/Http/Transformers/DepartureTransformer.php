<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Organization;
use App\Models\Departure;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class DepartureTransformer extends TransformerAbstract
{
    /**
     * @param Departure $oItem
     * @return array
     */
    public function transformSelect(Departure $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'label' => trim($oItem->title),
            'name' => trim($oItem->name),
        ];
    }

    /**
     * @param Departure $oItem
     * @return array
     */
    public function transformMention(Departure $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'name' => trim($oItem->name),
            'title' => trim($oItem->title),
        ];
    }

    /**
     * @param Departure $oItem
     * @return array
     */
    public function transform(Departure $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'description' => $oItem->pivot->description,
            'location' => [
                $oItem->latitude,
                $oItem->longitude,
            ],
            'departure_at_date' => !is_null($oItem->pivot->departure_at)
                ? Carbon::parse($oItem->pivot->departure_at)->format('d.m')
                : null,
            'departure_at_time' => !is_null($oItem->pivot->departure_at) && $oItem->pivot->departure_at_hourly
                ? Carbon::parse($oItem->pivot->departure_at)->format('H:i')
                : null,
            'departure_at_hourly' => $oItem->pivot->departure_at_hourly,
        ];
    }



    /**
     * @param Departure $oItem
     * @return array
     */
    public function transformTitleOnly(Departure $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'title' => trim($oItem->title),
        ];
    }
}
