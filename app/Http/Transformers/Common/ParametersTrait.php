<?php

declare(strict_types=1);

namespace App\Http\Transformers\Common;

use App\Cmf\Core\MainController;
use App\Models\Option;

trait ParametersTrait
{
    /**
     * @param Option $oOption
     * @param array $aValues
     * @return array
     */
    protected function commonTransform(Option $oOption, array $aValues = []): array
    {
        return [
            'id' => (int)$oOption->id,
            'name' => $oOption->name,
            'title' => $oOption->title,
            'description' => $oOption->description,
            'unit' => $oOption->unit,
            'tooltip' => $oOption->tooltip,
            'value' => $aValues[$oOption->name]['value'],
        ];
    }

    /**
     * @param mixed $oOptions
     * @param array $aValues
     * @return array
     */
    protected function commonTransformParameters($oOptions, array $aValues): array
    {
        $aParameters = [];
        foreach ($oOptions as $oOption) {
            if (isset($aValues[$oOption->name])) {
                $aParameters[$oOption->name] = $this->commonTransform($oOption, $aValues);
                if ($oOption->name === Option::NAME_VIDEO_YOUTUBE) {
                    $aParameters[$oOption->name]['embed'] = $this->getYoutubeEmbedUrl($aParameters[$oOption->name]['value']);
                    $aParameters[$oOption->name]['meta'] = $this->getYoutubeMeta($aParameters[$oOption->name]['value']);
                }
                if ($oOption->type === MainController::DATA_TYPE_CHECKBOX) {
                    $aParameters[$oOption->name]['value'] = (int)$aParameters[$oOption->name]['value'] ? 'Да' : 'Нет';
                }
            }
        }
        return $aParameters;
    }

    /**
     * @param array $aParameters
     * @return array
     */
    protected function commonTransformParametersForView(array $aParameters): array
    {
        $aSortParameters = [];
        foreach ($aParameters as $key => $aParameter) {
            if (!in_array($key, [
                Option::NAME_INCLUDE,
                Option::NAME_NOT_INCLUDE,
                Option::NAME_MORE_INCLUDE,
                Option::NAME_VIDEO_YOUTUBE,
                Option::NAME_TIMELINE_NOTE,
                Option::NAME_OUTFIT_RENT_NOTE,
                Option::NAME_RESERVATION,
                Option::NAME_PREPAYMENT_CANCELLATION,
            ])) {
                $aSortParameters[$key] = $aParameter;
            }
        }
        return $aSortParameters;
    }
}
