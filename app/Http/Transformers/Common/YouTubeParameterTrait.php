<?php

declare(strict_types=1);

namespace App\Http\Transformers\Common;

trait YouTubeParameterTrait
{
    /**
     * @param string $url
     * @return string
     */
    protected function getYoutubeEmbedUrl(string $url): string
    {
        $shortUrlRegex = '/youtu.be\/([a-zA-Z0-9_-]+)\??/i';
        $longUrlRegex = '/youtube.com\/((?:embed)|(?:watch))((?:\?v\=)|(?:\/))([a-zA-Z0-9_-]+)/i';

        $youtube_id = '';

        if (preg_match($longUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }

        if (preg_match($shortUrlRegex, $url, $matches)) {
            $youtube_id = $matches[count($matches) - 1];
        }
        return 'https://www.youtube.com/embed/' . $youtube_id;
    }

    /**
     * @param string $url
     * @return array
     */
    protected function getYoutubeMeta(string $url): array
    {
        $youtube = "http://www.youtube.com/oembed?url=" . $url . "&format=json";
        $curl = curl_init($youtube);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        return json_decode($return, true);
    }
}
