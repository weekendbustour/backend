<?php

declare(strict_types=1);

namespace App\Http\Transformers\Common;

use App\Models\Article;
use App\Models\Attraction;
use App\Models\Category;
use App\Models\Direction;
use App\Models\News;
use App\Models\Organization;
use App\Models\Promotion;
use App\Models\Tour;
use App\Models\TourTimeline;
use App\Models\User;
use App\Services\Image\ImageSize;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;

trait ImageTransformerTrait
{
    /**
     * @var Agent|null
     */
    private $agent = null;

    /**
     * @return bool
     */
    public function isOsx()
    {
        if (env('TEST_IS_OSX')) {
            return true;
        }
        if (is_null($this->agent)) {
            $this->agent = (new Agent());
        }
        return $this->agent->is('OS X');
    }

    /**
     * @param Tour $oItem
     * @param string $role
     *
     * banner - баннер, если не установлен, то первое изображение
     * banner_caption - информация картинок
     * images - все изображения
     * images_captions - информация картинок
     *
     * @return array
     */
    protected function tourImage(Tour $oItem, string $role = 'detail'): array
    {
        $data = [];
        if (!$oItem->imageHasBanner()) {
            $data['banner'] = $oItem->getImageDefaultAttribute();
            $data['banner_caption'] = $oItem->getImageModelCaption();
            $data['banner_has'] = false;
        } else {
            $data['banner'] = $oItem->getImageBannerAttribute();
            $data['banner_caption'] = $oItem->getImageBannerCaption();
            $data['banner_has'] = true;
        }
        if ($role === 'detail') {
            $data['images'] = $oItem->getImageGalleryAttribute();
        } else {
            $data['images'] = $oItem->getImageGalleryCardMdAttribute();
        }
        $data['images_captions'] = $oItem->getImageGalleryCaption();
        if ($this->isOsx()) {
            $data = $this->replaceExtension($data);
        }
        return $data;
    }

    /**
     * @param Organization $oItem
     * @param string $role
     *
     * logo - логотип в карточке тура, на странице организатора, в карточке организатора
     * card - баннер в карточке организатора
     * banner - баннер на странице организатора
     * images - галлерея организатора
     *
     * @return array
     */
    protected function organizationImage(Organization $oItem, string $role = 'detail'): array
    {
        $data = [];
        $data['logo'] = $oItem->getImageModelPath(ImageSize::SQUARE);
        $data['card'] = $oItem->getImageBannerPath(ImageSize::CARD_DEFAULT);
        if ($role === 'detail') {
            $data['banner'] = $oItem->getImageBannerPath(ImageSize::XL);
            $data['images'] = $oItem->getImageGalleryPath(ImageSize::XL);
        }
        if ($this->isOsx()) {
            $data = $this->replaceExtension($data);
        }
        return $data;
    }

    /**
     * @param Direction $oItem
     *
     * card - изображение в карточке
     * banner - изображение на главной в виде баннера
     * images - галлерея
     *
     * @return array
     */
    protected function directionImage(Direction $oItem): array
    {
        $data = [];
        $data['card'] = $oItem->getImageModelPath(ImageSize::CARD_DEFAULT);
        $data['banner'] = $oItem->getImageModelPath(ImageSize::XL);
        $data['banner_caption'] = $oItem->getImageBannerCaption();
        $data['images'] = $oItem->getImageGalleryPath(ImageSize::XL);
        if ($this->isOsx()) {
            $data = $this->replaceExtension($data);
        }
        return $data;
    }

    /**
     * @param Article $oItem
     *
     * card - изображение в карточке
     * banner - изображение на главной в виде баннера
     * images - галлерея
     *
     * @return array
     */
    protected function articleImage(Article $oItem): array
    {
        $data = [];
        $data['card'] = $oItem->getImageModelPath(ImageSize::CARD_MD);
        $data['banner'] = $oItem->getImageModelPath(ImageSize::XL);
        $data['banner_caption'] = $oItem->getImageBannerCaption();
        $data['images'] = $oItem->getImageGalleryPath(ImageSize::XL);
        $data['images_captions'] = $oItem->getGalleryCaption();
        if ($this->isOsx()) {
            $data = $this->replaceExtension($data);
        }
        return $data;
    }

    /**
     * @param Attraction $oItem
     *
     * card - изображение в карточке
     * banner - изображение на главной в виде баннера
     * images - галлерея
     *
     * @return array
     */
    protected function attractionImage(Attraction $oItem): array
    {
        $data = [];
        $data['card'] = $oItem->getImageModelPath(ImageSize::CARD_DEFAULT);
        $data['banner'] = $oItem->getImageModelPath(ImageSize::XL);
        $data['images'] = $oItem->getImageGalleryPath(ImageSize::XL);
        $data['images_captions'] = $oItem->getGalleryCaption();
        if ($this->isOsx()) {
            $data = $this->replaceExtension($data);
        }
        return $data;
    }

    /**
     * @param News $oItem
     *
     * card - изображение в карточке
     * banner - изображение на главной в виде баннера
     *
     * @return array
     */
    protected function newsImage(News $oItem): array
    {
        $data = [];
        $data['card'] = $oItem->getImageModelPath(ImageSize::CARD_MD);
        $data['banner'] = $oItem->getImageModelPath(ImageSize::XL);
        $data['banner_caption'] = $oItem->getImageBannerCaption();
        if ($this->isOsx()) {
            $data = $this->replaceExtension($data);
        }
        return $data;
    }

    /**
     * @param Category $oItem
     *
     * card - изображение в карточке
     * banner - изображение на главной в виде баннера
     *
     * @return array
     */
    protected function categoryImage(Category $oItem)
    {
        $data = [];
        $data['card'] = $oItem->getImageModelPath(ImageSize::CARD_MD);
        $data['banner'] = $oItem->getImageModelPath(ImageSize::XL);
        $data['banner_caption'] = $oItem->getImageBannerCaption();
        if ($this->isOsx()) {
            $data = $this->replaceExtension($data);
        }
        return $data;
    }

    /**
     * @param TourTimeline $oItem
     * @return array
     */
    protected function tourTimelineImage(TourTimeline $oItem)
    {
        $data = [];
        $data['images'] = $oItem->gallery;
        if ($this->isOsx()) {
            $data = $this->replaceExtension($data);
        }
        return $data;
    }

    /**
     * @param Promotion $oItem
     * @return array
     */
    protected function promotionImage(Promotion $oItem)
    {
        $data = [];
        $data['images'] = $oItem->gallery;
        if ($this->isOsx()) {
            $data = $this->replaceExtension($data);
        }
        return $data;
    }

    /**
     * @param User $oItem
     * @return string
     */
    protected function userImage(User $oItem)
    {
        $image = $oItem->image_square;
        if ($this->isOsx()) {
            $image = $this->replace($image, '.webp', '.jpg');
        }
        return $image;
    }

    /**
     * @param array $data
     * @param string $from
     * @param string $to
     * @return array
     */
    public function replaceExtension(array $data, string $from = '.webp', string $to = '.jpg')
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                foreach ($value as $i => $v) {
                    $data[$key][$i] = $this->replace($v, $from, $to);
                }
            } else {
                $data[$key] = $this->replace($value, $from, $to);
            }
        }
        return $data;
    }

    /**
     * @param array|string $value
     * @param string $from
     * @param string $to
     * @return string
     */
    public function replace($value, string $from, string $to)
    {
        if (is_array($value)) {
            return $value;
        }
        return Str::endsWith($value, $from) ? str_replace($from, $to, $value) : $value;
    }
}
