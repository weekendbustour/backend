<?php

declare(strict_types=1);

namespace App\Http\Transformers\Common;

use App\Http\Transformers\SourceTransformer;
use App\Models\Article;
use App\Models\News;
use App\Models\Source;

trait SourceableTrait
{
    /**
     * @param Article|News $oItem
     * @return null|array
     */
    protected function source($oItem): ?array
    {
        /** @var Source $oSource */
        $oSource = $oItem->source;
        if (!is_null($oSource)) {
            return (new SourceTransformer())->transform($oSource);
        }
        return null;
    }
}
