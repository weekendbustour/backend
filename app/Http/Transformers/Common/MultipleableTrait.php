<?php

declare(strict_types=1);

namespace App\Http\Transformers\Common;

use App\Http\Transformers\SourceTransformer;
use App\Models\Article;
use App\Models\News;
use App\Models\Source;
use App\Models\Tour;

trait MultipleableTrait
{
    /**
     * @param Tour $oItem
     * @return array
     */
    private function tourMultiplesMention(Tour $oItem): array
    {
        return $oItem->multi === 1 ? $oItem->multiplesActive->transform(function ($item) {
            return $this->transformMentionMultiple($item);
        })->toArray() : [];
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    private function tourMultiplesCardWithoutMultiples(Tour $oItem): array
    {
        $oItems = $this->tourMultiplesCard($oItem);
        foreach ($oItems as $key => $oItem) {
            $oItems[$key]['multiples'] = [];
        }
        return $oItems;
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    private function tourMultiplesCard(Tour $oItem): array
    {
        return $oItem->multi === 1 ? $oItem->multiplesActive->transform(function ($item) {
            return $this->transformCard($item);
        })->toArray() : [];
    }
}
