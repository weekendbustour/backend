<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Http\Transformers\Common\SourceableTrait;
use App\Models\Article;
use App\Models\Attraction;
use App\Models\News;
use App\Services\Image\ImageSize;
use League\Fractal\TransformerAbstract;

class ArticleTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;
    use SourceableTrait;

    /**
     * @param Article $oItem
     * @return array
     */
    public function transformMention(Article $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'position' => trim($oItem->position),
            'name' => $oItem->name,
            'title' => $oItem->title,
            'type' => trim($oItem->type),
            'number' => $oItem->number,
        ];
    }

    /**
     * @param Article $oItem
     * @return array
     */
    public function transformCard(Article $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'position' => trim($oItem->position),
            'type' => trim($oItem->type),
            'number' => $oItem->number,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
            'categories' => $oItem->categories->transform(function ($item) {
                return (new CategoryTransformer())->transformMention($item);
            })->toArray(),
            // @todo не включать images
            'image' => $this->articleImage($oItem),
            'statistic' => $this->statistic($oItem, false),
            'published_at_format' => $oItem->published_at->format('d.m.Y'),
            'published_at_day' => $oItem->published_at->format('d'),
            'published_at_month' => $oItem->published_at->translatedFormat('M'),
        ];
    }

    /**
     * @param Article $oItem
     * @return array
     */
    public function transformDetail(Article $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'position' => trim($oItem->position),
            'type' => trim($oItem->type),
            'number' => $oItem->number,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
            'categories' => $oItem->categories->transform(function ($item) {
                return (new CategoryTransformer())->transformMention($item);
            })->toArray(),
            'description' => $oItem->description,
            'attractions' => $oItem->attractionsActive()->ordered()->get()->transform(function (Attraction $item) {
                return (new AttractionTransformer())->transformCard($item);
            })->toArray(),
            'source' => $this->source($oItem),
            'with_banner' => $oItem->with_banner ? true : false,
            'image' => $this->articleImage($oItem),
            'statistic' => $this->statistic($oItem, true),
            'recommended' => $this->recommended($oItem),
            'location' => !is_null($oItem->location)
                ? (new LocationTransformer())->transform($oItem->location)
                : null,
            'locations' => $oItem->locations->transform(function ($item) {
                return (new LocationTransformer())->transform($item);
            })->toArray(),
            'parameters' => (new ArticleParameterTransformer())->transformArticleParameters($oItem),
            'published_at_format' => $oItem->published_at->format('d.m.Y'),
            'published_at_day' => $oItem->published_at->format('d'),
            'published_at_month' => $oItem->published_at->format('MMM'),
        ];
    }

    /**
     * @param Article $oItem
     * @param bool $setCount
     * @return array
     */
    private function statistic(Article $oItem, bool $setCount)
    {
        $oStatistic = $oItem->statistic;
        if ($setCount) {
            visits($oItem)->increment();
        }
        if (!is_null($oStatistic)) {
            return [
                'comments' => $oItem->comments()->count(),
                //'comments' => (int)$oStatistic->comments,
                'views' => (int)$oStatistic->views,
                'likes' => (int)$oStatistic->likes,
            ];
        }
        //dd(visits($oItem)->count());
        return [
            'comments' => $oItem->comments()->count(),
            'views' => visits($oItem)->count(),
            'likes' => rand(1, 100),
        ];
    }

    /**
     * @param Article $oItem
     * @return array
     */
    private function recommended(Article $oItem)
    {
        $oPrevious = null;
        $oNext = null;

        $oPrevious = Article::active()
            ->where('published_at', '<=', $oItem->published_at)
            ->where('id', '<>', $oItem->id)
            ->ordered();

        if ($oItem->type === Article::TYPE_TOP) {
            $oPrevious = $oPrevious->where('type', Article::TYPE_TOP);
        }

        $oPrevious = $oPrevious->first();

        $oNext = Article::active()
            ->where('published_at', '>=', $oItem->published_at)
            ->where('id', '<>', $oItem->id)
            ->orderedReserve();

        if ($oItem->type === Article::TYPE_TOP) {
            $oNext = $oNext->where('type', Article::TYPE_TOP);
        }

        $oNext = $oNext->first();

        return [
            'previous' => !is_null($oPrevious) ? $this->transformMention($oPrevious) : null,
            'next' => !is_null($oNext) ? $this->transformMention($oNext) : null,
        ];
    }
}
