<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Models\Category;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\Price;
use App\Models\Promotion;
use App\Models\Tag;
use App\Models\Tour;
use App\Services\Modelable\Priceable;
use League\Fractal\TransformerAbstract;

class PromotionTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;

    /**
     * @param Promotion $oItem
     * @return array
     */
    public function transform(Promotion $oItem)
    {
        return [
            'id' => $oItem->id,
            'title' => $oItem->title,
            'description' => $oItem->description,
            'url' => $oItem->getUrl(),
            'image' => $this->promotionImage($oItem),
        ];
    }
}
