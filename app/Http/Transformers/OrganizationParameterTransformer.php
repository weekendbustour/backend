<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Cmf\Core\MainController;
use App\Http\Transformers\Common\ParametersTrait;
use App\Http\Transformers\Common\YouTubeParameterTrait;
use App\Models\Category;
use App\Models\Option;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\TourTimeline;
use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class OrganizationParameterTransformer extends TransformerAbstract
{
    use YouTubeParameterTrait;
    use ParametersTrait;

    /**
     * @param Option $oOption
     * @param array $aValues
     * @return array
     */
    public function transform(Option $oOption, array $aValues = []): array
    {
        return [
            'id' => (int)$oOption->id,
            'name' => $oOption->name,
            'title' => $oOption->title,
            'description' => $oOption->description,
            'unit' => $oOption->unit,
            'tooltip' => $oOption->tooltip,
            'value' => $aValues[$oOption->name]['value'],
        ];
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    public function transformOrganizationParameters(Organization $oItem): array
    {
        $aValues = $oItem->values->keyBy('option.name')->toArray();
        $oOptions = Option::active()->purposeOrganizations()->ordered()->get();
        $aParameters = $this->commonTransformParameters($oOptions, $aValues);
        return $aParameters;
    }

    /**
     * @param Organization $oItem
     * @return array
     */
    public function transformOrganizationParametersForView(Organization $oItem)
    {
        $aParameters = $this->transformOrganizationParameters($oItem);
        $aSortParameters = $this->commonTransformParametersForView($aParameters);
        return $aSortParameters;
    }
}
