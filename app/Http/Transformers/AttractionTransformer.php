<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Models\Article;
use App\Models\Attraction;
use App\Services\Image\ImageSize;
use League\Fractal\TransformerAbstract;

class AttractionTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;

    /**
     * @param Attraction $oItem
     * @return array
     */
    public function transformCard(Attraction $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
            'description' => $oItem->description,
            'statistic' => $this->statistic($oItem, false),
            'image' => $this->attractionImage($oItem),
            'location' => !is_null($oItem->location)
                ? (new LocationTransformer())->transform($oItem->location)
                : null,
        ];
    }

    /**
     * @param Attraction $oItem
     * @return array
     */
    public function transformMention(Attraction $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
        ];
    }

    /**
     * @param Attraction $oItem
     * @return array
     */
    public function transform(Attraction $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
            'description' => $oItem->description,
            'image' => $this->attractionImage($oItem),
            'location' => !is_null($oItem->location)
                ? (new LocationTransformer())->transform($oItem->location)
                : null,
        ];
    }

    /**
     * @param Attraction $oItem
     * @return array
     */
    public function transformDetail(Attraction $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
            'description' => $oItem->description,
            'direction' => !is_null($oItem->direction) ? (new DirectionTransformer())->transformMention($oItem->direction) : null,
            'articles' => $oItem->articlesOrderedActive()->get()->transform(function (Article $item) {
                return (new ArticleTransformer())->transformCard($item);
            })->toArray(),
            'image' => $this->attractionImage($oItem),
            'statistic' => $this->statistic($oItem, true),
            'with_banner' => $oItem->with_banner ? true : false,
            'location' => !is_null($oItem->location)
                ? (new LocationTransformer())->transform($oItem->location)
                : null,
            'published_at_format' => $oItem->created_at->format('d.m.Y'),
        ];
    }

    /**
     * @param Attraction $oItem
     * @param bool $setCount
     * @return array
     */
    private function statistic(Attraction $oItem, bool $setCount)
    {
        $oStatistic = $oItem->statistic;
        if ($setCount) {
            visits($oItem)->increment();
        }
        if (!is_null($oStatistic)) {
            return [
                'comments' => $oItem->comments()->count(),
                //'comments' => (int)$oStatistic->comments,
                'views' => (int)$oStatistic->views,
                'likes' => (int)$oStatistic->likes,
            ];
        }
        return [
            'comments' => $oItem->comments()->count(),
            'views' => visits($oItem)->count(),
            'likes' => rand(1, 100),
        ];
    }
}
