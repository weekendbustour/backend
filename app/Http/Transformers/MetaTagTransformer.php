<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Direction;
use App\Models\MetaTag;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\Tag;
use League\Fractal\TransformerAbstract;

class MetaTagTransformer extends TransformerAbstract
{
    /**
     * @param MetaTag $oItem
     * @return array
     */
    public function transform(MetaTag $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'title' => $oItem->title,
            'description' => $oItem->description,
            'keywords' => $oItem->keywords,
        ];
    }
}
