<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Http\Transformers\Common\SourceableTrait;
use App\Models\News;
use App\Services\Image\ImageSize;
use League\Fractal\TransformerAbstract;

class NewsTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;
    use SourceableTrait;

    /**
     * @param News $oItem
     * @return array
     */
    public function transformMention(News $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'position' => trim($oItem->position),
            'name' => $oItem->name,
            'title' => $oItem->title,
        ];
    }

    /**
     * @param News $oItem
     * @return array
     */
    public function transformCard(News $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'position' => trim($oItem->position),
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
            'categories' => $oItem->categories->transform(function ($item) {
                return (new CategoryTransformer())->transformMention($item);
            })->toArray(),
            'image' => $this->newsImage($oItem),
            'statistic' => $this->statistic($oItem, false),
            'published_at_format' => $oItem->published_at->format('d.m.Y'),
            'published_at_day' => $oItem->published_at->format('d'),
            'published_at_month' => $oItem->published_at->translatedFormat('M'),
        ];
    }

    /**
     * @param News $oItem
     * @return array
     */
    public function transformDetail(News $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'position' => trim($oItem->position),
            'name' => $oItem->name,
            'title' => $oItem->title,
            'preview_description' => $oItem->preview_description,
            'categories' => $oItem->categories->transform(function ($item) {
                return (new CategoryTransformer())->transformMention($item);
            })->toArray(),
            'description' => $oItem->description,
            'source' => $this->source($oItem),
            'with_banner' => $oItem->with_banner ? true : false,
            'image' => $this->newsImage($oItem),
            'statistic' => $this->statistic($oItem, true),
            'recommended' => $this->recommended($oItem),
            'published_at_format' => $oItem->published_at->format('d.m.Y'),
            'published_at_day' => $oItem->published_at->format('d'),
            'published_at_month' => $oItem->published_at->format('MMM'),
        ];
    }

    /**
     * @param News $oItem
     * @param bool $setCount
     * @return array
     */
    private function statistic(News $oItem, bool $setCount)
    {
        $oStatistic = $oItem->statistic;
        if ($setCount) {
            visits($oItem)->increment();
        }
        if (!is_null($oStatistic)) {
            return [
                'comments' => $oItem->comments()->count(),
                'views' => (int)$oStatistic->views,
                'likes' => (int)$oStatistic->likes,
            ];
        }
        return [
            'comments' => $oItem->comments()->count(),
            'views' => visits($oItem)->count(),
            'likes' => rand(1, 100),
        ];
    }

    /**
     * @param News $oItem
     * @return array
     */
    private function recommended(News $oItem)
    {
        $oPrevious = null;
        $oNext = null;

        $oPrevious = News::active()
            ->where('published_at', '<=', $oItem->published_at)
            ->where('id', '<>', $oItem->id)
            ->ordered()
            ->first();

        $oNext = News::active()
            ->where('published_at', '>=', $oItem->published_at)
            ->where('id', '<>', $oItem->id)
            ->orderedReserve()
            ->first();

        return [
            'previous' => !is_null($oPrevious) ? $this->transformMention($oPrevious) : null,
            'next' => !is_null($oNext) ? $this->transformMention($oNext) : null,
        ];
    }
}
