<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Direction;
use App\Models\Entertainment;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\Tag;
use App\Services\Image\ImageSize;
use League\Fractal\TransformerAbstract;

class EntertainmentTransformer extends TransformerAbstract
{
    /**
     * @param Entertainment $oItem
     * @return array
     */
    public function transform(Entertainment $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'title' => trim($oItem->title),
            'image' => [
                'default' => $oItem->getImageModelPath(ImageSize::CARD_MD),
            ],
            'images' => $oItem->image_gallery,
            'preview_description' => !is_null($oItem->preview_description)
                ? trim($oItem->preview_description)
                : null,
            'description' => !is_null($oItem->description)
                ? trim($oItem->description)
                : null,
        ];
    }
}
