<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\OrganizationStatistic;
use App\Models\Tour;
use App\Models\User;
use App\Services\Subscription\SubscriptionService;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;

    /**
     * @param User $oItem
     * @return array
     */
    public function transform(User $oItem)
    {
        return [
            'id' => (int)$oItem->id,
            'first_name' => trim($oItem->first_name),
            'last_name' => trim($oItem->last_name),
            'second_name' => trim($oItem->second_name),
            'email' => trim($oItem->email),
            'phone' => trim($oItem->phone),
            'image' => $this->userImage($oItem),
            'subscriber' => false,
            'isAdmin' => $oItem->hasRole(User::ROLE_SUPER_ADMIN) || $oItem->hasRole(User::ROLE_ADMIN),
            'birthday_at' => !is_null($oItem->birthday_at) ? $oItem->birthday_at->format('d.m.Y') : null,
        ];
    }

    /**
     * @param User $oItem
     * @return array
     */
    public function transformFavourites(User $oItem)
    {
        return [
            'tours' => $oItem->favouriteTours->pluck('favoriteable_id')->toArray(),
            'directions' => $oItem->favouriteDirections->pluck('favoriteable_id')->toArray(),
            'organizations' => $oItem->favouriteOrganizations->pluck('favoriteable_id')->toArray(),
            'attractions' => $oItem->favouriteAttractions->pluck('favoriteable_id')->toArray(),
        ];
    }

    /**
     * @param User $oItem
     * @return array
     */
    public function transformSubscriptions(User $oItem)
    {
        return [
            'directions' => $oItem->subscriptionsDirections()->active()->pluck('subscriptionable_id')->toArray(),
            'organizations' => $oItem->subscriptionsOrganizations()->active()->pluck('subscriptionable_id')->toArray(),
        ];
    }

    /**
     * @param User $oItem
     * @return array
     */
    public function transformReservations(User $oItem)
    {
        return [
            'tours' => $oItem->reservations()->active()->pluck('tour_id')->toArray(),
        ];
    }

    /**
     * @param User $oItem
     * @return array
     */
    public function transformVotes(User $oItem)
    {
        return [
            'up' => $oItem->votes()->active()->where('value', 1)->pluck('votable_id')->toArray(),
            'down' => $oItem->votes()->active()->where('value', -1)->pluck('votable_id')->toArray(),
        ];
    }

    /**
     * @param User $oItem
     * @return array
     */
    public function transformComments(User $oItem)
    {
        $oDirections = $oItem->commentsDirections()
            ->where('parent_id', null)
            ->get()
            ->pluck('commentable_id')
            ->toArray();

        $oOrganizations = $oItem->commentsOrganizations()
            ->where('parent_id', null)
            ->get()
            ->pluck('commentable_id')
            ->toArray();

        $oNews = $oItem->commentsNews()
            ->where('parent_id', null)
            ->get()
            ->pluck('commentable_id')
            ->toArray();

        $oAttractions = $oItem->commentsAttractions()
            ->where('parent_id', null)
            ->get()
            ->pluck('commentable_id')
            ->toArray();

        $oArticles = $oItem->commentsArticles()
            ->where('parent_id', null)
            ->get()
            ->pluck('commentable_id')
            ->toArray();

        $oDirections = array_unique($oDirections);
        $oOrganizations = array_unique($oOrganizations);
        $oNews = array_unique($oNews);
        $oAttractions = array_unique($oAttractions);
        $oArticles = array_unique($oArticles);

        sort($oDirections);
        sort($oOrganizations);
        sort($oNews);
        sort($oAttractions);
        sort($oArticles);

        return [
            'directions' => $oDirections,
            'organizations' => $oOrganizations,
            'news' => $oNews,
            'attractions' => $oAttractions,
            'articles' => $oArticles,
        ];
    }

    /**
     * @param User $oItem
     * @return array
     */
    public function transformFavouritesExtended(User $oItem)
    {
        $oTourTransformer = (new TourTransformer());
        $aTours = $oItem->favouriteTours->pluck('favoriteable_id')->toArray();
        $aTours = Tour::whereIn('id', $aTours)->ordered()->get()->transform(function ($item) use ($oTourTransformer) {
            return $oTourTransformer->transformCard($item);
        })->toArray();

        $aDirectories = $oItem->favouriteDirections->pluck('favoriteable_id')->toArray();
        $oDirectionsTransformer = (new DirectionTransformer());
        $aDirectories = Direction::whereIn('id', $aDirectories)->ordered()->get()->transform(function ($item) use ($oDirectionsTransformer) {
            return $oDirectionsTransformer->transformCard($item);
        })->toArray();

        $aOrganizations = $oItem->favouriteOrganizations->pluck('favoriteable_id')->toArray();
        $oOrganizationsTransformer = (new OrganizationTransformer());
        $aOrganizations = Organization::whereIn('id', $aOrganizations)->ordered()->get()->transform(function ($item) use ($oOrganizationsTransformer) {
            return $oOrganizationsTransformer->transformCard($item);
        })->toArray();

        return [
            'tours' => $aTours,
            'directions' => $aDirectories,
            'organizations' => $aOrganizations,
        ];
    }

    /**
     * @param User $oItem
     * @return array
     */
    public function transformSubscriptionsExtended(User $oItem)
    {
        $aDirectories = $oItem->subscriptionsDirections()->active()->pluck('subscriptionable_id')->toArray();
        $oDirectionsTransformer = (new DirectionTransformer());
        $aDirectories = Direction::whereIn('id', $aDirectories)->active()->ordered()->get()->transform(function ($item) use ($oDirectionsTransformer) {
            return $oDirectionsTransformer->transformCard($item);
        })->toArray();

        $aOrganizations = $oItem->subscriptionsOrganizations()->active()->pluck('subscriptionable_id')->toArray();
        $oOrganizationsTransformer = (new OrganizationTransformer());
        $aOrganizations = Organization::whereIn('id', $aOrganizations)->active()->ordered()->get()->transform(function ($item) use ($oOrganizationsTransformer) {
            return $oOrganizationsTransformer->transformCard($item);
        })->toArray();

        return [
            'tours' => [],
            'directions' => $aDirectories,
            'organizations' => $aOrganizations,
        ];
    }


    /**
     * @param User $oItem
     * @return array
     */
    public function transformReservationsExtended(User $oItem)
    {
        $oReservations = $oItem->reservations()->active()->get();

        $aTours = $oReservations->pluck('tour_id')->toArray();

        $oTourTransformer = (new TourTransformer());
        $aTours = Tour::whereIn('id', $aTours)->ordered()->get()->transform(function (Tour $item) use ($oTourTransformer) {
            return $oTourTransformer->transformCard($item);
        })->toArray();

        $oTourReservationTransformer = (new TourReservationTransformer());
        foreach ($aTours as $key => $aTour) {
            $oReservation = $oReservations->where('tour_id', $aTour['id'])->first();
            $aTours[$key]['reservation'] = $oTourReservationTransformer->transform($oReservation);
        }

        return [
            'tours' => $aTours,
        ];
    }
}
