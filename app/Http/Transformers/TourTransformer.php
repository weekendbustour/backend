<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Http\Transformers\Common\ImageTransformerTrait;
use App\Http\Transformers\Common\MultipleableTrait;
use App\Models\Attraction;
use App\Models\Category;
use App\Models\Option;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\TourCancellation;
use App\Models\TourTimeline;
use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class TourTransformer extends TransformerAbstract
{
    use ImageTransformerTrait;
    use MultipleableTrait;

    /**
     * @param Tour $oItem
     * @return array
     */
    public function transformMention(Tour $oItem): array
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'url' => $oItem->getUrl(),
            'title' => trim($oItem->title),
        ];
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    public function transformMentionMultiple(Tour $oItem): array
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'url' => $oItem->getUrl(),
            'title' => trim($oItem->title),
            'announcement' => $this->announcement($oItem),
            'start_at_days' => serviceTransformerTour()->startAtDays($oItem),
            'start_at_month' => serviceTransformerTour()->startAtMonth($oItem),
        ];
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    public function transformCard(Tour $oItem): array
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'title' => trim($oItem->title),
            'preview_description' => trim($oItem->preview_description),
            'image' => $this->tourImage($oItem, 'card'),
            'categories' => $oItem->categories->transform(function ($item) {
                return (new CategoryTransformer())->transformMention($item);
            })->toArray(),
            'tag' => $oItem->tag !== null ? (new TagTransformer())->transform($oItem->tag) : null,
            'departures' => $oItem->departures->transform(function ($item) {
                return (new DepartureTransformer())->transformMention($item);
            })->toArray(),
            'organization' => (new OrganizationTransformer())->transformMention($oItem->organization),
            'direction' => (new DirectionTransformer())->transformMention($oItem->direction),
            'price' => !is_null($oItem->price) ? (new PriceTransformer())->transform($oItem->price) : null,
            'cancellation' => $this->cancellation($oItem),
            'announcement' => $this->announcement($oItem),
            'multiples' => $this->tourMultiplesMention($oItem),
            'start_at' => $oItem->start_at->format('d.m.Y H:i:s'),
            'start_at_format' => $oItem->start_at->format('d.m'),
            'start_at_time' => $oItem->start_at->format('H:i'),
            'start_at_datetime' => $oItem->start_at->format('d.m H:i'),
            'start_at_days' => serviceTransformerTour()->startAtDays($oItem),
            'start_at_month' => serviceTransformerTour()->startAtMonth($oItem),
            'finish_at' => $this->finishAt($oItem, 'd.m.Y H:i:s'),
            'finish_at_format' => $this->finishAt($oItem, 'd.m'),
            'published_at_format' => !is_null($oItem->published_at) ? $oItem->published_at->format('d.m.Y') : null,
        ];
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    public function transformCardWithoutMultiples(Tour $oItem)
    {
        $data = $this->transformCard($oItem);
        $data['multiples'] = [];
        return $data;
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    public function transform(Tour $oItem): array
    {
        return [
            'id' => (int)$oItem->id,
            'name' => $oItem->name,
            'url' => $oItem->getUrl(),
            'title' => trim($oItem->title),
            'preview_description' => trim($oItem->preview_description),
            'description' => trim($oItem->description),
            'parameters' => (new TourParameterTransformer())->transformTourParameters($oItem),
            'parameters_view' => (new TourParameterTransformer())->transformTourParametersForView($oItem),
            'image' => $this->tourImage($oItem),
            'categories' => $oItem->categories->transform(function ($item) {
                return (new CategoryTransformer())->transformMention($item);
            })->toArray(),
            'tag' => $oItem->tag !== null ? (new TagTransformer())->transform($oItem->tag) : null,
            'meta_tag' => $this->metaTag($oItem),
            'entertainments' => $oItem->entertainments->transform(function ($item) {
                return (new EntertainmentTransformer())->transform($item);
            })->toArray(),
            'departures' => $oItem->departures->transform(function ($item) {
                return (new DepartureTransformer())->transform($item);
            })->toArray(),
            'outfits_rent' => $oItem->outfitsOrdered->transform(function ($item) {
                return (new OutfitTransformer())->transformRent($item);
            })->toArray(),
            'departures_empty' => $this->departuresEmpty($oItem),
            //'timelines' => $this->timelines($oItem),
            'organization' => (new OrganizationTransformer())->transformMention($oItem->organization),
            'direction' => (new DirectionTransformer())->transformMention($oItem->direction),
            'price' => !is_null($oItem->price) ? (new PriceTransformer())->transform($oItem->price) : null,
            'cancellation' => $this->cancellation($oItem),
            'announcement' => $this->announcement($oItem),
            'multiples' => $this->tourMultiplesCardWithoutMultiples($oItem),
            'duplicates' => $this->duplicates($oItem),
            'attractions' => $this->attractions($oItem),
            'start_at' => $oItem->start_at->format('d.m.Y H:i:s'),
            'start_at_date' => $oItem->start_at->format('d.m.Y'),
            'start_at_format' => $oItem->start_at->format('d.m'),
            'start_at_time' => $oItem->start_at->format('H:i'),
            'start_at_datetime' => $oItem->start_at->format('d.m H:i'),
            'start_at_days' => serviceTransformerTour()->startAtDays($oItem),
            'start_at_month' => serviceTransformerTour()->startAtMonth($oItem),
            'finish_at' => $this->finishAt($oItem, 'd.m.Y H:i:s'),
            'finish_at_format' => $this->finishAt($oItem, 'd.m'),
            'published_at_format' => !is_null($oItem->published_at) ? $oItem->published_at->format('d.m.Y') : null,
        ];
    }

    /**
     * @param Tour $oItem
     * @return null|array
     */
    private function cancellation(Tour $oItem): ?array
    {
        if (!is_null($oItem->cancellation)) {
            return (new TourCancellationTransformer())->transform($oItem->cancellation);
        }
        if (!$oItem->isActive()) {
            return (new TourCancellationTransformer())->transform((new TourCancellation([
                'type' => $oItem->start_at->startOfDay() >= now()->startOfDay() ? TourCancellation::TYPE_NOT_PUBLISHED : TourCancellation::TYPE_PASSED,
            ])));
        }
        return null;
    }

    /**
     * @param Tour $oItem
     * @return null|array
     */
    private function announcement(Tour $oItem): ?array
    {
        if (!is_null($oItem->announcement)) {
            return (new TourAnnouncementTransformer())->transform($oItem->announcement);
        }
        return null;
    }

    /**
     * @param Tour $oItem
     * @param string $format
     * @return string|null
     */
    private function finishAt(Tour $oItem, string $format = 'd.m'): ?string
    {
        if (is_null($oItem->finish_at)) {
            return null;
        }
        if ($oItem->start_at->diffInDays($oItem->finish_at) < 1) {
            return null;
        }
        return $oItem->finish_at->format($format);
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    public function timelines(Tour $oItem): array
    {
        $aTimelines = $oItem->timelines->transform(function (TourTimeline $item) {
            return (new TourTimelineTransformer())->transform($item);
        })->groupBy('date')->toArray();
        $data = [];
        foreach ($aTimelines as $date => $aTimeline) {
            $array = array_values(collect($aTimeline)->sortBy('type')->toArray());
            $data[$date] = $array;
        }
        return $data;
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    private function duplicates(Tour $oItem): array
    {
        $array = [];
        $this->duplicatesSearch($oItem, $array);
        $aDuplicates = collect($array)->collapse()->toArray();
        sort($aDuplicates);

        if (empty($aDuplicates)) {
            return [];
        }
        if (in_array($oItem->id, $aDuplicates)) {
            $key = array_search($oItem->id, $aDuplicates);
            if ($key !== false) {
                unset($aDuplicates[$key]);
                sort($aDuplicates);
            }
        }
        return Tour::whereIn('id', $aDuplicates)->active()->ordered()->get()->transform(function ($item) {
            return $this->transformCard($item);
        })->toArray();
    }

    /**
     * @param Tour $oItem
     * @param array $array
     */
    private function duplicatesSearch(Tour $oItem, array &$array = [])
    {
        if (is_null($oItem->duplicate_id)) {
            $oDuplicates = $oItem->duplicates;
            $array[$oItem->id] = array_merge([$oItem->id], $oItem->duplicates->pluck('id')->toArray());
            if (count($oDuplicates) !== 0) {
                foreach ($oDuplicates as $oDuplicate) {
                    $this->duplicatesSearchFromParent($oDuplicate, $array);
                }
            }
        } else {
            $oDuplicates = $oItem->duplicates;
            if (count($oDuplicates) !== 0 && !isset($array[$oItem->id])) {
                $array[$oItem->id] = $oDuplicates->pluck('id')->toArray();
                foreach ($oDuplicates as $oDuplicate) {
                    $this->duplicatesSearch($oDuplicate, $array);
                }
            }
            $oParent = $oItem->parentDuplicate;
            if (!is_null($oParent)) {
                $this->duplicatesSearch($oParent, $array);
            }
        }
    }

    /**
     * @param Tour $oItem
     * @param array $array
     */
    private function duplicatesSearchFromParent(Tour $oItem, array &$array = [])
    {
        $oDuplicates = $oItem->duplicates;
        if (count($oDuplicates) !== 0 && !isset($array[$oItem->id])) {
            $array[$oItem->id] = $oDuplicates->pluck('id')->toArray();
            foreach ($oDuplicates as $oDuplicate) {
                $this->duplicatesSearch($oDuplicate, $array);
            }
        }
    }

    /**
     * @param Tour $oItem
     * @return bool
     */
    private function departuresEmpty(Tour $oItem): bool
    {
        return $oItem->departures()->whereNotNull('departure_at')->count() === 0;
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    private function metaTag(Tour $oItem): array
    {
        $oMetaTag = $oItem->metaTag;
        if (!is_null($oMetaTag)) {
            return (new MetaTagTransformer())->transform($oMetaTag);
        }
        return [
            'title' => $oItem->title,
            'description' => $oItem->preview_description,
            'keywords' => null,
        ];
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    private function attractions(Tour $oItem): array
    {
        return $oItem->attractionsActive()->get()->transform(function (Attraction $item) {
            return (new AttractionTransformer())->transformCard($item);
        })->toArray();
    }
}
