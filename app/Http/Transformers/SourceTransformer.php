<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Source;
use League\Fractal\TransformerAbstract;

class SourceTransformer extends TransformerAbstract
{
    /**
     * @param Source $oItem
     * @return array
     */
    public function transform(Source $oItem)
    {
        return [
            'url' => $oItem->url,
            'description' => $oItem->description,
            'organization' => !is_null($oItem->organization_id) ? (new OrganizationTransformer())->transformMention($oItem->organization) : null,
            'published_at_format' => !is_null($oItem->published_at) ? $oItem->published_at->format('d.m.Y') : null,
        ];
    }
}
