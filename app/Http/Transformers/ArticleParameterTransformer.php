<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Cmf\Core\MainController;
use App\Http\Transformers\Common\ParametersTrait;
use App\Http\Transformers\Common\YouTubeParameterTrait;
use App\Models\Article;
use App\Models\Category;
use App\Models\Option;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\TourTimeline;
use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class ArticleParameterTransformer extends TransformerAbstract
{
    use YouTubeParameterTrait;
    use ParametersTrait;

    /**
     * @param Option $oOption
     * @param array $aValues
     * @return array
     */
    public function transform(Option $oOption, array $aValues = []): array
    {
        return $this->commonTransform($oOption, $aValues);
    }

    /**
     * @param Article $oItem
     * @return array
     */
    public function transformArticleParameters(Article $oItem): array
    {
        $aValues = $oItem->values->keyBy('option.name')->toArray();
        $oOptions = Option::active()->purposeArticles()->ordered()->get();
        $aParameters = $this->commonTransformParameters($oOptions, $aValues);
        return $aParameters;
    }
}
