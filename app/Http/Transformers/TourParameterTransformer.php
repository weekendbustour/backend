<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Cmf\Core\MainController;
use App\Http\Transformers\Common\ParametersTrait;
use App\Http\Transformers\Common\YouTubeParameterTrait;
use App\Models\Article;
use App\Models\Category;
use App\Models\Option;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\TourTimeline;
use App\Models\User;
use Illuminate\Support\Str;
use League\Fractal\TransformerAbstract;

class TourParameterTransformer extends TransformerAbstract
{
    use YouTubeParameterTrait;
    use ParametersTrait;

    /**
     * @param Option $oOption
     * @param array $aValues
     * @return array
     */
    public function transform(Option $oOption, array $aValues = []): array
    {
        return $this->commonTransform($oOption, $aValues);
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    public function transformTourParameters(Tour $oItem): array
    {
        $aValues = $oItem->values->keyBy('option.name')->toArray();
        $oOptions = Option::active()->purposeTours()->ordered()->get();
        $aParameters = $this->commonTransformParameters($oOptions, $aValues);
        if (!isset($aParameters[Option::NAME_RESERVATION])) {
            $oOptionReservation = Option::active()->purposeOrganizations()->where('name', Option::NAME_RESERVATION)->first();
            $aOrganizationValues = $oItem->organization->values->keyBy('option.name')->toArray();
            if (!is_null($oOptionReservation) && isset($aOrganizationValues[Option::NAME_RESERVATION])) {
                $aParameters[Option::NAME_RESERVATION] = $this->commonTransform($oOptionReservation, $aOrganizationValues);
            }
        }
        if (!isset($aParameters[Option::NAME_PREPAYMENT_CANCELLATION]) && !is_null($oItem->organization->parameterPrepaymentCancellation)) {
            $oOptionPrepaymentCancellation = Option::active()->purposeOrganizations()->where('name', Option::NAME_PREPAYMENT_CANCELLATION)->first();
            $aParameters[Option::NAME_PREPAYMENT_CANCELLATION] = $this->commonTransform($oOptionPrepaymentCancellation, [
                Option::NAME_PREPAYMENT_CANCELLATION => [
                    'value' => $oItem->organization->parameterPrepaymentCancellation,
                ]
            ]);
        }
        return $aParameters;
    }

    /**
     * @param Tour $oItem
     * @return array
     */
    public function transformTourParametersForView(Tour $oItem)
    {
        $aParameters = $this->transformTourParameters($oItem);
        $aSortParameters = $this->commonTransformParametersForView($aParameters);
        return $aSortParameters;
    }

    /**
     * @param Article $oItem
     * @return array
     */
    public function transformArticleParameters(Article $oItem): array
    {
        $aValues = $oItem->values->keyBy('option.name')->toArray();
        $oOptions = Option::active()->purposeArticles()->ordered()->get();
        $aParameters = $this->commonTransformParameters($oOptions, $aValues);
        return $aParameters;
    }
}
