<?php

declare(strict_types=1);

namespace App\Http\Transformers;

use App\Models\Category;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Departure;
use App\Models\Price;
use App\Models\Tag;
use App\Models\Tour;
use App\Services\Modelable\Priceable;
use League\Fractal\TransformerAbstract;

class PriceTransformer extends TransformerAbstract
{
    /**
     * @param Price $oItem
     * @return array
     */
    public function transform(Price $oItem)
    {
        return [
            'value' => $oItem->value,
            'value_description' => $oItem->value_description,
            'discount' => $oItem->discount,
            'discount_description' => $oItem->discount_description,
            'discount_before_at_format' => !is_null($oItem->discount_before_at) ? $oItem->discount_before_at->format('d.m.Y') : null,
            'prepayment' => $oItem->prepayment,
            'prepayment_description' => $oItem->prepayment_description,
        ];
    }
}
