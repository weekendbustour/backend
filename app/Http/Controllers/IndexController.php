<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Direction;
use App\Models\Organization;
use Illuminate\Http\Request;

class IndexController extends FrontendController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contacts(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function about(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function help(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function terms(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function privacy(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @param null|int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function news(Request $request, ?int $id = null)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @param string|null $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function name(Request $request, ?string $name)
    {
        $oOrganizations = Organization::active()->ordered()->get();
        foreach ($oOrganizations as $oOrganization) {
            if ($name === $oOrganization->name) {
                $this->seo('organization', $oOrganization);
                // 'organization' => (new OrganizationTransformer())->transformDetail($oOrganization),
                return view('app.react', ['aPage' => ['organization' => ['id' => $oOrganization->id]]]);
            }
        }
        $oDirections = Direction::active()->ordered()->get();
        foreach ($oDirections as $oDirection) {
            if ($name === $oDirection->name) {
                $this->seo('direction', $oDirection);
                // 'organization' => (new OrganizationTransformer())->transformDetail($oOrganization),
                return view('app.react', ['aPage' => ['direction' => ['id' => $oDirection->id]]]);
            }
        }
        return view('app.react');
    }
}
