<?php

declare(strict_types=1);

namespace App\Http\Controllers\Socialite;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\Socialite\FacebookAccountService;
use App\Services\Socialite\GoogleAccountService;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthFacebookController extends Controller
{
    /**
     * Create a redirect method to google api.
     *
     * @return void
     */
    public function redirect()
    {
        return Socialite::with('facebook')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @param FacebookAccountService $service
     * @return \Illuminate\Http\RedirectResponse|array
     */
    public function callback(FacebookAccountService $service)
    {
        $oUser = $service->createOrGetUser(Socialite::with('facebook')->user());
        if (!is_null($oUser)) {
            if (count($oUser->tokens) !== 0) {
                $oUser->tokens()->delete();
            }
            $token = $oUser->createToken(User::TOKEN_AUTH_NAME);
            $token = $token->plainTextToken;
            return response()->redirectTo(route('app.index', ['token' => $token]));
        }
        return responseCommon()->success();
    }
}
