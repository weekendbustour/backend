<?php

declare(strict_types=1);

namespace App\Http\Controllers\Socialite;

use App\Http\Controllers\Controller;
use App\Models\Tour;
use App\Models\User;
use App\Services\Socialite\VkontakteAccountService;
use App\Services\Vk\VkWall;
use App\Services\Vk\VkMarket;
use App\VkontakteBot\Response\ActionResponse;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Routing\Controller as BaseController;
use VK\Client\VKApiClient;
use VK\OAuth\Scopes\VKOAuthUserScope;
use VK\OAuth\VKOAuth;
use VK\OAuth\VKOAuthDisplay;
use VK\OAuth\VKOAuthResponseType;
use Asil\VkMarket\VkConnect;
use Asil\VkMarket\VkServiceDispatcher;
use Asil\VkMarket\Model\Photo;
use Asil\VkMarket\Model\Product;

class SocialAuthVkontakteController extends BaseController
{
    /**
     * Create a redirect method to google api.
     *
     * @return void
     */
    public function redirectToProvider()
    {
        return Socialite::with('vkontakte')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @param Request $request
     * @param VkontakteAccountService $service
     * @return \Illuminate\Http\RedirectResponse|array
     */
    public function handleProviderCallback(Request $request, VkontakteAccountService $service)
    {
        $oUser = $service->createOrGetUser($request);
        if (!is_null($oUser)) {
            if (count($oUser->tokens) === 0) {
                $token = $oUser->createToken(User::TOKEN_AUTH_NAME);
                $token = $token->plainTextToken;
            } else {
                $oUser->tokens()->delete();
                $token = $oUser->createToken(User::TOKEN_AUTH_NAME);
                $token = $token->plainTextToken;
            }
            return response()->redirectTo(route('app.index', ['token' => $token]));
        }
        return responseCommon()->success();
    }

    /**
     * @param Request $request
     * @param VkontakteAccountService $service
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function handleProviderCallbackPost(Request $request, VkontakteAccountService $service)
    {
        $oUser = $service->createOrGetUser($request);
        if (!is_null($oUser)) {
            if (count($oUser->tokens) === 0) {
                $token = $oUser->createToken(User::TOKEN_AUTH_NAME);
                $token = $token->plainTextToken;
            } else {
                $oUser->tokens()->delete();
                $token = $oUser->createToken(User::TOKEN_AUTH_NAME);
                $token = $token->plainTextToken;
            }
            return responseCommon()->success([
                'data' => [
                    'token' => $token,
                ]
            ], 'Вход успешно выполнен');
        }
        return responseCommon()->apiError([], 'Ошибка авторизации');
    }



    public function code(Request $request)
    {
        if (!$request->exists('code')) {
            slackDebug('NOT CODE');
            $oauth = new VKOAuth('5.110');
            $client_id = env('VK_STANDALONE_APP_ID');
            $redirect_uri = route('socialite.vkontakte.wall');
            //$redirect_uri = 'https://oauth.vk.com/blank.html';
            $display = VKOAuthDisplay::PAGE;
            $scope = [
                VKOAuthUserScope::OFFLINE,
                VKOAuthUserScope::WALL,
                VKOAuthUserScope::GROUPS,
                VKOAuthUserScope::MARKET,
                VKOAuthUserScope::PHOTOS,
            ];
            $state = 'secret_state_code';
            $revoke_auth = true;
            $browser_url = $oauth->getAuthorizeUrl(VKOAuthResponseType::TOKEN, $client_id, $redirect_uri, $display, $scope, $state, null, $revoke_auth);
            slackDebug($browser_url);
            return response()->redirectTo($browser_url);
        }



        return view('app.react');
    }

    private function addCart()
    {

        $accessToken = env('VK_STANDALONE_ACCESS_TOKEN');
        $ownerId = -1; // идентификатор владельца группы
        $groupId = env('VKONTAKTE_GROUP_ID'); // идентификатор группы

        $connect = new VkConnect($accessToken, $groupId, $ownerId);
        $vkService = new VkServiceDispatcher($connect);

        $product = new Product('Товар 1', 'Описание товара...', 60, 212);
        $photo = new Photo();

        $photo->createMainPhoto(public_path('/img/about.jpg'));
        $photo->createAdditionalPhoto([
            public_path('/img/trees.jpg'),
        ]);
        $id = $vkService->addProduct($product, $photo);
        dd($id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function wall(Request $request)
    {
        if ($request->exists('market')) {
//            (new VkMarket())
//                ->product('Название товара', 'Описание товара', 800, 121)
//                ->attachUrl('https://weekendbustour.ru/tour/47/fitnes-tur-adygeya-guamskoe-ushchele')
//                ->attachPhoto(public_path('img/about.jpg'))
//                //->attachAlbum('Архыз', public_path('img/about.jpg'))
//                //->attachAlbum('Название организатора')
//                ->send();

            /** @var Tour $oTour */
            $oTour = Tour::find(50);
            (new VkMarket($oTour))
                ->product($oTour->title, $oTour->title . "\n\n" . $oTour->preview_description . "\n\n" . $oTour->getUrl(), 800, $oTour->price->value)
                ->attachUrl($oTour->getUrl())
                ->attachPhoto($oTour->getImageModelOriginalStorage($oTour->modelImages()->where('is_main', 1)->first()))
                //->attachAlbum('Архыз', public_path('img/about.jpg'))
                //->attachAlbum('Название организатора')
                ->send();
        }
        if ($request->exists('post')) {
            /** @var Tour $oTour */
            $oTour = Tour::find(50);
            (new VkWall($oTour))
                ->post($oTour->title . "\n\n" . $oTour->preview_description . "\n\n" . $oTour->getUrl())
                ->attachUrl($oTour->getUrl())
                ->attachPhoto($oTour->getImageModelOriginalStorage($oTour->modelImages()->where('is_main', 1)->first()))
                ->send();
        }
        if ($request->exists('token')) {
            $code = $request->get('token');
            $client_id = env('VK_APP_KEY');
            $client_secret = env('VK_APP_SECRET');
            $redirect_uri = route('socialite.vkontakte.wall');
            //$redirect_uri = 'https://oauth.vk.com/blank.html';
            $oauth = new VKOAuth();
            //dd($client_id, $client_secret, $redirect_uri, $code);
            $response = $oauth->getAccessToken($client_id, $client_secret, $redirect_uri, $code);
            dd($response);
        }
        if ($request->exists('access_token')) {
            slackDebug(json_encode($request->all()));
            $actionResponse = new ActionResponse($request, new VKApiClient('5.110'), $request->get('access_token'));
            $actionResponse->post('текст');
            //$actionResponse->market('текст');
            dd('hi');
        }
        if ($request->exists('code')) {
            slackDebug('CODE');
            $code = $request->get('code');
            slackDebug($code);
            $oauth = new VKOAuth('5.110');
            $client_id = env('VK_STANDALONE_APP_ID');
            $client_secret = env('VK_STANDALONE_SECRET');
            $redirect_uri = route('socialite.vkontakte.wall');

            $response = $oauth->getAccessToken($client_id, $client_secret, $redirect_uri, $code);
            dd($response);
            $access_token = $response['access_token'];
            slackDebug($access_token);

            $actionResponse = new ActionResponse($request, new VKApiClient('5.110'), $access_token);
            $actionResponse->post('текст');
        }
        return view('app.react');
    }
}
