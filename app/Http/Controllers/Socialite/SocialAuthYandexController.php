<?php

declare(strict_types=1);

namespace App\Http\Controllers\Socialite;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\Socialite\VkontakteAccountService;
use App\Services\Socialite\YandexAccountService;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthYandexController extends Controller
{
    /**
     * Create a redirect method to google api.
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect()
    {
        return Socialite::with('yandex')->redirect();
    }

    /**
     * Return a callback method from google api.
     *
     * @param Request $request
     * @param YandexAccountService $service
     * @return \Illuminate\Http\RedirectResponse|array
     */
    public function handleProviderCallback(Request $request, YandexAccountService $service)
    {
        $oUser = $service->createOrGetUser(Socialite::with('yandex')->user());
        if (!is_null($oUser)) {
            if (count($oUser->tokens) !== 0) {
                $oUser->tokens()->delete();
            }
            $token = $oUser->createToken(User::TOKEN_AUTH_NAME);
            $token = $token->plainTextToken;
            return response()->redirectTo(route('app.index', ['token' => $token]));
        }
        return responseCommon()->success();
    }
}
