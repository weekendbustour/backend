<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Tour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class TourController extends FrontendController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tours(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function tour(Request $request, int $id)
    {
        /** @var null|Tour $oItem */
        $oItem = Tour::activePassed()->where('id', $id)->first();
        if (is_null($oItem)) {
            if ($this->isPrivateShow($request)) {
                View::share('isPrivateShow', true);
                $oItem = Tour::find($id);
                if (is_null($oItem)) {
                    abort(404);
                }
            } else {
                abort(404);
            }
        }
        $this->seo(__FUNCTION__, $oItem);
        return view('app.react');
    }
}
