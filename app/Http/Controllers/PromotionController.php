<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Promotion;
use Illuminate\Http\Request;

class PromotionController extends FrontendController
{
    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function promotion(Request $request, int $id)
    {
        $oPromotion = Promotion::where('id', $id)->first();
        if (is_null($oPromotion)) {
            abort(404);
        }
        visits($oPromotion, 'promotion')->increment();

        return redirect()->to($oPromotion->url);
    }
}
