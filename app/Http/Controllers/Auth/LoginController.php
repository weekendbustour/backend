<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\Toastr\Toastr;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Определение если Админский вход
     *
     * @var bool
     */
    protected $isAdmin = false;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')
            ->except('logout')
            ->except('logoutAdmin');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'email';
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $this->isAdmin = $request->has('adminToken');

        $this->clearLoginAttempts($request);

        $response = new JsonResponse();
        $response->setData([
            'success' => true,
            'redirect' => $request->get('backTo') ?? redirect()->back()->getTargetUrl(),
        ]);

        return $this->authenticated($request, $this->guard()->user()) ?: $response;
    }

    /**
     * The user has been authenticated.
     *
     * @param \Illuminate\Http\Request $request
     * @param User $user
     * @return mixed
     */
    protected function authenticated(Request $request, User $user)
    {
        if (!$user->hasRole(User::ROLE_SUPER_ADMIN) && !$user->hasRole(User::ROLE_ADMIN)) {
            $this->guard()->logout();

            $request->session()->invalidate();

            (new Toastr('Вход доступен только для Администраторов'))->error(false);

            return $this->loggedOut($request) ?: responseCommon()->success([
                'redirect' => url('/admin'),
            ]);
        }
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\View\View
     */
    public function showAdminLoginForm()
    {
        return view('cmf.auth.login');
    }

    public function showActivateForm()
    {
        return view('cmf.auth.login');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function logoutAdmin(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: responseCommon()->success([
            'redirect' => url('/'),
        ]);
    }
}
