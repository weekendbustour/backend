<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class OrganizationController extends FrontendController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function organizations(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function organization(Request $request, int $id)
    {
        $oOrganization = Organization::active()->where('id', $id)->first();
        if (is_null($oOrganization)) {
            if ($this->isPrivateShow($request)) {
                View::share('isPrivateShow', true);
                $oOrganization = Organization::find($id);
                if (is_null($oOrganization)) {
                    abort(404);
                }
            } else {
                abort(404);
            }
        }
        $this->seo(__FUNCTION__, $oOrganization);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function organizationArticles(Request $request, int $id)
    {
        $oOrganization = Organization::active()->where('id', $id)->first();
        if (is_null($oOrganization)) {
            if ($this->isPrivateShow($request)) {
                View::share('isPrivateShow', true);
                $oOrganization = Organization::find($id);
                if (is_null($oOrganization)) {
                    abort(404);
                }
            } else {
                abort(404);
            }
        }
        $this->seo(__FUNCTION__, $oOrganization);
        return view('app.react');
    }
}
