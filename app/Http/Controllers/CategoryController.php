<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends FrontendController
{
    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function category(Request $request, int $id)
    {
        $oCategory = Category::where('id', $id)->first();
        if (is_null($oCategory)) {
            abort(404);
        }
        visits($oCategory, 'category')->increment();
        $this->seo(__FUNCTION__, $oCategory);
        return view('app.react');
    }
}
