<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Promotion;

use App\Http\Transformers\PromotionTransformer;
use App\Models\Promotion;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Show
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oPromotion = Promotion::find($id);
        $aPromotion = (new PromotionTransformer())->transform($oPromotion);

        visits($oPromotion, 'promotion')->increment();

        ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aPromotion,
        ]);
    }
}
