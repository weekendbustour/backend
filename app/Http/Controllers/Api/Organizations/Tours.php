<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Organizations;

use App\Http\Transformers\TourTransformer;
use App\Models\Organization;
use App\Models\Tour;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Tours
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        /** @var Organization $oOrganization */
        $oOrganization = Organization::find($id);
        $date = Carbon::parse($request->get('date'));
        $aTours = $oOrganization->toursActive()->whereBetween('start_at', [
            $date->copy()->startOfDay(),
            $date->copy()->endOfDay(),
        ])->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aTours,
        ]);
    }
}
