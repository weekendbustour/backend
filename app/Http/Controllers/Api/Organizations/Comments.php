<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Organizations;

use App\Http\Transformers\CommentTransformer;
use App\Models\Comment;
use App\Models\Organization;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Comments
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oOrganization = Organization::find($id);
        $aComments = $oOrganization->approvedComments()->where('parent_id', null)->with('approvedChildren')->get()->transform(function (Comment $item) {
            return (new CommentTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aComments,
        ]);
    }
}
