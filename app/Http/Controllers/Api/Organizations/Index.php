<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Organizations;

use App\Http\Transformers\OrganizationTransformer;
use App\Models\Organization;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Index
{
    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function __invoke(Request $request)
    {
        $aOrganizations = Organization::with(['images', 'departures', 'toursActive', 'ratings'])->active()->orderedRate()->get()->transform(function (Organization $item) {
            return (new OrganizationTransformer())->transformCard($item);
        })->toArray();

        ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aOrganizations,
        ]);
    }
}
