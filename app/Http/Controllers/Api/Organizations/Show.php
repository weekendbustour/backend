<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Organizations;

use App\Http\Transformers\OrganizationTransformer;
use App\Models\Organization;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Show
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oOrganization = Organization::find($id);
        $aOrganization = (new OrganizationTransformer())->transformDetail($oOrganization);

        visits($oOrganization, 'organization')->increment();

        ga(__FUNCTION__, $oOrganization);

        return responseCommon()->apiSuccess([
            'data' => $aOrganization,
        ]);
    }
}
