<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Tours;

use App\Http\Transformers\TourTransformer;
use App\Models\Tour;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Show
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oTour = Tour::find($id);
        if (is_null($oTour)) {
            return responseCommon()->apiError([
                'redirect' => '/',
            ], null, 400);
        }

        $aTour = (new TourTransformer())->transform($oTour);

        visits($oTour, 'tour')->increment();
        ga(__FUNCTION__, $oTour);

        return responseCommon()->apiSuccess([
            'data' => $aTour,
        ]);
    }
}
