<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Tours;

use App\Http\Transformers\TourTransformer;
use App\Models\Tour;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Timelines
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oTour = Tour::find($id);

        $aTimelines = (new TourTransformer())->timelines($oTour);

        return responseCommon()->apiSuccess([
            'data' => $aTimelines,
        ]);
    }
}
