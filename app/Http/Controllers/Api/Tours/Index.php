<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Tours;

use App\Http\Transformers\TourTransformer;
use App\Models\Tour;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Index
{
    const LIMIT = 12;

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function __invoke(Request $request)
    {
        $page = $request->exists('page') ? (int)$request->get('page') : 1;

        $oQuery = Tour::active()->orderedNearest();
        $oItems = serviceTransformerTour()->queryMultiple($oQuery);
        $oResult = serviceTransformerTour()->returnPaginate($oItems->values(), $request, self::LIMIT, $oItems->count());
        //->activeByDirection()
        //->activeByOrganization()

        //$oResult = $oQuery->orderedNearest()->paginate(self::LIMIT);
        $aTours = $oResult->transform(function (Tour $item) {
            return (new TourTransformer())->transformCard($item);
        })->toArray();
        $aTours = collect($aTours)->values()->toArray();

        $pagination = [
            'total' => $oResult->total(),
            'page' => (int)$page,
            'limit' => $oResult->perPage(),
        ];
        $pagination['has_more_pages'] = $oResult->hasMorePages();

        ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aTours,
            'pagination' => $pagination,
        ]);
    }
}
