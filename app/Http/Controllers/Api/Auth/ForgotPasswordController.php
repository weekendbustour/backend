<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Auth;

use App\Events\Auth\RegisteredEvent;
use App\Events\Auth\ResetPasswordEvent;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;

class ForgotPasswordController extends Controller
{
    use SendsPasswordResetEmails;

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
        ]);
        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        }

        $oUser = User::where('email', $request->get('email'))->first();

        if (is_null($oUser)) {
            return responseCommon()->validationMessages(null, [
                'email' => 'Пользователь с указанным email не найден.',
            ]);
        }

        event(new ResetPasswordEvent($oUser));

        return responseCommon()->apiSuccess([], 'Ссылка успешно отправлена.');
//        $response = Password::RESET_LINK_SENT . '3';
//
//        if ($response == Password::RESET_LINK_SENT) {
//
//        } else {
//            return responseCommon()->apiError([], 'Ошибка отправки. Попробуйте попытку позже.');
//            return responseCommon()->validationMessages(null, [
//                'email' => 'Ошибка отправки. Попробуйте попытку позже.'
//            ]);
//        }
    }
}
