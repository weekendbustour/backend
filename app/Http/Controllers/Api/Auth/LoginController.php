<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        //$request->session()->regenerate();

        //$this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

    /**
     * @param Request $request
     * @param string $hash
     * @return array
     */
    public function loginByHash(Request $request, string $hash)
    {
        $oUser = $this->getUserByHash($hash);

        if (is_null($oUser)) {
            return responseCommon()->error();
        }

        return $this->authenticated($request, $oUser);
    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param User $oUser
     * @return array
     */
    protected function authenticated(Request $request, $oUser)
    {
        //$oUser->hash = Crypt::encrypt($oUser->email);
        if (count($oUser->tokens) === 0) {
            $token = $oUser->createToken(User::TOKEN_AUTH_NAME);
            $token = $token->plainTextToken;
        } else {
            $oUser->tokens()->delete();
            $token = $oUser->createToken(User::TOKEN_AUTH_NAME);
            $token = $token->plainTextToken;
        }
        return responseCommon()->success([
            'data' => [
                'token' => $token,
            ]
        ], 'Вход успешно выполнен');
    }

    /**
     * @param string $hash
     * @return mixed
     */
    public function getUserByHash(string $hash)
    {
        $email = Crypt::decrypt($hash);
        /** @var User $oUser */
        $oUser = User::where('email', $email)->first();
        if (is_null($oUser)) {
            return null;
        }
        return $oUser;
    }
}
