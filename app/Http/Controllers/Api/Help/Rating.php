<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Help;

use App\Models\FeedbackRating;
use App\Services\Notification\Slack\SlackFeedbackRatingNotification;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Rating
{
    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function __invoke(Request $request)
    {
        $validated = $request->validated();
        $oItem = FeedbackRating::create([
            'user_id' => $request->exists('user_id') ? $request->get('user_id') : null,
            'description' => $validated['description'],
            'rating' => $validated['rating'],
            'email' => $validated['email'],
        ]);

        (new SlackFeedbackRatingNotification())->send($validated['email'] . ' : ' . $validated['description'] . ' : ' . $validated['rating']);

        return responseCommon()->apiSuccess([], 'Ваше сообщение успешно отправлено');
    }
}
