<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Help;

use App\Models\FeedbackOffer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Offer
{
    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function __invoke(Request $request)
    {
        $validated = $request->validated();
        $oItem = FeedbackOffer::create([
            'user_id' => $request->exists('user_id') ? $request->get('user_id') : null,
            'description' => $validated['description'],
        ]);

        return responseCommon()->apiSuccess([], 'Данные успешно отправлены');
    }
}
