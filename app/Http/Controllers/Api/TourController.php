<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\TourTransformer;
use App\Http\Controllers\Controller;
use App\Models\Tour;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class TourController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class TourController extends ApiController
{
    use Helpers;

    const LIMIT = 12;

    /**
     * @param Request $request
     * @return array
     */
    public function tours(Request $request)
    {
        $page = $request->exists('page') ? (int)$request->get('page') : 1;

        $oQuery = Tour::active()->orderedNearest();
        $oItems = serviceTransformerTour()->queryMultiple($oQuery);
        $oResult = serviceTransformerTour()->returnPaginate($oItems->values(), $request, self::LIMIT, $oItems->count());
        //->activeByDirection()
        //->activeByOrganization()

        //$oResult = $oQuery->orderedNearest()->paginate(self::LIMIT);
        $aTours = $oResult->transform(function (Tour $item) {
            return (new TourTransformer())->transformCard($item);
        })->toArray();
        $aTours = collect($aTours)->values()->toArray();

        $pagination = [
            'total' => $oResult->total(),
            'page' => (int)$page,
            'limit' => $oResult->perPage(),
        ];
        $pagination['has_more_pages'] = $oResult->hasMorePages();

        $this->ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aTours,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param Collection $oItems
     * @param Request $request
     * @param int|null $count
     * @return LengthAwarePaginator
     */
    protected function returnPaginate(Collection $oItems, Request $request, ?int $count = null): LengthAwarePaginator
    {
        $page = $request->exists('page') ? $request->get('page') - 1 : 0;
        if (is_null($count)) {
            $count = $oItems->count();
        }
        $oItems = $oItems->slice($page * self::LIMIT)->take(self::LIMIT);
        return new LengthAwarePaginator($oItems, $count, self::LIMIT);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function tour(Request $request, int $id)
    {
        $oTour = Tour::find($id);
        if (is_null($oTour)) {
            return responseCommon()->apiError([
                'redirect' => '/',
            ], null, 400);
        }

        $aTour = (new TourTransformer())->transform($oTour);

        visits($oTour, 'tour')->increment();
        $this->ga(__FUNCTION__, $oTour);

        return responseCommon()->apiSuccess([
            'data' => $aTour,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function timelines(Request $request, int $id): array
    {
        $oTour = Tour::find($id);

        $aTimelines = (new TourTransformer())->timelines($oTour);

        return responseCommon()->apiSuccess([
            'data' => $aTimelines,
        ]);
    }
}
