<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\CommentTransformer;
use App\Http\Transformers\DirectionTransformer;
use App\Http\Transformers\TourTransformer;
use App\Models\Comment;
use App\Models\Direction;
use App\Models\Tour;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * Class DirectionController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class DirectionController extends ApiController
{
    use Helpers;

    /**
     * @param Request $request
     * @return array
     */
    public function directions(Request $request)
    {
        $aDirections = Direction::active()->ordered()->get()->transform(function (Direction $item) {
            return (new DirectionTransformer())->transformCard($item);
        })->toArray();

        $this->ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aDirections,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function comments(Request $request, $id)
    {
        $oDirection = Direction::find($id);
        $aComments = $oDirection->approvedComments()->where('parent_id', null)->with('approvedChildren')->get()->transform(function (Comment $item) {
            return (new CommentTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aComments,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function tours(Request $request, int $id)
    {
        /** @var Direction $oDirection */
        $oDirection = Direction::find($id);
        $date = Carbon::parse($request->get('date'));
        $aTours = $oDirection->toursActive()->whereBetween('start_at', [
            $date->copy()->startOfDay(),
            $date->copy()->endOfDay(),
        ])->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aTours,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function direction(Request $request, $id)
    {
        $oDirection = Direction::find($id);
        $aDirection = (new DirectionTransformer())->transformDetail($oDirection);

        visits($oDirection, 'direction')->increment();

        $this->ga(__FUNCTION__, $oDirection);

        return responseCommon()->apiSuccess([
            'data' => $aDirection,
        ]);
    }
}
