<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\CategoryTransformer;
use App\Http\Transformers\DepartureTransformer;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Transformers\CommentTransformer;
use App\Http\Transformers\DirectionTransformer;
use App\Http\Transformers\OrganizationTransformer;
use App\Http\Transformers\UserTransformer;
use App\Http\Transformers\TourTransformer;
use App\Http\Controllers\Api\User\UserCommentsController;
use App\Http\Controllers\Api\User\UserFavouritesController;
use App\Http\Controllers\Api\User\UserReservationsController;
use App\Http\Controllers\Api\User\UserSubscriptionsController;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Departure;
use App\Models\Direction;
use App\Models\Image;
use App\Models\News;
use App\Models\Organization;
use App\Models\Subscription;
use App\Models\Tour;
use App\Models\TourReservation;
use App\Models\User;
use App\Services\Image\ImageType;
use App\Services\Image\Upload\ImageUploadModelService;
use App\Services\ImageService;
use App\Services\Notification\Slack\SlackReservationsNotification;
use App\Services\Subscription\SubscriptionService;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserController extends ApiController
{
    use Helpers;

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function user(Request $request)
    {
        /** @var User $oUser */
        $oUser = $request->user();

        if (is_null($oUser)) {
            $this->response->error('Пользователь не авторизован.', 401);
        }

        if (count($oUser->tokens) === 0) {
            $this->response->error('Пользователь не авторизован.', 401);
        }

        $aItem = (new UserTransformer())->transform($oUser);

        return $this->response->array([
            'data' => $aItem,
        ]);
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable|User $oUser
     * @return array
     */
    public function userData($oUser): array
    {
        $aItem = (new UserTransformer())->transform($oUser);
        $request = new Request();
        $request->merge([
            'user_id' => $oUser->id,
        ]);
        $aItem['favourites'] = (new UserFavouritesController())->userFavourites($request)['data'];
        $aItem['comments'] = (new UserCommentsController())->userComments($request)['data'];
        $aItem['subscriptions'] = (new UserSubscriptionsController())->userSubscriptions($request)['data'];
        $aItem['reservations'] = (new UserReservationsController())->userReservations($request)['data'];
        return responseCommon()->apiSuccess([
            'data' => $aItem,
        ]);
    }
}
