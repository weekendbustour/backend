<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\News;

use App\Http\Transformers\NewsTransformer;
use App\Models\News;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Index
{
    const LIMIT = 20;

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function __invoke(Request $request)
    {
        $page = $request->exists('page') ? (int)$request->get('page') : 1;

        $oQuery = News::active()->ordered()->paginate(self::LIMIT);

        $aNews = $oQuery->transform(function (News $item) {
            return (new NewsTransformer())->transformCard($item);
        })->toArray();

        $pagination = [
            'total' => $oQuery->total(),
            'page' => (int)$page,
            'limit' => $oQuery->perPage(),
        ];
        $pagination['has_more_pages'] = $oQuery->hasMorePages();

        ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aNews,
            'pagination' => $pagination,
        ]);
    }
}
