<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\News;

use App\Http\Transformers\NewsTransformer;
use App\Models\News;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Show
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oNews = News::find($id);
        $aNews = (new NewsTransformer())->transformDetail($oNews);

        visits($oNews, 'news')->increment();

        ga(__FUNCTION__, $oNews);

        return responseCommon()->apiSuccess([
            'data' => $aNews,
        ]);
    }
}
