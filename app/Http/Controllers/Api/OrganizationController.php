<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\CommentTransformer;
use App\Http\Transformers\OrganizationTransformer;
use App\Http\Transformers\TourTransformer;
use App\Models\Comment;
use App\Models\Organization;
use App\Models\Tour;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * Class OrganizationController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class OrganizationController extends ApiController
{
    use Helpers;

    /**
     * @param Request $request
     * @return array
     */
    public function organizations(Request $request)
    {
        $aOrganizations = Organization::with(['images', 'departures', 'toursActive', 'ratings'])->active()->orderedRate()->get()->transform(function (Organization $item) {
            return (new OrganizationTransformer())->transformCard($item);
        })->toArray();

        $this->ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aOrganizations,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function organization(Request $request, int $id)
    {
        $oOrganization = Organization::find($id);
        $aOrganization = (new OrganizationTransformer())->transformDetail($oOrganization);

        visits($oOrganization, 'organization')->increment();

        $this->ga(__FUNCTION__, $oOrganization);

        return responseCommon()->apiSuccess([
            'data' => $aOrganization,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function comments(Request $request, int $id)
    {
        $oOrganization = Organization::find($id);
        $aComments = $oOrganization->approvedComments()->where('parent_id', null)->with('approvedChildren')->get()->transform(function (Comment $item) {
            return (new CommentTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aComments,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function tours(Request $request, int $id)
    {
        /** @var Organization $oOrganization */
        $oOrganization = Organization::find($id);
        $date = Carbon::parse($request->get('date'));
        $aTours = $oOrganization->toursActive()->whereBetween('start_at', [
            $date->copy()->startOfDay(),
            $date->copy()->endOfDay(),
        ])->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aTours,
        ]);
    }
}
