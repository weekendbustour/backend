<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Category;

use App\Http\Transformers\CategoryTransformer;
use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Show
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oCategory = Category::find($id);
        $aCategory = (new CategoryTransformer())->transformDetail($oCategory);

        visits($oCategory, 'category')->increment();

        ga(__FUNCTION__, $oCategory);

        return responseCommon()->apiSuccess([
            'data' => $aCategory,
        ]);
    }
}
