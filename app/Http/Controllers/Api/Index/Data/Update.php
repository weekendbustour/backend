<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Index\Data;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Update
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        (new Index())->__invoke($request);
        return responseCommon()->apiSuccess([]);
    }
}
