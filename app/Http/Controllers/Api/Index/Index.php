<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Index;

use Dingo\Api\Routing\Helpers;

class Index
{
    /**
     * @return string
     */
    public function __invoke()
    {
        return '200';
    }
}
