<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\ArticleTransformer;
use App\Http\Transformers\CategoryTransformer;
use App\Http\Transformers\DepartureTransformer;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Transformers\CommentTransformer;
use App\Http\Transformers\DirectionTransformer;
use App\Http\Transformers\NewsTransformer;
use App\Http\Transformers\OrganizationTransformer;
use App\Http\Transformers\PromotionTransformer;
use App\Http\Transformers\QuestionTransformer;
use App\Http\Transformers\UserTransformer;
use App\Http\Transformers\TourTransformer;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Departure;
use App\Models\Direction;
use App\Models\News;
use App\Models\Organization;
use App\Models\Promotion;
use App\Models\Question;
use App\Models\Tour;
use App\Models\User;
use App\Services\Search\SearchService;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Class IndexController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class IndexController extends ApiController
{
    use Helpers;

    /**
     * @return \Dingo\Api\Http\Response
     */
    public function index()
    {
        return $this->response->noContent();
    }

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function user(Request $request)
    {
        $controller = new LoginController();

        if ($request->has('hash') && $request->get('hash') !== 'null') {
            $result = $controller->loginByHash($request, $request->get('hash'));

            if (!$result['success']) {
                $this->response->error('Пользователь не авторизован.', 401);
            }

            /** @var User $oUser */
            $oUser = $controller->getUserByHash($request->get('hash'));

            if (is_null($oUser)) {
                $this->response->error('Пользователь не авторизован.', 401);
            }
            $token = $oUser->createToken(User::TOKEN_AUTH_NAME);

            $aItem = (new UserTransformer())->transform($oUser);

            return $this->response->array([
                'data' => $aItem,
                'token' => $token->plainTextToken,
            ]);
        }
        $this->response->error('Пользователь не авторизован.', 401);

        return $this->response->array([]);
    }

    /**
     * @param Request $request
     * @return array|\Dingo\Api\Http\Response
     */
    public function data(Request $request)
    {
        $oSearchService = (new SearchService());
        $aOrders = $oSearchService->orders();
        $aSearchOrders = [];
        foreach ($aOrders as $key => $order) {
            $aSearchOrders[] = [
                'id' => $key,
                'label' => $order,
            ];
        }

        $aSearchDirections = remember('data:search:directions', function () {
            return $this->dataSearchDirections();
        });

        $aSearchCategories = remember('data:search:categories', function () {
            return $this->dataSearchCategories();
        });

        $aSearchOrganizations = remember('data:search:organizations', function () {
            return $this->dataSearchOrganizations();
        });

        $aSearchDepartures = remember('data:search:departures', function () {
            return $this->dataSearchDepartures();
        });

        $aTours = remember('data:tours', function () {
            return $this->dataTours();
        });

        $aOrganizations = remember('data:organizations', function () {
            return $this->dataOrganizations();
        });

        $aDirections = remember('data:directions', function () {
            return $this->dataDirections();
        });

        $aNews = remember('data:news', function () {
            return $this->dataNews();
        });

        $aArticles = remember('data:articles', function () {
            return $this->dataArticles();
        });

        $aPromotions = remember('data:promotions', function () {
            return $this->dataPromotions();
        });

        $aQuestions = remember('data:questions', function () {
            return $this->dataQuestions();
        });

        $aTitles = remember('data:titles', function () {
            return $this->dataTitles();
        });

        $aAbout = remember('data:about', function () {
            return $this->dataAbout();
        });

        $aStatistic = remember('data:statistic', function () {
            return $this->dataStatistic();
        });

        return $this->response->array([
            'data' => [
                'tours' => $aTours,
                'organizations' => $aOrganizations,
                'directions' => $aDirections,
                'news' => $aNews,
                'articles' => $aArticles,
                'search' => [
                    'orders' => $aSearchOrders,
                    'directions' => $aSearchDirections,
                    'categories' => $aSearchCategories,
                    'organizations' => $aSearchOrganizations,
                    'departures' => $aSearchDepartures,
                ],
                'social' => [],
                'promotions' => $aPromotions,
                'questions' => $aQuestions,
                'titles' => $aTitles,
                'about' => $aAbout,
                'statistic' => $aStatistic,
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function dataUpdate(Request $request)
    {
        $this->data($request);
        return responseCommon()->apiSuccess([]);
    }

    /**
     * @return array
     */
    private function dataSearchDirections()
    {
        $oDirections = Direction::active()->ordered()->get();
        // только в которые есть туры
        $oDirections = $oDirections->reject(function (Direction $item) {
            return $item->toursActive()->count() === 0;
        });
        $aDirections = $oDirections->transform(function (Direction $item) {
            return (new DirectionTransformer())->transformSelect($item);
        })->values()->toArray();
        return $aDirections;
    }

    /**
     * @return array
     */
    private function dataSearchOrganizations()
    {
        $oOrganization = Organization::active()->ordered()->get();
        // только у которых есть туры
        $oOrganization = $oOrganization->reject(function (Organization $item) {
            return $item->toursActive()->count() === 0;
        });
        $aOrganization = $oOrganization->transform(function (Organization $item) {
            return (new OrganizationTransformer())->transformSelect($item);
        })->values()->toArray();
        return $aOrganization;
    }

    /**
     * @return array
     */
    private function dataSearchCategories()
    {
        return Category::active()->where('type', Category::TYPE_TOURS)->ordered()->get()->transform(function (Category $item) {
            return (new CategoryTransformer())->transformSelect($item);
        })->toArray();
    }

    /**
     * @return array
     */
    private function dataSearchDepartures()
    {
        return Departure::active()->ordered()->get()->transform(function (Departure $item) {
            return (new DepartureTransformer())->transformSelect($item);
        })->toArray();
    }

    /**
     * @return array
     */
    private function dataTours()
    {
        $oQuery = Tour::active()->orderedNearest();
        $oItems = serviceTransformerTour()->queryMultiple($oQuery);
        //->activeByDirection()
        //->activeByOrganization()

        return $oItems->take(6)->values()->transform(function (Tour $item) {
            return (new TourTransformer())->transformCard($item);
        })->toArray();
    }

    /**
     * @return array
     */
    private function dataOrganizations()
    {
        return Organization::active()->orderedRate()->take(6)->get()->transform(function (Organization $item) {
            return (new OrganizationTransformer())->transformDetail($item);
        })->toArray();
    }

    /**
     * @return array
     */
    private function dataDirections()
    {
        return Direction::active()->ordered()->take(8)->get()->transform(function (Direction $item) {
            return (new DirectionTransformer())->transformCard($item);
        })->toArray();
    }

    /**
     * @return array
     */
    private function dataNews()
    {
        return News::active()->ordered()->take(4)->get()->transform(function (News $item) {
            return (new NewsTransformer())->transformCard($item);
        })->toArray();
    }

    /**
     * @return array
     */
    private function dataArticles()
    {
        return Article::active()->ordered()->take(4)->get()->transform(function (Article $item) {
            return (new ArticleTransformer())->transformCard($item);
        })->toArray();
    }

    /**
     * @return array
     */
    private function dataPromotions()
    {
        $oDefaultPromotions = Promotion::activeDefault()->ordered()->get();
        $oTargetPromotions = Promotion::activeTarget()->ordered()->get();
        $aPromotions = [];

        foreach ($oDefaultPromotions as $oDefaultPromotion) {
            $aPromotions[$oDefaultPromotion->type] = (new PromotionTransformer())->transform($oDefaultPromotion);
        }
        foreach ($oTargetPromotions as $oTargetPromotion) {
            $aPromotions[$oTargetPromotion->type] = (new PromotionTransformer())->transform($oTargetPromotion);
        }
        return $aPromotions;
    }

    /**
     * @return array
     */
    private function dataQuestions()
    {
        return Question::whereNull('questionable_id')->active()->ordered()->get()->transform(function (Question $item) {
            return (new QuestionTransformer())->transform($item);
        })->toArray();
    }

    /**
     * @return array
     */
    private function dataTitles()
    {
        return [
            'app' => [
                'title' => 'НАЙДИ СВОЙ НЕЗАБЫВАЕМЫЙ ВЫХОДНОЙ',
                'description' => 'У нас можно найти увлекательные автобусные поездки по России у лучших Организаторов',
            ],
            'about' => [
                'description' => '**' . config('app.name') . '** - это платформа, которая собрала туры разных организаторов в одном месте. Благодаря этому на сайте большой выбор авторских туров, походов и экскурсий из разных городов. Вы всегда можете сравнить и выбрать наиболее подходящее предложение.',
            ],
            \App\Cmf\Project\Organization\OrganizationController::NAME => [
                'title' => 'Организаторы',
                'description' => 'Отсортированы по рейтингу',
            ],
            \App\Cmf\Project\Tour\TourController::NAME => [
                'title' => 'Туры',
                'description' => 'Отсортированы по Дате',
                'near' => [
                    'title' => 'Ближайщие туры',
                    'description' => 'Успейте забронировать место, до них осталось совсем немного',
                ],
                'duplicate' => [
                    'title' => 'Этот тур на другие даты',
                    'description' => 'На случай если у вас не получается в этот день',
                ],
                'reservations' => [
                    'title' => 'Туры',
                    'description' => 'Забронированные туры',
                ],
            ],
            \App\Cmf\Project\Direction\DirectionController::NAME => [
                'title' => 'Направления',
                'description' => 'Отсортированы по алфавиту',
                'attractions' => [
                    'title' => 'Достопримечательности',
                    'description' => 'Которые находятся по направлению',
                ],
            ],
            \App\Cmf\Project\Article\ArticleController::NAME => [
                'title' => 'Статьи',
                'description' => 'Отсортированы по Дате',
                'other' => [
                    'title' => 'Другие статьи',
                    'description' => 'Рекомендуем прочитать',
                ],
                'organization' => [
                    'title' => 'Статьи',
                    'description' => 'Написанные Организатором',
                ],
                'attractions' => [
                    'title' => 'Достопримечательности',
                    'description' => 'Которые упоминаются в этой статье',
                ],
            ],
            \App\Cmf\Project\News\NewsController::NAME => [
                'title' => 'Новости',
                'description' => ' Отсортированы по Дате',
                'other' => [
                    'title' => 'Другие новости',
                    'description' => 'Рекомендуем прочитать',
                ],
            ],
            \App\Cmf\Project\Attraction\AttractionController::NAME => [
                'title' => 'Достопримечательности',
                'description' => ' Отсортированы по алфавиту',
                'article' => [
                    'title' => 'Достопримечательности',
                    'description' => 'Которые упоминаются в этой статье',
                ],
                'articles' => [
                    'title' => 'Статьи',
                    'description' => 'В которых упоминается',
                ],
                'direction' => [
                    'title' => 'Достопримечательности',
                    'description' => 'Которые можно встретить',
                ],
                'tour' => [
                    'title' => 'Достопримечательности',
                    'description' => 'Которые можно встретить',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    private function dataAbout()
    {
        $data = [
            'share' => [
                'title' => 'Расскажите о нас',
                'description' => '
                    Мы будем вам очень признательны, если вы расскажете о нас Организатору что нашли тур у нас.
                    Платформа только набирает обороты и наполняется контентом, мы рады собирать всю информация чтобы вам было удобней выбрать Тур Выходного Дня.
                ',
                'image' => asset('/img/about.jpg'),
            ],
        ];
        return $data;
    }

    /**
     * @return array
     */
    private function dataStatistic()
    {
        $data = [
            'tours' => [
                'count' => Tour::active()->count(),
            ],
            'organizations' => [
                'count' => Organization::active()->count(),
            ],
            'directions' => [
                'count' => Direction::active()->count(),
            ],
            'articles' => [
                'count' => Article::active()->count(),
            ],
            'news' => [
                'count' => News::active()->count(),
            ],
            'users' => [
                'count' => User::active()->count(),
            ],
        ];
        return $data;
    }
}
