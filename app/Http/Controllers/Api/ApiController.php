<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Services\Seo\SeoGoogleAnalyticsApiService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class ApiController extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * @param string $method
     * @param null|mixed $oItem
     */
    protected function ga(string $method, $oItem = null): void
    {
        $ga = (new SeoGoogleAnalyticsApiService());
        if (method_exists($ga, $method)) {
            if (!is_null($oItem)) {
                $ga->{$method}($oItem)->run();
            } else {
                $ga->{$method}()->run();
            }
        }
    }
}
