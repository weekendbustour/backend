<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Attractions;

use App\Http\Transformers\AttractionTransformer;
use App\Models\Attraction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Show
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oAttraction = Attraction::find($id);
        $aAttraction = (new AttractionTransformer())->transformDetail($oAttraction);

        visits($oAttraction, 'attraction')->increment();

        ga(__FUNCTION__, $oAttraction);

        return responseCommon()->apiSuccess([
            'data' => $aAttraction,
        ]);
    }
}
