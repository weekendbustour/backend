<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Attractions;

use App\Http\Transformers\AttractionTransformer;
use App\Models\Attraction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Index
{
    const LIMIT = 20;

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function __invoke(Request $request)
    {
        $page = $request->exists('page') ? (int)$request->get('page') : 1;
        $direction_id = $request->exists('direction_id') ? (int)$request->get('direction_id') : null;

        $oQuery = Attraction::active();

        if (!is_null($direction_id)) {
            $oQuery = $oQuery->where('direction_id', $direction_id);
        }

        $oQuery = $oQuery->ordered()->paginate(self::LIMIT);

        $aAttractions = $oQuery->transform(function (Attraction $item) {
            return (new AttractionTransformer())->transformCard($item);
        })->toArray();

        $pagination = [
            'total' => $oQuery->total(),
            'page' => (int)$page,
            'limit' => $oQuery->perPage(),
        ];
        $pagination['has_more_pages'] = $oQuery->hasMorePages();

        ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aAttractions,
            'pagination' => $pagination,
        ]);
    }
}
