<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\CategoryTransformer;
use App\Http\Transformers\DepartureTransformer;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Transformers\CommentTransformer;
use App\Http\Transformers\DirectionTransformer;
use App\Http\Transformers\OrganizationTransformer;
use App\Http\Transformers\UserTransformer;
use App\Http\Transformers\TourTransformer;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Departure;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\User;
use App\Services\Search\SearchService;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * Class SearchController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class SearchController extends ApiController
{
    use Helpers;

    const LIMIT = 12;

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $page = $request->exists('page') ? (int)$request->get('page') : 1;

        $oTours = Tour::active();

        $oTours->where('start_at', '>=', now()->startOfDay());

        if ($request->has('departure_id')) {
            $oTours = $this->searchByDeparture($oTours, $request->get('departure_id'));
        }
        if ($request->has('category_id')) {
            $oTours = $this->searchByCategories($oTours, $request->get('category_id'));
        }
        if ($request->has('direction_id')) {
            $oTours = $this->searchByDirection($oTours, $request->get('direction_id'));
        }
        if ($request->has('organization_id')) {
            $oTours = $this->searchByOrganization($oTours, $request->get('organization_id'));
        }

        $start_at = $request->has('start_at') ? $request->get('start_at') : null;
        $finish_at = $request->has('finish_at') ? $request->get('finish_at') : null;
        $oTours = $this->searchByDate($oTours, $start_at, $finish_at);

        $price_from = $request->has('price_from') ? $request->get('price_from') : null;
        $price_to = $request->has('price_to') ? $request->get('price_to') : null;
        $oTours = $this->searchByPrice($oTours, $price_from, $price_to);

        $oTours = $this->searchByOrderBy($oTours, $request->get('order_by'));

        $oItems = serviceTransformerTour()->queryMultiple($oTours);
        $oResult = serviceTransformerTour()->returnPaginate($oItems->values(), $request, self::LIMIT, $oItems->count());

        //$oResult = $oTours->paginate(self::LIMIT);

        $aTours = $oResult->values()->transform(function (Tour $item) {
            return (new TourTransformer())->transform($item);
        })->toArray();

        $pagination = [
            'total' => $oResult->total(),
            'page' => (int)$page,
            'limit' => $oResult->perPage(),
        ];
        $pagination['has_more_pages'] = $oResult->hasMorePages();

        $this->ga(__FUNCTION__, $request);

        return responseCommon()->apiSuccess([
            'data' => $aTours,
            'pagination' => $pagination,
        ]);
    }

    private function searchByDeparture($oTours, int $departure_id)
    {
        $oTours->whereHas('departures', function ($query) use ($departure_id) {
            $query->where('departure_id', $departure_id);
        });
        return $oTours;
    }

    private function searchByDirection($oTours, int $direction_id)
    {
        $oTours->where('direction_id', $direction_id);
        return $oTours;
    }

    private function searchByOrganization($oTours, int $organization_id)
    {
        $oTours->where('organization_id', $organization_id);
        return $oTours;
    }

    private function searchByCategories($oTours, array $category_id)
    {
        $oTours->whereHas('categories', function ($query) use ($category_id) {
            $query->whereIn('category_id', $category_id);
        });
        return $oTours;
    }

    private function searchByDate($oTours, ?string $start_at, ?string $finish_at)
    {
        if ($start_at !== null && $finish_at !== null) {
            $oTours
                ->whereBetween('start_at', [
                    Carbon::parse($start_at)->startOfDay(),
                    Carbon::parse($finish_at)->endOfDay(),
                ]);
        } elseif ($start_at !== null) {
            $oTours->where('start_at', '>=', Carbon::parse($start_at)->startOfDay());
        } elseif ($finish_at !== null) {
            $oTours->where('start_at', '<=', Carbon::parse($finish_at)->endOfDay());
        }
        return $oTours;
    }

    private function searchByPrice($oTours, ?int $price_from, ?int $price_to)
    {
        if ($price_from !== null) {
            $oTours->whereHas('price', function ($query) use ($price_from) {
                return $query->where('value', '>=', $price_from);
            });
            //$oTours->where('price', '>=', $price_from);
        }
        if ($price_to !== null) {
            $oTours->whereHas('price', function ($query) use ($price_to) {
                return $query->where('value', '<=', $price_to);
            });
        }
        return $oTours;
    }

    private function searchByOrderBy($oTours, ?string $value = null)
    {
        if (is_null($value)) {
            $value = SearchService::ORDER_BY_NEAREST;
        }
        switch ($value) {
            case SearchService::ORDER_BY_NEAREST:
                $oTours = $oTours->orderBy('start_at', 'asc');
                break;
            case SearchService::ORDER_BY_NEW:
                $oTours = $oTours->orderBy('created_at', 'desc');
                break;
            case SearchService::ORDER_BY_PRICE_DOWN:
                $oTours = $oTours->whereHas('price', function ($query) {
                    return $query->orderBy('value', 'asc');
                });
                break;
            case SearchService::ORDER_BY_PRICE_UP:
                $oTours = $oTours->whereHas('price', function ($query) {
                    return $query->orderBy('value', 'desc');
                });
                break;
//            case SearchService::ORDER_BY_RATING:
//                $oTours->orderedRate();
//                break;
        }
        //dd($value, $oTours->toSql());//->pluck('price')->toArray());
        return $oTours;
    }
}
