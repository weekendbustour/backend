<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\ArticleTransformer;
use App\Http\Transformers\CommentTransformer;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Direction;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * Class ArticleController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class ArticleController extends ApiController
{
    use Helpers;

    const LIMIT = 20;

    /**
     * @param Request $request
     * @return array
     */
    public function articles(Request $request)
    {
        $page = $request->exists('page') ? (int)$request->get('page') : 1;
        $organization_id = $request->exists('organization_id') ? (int)$request->get('organization_id') : null;

        $oQuery = Article::active();

        if (!is_null($organization_id)) {
            $oQuery = $oQuery->where('organization_id', $organization_id);
        }

        $oQuery = $oQuery->ordered()->paginate(self::LIMIT);

        $aArticles = $oQuery->transform(function (Article $item) {
            return (new ArticleTransformer())->transformCard($item);
        })->toArray();

        $pagination = [
            'total' => $oQuery->total(),
            'page' => (int)$page,
            'limit' => $oQuery->perPage(),
        ];
        $pagination['has_more_pages'] = $oQuery->hasMorePages();

        $this->ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aArticles,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function comments(Request $request, $id)
    {
        $oArticle = Article::find($id);
        $aComments = $oArticle->approvedComments()->where('parent_id', null)->with('approvedChildren')->get()->transform(function (Comment $item) {
            return (new CommentTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aComments,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function article(Request $request, $id)
    {
        $oArticle = Article::find($id);
        $aArticle = (new ArticleTransformer())->transformDetail($oArticle);

        visits($oArticle, 'article')->increment();

        $this->ga(__FUNCTION__, $oArticle);

        return responseCommon()->apiSuccess([
            'data' => $aArticle,
        ]);
    }
}
