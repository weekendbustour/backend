<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\AttractionTransformer;
use App\Http\Transformers\CommentTransformer;
use App\Models\Attraction;
use App\Models\Comment;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * Class AttractionController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class AttractionController extends ApiController
{
    use Helpers;

    const LIMIT = 20;

    /**
     * @param Request $request
     * @return array
     */
    public function attractions(Request $request)
    {
        $page = $request->exists('page') ? (int)$request->get('page') : 1;
        $direction_id = $request->exists('direction_id') ? (int)$request->get('direction_id') : null;

        $oQuery = Attraction::active();

        if (!is_null($direction_id)) {
            $oQuery = $oQuery->where('direction_id', $direction_id);
        }

        $oQuery = $oQuery->ordered()->paginate(self::LIMIT);

        $aAttractions = $oQuery->transform(function (Attraction $item) {
            return (new AttractionTransformer())->transformCard($item);
        })->toArray();

        $pagination = [
            'total' => $oQuery->total(),
            'page' => (int)$page,
            'limit' => $oQuery->perPage(),
        ];
        $pagination['has_more_pages'] = $oQuery->hasMorePages();

        $this->ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aAttractions,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function comments(Request $request, $id)
    {
        $oAttraction = Attraction::find($id);
        $aComments = $oAttraction->approvedComments()->where('parent_id', null)->with('approvedChildren')->get()->transform(function (Comment $item) {
            return (new CommentTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aComments,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function attraction(Request $request, $id)
    {
        $oAttraction = Attraction::find($id);
        $aAttraction = (new AttractionTransformer())->transformDetail($oAttraction);

        visits($oAttraction, 'attraction')->increment();

        $this->ga(__FUNCTION__, $oAttraction);

        return responseCommon()->apiSuccess([
            'data' => $aAttraction,
        ]);
    }
}
