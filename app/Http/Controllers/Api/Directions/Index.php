<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Directions;

use App\Http\Transformers\DirectionTransformer;
use App\Models\Direction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Index
{
    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function __invoke(Request $request)
    {
        $aDirections = Direction::active()->ordered()->get()->transform(function (Direction $item) {
            return (new DirectionTransformer())->transformCard($item);
        })->toArray();

        ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aDirections,
        ]);
    }
}
