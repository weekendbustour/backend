<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Directions;

use App\Http\Transformers\TourTransformer;
use App\Models\Direction;
use App\Models\Tour;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Tours
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        /** @var Direction $oDirection */
        $oDirection = Direction::find($id);
        $date = Carbon::parse($request->get('date'));
        $aTours = $oDirection->toursActive()->whereBetween('start_at', [
            $date->copy()->startOfDay(),
            $date->copy()->endOfDay(),
        ])->get()->transform(function (Tour $item) {
            return (new TourTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aTours,
        ]);
    }
}
