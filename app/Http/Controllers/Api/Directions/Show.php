<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Directions;

use App\Http\Transformers\DirectionTransformer;
use App\Models\Direction;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Show
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oDirection = Direction::find($id);
        $aDirection = (new DirectionTransformer())->transformDetail($oDirection);

        visits($oDirection, 'direction')->increment();

        ga(__FUNCTION__, $oDirection);

        return responseCommon()->apiSuccess([
            'data' => $aDirection,
        ]);
    }
}
