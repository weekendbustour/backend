<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\CommentTransformer;
use App\Http\Transformers\NewsTransformer;
use App\Models\Comment;
use App\Models\News;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * Class NewsController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class NewsController extends ApiController
{
    use Helpers;

    const LIMIT = 20;

    /**
     * @param Request $request
     * @return \Dingo\Api\Http\Response
     */
    public function news(Request $request)
    {
        $page = $request->exists('page') ? (int)$request->get('page') : 1;

        $oQuery = News::active()->ordered()->paginate(self::LIMIT);

        $aNews = $oQuery->transform(function (News $item) {
            return (new NewsTransformer())->transformCard($item);
        })->toArray();

        $pagination = [
            'total' => $oQuery->total(),
            'page' => (int)$page,
            'limit' => $oQuery->perPage(),
        ];
        $pagination['has_more_pages'] = $oQuery->hasMorePages();

        $this->ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aNews,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function newsItem(Request $request, $id)
    {
        $oNews = News::find($id);
        $aNews = (new NewsTransformer())->transformDetail($oNews);

        visits($oNews, 'news')->increment();

        $this->ga(__FUNCTION__, $oNews);

        return responseCommon()->apiSuccess([
            'data' => $aNews,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function comments(Request $request, $id)
    {
        /** @var News $oNews */
        $oNews = News::find($id);
        $aComments = $oNews->approvedComments()->where('parent_id', null)->with('approvedChildren')->get()->transform(function (Comment $item) {
            return (new CommentTransformer())->transform($item);
        })->toArray();

        return responseCommon()->apiSuccess([
            'data' => $aComments,
        ]);
    }
}
