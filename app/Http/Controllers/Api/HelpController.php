<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\Help\OfferStoreRequest;
use App\Http\Requests\Api\Help\RatingStoreRequest;
use App\Http\Requests\Api\Help\ReportStoreRequest;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Departure;
use App\Models\Direction;
use App\Models\FeedbackOffer;
use App\Models\FeedbackRating;
use App\Models\FeedbackReport;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\User;
use App\Services\Notification\Slack\SlackFeedbackRatingNotification;
use App\Services\Search\SearchService;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * Class HelpController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class HelpController extends ApiController
{
    use Helpers;

    /**
     * @param ReportStoreRequest $request
     * @return array
     */
    public function report(ReportStoreRequest $request)
    {
        $validated = $request->validated();
        $oItem = FeedbackReport::create([
            'user_id' => $request->exists('user_id') ? $request->get('user_id') : null,
            'description' => $validated['description'],
        ]);

        return responseCommon()->apiSuccess([], 'Данные успешно отправлены');
    }

    /**
     * @param OfferStoreRequest $request
     * @return array
     */
    public function offer(OfferStoreRequest $request)
    {
        $validated = $request->validated();
        $oItem = FeedbackOffer::create([
            'user_id' => $request->exists('user_id') ? $request->get('user_id') : null,
            'description' => $validated['description'],
        ]);

        return responseCommon()->apiSuccess([], 'Данные успешно отправлены');
    }

    /**
     * @param RatingStoreRequest $request
     * @return array
     */
    public function rating(RatingStoreRequest $request)
    {
        $validated = $request->validated();
        $oItem = FeedbackRating::create([
            'user_id' => $request->exists('user_id') ? $request->get('user_id') : null,
            'description' => $validated['description'],
            'rating' => $validated['rating'],
            'email' => $validated['email'],
        ]);

        (new SlackFeedbackRatingNotification())->send($validated['email'] . ' : ' . $validated['description'] . ' : ' . $validated['rating']);

        return responseCommon()->apiSuccess([], 'Ваше сообщение успешно отправлено');
    }
}
