<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Articles;

use App\Http\Transformers\ArticleTransformer;
use App\Models\Article;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Index
{
    const LIMIT = 20;

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function __invoke(Request $request)
    {
        $page = $request->exists('page') ? (int)$request->get('page') : 1;
        $organization_id = $request->exists('organization_id') ? (int)$request->get('organization_id') : null;

        $oQuery = Article::active();

        if (!is_null($organization_id)) {
            $oQuery = $oQuery->where('organization_id', $organization_id);
        }

        $oQuery = $oQuery->ordered()->paginate(self::LIMIT);

        $aArticles = $oQuery->transform(function (Article $item) {
            return (new ArticleTransformer())->transformCard($item);
        })->toArray();

        $pagination = [
            'total' => $oQuery->total(),
            'page' => (int)$page,
            'limit' => $oQuery->perPage(),
        ];
        $pagination['has_more_pages'] = $oQuery->hasMorePages();

        ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aArticles,
            'pagination' => $pagination,
        ]);
    }
}
