<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\Articles;

use App\Http\Transformers\ArticleTransformer;
use App\Models\Article;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class Show
{
    /**
     * @param Request $request
     * @param int $id
     * @return array|JsonResponse
     */
    public function __invoke(Request $request, int $id)
    {
        $oArticle = Article::find($id);
        $aArticle = (new ArticleTransformer())->transformDetail($oArticle);

        visits($oArticle, 'article')->increment();

        ga(__FUNCTION__, $oArticle);

        return responseCommon()->apiSuccess([
            'data' => $aArticle,
        ]);
    }
}
