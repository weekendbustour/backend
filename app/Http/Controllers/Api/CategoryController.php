<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\CategoryTransformer;
use App\Http\Controllers\Controller;
use App\Models\Category;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * Class CategoryController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class CategoryController extends ApiController
{
    use Helpers;

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function category(Request $request, $id)
    {
        $oCategory = Category::find($id);
        $aCategory = (new CategoryTransformer())->transformDetail($oCategory);

        visits($oCategory, 'category')->increment();

        $this->ga(__FUNCTION__, $oCategory);

        return responseCommon()->apiSuccess([
            'data' => $aCategory,
        ]);
    }
}
