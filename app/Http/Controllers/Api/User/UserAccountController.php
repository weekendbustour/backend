<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\User;

use App\Http\Transformers\CategoryTransformer;
use App\Http\Transformers\DepartureTransformer;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Transformers\CommentTransformer;
use App\Http\Transformers\DirectionTransformer;
use App\Http\Transformers\OrganizationTransformer;
use App\Http\Transformers\UserTransformer;
use App\Http\Transformers\TourTransformer;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Comment;
use App\Models\Departure;
use App\Models\Direction;
use App\Models\Image;
use App\Models\News;
use App\Models\Organization;
use App\Models\Subscription;
use App\Models\Tour;
use App\Models\TourReservation;
use App\Models\User;
use App\Services\Image\ImageType;
use App\Services\Image\Upload\ImageUploadModelService;
use App\Services\ImageService;
use App\Services\Notification\Slack\SlackReservationsNotification;
use App\Services\Subscription\SubscriptionService;
use Carbon\Carbon;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UserAccountController extends Controller
{
    use Helpers;

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function accountProfile(Request $request)
    {
        $oUser = $request->user();
        if (is_null($oUser)) {
            return responseCommon()->apiError([], 'Пользователь не найден');
        }
        $birthday = null;
        if ($request->exists(['day', 'month', 'year'])) {
            $birthday = Carbon::parse($request->get('day') . '.' . $request->get('month') . '.' . $request->get('year'));
            $birthday = $birthday->format('Y-m-d');
        }

        $rules = (new \App\Cmf\Project\User\UserController())->rules($oUser)['update'];
        if (isset($rules['login'])) {
            unset($rules['login']);
        }
        if (isset($rules['password'])) {
            unset($rules['password']);
        }
        $validation = Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        }
        $oUser->update([
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
            'second_name' => $request->get('second_name'),
            'email' => $request->get('email'),
            'phone' => $request->get('phone'),
            'birthday_at' => $birthday,
        ]);
        return responseCommon()->apiSuccess([], 'Данные упешно изменены');
    }

    /**
     * @param Request $request
     * @return array
     */
    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function accountImage(Request $request)
    {
        $oUser = $request->user();
        if (is_null($oUser)) {
            return responseCommon()->apiError([], 'Пользователь не найден');
        }
        $oFile = $request->file('image');

        $oImageService = (new ImageService());

        //$oService = new ImageUploadModelService();
        $type = ImageType::MODEL;
        $options = (new \App\Cmf\Project\User\UserController())->image[$type];

        foreach ($options['filters'] as $key => $option) {
            $options['filters'][$key]['options']['crop'] = $request->get('crop');
        }
        $filename = $oImageService->upload($oUser, [$oFile], $options, $type);

        return responseCommon()->apiSuccess([], 'Изображение успешно загружено');
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function accountImageDelete(Request $request)
    {
        $oUser = $request->user();
        if (is_null($oUser)) {
            return responseCommon()->apiError([], 'Пользователь не найден');
        }

        $type = ImageType::MODEL;
        $options = (new \App\Cmf\Project\User\UserController())->image[ImageType::MODEL];

        $oImageService = (new ImageService());
        //$oService = new ImageUploadModelService();
        $oImages = Image::where('type', $type)
            ->where('imageable_type', get_class($oUser))
            ->where('imageable_id', $oUser->id)
            ->get();
        if (count($oImages) !== 0) {
            foreach ($oImages as $oImage) {
                $result = $oImageService->delete($oUser, $oImage, $options, $type);
            }
        }
        return responseCommon()->apiSuccess([], 'Изображение успешно удалено');
    }
}
