<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\User;

use App\Http\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Tour;
use App\Services\Subscription\SubscriptionService;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class UserSubscriptionsController extends Controller
{
    use Helpers;

    /**
     * @param Request $request
     * @return mixed
     */
    public function userSubscriptions(Request $request)
    {
        $oUser = $request->user();
        if ($request->has('extended')) {
            $aItem = (new UserTransformer())->transformSubscriptionsExtended($oUser);
        } else {
            $aItem = (new UserTransformer())->transformSubscriptions($oUser);
        }
        return responseCommon()->apiSuccess([
            'data' => $aItem,
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function userSubscribe(Request $request)
    {
        $oUser = $request->user();
        $id = $request->get('id');
        $model = $request->get('model');
        $oModel = null;
        $nameModel = '';
        $textToggle = '';
        switch ($model) {
            case 'tour':
                $nameModel = 'Тур';
                $oModel = Tour::find($id);
                break;
            case 'direction':
                $nameModel = 'Направление';
                $oModel = Direction::find($id);
                break;
            case 'organization':
                $nameModel = 'Организатор';
                $oModel = Organization::find($id);
                break;
            default:
                break;
        }
        if (!is_null($oModel)) {
            $oService = (new SubscriptionService($oUser, $oModel));

            if ($oService->isActive()) {
                $textToggle = 'удален из Подписок';
                $oService->unsubscribe();
            } else {
                $textToggle = 'добавлен в Подписки';
                $oService->subscribe();
            }
        }

        $aItems = (new UserTransformer())->transformSubscriptions($oUser);

        return responseCommon()->apiSuccess([
            'data' => $aItems,
        ], $nameModel . ' успешно ' . $textToggle);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function userSubscription(Request $request)
    {
        $oUser = $request->user();
        $value = $request->get('value');
        $value = (int)$value ? true : false;

        $oService = (new SubscriptionService($oUser));

        if ($value) {
            $oService->subscribe();
            return responseCommon()->apiSuccess([], 'Вы успешно подписаны на новости');
        } else {
            $oService->unsubscribe();
            return responseCommon()->apiSuccess([], 'Вы успешно отписаны от новостей');
        }
    }
}
