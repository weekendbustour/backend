<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\User;

use App\Http\Transformers\CommentTransformer;
use App\Http\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Attraction;
use App\Models\Comment;
use App\Models\Direction;
use App\Models\News;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\User;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserCommentsController extends Controller
{
    use Helpers;

    /**
     * @param Request $request
     * @return array
     */
    public function userComments(Request $request)
    {
        $oUser = $request->user();
        if ($request->has('extended')) {
            $aItem = [
                'organizations' => [],
                'directions' => [],
                'news' => [],
                'articles' => [],
                'attractions' => [],
            ];
        } else {
            $aItem = (new UserTransformer())->transformComments($oUser);
        }
        return responseCommon()->apiSuccess([
            'data' => $aItem,
        ]);
    }

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function userComment(Request $request)
    {
        /** @var User $oUser */
        $oUser = $request->user();
        $id = $request->get('id');
        $model = $request->get('model');
        $text = $request->get('text');
        $rating = $request->get('rating');
        $anon = (int)$request->get('anon');
        if (empty($text) && $request->has('reply_id')) {
            return responseCommon()->apiError([], 'Коментарий не может быть пустым');
        }

        $oModel = null;
        $title = null;
        $canManyComments = true;
        switch ($model) {
            case 'tour':
                $oModel = Tour::find($id);
                break;
            case 'direction':
                $oModel = Direction::find($id);
                $title = 'это Направление';
                $canManyComments = false;
                break;
            case 'organization':
                $oModel = Organization::find($id);
                $title = 'этого Организатора';
                $canManyComments = false;
                break;
            case 'news':
                $oModel = News::find($id);
                break;
            case 'article':
                $oModel = Article::find($id);
                break;
            case 'attraction':
                $oModel = Attraction::find($id);
                break;
            default:
                break;
        }
        if (!is_null($oModel)) {
            if (!$canManyComments) {
                $oComment = $oUser->commentGetMain($oModel);
                // если уже есть комментарий
                if (!is_null($oComment)) {
                    // если он не был анонимный, то запрещаем
                    // если он был анонимный и сейчас тоже анонимно, то запрещаем
                    // и это не ответ
                    if ((!$oComment->anon || $anon) && !$request->has('reply_id')) {
                        return responseCommon()->apiError([], 'Вы уже оценивали ' . $title);
                    }
                }
//                if ($oUser->isRated($oModel)) {
//                    return responseCommon()->apiError([], 'Вы уже оценивали ' . $title);
//                }
            }
            if ($request->has('rating')) {
                $oRate = $oUser->rateByComment($oModel, $rating);
                $oComment = $oUser->comment($oModel, $text, 0, $anon);
                $oComment->update([
                    'rating_id' => $oRate->id,
                ]);
            } elseif ($request->has('reply_id')) {
                $oComment = $oUser->comment($oModel, $text, 0, $anon, $request->get('parent_id'), $request->get('reply_id'));
            } elseif ($oModel instanceof News) {
                $oComment = $oUser->comment($oModel, $text, 0, false);
            } elseif ($oModel instanceof Article) {
                $oComment = $oUser->comment($oModel, $text, 0, false);
            } elseif ($oModel instanceof Attraction) {
                $oComment = $oUser->comment($oModel, $text, 0, false);
            }
        }
        return responseCommon()->apiSuccess([
            'success' => true,
        ], 'Ваш отзыв успешно отправлен');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function userCommentVote(Request $request, $id)
    {
        /** @var User $oUser */
        $oUser = $request->user();
        $value = $request->get('value');
        /** @var Comment $oComment */
        $oComment = Comment::find($id);
        $value = (int)$value ? 1 : -1;
        if ($oUser->commentCanBeVoted($oComment)) {
            $oUser->commentVote($oComment, $value);
        } else {
            $oVote = $oUser->commentGetVote($oComment);
            if ($oVote->value !== $value) {
                $oVote->update([
                    'value' => $value,
                ]);
            } else {
                return response()->json([
                    'message' => 'Вы уже оценивали этот комментарий',
                    'vote' => [
                        'id' => $oComment->id,
                        'vote' => $value,
                    ],
                ], 401);
            }
        }
        $oComment->refresh();
        return responseCommon()->apiSuccess([
            'comment' => (new CommentTransformer())->transform($oComment),
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function userVotes(Request $request)
    {
        $oUser = $request->user();
        if ($request->has('extended')) {
            $aItem = (new UserTransformer())->transformVotes($oUser);
        } else {
            $aItem = (new UserTransformer())->transformVotes($oUser);
        }
        return responseCommon()->apiSuccess([
            'data' => $aItem,
        ]);
    }
}
