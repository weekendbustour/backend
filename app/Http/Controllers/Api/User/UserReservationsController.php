<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\User;

use App\Http\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\Tour;
use App\Models\TourReservation;
use App\Models\User;
use App\Services\Notification\Slack\SlackReservationsNotification;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UserReservationsController extends Controller
{
    use Helpers;

    /**
     * @param Request $request
     * @return array
     */
    public function userReservations(Request $request)
    {
        $oUser = $request->user();
        if ($request->has('extended')) {
            $aItem = (new UserTransformer())->transformReservationsExtended($oUser);
        } else {
            $aItem = (new UserTransformer())->transformReservations($oUser);
        }
        return responseCommon()->apiSuccess([
            'data' => $aItem,
        ]);
    }

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function userReservation(Request $request)
    {
        $oUser = $request->user();
        /** @var Tour $oTour */
        $oTour = Tour::find($request->get('id'));
        $places = (int)$request->get('places');
        $phone = $request->get('phone');
        $email = $request->get('email');

        /** @var TourReservation|null $oIssetReservations */
        $oIssetReservations = $oUser->reservations()->where('tour_id', $oTour->id)->first();
        if (!is_null($oIssetReservations) && $oIssetReservations->isActive()) {
            return responseCommon()->apiError([], 'У вас уже есть бронь на этот тур, если хотите изменить, то сначала удалите предыдущее бронирование');
        }

        $oTourReservation = $oUser->reservations()->create([
            'tour_id' => $oTour->id,
            'amount' => 1000 * $places,
            'places' => $places,
            'code' => rand(100000, 999999),
            'status' => TourReservation::STATUS_PROCESSED,
        ]);
        if (empty($oUser->phone)) {
            $oUser->update([
                'phone' => $phone,
            ]);
        }
        if (empty($oUser->email)) {
            $oUser->update([
                'email' => $email,
            ]);
        }
        $data = $this->userReservations($request)['data'];

        (new SlackReservationsNotification())->send('#' . $oUser->id . ':' . $oUser->email . ' забронировал тур #' . $oTour->id . ':' . $oTour->title);
        return responseCommon()->apiSuccess([
            'data' => $data,
        ], 'Ваша заявка успешно отправлена');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function userReservationCancel(Request $request)
    {
        /** @var User $oUser */
        $oUser = $request->user();
        /** @var TourReservation|null $oTourReservation */
        $oTourReservation = TourReservation::find($request->get('id'));
        $oTour = $oTourReservation->tour;

        $oTourReservation->update([
            //'status' => TourReservation::STATUS_CANCELED_USER,
        ]);

        $data = $this->userReservations($request)['data'];

        (new SlackReservationsNotification())->send('#' . $oUser->id . ':' . $oUser->email . ' отменил бронь тура #' . $oTour->id . ':' . $oTour->title);
        return responseCommon()->apiSuccess([
            'data' => $data,
        ], 'Бронь тура успешно отменена');
    }
}
