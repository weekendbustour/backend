<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api\User;

use App\Http\Transformers\UserTransformer;
use App\Http\Controllers\Controller;
use App\Models\Attraction;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Tour;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

class UserFavouritesController extends Controller
{
    use Helpers;

    /**
     * @param Request $request
     * @return mixed
     */
    public function userFavourites(Request $request)
    {
        $oUser = $request->user();
        if ($request->has('extended')) {
            $aItem = (new UserTransformer())->transformFavouritesExtended($oUser);
        } else {
            $aItem = (new UserTransformer())->transformFavourites($oUser);
        }
        return responseCommon()->apiSuccess([
            'data' => $aItem,
        ]);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function userFavourite(Request $request)
    {
        $oUser = $request->user();
        $id = $request->get('id');
        $model = $request->get('model');
        $oModel = null;
        $nameModel = '';
        $textToggle = '';
        switch ($model) {
            case 'tour':
                $nameModel = 'Тур';
                $oModel = Tour::find($id);
                break;
            case 'direction':
                $nameModel = 'Направление';
                $oModel = Direction::find($id);
                break;
            case 'organization':
                $nameModel = 'Организатор';
                $oModel = Organization::find($id);
                break;
            case 'attraction':
                $nameModel = 'Достопримечательность';
                $oModel = Attraction::find($id);
                break;
            default:
                break;
        }
        if (!is_null($oModel)) {
            if ($oUser->hasFavorited($oModel)) {
                $textToggle = 'удален из Избранного';
            } else {
                $textToggle = 'добавлен в Избранное';
            }
            $oUser->toggleFavorite($oModel);
        }

        $aItems = (new UserTransformer())->transformFavourites($oUser);

        return responseCommon()->apiSuccess([
            'data' => $aItems,
        ], $nameModel . ' успешно ' . $textToggle);
    }
}
