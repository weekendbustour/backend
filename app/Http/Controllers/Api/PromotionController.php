<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Http\Transformers\PromotionTransformer;
use App\Models\Promotion;
use Dingo\Api\Routing\Helpers;
use Illuminate\Http\Request;

/**
 * Class PromotionController
 * @package App\Http\Controllers\Api
 *
 * @deprecated
 */
class PromotionController extends ApiController
{
    use Helpers;

    /**
     * @param Request $request
     * @param int $id
     * @return \Dingo\Api\Http\Response
     */
    public function promotion(Request $request, int $id)
    {
        $oPromotion = Promotion::find($id);
        $aPromotion = (new PromotionTransformer())->transform($oPromotion);

        visits($oPromotion, 'promotion')->increment();

        $this->ga(__FUNCTION__);

        return responseCommon()->apiSuccess([
            'data' => $aPromotion,
        ]);
    }
}
