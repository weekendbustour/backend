<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\Seo\SeoService;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class FrontendController extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * @param string $method
     * @param null|mixed $oItem
     */
    protected function seo(string $method, $oItem = null): void
    {
        $seo = (new SeoService());
        if (method_exists($seo, $method)) {
            if (!is_null($oItem)) {
                $seo->{$method}($oItem);
            } else {
                $seo->{$method}();
            }
        } else {
            $seo->index();
        }
    }

    /**
     * @param Request $request
     * @return bool
     */
    protected function isPrivateShow(Request $request)
    {
        if (!$request->exists('hash')) {
            return false;
        }
        if (Auth::guest()) {
            return false;
        }
        if (!Auth::user()->hasRole(User::ROLE_SUPER_ADMIN) && !Auth::user()->hasRole(User::ROLE_ADMIN)) {
            return false;
        }
        return true;
    }
}
