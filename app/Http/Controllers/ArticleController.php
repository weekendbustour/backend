<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends FrontendController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function articles(Request $request)
    {
        $this->seo(__FUNCTION__);
        return view('app.react');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function article(Request $request, int $id)
    {
        $oItem = Article::active()->where('id', $id)->first();
        if (is_null($oItem)) {
            if ($this->isPrivateShow($request)) {
                $oItem = Article::find($id);
                if (is_null($oItem)) {
                    abort(404);
                }
            } else {
                abort(404);
            }
        }
        $this->seo(__FUNCTION__, $oItem);
        return view('app.react');
    }
}
