<?php

declare(strict_types=1);

namespace App\Http\Requests\Api\Help;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RatingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required|string',
            'email' => 'required|string',
            'rating' => 'int',
            //'rating' => [Rule::notIn([0]),],
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'description.required' => 'Поле Описание обязательно для заполнения',
            'rating.required' => 'Оцените платформу',
            'rating.not_in' => 'Оцените платформу',
        ];
    }
}
