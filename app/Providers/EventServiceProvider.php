<?php

declare(strict_types=1);

namespace App\Providers;

use App\Events\Auth\RegisteredEvent;
use App\Events\Auth\ResetPasswordEvent;
use App\Events\ChangeCacheEvent;
use App\Listeners\ChangeCacheListener;
use App\Listeners\Auth\RegisteredListener;
use App\Listeners\Auth\ResetPasswordListener;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RegisteredEvent::class => [
            RegisteredListener::class,
        ],
        ResetPasswordEvent::class => [
            ResetPasswordListener::class,
        ],
        ChangeCacheEvent::class => [
            ChangeCacheListener::class,
        ],
        \SocialiteProviders\Manager\SocialiteWasCalled::class => [
            // add your listeners (aka providers) here
            'SocialiteProviders\\VKontakte\\VKontakteExtendSocialite@handle',
            'SocialiteProviders\\Yandex\\YandexExtendSocialite@handle',
            'SocialiteProviders\\Google\\GoogleExtendSocialite@handle',
            'SocialiteProviders\\Facebook\\FacebookExtendSocialite@handle',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
