<?php

declare(strict_types=1);

namespace App\Providers;

use App\Http\Composers\ApiComposer;
use App\Cmf\Composers\CmfComposer;
use App\Cmf\Composers\CmfFieldsComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('cmf.*', CmfComposer::class);
        View::composer('cmf.content.user.modals.tabs.activations', CmfFieldsComposer::class);
        View::composer('app.react', ApiComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
