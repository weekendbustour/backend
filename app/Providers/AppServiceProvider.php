<?php

declare(strict_types=1);

namespace App\Providers;

use App\Models\Category;
use Illuminate\Routing\UrlGenerator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Butschster\Head\Facades\Meta;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if (config('app.redirect_https')) {
            $this->app['request']->server->set('HTTPS', true);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if (config('app.redirect_https')) {
            $url->formatScheme('https');
        }

        DB::listen(function ($query) {
            //info($query->sql);
            // $query->sql
            // $query->bindings
            // $query->time
        });
    }
}
