<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Http\Request;
use Dingo\Api\Routing\Route;
use Dingo\Api\Auth\Provider\Authorization;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class ApiAuthCustomServiceProvider extends Authorization
{
    public function authenticate(Request $request, Route $route)
    {
        if (!config('api.auth_enabled')) {
            return true;
        }
        $authorization = (string)$request->headers->get('authorization');
        if (!Str::startsWith(strtolower($authorization), $this->getAuthorizationMethod())) {
            throw new UnauthorizedHttpException('Unable to authenticate with supplied username and password.');
        }
        $basic = str_replace('Basic ', '', $authorization);

        $apps = config('api.apps');

        $hasAuth = false;
        foreach ($apps as $app) {
            $base = base64_encode($app['login'] . ':' . $app['password']);

            if ($base === $basic) {
                $hasAuth = true;
                break;
            }
        }
        if (!$hasAuth) {
            throw new UnauthorizedHttpException('Unable to authenticate with supplied username and password.');
        }
        // If the authorization header passed validation we can continue to authenticate.
        // If authentication then fails we must throw the UnauthorizedHttpException.
    }

    public function getAuthorizationMethod()
    {
        return 'basic';
    }
}
