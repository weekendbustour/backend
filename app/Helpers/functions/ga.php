<?php

declare(strict_types=1);

use App\Services\Seo\SeoGoogleAnalyticsApiService;

if (!function_exists('ga')) {
    /**
     * @param string $method
     * @param null|mixed $oItem
     */
    function ga(string $method, $oItem = null): void
    {
        $ga = (new SeoGoogleAnalyticsApiService());
        if (method_exists($ga, $method)) {
            if (!is_null($oItem)) {
                $ga->{$method}($oItem)->run();
            } else {
                $ga->{$method}()->run();
            }
        }
    }
}
