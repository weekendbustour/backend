<?php

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Cache;

if (!function_exists('responseCommon')) {

    function responseCommon()
    {
        return (new \App\Services\ResponseCommon\ResponseCommonHelpers());
    }
}

if (!function_exists('routeCmf')) {
    /**
     * Generate the URL to a named route.
     *
     * routeCmf('create.modal.post')
     *
     * @param string $view
     * @param array $parameters
     * @param bool $absolute
     * @return string
     * @see \App\Cmf\Core\RouteCmf::resource()
     *
     */
    function routeCmf($view, $parameters = [], $absolute = true)
    {
        $prefix = config('cmf.as');
        if ($prefix !== '') {
            $view = $prefix . '.' . $view;
        }
        return app('url')->route($view, $parameters, $absolute);
    }
}

if (!function_exists('viewAdminContent')) {
    /**
     * Generate the URL to a named route.
     *
     * viewAdminContent('components.table')
     *
     * @param string $name
     * @return string
     * @see \App\Cmf\Core\RouteCmf::resource()
     *
     */
    function viewAdminContent($name)
    {
        $view = 'admin.content.';

        if (!is_null(\Illuminate\Support\Facades\Route::current())) {
            $view = $view . stristr(\Illuminate\Support\Facades\Route::current()->getName(), '.', true) . '.' . $name;
        }

        return $view;
    }
}

if (!function_exists('cmfHelper')) {

    function cmfHelper($type = 'cmf')
    {
        return new \App\Cmf\Core\Helper($type);
    }
}

if (!function_exists('apiRoutes')) {

    function apiRoutes()
    {
        $routes = [];
        app()->singleton('api.router', function ($app) use (&$routes) {
            $router = new \Dingo\Api\Routing\Router(
                $app[\Dingo\Api\Contract\Routing\Adapter::class],
                $app[\Dingo\Api\Contract\Debug\ExceptionHandler::class],
                $app,
                config('api.domain'),
                config('api.prefix')
            );
            $router->setConditionalRequest(config('api.conditionalRequest'));
            $routes = $router->getRoutes();
            return $router;
        });
        dd($routes);
        return $routes;
    }
}

if (!function_exists('formatBytes')) {

    function formatBytes($bytes, $precision = 2)
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}

if (!function_exists('member')) {

    function member()
    {
        return new \App\Services\Member\Member();
    }
}

if (!function_exists('isImageable')) {
    /**
     * @param string $model
     * @return bool
     */
    function isImageable(string $model)
    {
        $array = [
            \App\Cmf\Project\User\UserController::NAME,
            \App\Cmf\Project\Organization\OrganizationController::NAME,
            \App\Cmf\Project\Direction\DirectionController::NAME,
            \App\Cmf\Project\Departure\DepartureController::NAME,
            \App\Cmf\Project\Category\CategoryController::NAME,
            \App\Cmf\Project\Tour\TourController::NAME,
            \App\Cmf\Project\Entertainment\EntertainmentController::NAME,
            \App\Cmf\Project\Promotion\PromotionController::NAME,
            \App\Cmf\Project\News\NewsController::NAME,
            \App\Cmf\Project\Attraction\AttractionController::NAME,
            \App\Cmf\Project\Article\ArticleController::NAME,
            \App\Cmf\Project\Outfit\OutfitController::NAME,
        ];
        return in_array($model, $array);
    }
}

if (!function_exists('isTextImageable')) {
    /**
     * @param string $model
     * @return bool
     */
    function isTextImageable(string $model)
    {
        $array = [
            \App\Cmf\Project\News\NewsController::NAME,
            \App\Cmf\Project\Article\ArticleController::NAME,
            \App\Cmf\Project\Direction\DirectionController::NAME,
            \App\Cmf\Project\Organization\OrganizationController::NAME,
            \App\Cmf\Project\Attraction\AttractionController::NAME,
        ];
        return in_array($model, $array);
    }
}

if (!function_exists('strpos_all')) {
    /**
     * Все позиции strpos
     *
     * @param string $haystack
     * @param string $needle
     * @return array
     */
    function strpos_all($haystack, $needle)
    {
        $offset = 0;
        $allpos = [];
        while (($pos = mb_strpos($haystack, $needle, $offset)) !== false) {
            $offset = $pos + 1;
            $allpos[] = $pos;
        }
        return $allpos;
    }
}

if (!function_exists('isDownForMaintenance')) {
    /**
     * @return bool
     */
    function isDownForMaintenance(): bool
    {
        return \Illuminate\Support\Facades\App::isDownForMaintenance();
    }
}


if (!function_exists('imagePath')) {

    /**
     * @param $key
     * @param $size
     * @param $image
     * @param string $type
     * @return string|null
     */
    function imagePath($key, $size, $image, $type = \App\Services\Image\ImageType::MODEL): ?string
    {
        if ($type === \App\Services\Image\ImageType::MODEL) {
            return (new \App\Services\Image\Path\ImagePathModelService())->getImage($key, $size, $image);
        }
        if ($type === \App\Services\Image\ImageType::GALLERY) {
            return (new \App\Services\Image\Path\ImagePathGalleryService())->getImage($key, $size, $image);
        }
        if ($type === \App\Services\Image\ImageType::BANNER) {
            return (new \App\Services\Image\Path\ImagePathBannerService())->getImage($key, $size, $image);
        }
        return null;
    }
}

if (!function_exists('declensionWord')) {

    /**
     * Окончания для слов по count
     *
     * @param $count
     * @param array $aWords ['дубль', 'дубля', 'дублей']
     * @param null $originalCount
     * @return mixed|null
     */
    function declensionWord($count, array $aWords, $originalCount = null)
    {
        $count = intval($count);
        $oldCount = $count;
        if (!is_null($originalCount)) {
            $count = $originalCount;
        }
        if ($count === 0) {
            return $aWords[2];
        }
        if ($count === 1) {
            return $aWords[0];
        }
        if ($count > 1 && $count < 5) {
            return $aWords[1];
        }
        if ($count >= 5 && $count < 21) {
            return $aWords[2];
        }
        if ($count > 20) {
            return declensionWord($oldCount, $aWords, intval(substr($count, 1)));
        }
        return null;
    }
}

if (!function_exists('frontendCommon')) {

    function frontendCommon()
    {
        return (new \App\Services\FrontendCommon());
    }
}

if (!function_exists('commandsExecute')) {

    function commandsExecute()
    {
        $prefix = 'queue:work';
        $output = [];

        switch (PHP_OS_FAMILY) {
            case 'Linux':
                exec(sprintf('pgrep -fa "%s"', $prefix), $output);
                break;
            case 'Darwin':
                exec(sprintf('pgrep -fl "%s"', $prefix), $output);
                break;
            case 'Windows':
                break;
            default:
                break;
        }

        $output = array_map(function (string $output) {
            $fullExplode = explode(' ', $output);
            return [$fullExplode[0], implode(' ', \array_slice($fullExplode, 1))];
        }, $output);

        return $output;
    }
}
if (!function_exists('checkQueue')) {

    /**
     * @param string $queue
     * @return bool
     */
    function checkQueue(string $queue = \App\Jobs\QueueCommon::QUEUE_NAME_SEO): bool
    {
        $commands = commandsExecute();
        $path = config('queue.path');
        foreach ($commands as $value) {
            if (isset($value[1])) {
                if (!is_null($path)) {
                    $re = '/(.*)(\/' . $path . ')(.*)(queue:work)(.*)(\-\-queue=' . $queue . ')(.*)/';
                } else {
                    $re = '/(.*)(queue:work)(.*)(\-\-queue=' . $queue . ')(.*)/';
                }
                $found = preg_match($re, $value[1], $matches);
                if ($found) {
                    return true;
                }
            }
        }
        return false;
    }
}

if (!function_exists('checkQueueGetId')) {

    /**
     * @param string $queue
     * @return null|int
     */
    function checkQueueGetId(string $queue = \App\Jobs\QueueCommon::QUEUE_NAME_SEO): ?int
    {
        $commands = commandsExecute();
        $path = config('queue.path');
        foreach ($commands as $value) {
            if (isset($value[1])) {
                if (!is_null($path)) {
                    $re = '/(.*)(\/' . $path . ')(.*)(queue:work)(.*)(\-\-queue=' . $queue . ')(.*)/';
                } else {
                    $re = '/(.*)(queue:work)(.*)(\-\-queue=' . $queue . ')(.*)/';
                }
                $found = preg_match($re, $value[1], $matches);
                if ($found) {
                    return $value[0];
                }
            }
        }
        return null;
    }
}

if (!function_exists('progressWidth')) {

    /**
     * @param int|float $max
     * @param int|float $current
     * @return int
     */
    function progressWidth($max, $current): int
    {
        return ceil(($current / $max) * 100);
    }
}

if (!function_exists('remember')) {

    /**
     * @param string $name
     * @param \Closure $function
     * @param int $time
     * @return mixed
     */
    function remember($name, $function, $time = 3600)
    {
        return Cache::remember($name, $time, $function);
    }
}


if (!function_exists('uploadFileFromPath')) {

    /**
     * @param string $filePathWithFilename
     * @param string $fileName
     * @return UploadedFile
     */
    function uploadFileFromPath(string $filePathWithFilename, string $fileName): UploadedFile
    {
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        return new UploadedFile($filePathWithFilename, $fileName, $finfo->file($filePathWithFilename), filesize($filePathWithFilename));
    }
}

if (!function_exists('array_get')) {

    /**
     * @param array $array
     * @param string $key
     * @return mixed
     */
    function array_get(array $array, string $key)
    {
        return \Illuminate\Support\Arr::get($array, $key);
    }
}

if (!function_exists('colorFromFirstLetter')) {
    function colorFromFirstLetter(string $string)
    {
        $word = mb_strtolower(mb_substr($string, 0, 1));
        $data = [
            'а' => '#1c8b82', 'б' => '#2f4335', 'в' => '#b4986d',
            'г' => '#060054', 'д' => '#d6d2b5', 'е' => '#bbb584',
            'ё' => '#0f5411', 'ж' => '#098765', 'з' => '#234567',
            'и' => '#677587', 'й' => '#246801', 'к' => '#ffa07a',
            'л' => '#123456', 'м' => '#203984', 'н' => '#675489',
            'о' => '#394857', 'п' => '#674887', 'р' => '#768433',
            'с' => '#977586', 'т' => '#c1cefa', 'у' => '#194f48',
            'ф' => '#956389', 'х' => '#049853', 'ц' => '#023984',
            'ч' => '#678549', 'ш' => '#565784', 'щ' => '#758475',
            'ь' => '#046b00', 'ы' => '#f54000', 'ъ' => '#8b0101',
            'э' => '#37595c', 'ю' => '#942d56', 'я' => '#339ca8',

            'a' => '#1c8b82', 'b' => '#2f4335', 'c' => '#b4986d',
            'd' => '#060054', 'e' => '#d6d2b5', 'f' => '#bbb584',
            'g' => '#0f5411', 'h' => '#098765', 'i' => '#023984',
            'j' => '#234567', 'k' => '#677587', 'l' => '#246801',
            'm' => '#ffa07a', 'n' => '#123456', 'o' => '#758475',
            'p' => '#203984', 'q' => '#675489', 'r' => '#394857',
            's' => '#942d56', 't' => '#768433', 'u' => '#8b0101',
            'v' => '#977586', 'w' => '#c1cefa', 'x' => '#194f48',
            'y' => '#678549', 'z' => '#565784',
        ];
        return strtr($word, $data);
    }
}

if (!function_exists('natural_language_join')) {
    /**
     * @param array $list
     * @param string $conjunction
     * @return mixed|string
     */
    function natural_language_join(array $list, $conjunction = 'и') {
        $last = array_pop($list);
        if ($list) {
            return implode(', ', $list) . ' ' . $conjunction . ' ' . $last;
        }
        return $last;
    }
}

if (!function_exists('slackDebug')) {
    function slackDebug(string $message) {
        (new \App\Services\Notification\Slack\SlackDebugNotification())->send($message);
    }
}

if (!function_exists('serviceTransformerTour')) {
    /**
     * @return \App\Services\Transformers\TourServiceTransformer
     */
    function serviceTransformerTour() {
        return new \App\Services\Transformers\TourServiceTransformer();
    }
}

if (!function_exists('serviceFieldTour')) {
    /**
     * @return \App\Services\Transformers\TourServiceField
     */
    function serviceFieldTour() {
        return new \App\Services\Transformers\TourServiceField();
    }
}

if (!function_exists('userActionService')) {

    /**
     * @param \App\Models\User|\Illuminate\Contracts\Auth\Authenticatable|null $oUser
     * @return \App\Services\UserAction\UserActionService
     */
    function userActionService($oUser = null) {
        return new \App\Services\UserAction\UserActionService($oUser);
    }
}


