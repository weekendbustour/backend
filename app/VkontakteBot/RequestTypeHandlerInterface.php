<?php

declare(strict_types=1);

namespace App\VkontakteBot;

use Illuminate\Http\Request;

interface RequestTypeHandlerInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public static function handle(Request $request);
}
