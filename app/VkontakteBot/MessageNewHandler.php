<?php

declare(strict_types=1);

namespace App\VkontakteBot;

use App\VkontakteBot\Response\ActionResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use VK\Client\VKApiClient;

abstract class MessageNewHandler implements RequestTypeHandlerInterface
{
    /**
     * @param Request $request
     * @return void
     */
    public static function handle(Request $request)
    {
        $userId = $request->object['from_id'];

        if (config('app.env') === 'production') {
            $actionResponse = new ActionResponse(
                $request,
                new VKApiClient('5.110'),
                env('VK_SECRET_KEY_GROUP')
            );

            //$actionResponse->start();
            $actionResponse->post();
        }
    }
}
