<?php

declare(strict_types=1);

namespace App\VkontakteBot\Response;

use App\VkontakteBot\BotKeyboard\Button;
use App\VkontakteBot\BotKeyboard\ButtonRowFactory;
use App\VkontakteBot\BotKeyboard\KeyboardFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use VK\Client\VKApiClient;

class ActionResponse
{

    private $vkApiClient;
    private $request;
    private $accessToken;
    private $botStandartMessages;
    private $botStandartAttachments;
    private $botButtonLabels;

    public function __construct(
        Request $request,
        VKApiClient $vkApiClient,
        string $accessToken
    ) {
        $this->botStandartMessages = config('bot.messages');
        $this->botStandartAttachments = config('bot.media');
        $this->botButtonLabels = config('bot.buttons');
        $this->request = $request;
        $this->vkApiClient = $vkApiClient;
        $this->accessToken = $accessToken;
    }

//    public function start()
//    {
//        $params = [
//            'user_id' => $this->request->object['from_id'],
//            'random_id' => rand(0, 2 ** 31),
//            'message' => 'Привет чепушило, я тебя ждал',
//        ];
//
//        $this->vkApiClient->messages()->send($this->accessToken, $params);
//    }

    public function start()
    {
        $btnYes = Button::create(['button' => 'yes'], 'Да', 'primary');
        $btnNo = Button::create(['button' => 'no'], 'Нет', 'negative');

        $btnRow1 = ButtonRowFactory::createRow()
            ->addButton($btnYes)
            ->addButton($btnNo)
            ->getRow();

        $kb = KeyboardFactory::createKeyboard()
            ->addRow($btnRow1)
            ->setOneTime(false)
            ->getKeyboardJson();

        $params = [
            'user_id' => $this->request->object['from_id'],
            'random_id' => rand(0, 2 ** 31),
            'message' => 'Че ты пес, идешь на репу сегодня в 14?',
            'keyboard' => $kb,
        ];

        $this->vkApiClient->messages()->send($this->accessToken, $params);
    }

    public function post($message)
    {
        $params = [
            'group_id' => env('VKONTAKTE_GROUP_ID'),
        ];
        $server = $this->vkApiClient->photos()->getWallUploadServer($this->accessToken, $params);

        if (isset($server['upload_url'])) {
            $image = public_path('img/about.jpg');
            // Отправка изображения на сервер.
            if (function_exists('curl_file_create')) {
                $curl_file = curl_file_create($image);
            } else {
                $curl_file = '@' . $image;
            }
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $server['upload_url']);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['photo' => $curl_file]);
            $upload = curl_exec($ch);
            curl_close($ch);

            $upload = json_decode($upload);
            if (!empty($upload->server)) {
                $save = $this->vkApiClient->photos()->saveWallPhoto($this->accessToken, [
                    'group_id' => env('VKONTAKTE_GROUP_ID'),
                    'server' => $upload->server,
                    'photo' => stripslashes($upload->photo),
                    'hash' => $upload->hash,
                ]);
                $response = $save[0];
                if (!empty($response['id'])) {
                    // Отправляем сообщение.
                    $params = [
                        'owner_id' => -env('VKONTAKTE_GROUP_ID'),
                        'from_group' => 1,
                        'message' => $message,
                        'copyright' => asset('/'),
                        'attachments' => 'photo' . $response['owner_id'] . '_' . $response['id'] . ',https://weekendbustour.ru/tour/47/fitnes-tur-adygeya-guamskoe-ushchele',
                    ];
                    $this->vkApiClient->wall()->post($this->accessToken, $params);
                }
            }
        }
        dd('error');

        $params = [
            'owner_id' => -env('VKONTAKTE_GROUP_ID'),
            'from_group' => 1,
            'message' => $message,
            'copyright' => asset('/'),
            'attachments' => 'https://weekendbustour.ru/tour/47/fitnes-tur-adygeya-guamskoe-ushchele',
        ];
        $this->vkApiClient->wall()->post($this->accessToken, $params);
    }

    public function market($message)
    {
        $params = [
            'group_id' => env('VKONTAKTE_GROUP_ID'),
            'main_photo' => 1,
        ];
        $this->vkApiClient->photos()->getMarketUploadServer($this->accessToken, $params);

        $params = [
            'owner_id' => -env('VKONTAKTE_GROUP_ID'),
            'name' => $message,
            'description' => 'description',
            'category_id' => 1,
            'price' => 10,
            'url' => 'https://weekendbustour.ru',
            'main_photo_id' => 'https://weekendbustour.ru',
        ];
        $this->vkApiClient->market()->add($this->accessToken, $params);
    }
}
