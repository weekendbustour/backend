<?php

declare(strict_types=1);

namespace App\VkontakteBot\BotKeyboard;

class ButtonRow
{

    /**
     * @var array
     */
    private $buttonRow = [];

    /**
     * @param array $button
     * @return ButtonRow
     */
    public function addButton(array $button): self
    {
        $this->buttonRow[] = $button;

        return $this;
    }

    /**
     * @return array
     */
    public function getRow(): array
    {
        return $this->buttonRow;
    }
}
