<?php

declare(strict_types=1);

namespace App\VkontakteBot\BotKeyboard;

class Button
{
    /**
     * @param array $payload
     * @param string $label
     * @param string $colorType
     * @return array
     */
    public static function create(array $payload, string $label, string $colorType): array
    {
        $button['action']['type'] = 'text';
        $button['action']['payload'] = json_encode($payload, JSON_UNESCAPED_UNICODE);
        $button['action']['label'] = $label;
        $button['color'] = $colorType;
        return $button;
    }
}
