<?php

declare(strict_types=1);

namespace App\VkontakteBot\BotKeyboard;

class KeyboardFactory
{

    public static function createKeyboard(): Keyboard
    {
        return new Keyboard();
    }
}
