<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Services\Notification\Slack\SlackSitemapNotification;
use App\Services\Seo\SeoSitemapService;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Spatie\Sitemap\Sitemap;

class UpdateSitemapJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;
    use Batchable;

    /**
     *
     */
    public function __construct()
    {
        $this->onQueue(QueueCommon::QUEUE_NAME_SEO);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (!is_null($this->batch()) && $this->batch()->cancelled()) {
            return;
        }
        $oSitemap = (new SeoSitemapService())->getSitemap();

        $oSitemap->writeToFile(public_path('sitemap.xml'));

        (new SlackSitemapNotification())->send('WAS UPDATED');

        Log::info('sitemap created');
    }

    /**
     * The job failed to process.
     *
     * @param \Exception $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        Log::critical($exception->getMessage());
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['seo'];
    }
}
