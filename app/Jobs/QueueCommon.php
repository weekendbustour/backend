<?php

declare(strict_types=1);

namespace App\Jobs;

class QueueCommon
{
    /**
     * Название очереди
     */
    const QUEUE_NAME_SEO = 'seo';

    /**
     * Название очереди
     */
    const QUEUE_NAME_MAIL = 'mail';

    /**
     * Название очереди
     */
    const QUEUE_NAME_SUBSCRIPTION = 'subscription';

    /**
     * Название очереди
     */
    const QUEUE_NAME_NOTIFICATION = 'notification';

    /**
     * @return string
     */
    public static function commandSeo(): string
    {
        return 'queue:listen --queue=' . self::QUEUE_NAME_SEO;
    }

    /**
     * @return string
     */
    public static function commandMail(): string
    {
        return 'queue:listen --queue=' . self::QUEUE_NAME_MAIL;
    }

    /**
     * @return string
     */
    public static function commandNotification(): string
    {
        return 'queue:listen --queue=' . self::QUEUE_NAME_NOTIFICATION;
    }

    /**
     * @return string
     */
    public static function commandSubscription(): string
    {
        return 'queue:listen --queue=' . self::QUEUE_NAME_SUBSCRIPTION;
    }

    /**
     * @return array
     */
    public function commands(): array
    {
        return [
            self::QUEUE_NAME_SEO => self::commandSeo(),
            self::QUEUE_NAME_MAIL => self::commandMail(),
            self::QUEUE_NAME_SUBSCRIPTION => self::commandSubscription(),
            self::QUEUE_NAME_NOTIFICATION => self::commandNotification(),
        ];
    }


    /**
     * @return bool
     */
    public static function commandSeoIsEnabled(): bool
    {
        return config('queue.channels.' . self::QUEUE_NAME_SEO . '.enabled');
    }

    /**
     * @return bool
     */
    public static function commandMailIsEnabled(): bool
    {
        return config('queue.channels.' . self::QUEUE_NAME_MAIL . '.enabled');
    }

    /**
     * @return bool
     */
    public static function commandNotificationIsEnabled(): bool
    {
        return config('queue.channels.' . self::QUEUE_NAME_NOTIFICATION . '.enabled');
    }

    /**
     * @return bool
     */
    public static function commandSubscriptionIsEnabled(): bool
    {
        return config('queue.channels.' . self::QUEUE_NAME_SUBSCRIPTION . '.enabled');
    }
}
