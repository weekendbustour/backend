<?php

declare(strict_types=1);

namespace App\Jobs\Mail\Subscription;

use App\Mail\Auth\RegisteredMail;
use App\Mail\Subscription\SubscriptionDirectionMail;
use App\Models\Direction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMailSubscriptionDirectionJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var User|object
     */
    protected $oUser;

    /**
     * @var Direction
     */
    protected $oDirection;

    /**
     * SendEmailJob constructor.
     * @param object|User $oUser
     * @param Direction $oDirection
     */
    public function __construct(object $oUser, Direction $oDirection)
    {
        $this->oUser = $oUser;
        $this->oDirection = $oDirection;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Mail::to($this->oUser->email)->send((new SubscriptionDirectionMail()));
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return [
            'subscription.' . SubscriptionDirectionMail::NAME,
            'direction.' . $this->oDirection->id,
            'user.' . $this->oUser->id,
        ];
    }
}
