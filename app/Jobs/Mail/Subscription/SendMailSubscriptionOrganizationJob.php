<?php

declare(strict_types=1);

namespace App\Jobs\Mail\Subscription;

use App\Mail\Auth\RegisteredMail;
use App\Mail\Subscription\SubscriptionOrganizationMail;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendMailSubscriptionOrganizationJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    /**
     * @var User|object
     */
    protected $oUser;

    /**
     * @var Organization
     */
    protected $oOrganization;

    /**
     * SendEmailJob constructor.
     * @param object|User $oUser
     * @param Organization $oOrganization
     */
    public function __construct(object $oUser, Organization $oOrganization)
    {
        $this->oUser = $oUser;
        $this->oOrganization = $oOrganization;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Mail::to($this->oUser->email)->send((new SubscriptionOrganizationMail()));
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return [
            'subscription.' . SubscriptionOrganizationMail::NAME,
            'organization.' . $this->oOrganization->id,
            'user.' . $this->oUser->id,
        ];
    }
}
