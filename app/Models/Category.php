<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Imageable;
use App\Services\Modelable\Parentable;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Departure
 *
 * @property int $id
 * @property string $type
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $color
 * @property int|null $parent_id
 * @property int $priority
 * @property int $status
 *
 * @property mixed $articles
 * @property mixed $articlesActiveOrdered
 */
class Category extends Model
{
    use Statusable;
    use Imageable;
    use Parentable;
    use Typeable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var array
     */
    protected $fillable = [
        'type', 'name', 'title', 'description', 'color', 'parent_id', 'priority', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    const TYPE_DEFAULT = 'default';
    const TYPE_TOURS = 'tours';
    const TYPE_ORGANIZATIONS = 'organizations';
    const TYPE_NEWS = 'news';
    const TYPE_ARTICLES = 'articles';

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $types = [
        self::TYPE_DEFAULT => 'По умолчанию',
        self::TYPE_TOURS => 'Туры',
        self::TYPE_ORGANIZATIONS => 'Организации',
        self::TYPE_NEWS => 'Новости',
        self::TYPE_ARTICLES => 'Статьи',
    ];

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('title');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = Str::slug($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articles()
    {
        return $this->belongsToMany(Article::class, 'article_category')->ordered();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articlesActiveOrdered()
    {
        return $this->belongsToMany(Article::class, 'article_category')->active()->ordered();
    }
}
