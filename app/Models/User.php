<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Comment\CanComment;
use App\Services\Modelable\Imageable;
use App\Services\Modelable\Rate\CanRate;
use App\Services\Modelable\Socialable;
use App\Services\Modelable\Statusable;
use Carbon\Carbon;
use ChristianKuri\LaravelFavorite\Traits\Favoriteability;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

/**
 * Class User
 *
 * @property int $id
 * @property string $email
 * @property string $login
 * @property string $first_name
 * @property string $second_name
 * @property string $last_name
 * @property string $phone
 * @property Carbon|null $birthday_at
 * @property int $status
 *
 * @property mixed $tokens
 *
 *
 * @see Imageable::getImageAttribute()
 * @property string $image
 * @property string $image_square
 *
 * @see Imageable::getGalleryAttribute()
 * @property array $gallery
 *
 *
 * @property mixed|Collection $favouriteTours
 * @property mixed|Collection $favouriteDirections
 * @property mixed|Collection $favouriteOrganizations
 * @property mixed|Collection $favouriteAttractions
 */
class User extends Authenticatable
{
    use Notifiable;
    use Statusable;
    use HasRoles;
    use Imageable;
    use Socialable;
    use CanRate;
    use Favoriteability;
    use CanComment;
    use HasApiTokens;

    /**
     *
     */
    const TOKEN_AUTH_NAME = 'token-auth';

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    const ROLE_USER = 'user';
    const ROLE_ORGANIZATION = 'organization';
    const ROLE_ADMIN = 'admin';
    const ROLE_SUPER_ADMIN = 'super-admin';

    const PERMISSION_ADMIN_VIEW = 'admin view';
    const PERMISSION_ADMIN_EDIT = 'admin edit';
    const PERMISSION_ADMIN_DELETE = 'admin delete';

    /**
     * @var string
     */
    protected $table = 'users';

    /**
     * @var array
     */
    protected $fillable = [
        'email', 'login', 'password', 'first_name', 'second_name', 'last_name', 'phone', 'email_verified_at',
        'birthday_at', 'gender', 'agree', 'mailing', 'is_online', 'parent_id', 'status',
    ];

    /**
     * @var array
     */
    public $dates = [
        'email_verified_at', 'birthday_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return mixed
     */
    public function getBirthdayDayAttribute()
    {
        return !is_null($this->birthday_at) ? $this->birthday_at->format('d') : '';
    }

    /**
     * @return mixed
     */
    public function getBirthdayMonthAttribute()
    {
        return !is_null($this->birthday_at) ? $this->birthday_at->format('m') : '';
    }

    /**
     * @return mixed
     */
    public function getBirthdayYearAttribute()
    {
        return !is_null($this->birthday_at) ? $this->birthday_at->format('Y') : '';
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param null|string $value
     */
    public function setPhoneAttribute(?string $value)
    {
        $value = preg_replace('/[^0-9]/', '', $value);
        if (Str::startsWith($value, '8')) {
            $value = '7' . substr($value, 1);
        }
        $this->attributes['phone'] = $value;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activations()
    {
        return $this->hasMany(Activation::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favouriteTours()
    {
        return $this->favorites()->where('favoriteable_type', Tour::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favouriteDirections()
    {
        return $this->favorites()->where('favoriteable_type', Direction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favouriteOrganizations()
    {
        return $this->favorites()->where('favoriteable_type', Organization::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favouriteAttractions()
    {
        return $this->favorites()->where('favoriteable_type', Attraction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptionsDirections()
    {
        return $this->subscriptions()->where('subscriptionable_type', Direction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptionsOrganizations()
    {
        return $this->subscriptions()->where('subscriptionable_type', Organization::class);
    }

    /**
     * @param Comment $oComment
     * @return bool
     */
    public function commentCanBeVoted(Comment $oComment)
    {
        $oVote = $oComment->votes()->where('user_id', $this->id)->first();
        return is_null($oVote);
    }

    /**
     * @param Comment $oComment
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\MorphMany|CommentVote|null
     */
    public function commentGetVote(Comment $oComment)
    {
        return $oComment->votes()->where('user_id', $this->id)->first();
    }

    /**
     * @param Comment $oComment
     * @param int $value
     * @return Comment
     */
    public function commentVote(Comment $oComment, int $value)
    {
        $oComment->votes()->create([
            'user_id' => $this->id,
            'value' => $value,
        ]);
        return $oComment;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commentsOrganizations()
    {
        return $this->morphMany(Comment::class, 'commented')->where('commentable_type', Organization::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commentsDirections()
    {
        return $this->morphMany(Comment::class, 'commented')->where('commentable_type', Direction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commentsNews()
    {
        return $this->morphMany(Comment::class, 'commented')->where('commentable_type', News::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commentsArticles()
    {
        return $this->morphMany(Comment::class, 'commented')->where('commentable_type', Article::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function commentsAttractions()
    {
        return $this->morphMany(Comment::class, 'commented')->where('commentable_type', Attraction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservations()
    {
        return $this->hasMany(TourReservation::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function votes()
    {
        return $this->hasMany(CommentVote::class, 'user_id');
    }

    public function getRoleNameAttribute(): string
    {
        $roles = $this->roles();
        $default = 'Неизвестно';
        return !is_null($roles->first()) ? $roles->first()->toArray()['title'] : $default;
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return $this->last_name . ' ' . $this->first_name . ' ' . $this->second_name;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function socialAccount()
    {
        return $this->hasOne(UserSocialAccount::class, 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userActions()
    {
        return $this->hasMany(UserAction::class, 'user_id');
    }
}
