<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Option
 *
 * @property int $id
 * @property string $purpose
 * @property string $title
 * @property string $description
 * @property string $placeholder
 * @property string $name
 * @property string $unit
 * @property int $type
 * @property string $tooltip
 * @property int $priority
 * @property int $status
 */
class Option extends Model
{
    use Statusable;

    /**
     * @var string
     */
    protected $table = 'options';

    /**
     * @var array
     */
    protected $fillable = [
        'purpose', 'title', 'description', 'placeholder', 'name', 'unit', 'type', 'tooltip', 'priority', 'status',
    ];

    /**
     *
     */
    const NAME_VIDEO_YOUTUBE = 'video_youtube';
    const NAME_TIME_ROAD = 'time_road';
    const NAME_TAKE_WITH_YOU = 'take_with_you';
    const NAME_DAYS = 'days';
    const NAME_PLACES = 'places';
    const NAME_PHONE_INFO = 'phone_info';
    const NAME_INCLUDE = 'include';
    const NAME_NOT_INCLUDE = 'not_include';
    const NAME_MORE_INCLUDE = 'more_include';
    const NAME_GID = 'gid';
    const NAME_TAKE_WITH_YOU_IN_MOUNT = 'take_with_you_in_mount';
    const NAME_PHOTO_SESSION = 'fotootchet';
    const NAME_TIMELINE_NOTE = 'timeline_note';
    const NAME_OUTFIT_RENT_NOTE = 'outfit_rent_note';
    const NAME_RESERVATION = 'reservation';

    const NAME_PREPAYMENT_CANCELLATION = 'prepayment_cancellation';

    const PURPOSE_DEFAULT = 'default';
    const PURPOSE_TOURS = 'tour';
    const PURPOSE_ORGANIZATIONS = 'organization';
    const PURPOSE_ARTICLES = 'article';
    const PURPOSE_ATTRACTION = 'attraction';

    const NAME_DISTANCE_FROM_ROSTOV = 'distance_from_rostov';
    const NAME_DISTANCE_FROM_KRASNODAR = 'distance_from_krasnodar';

    const NAME_WAY = 'way';

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        if (!isset($this->attributes['name'])) {
            $this->attributes['name'] = Str::slug($value, '_');
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function parameters()
    {
        return $this->hasMany(OptionParameter::class, 'option_id', 'id');
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('priority', 'desc');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopePurposeTours(Builder $query): Builder
    {
        return $query->where('purpose', self::PURPOSE_TOURS);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopePurposeOrganizations(Builder $query): Builder
    {
        return $query->where('purpose', self::PURPOSE_ORGANIZATIONS);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopePurposeArticles(Builder $query): Builder
    {
        return $query->where('purpose', self::PURPOSE_ARTICLES);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopePurposeAttraction(Builder $query): Builder
    {
        return $query->where('purpose', self::PURPOSE_ATTRACTION);
    }
}
