<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class NewsStatistic
 *
 * @property int $id
 * @property int $news_id
 * @property int $views
 * @property int $comments
 * @property int $redirects
 * @property int $likes
 * @property int $favourites
 */
class NewsStatistic extends Model
{
    /**
     * @var string
     */
    protected $table = 'news_statistics';

    /**
     * @var array
     */
    protected $fillable = [
        'news_id', 'views', 'comments', 'redirects', 'likes', 'favourites',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function news()
    {
        return $this->belongsTo(News::class);
    }
}
