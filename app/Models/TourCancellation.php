<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Imageable;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class TourTimeline
 *
 * @property int $id
 * @property int $tour_id
 * @property string $type
 * @property string $tooltip
 * @property string $description
 * @property string $title_by_type
 * @property null|Carbon $start_at
 * @property bool $start_at_hourly
 * @property null|Carbon $finish_at
 * @property bool $finish_at_hourly
 * @property int $status
 */
class TourCancellation extends Model
{
    use Statusable;
    use Typeable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'tour_cancellations';

    /**
     * @var array
     */
    protected $fillable = [
        'tour_id',
        'type',
        'tooltip',
        'description',
        'start_at', 'finish_at',
        'start_at_hourly', 'finish_at_hourly',
        'status',
    ];

    /**
     * @var array
     */
    public $dates = [
        'start_at', 'finish_at',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     *
     */
    const TYPE_QUARANTINE = 'quarantine';
    const TYPE_CANCELED = 'canceled';
    const TYPE_PASSED = 'passed';
    const TYPE_CARRYOVER = 'carryover';
    const TYPE_NOT_PUBLISHED = 'not_published';

    /**
     * @var array
     */
    protected $types = [
        self::TYPE_QUARANTINE => 'Карантин',
        self::TYPE_CANCELED => 'Отменен',
        self::TYPE_PASSED => 'Прошел',
        self::TYPE_CARRYOVER => 'Перенесен',
        self::TYPE_NOT_PUBLISHED => 'Не опубликован',
    ];

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('type');
    }

    /**
     * @return string
     */
    public function getTitleByTypeAttribute()
    {
        switch ($this->type) {
            case self::TYPE_QUARANTINE:
                return 'Тур отменен из-за Карантина';
            case self::TYPE_CANCELED:
                return 'Тур отменен';
            case self::TYPE_PASSED:
                return 'Тур уже прошел';
            case self::TYPE_CARRYOVER:
                return 'Тур перенесен';
            case self::TYPE_NOT_PUBLISHED:
                return 'Тур еще не опубликован';
        }
        return '';
    }
}
