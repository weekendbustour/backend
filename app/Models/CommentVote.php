<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class CommentVote extends Model
{
    protected $guarded = [];

    protected $table = 'comment_votes';

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', 1);
    }

    public function model()
    {
        return $this->morphTo();
    }

    public function votable()
    {
        return $this->morphTo();
    }
}
