<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ExternalVkParameter
 * @package App\Models
 *
 * @property string $type
 * @property int $item_id
 *
 *
 */
class ExternalVkParameter extends Model
{
    use Statusable;
    use Typeable;

    /**
     * @var string
     */
    protected $table = 'external_vk_parameters';

    /**
     * @var array
     */
    protected $fillable = [
        'type', 'external_vk_parametrable_id', 'external_vk_parametrable_type', 'item_id', 'photo_id',
        'images', 'album_id', 'category_id', 'published_at', 'status',
    ];

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    const TYPE_POST = 'post';
    const TYPE_MARKET = 'market';
    const TYPE_ALBUM = 'album';

    /**
     * @var array
     */
    public $dates = [
        'published_at',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];
}
