<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Subscription
 *
 * @property int $id
 * @property string $type
 * @property int $priceable_id
 * @property string $priceable_type
 * @property int $value
 * @property string $value_description
 * @property int $discount
 * @property string $discount_description
 * @property int $prepayment
 * @property string $prepayment_description
 * @property Carbon|null $discount_before_at
 * @property int $status
 */
class Price extends Model
{
    use Statusable;

    /**
     * @var string
     */
    protected $table = 'prices';

    /**
     * @var array
     */
    protected $fillable = [
        'type', 'priceable_id', 'priceable_type', 'value', 'value_description',
        'discount',
        'discount_description',
        'discount_before_at',
        'prepayment', 'prepayment_description',
        'status',
    ];

    public $dates = [
        'discount_before_at',
    ];

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     *
     */
    const TYPE_APP = 'app';
    const TYPE_MODEL = 'model';

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('created_at');
    }
}
