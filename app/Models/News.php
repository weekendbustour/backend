<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Comment\Commentable;
use App\Services\Modelable\Imageable;
use App\Services\Modelable\Locationable;
use App\Services\Modelable\Rate\Rateable;
use App\Services\Modelable\Sourceable;
use App\Services\Modelable\Statusable;
use Carbon\Carbon;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class News
 * @property int $id
 * @property string $position
 * @property string $name
 * @property string $title
 * @property string $preview_description
 * @property string $description
 * @property string $source
 * @property null|Carbon $release_at
 * @property null|Carbon $published_at
 * @property int $status
 * @property int|bool $with_banner
 *
 * @property mixed|Collection $categories
 * @property mixed|NewsStatistic $statistic
 *
 * @package App\Models
 */
class News extends Model
{
    use Statusable;
    use Imageable;
    use Commentable;
    use Rateable;
    use Favoriteable;
    use Sourceable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'news';

    /**
     * @var array
     */
    protected $fillable = [
        'position', 'name', 'title', 'preview_description', 'description', 'with_banner', 'release_at', 'published_at', 'status',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'release_at', 'published_at',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    const POSITION_DEFAULT = 'default';
    const POSITION_BANNER = 'banner';


    /**
     * @return string
     */
    public function textableKey(): string
    {
        return 'description';
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE
            && $this->published_at !== null
            && $this->release_at !== null
            && $this->release_at < now();
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query
            ->where('status', self::STATUS_ACTIVE)
            ->whereNotNull('release_at')
            ->whereNotNull('published_at');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('published_at', 'desc');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderedReserve(Builder $query): Builder
    {
        return $query->orderBy('published_at', 'asc');
    }

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        if (!isset($this->attributes['name'])) {
            $this->attributes['name'] = Str::slug($value, '-');
        }
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('app.news.item', ['id' => $this->id, 'name' => $this->name]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'news_category')
            ->whereIn('type', [
                Category::TYPE_NEWS,
            ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function statistic()
    {
        return $this->hasOne(NewsStatistic::class, 'news_id', 'id');
    }
}
