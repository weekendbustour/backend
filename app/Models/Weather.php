<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Weather
 *
 * @property int $id
 * @property int $location_id
 * @property string $type
 * @property int $degrees
 * @property string $precipitation
 * @property string $humidity
 * @property string $wind
 * @property string $options
 * @property int $status
 * @property float $latitude
 * @property float $longitude
 * @property int $zoom
 */
class Weather extends Model
{
    use Statusable;

    /**
     * @var string
     */
    protected $table = 'weathers';

    /**
     * @var array
     */
    protected $fillable = [
        'location_id', 'type', 'icon', 'degrees', 'condition', 'precipitation', 'humidity', 'wind', 'options',
        'status',
        'temp',
        'feels_like',
        'pressure',
        'cloudness',
    ];

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('created_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }
}
