<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizationOptionValues extends Model
{
    /**
     * @var string
     */
    protected $table = 'organization_option_values';

    /**
     * @var array
     */
    protected $fillable = [
        'organization_id', 'option_id', 'parameter_id', 'value', 'priority'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parameter()
    {
        return $this->hasOne(OptionParameter::class, 'id', 'parameter_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function option()
    {
        return $this->hasOne(Option::class, 'id', 'option_id');
    }
}
