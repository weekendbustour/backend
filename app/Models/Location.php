<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Subscription
 *
 * @property int $id
 * @property int $status
 * @property float $latitude
 * @property float $longitude
 * @property int $zoom
 * @property string $type
 * @property string $title
 * @property string $description
 * @property string $placeholder
 */
class Location extends Model
{
    use Statusable;
    use Typeable;

    /**
     * @var string
     */
    protected $table = 'locations';

    /**
     * @var array
     */
    protected $fillable = [
        'city_id', 'locationable_id', 'locationable_type', 'location', 'latitude', 'longitude',
        'zoom', 'components', 'options', 'status',
        'title', 'description', 'placeholder', 'type',
    ];

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    const TYPE_DEFAULT = 'default';
    const TYPE_ATTRACTION = 'attraction';

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $types = [
        self::TYPE_DEFAULT => 'По умолчанию',
        self::TYPE_ATTRACTION => 'Достопримечательность',
    ];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('created_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function weather()
    {
        return $this->hasOne(Weather::class, 'location_id', 'id');
    }


    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeDirections(Builder $query): Builder
    {
        return $query->where('locationable_type', Direction::class);
    }
}
