<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TourReservation
 * @property int $id
 * @property int|null $tour_id
 * @property int|null $user_id
 * @property string|null $phone
 * @property float $amount
 * @property string $code
 * @property int $places
 * @property int|null $payment_id
 * @property Carbon|null $payment_at
 * @property string|null $source
 * @property int $status
 *
 * @property null|Tour $tour
 * @property null|Payment $payment
 *
 *
 * @package App\Models
 */
class TourReservation extends Model
{
    use Statusable;
    use Typeable;

    /**
     * @var string
     */
    protected $table = 'tour_reservations';

    /**
     * @var array
     */
    protected $fillable = [
        'tour_id', 'user_id', 'phone', 'amount', 'places', 'code', 'payment_id', 'payment_at', 'source', 'status',
    ];

    public $dates = [
        'payment_at',
    ];

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_PROCESSED = 1;
    const STATUS_APPROVED = 2;
    const STATUS_PAYED_PROCESS = 3;
    const STATUS_PAYED_SUCCESS = 4;
    const STATUS_PAYED_ERROR = 5;
    const STATUS_SUCCESS = 6;
    const STATUS_CLOSED = 7;
    const STATUS_CANCELED = 8;
    const STATUS_CANCELED_ORGANIZATION = 9;
    const STATUS_CANCELED_USER = 10;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_PROCESSED => 'На рассмотрении',
        self::STATUS_APPROVED => 'Одобрено',
        self::STATUS_PAYED_PROCESS => 'Обрабатывается платеж',
        self::STATUS_PAYED_SUCCESS => 'Платеж обработан',
        self::STATUS_PAYED_ERROR => 'Ошибка платежа',
        self::STATUS_SUCCESS => 'Успешно забронировано',
        self::STATUS_CANCELED => 'Отменено',
        self::STATUS_CANCELED_ORGANIZATION => 'Отменено Организатором',
        self::STATUS_CANCELED_USER => 'Отменено Пользователем',
        self::STATUS_CLOSED => 'Закрыто',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
            'type' => 'default',
            'description' => null,
        ],
        self::STATUS_PROCESSED => [
            'class' => 'badge badge-success',
            'type' => 'success',
            //'description' => 'Расмотрение заявки в среднем занимает 1 день.',
        ],
        self::STATUS_APPROVED => [
            'class' => 'badge badge-info',
            'type' => 'info',
        ],
        self::STATUS_PAYED_PROCESS => [
            'class' => 'badge badge-default',
            'type' => 'default',
        ],
        self::STATUS_PAYED_SUCCESS => [
            'class' => 'badge badge-success',
            'type' => 'success',
        ],
        self::STATUS_PAYED_ERROR => [
            'class' => 'badge badge-danger',
            'type' => 'danger',
        ],
        self::STATUS_SUCCESS => [
            'class' => 'badge badge-success',
            'type' => 'success',
        ],
        self::STATUS_CANCELED => [
            'class' => 'badge badge-danger',
            'type' => 'danger',
        ],
        self::STATUS_CLOSED => [
            'class' => 'badge badge-default',
            'type' => 'default',
        ],
        self::STATUS_CANCELED_ORGANIZATION => [
            'class' => 'badge badge-default',
            'type' => 'default',
        ],
        self::STATUS_CANCELED_USER => [
            'class' => 'badge badge-default',
            'type' => 'default',
        ],
    ];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->whereNotIn('status', [
            self::STATUS_NOT_ACTIVE,
            self::STATUS_CANCELED,
            self::STATUS_CANCELED_ORGANIZATION,
            self::STATUS_CANCELED_USER,
        ]);
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return !in_array($this->status, [
            self::STATUS_NOT_ACTIVE,
            self::STATUS_CANCELED,
            self::STATUS_CANCELED_ORGANIZATION,
            self::STATUS_CANCELED_USER,
        ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function payment()
    {
        return $this->belongsTo(Payment::class);
    }
}
