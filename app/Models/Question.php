<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 *
 * @property int $id
 * @property string $type
 * @property int $questionable_id
 * @property string $questionable_type
 * @property string $question
 * @property string $answer
 * @property int $priority
 * @property int $status
 *
 *
 * @property mixed $questionable
 * @property string|null $questionableTitle
 *
 * @package App\Models
 */
class Question extends Model
{
    /**
     *
     */
    const TYPE_DEFAULT = 'default';

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;


    /**
     * @var string
     */
    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'questionable_id', 'questionable_type', 'question', 'answer', 'priority', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     * @var array
     */
    public $types = [
        self::TYPE_DEFAULT => 'Обычный вопрос',
    ];

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    public $itemTypes = [
        null => [
            'title' => 'Сайт',
            'type' => 'default',
        ],
        Organization::class => [
            'title' => 'Организатор',
            'type' => 'success',
        ],
        Tour::class => [
            'title' => 'Тур',
            'type' => 'success',
        ]
    ];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('priority', 'desc');
    }

    /**
     * @return array
     */
    public function getQuestionableObjectDataAttribute()
    {
        $data = $this->itemTypes[$this->questionable_type];
        if (!is_null($this->questionable_type)) {
            $data['title'] = $this->questionableTitle;
        }
        return $data;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function questionable()
    {
        return $this->morphTo();
    }

    /**
     * @return string|null
     */
    public function getQuestionableTitleAttribute(): ?string
    {
        switch ($this->questionable_type) {
            case Organization::class:
                return 'Организатор: ' . $this->questionable->title;
            case Tour::class:
                return 'Тур: ' . $this->questionable->title;
            default:
                return null;
        }
    }
}
