<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleStatistic
 *
 * @property int $id
 * @property int $article_id
 * @property int $views
 * @property int $comments
 * @property int $redirects
 * @property int $likes
 * @property int $favourites
 */
class ArticleStatistic extends Model
{
    /**
     * @var string
     */
    protected $table = 'article_statistics';

    /**
     * @var array
     */
    protected $fillable = [
        'article_id', 'views', 'comments', 'redirects', 'likes', 'favourites',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function article()
    {
        return $this->belongsTo(Article::class);
    }
}
