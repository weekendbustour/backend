<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\ExternalVkParamentable;
use App\Services\Modelable\Imageable;
use App\Services\Modelable\MetaTagable;
use App\Services\Modelable\Parentable;
use App\Services\Modelable\Previewable;
use App\Services\Modelable\Priceable;
use App\Services\Modelable\Questionable;
use App\Services\Modelable\Sourceable;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\UserActionable;
use Carbon\Carbon;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class Tour
 *
 * @property int $id
 * @property int $organization_id
 * @property int $direction_id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $preview_description
 * @property int $days
 * @property int $places
 * @property string $include
 * @property string $not_include
 * @property boolean $multi
 * @property string $multi_code
 *
 * @property null|mixed|Collection|Price $price
 * @property null|mixed|TourCancellation $cancellation
 * @property null|mixed|TourAnnouncement $announcement
 *
 * @property Carbon $start_at
 * @property Carbon $finish_at
 *
 * @property Carbon $published_at
 * @property Carbon $release_at
 *
 * @property int $duplicate_id
 * @property int $status
 *
 * @property mixed $timelines
 *
 * @property mixed $values
 *
 * @property null|Tag $tag
 * @property null|Organization $organization
 * @property null|Direction $direction
 *
 * @property mixed|Collection $outfits
 * @property mixed|Collection $outfitsOrdered
 * @property mixed|Collection $departures
 * @property mixed|Collection $categories
 * @property mixed|Collection $entertainments
 * @property mixed|Collection $attractions
 * @property mixed|Collection $images
 *
 * @see Imageable::getImageAttribute()
 * @property string $image
 * @property string $image_default
 * @property string $image_card
 *
 * @see Imageable::getGalleryAttribute()
 * @property array $gallery
 * @property array $image_gallery
 *
 * @see Imageable::getImageBannerAttribute()
 * @property array $image_banner
 *
 *
 * @property mixed|Collection $multiples
 * @property mixed|Collection $multiplesActive
 * @property mixed|Collection $duplicates
 * @property null|Tour $parentDuplicate
 *
 *
 * @property null|ExternalVkParameter $vkPost
 * @property null|ExternalVkParameter $vkMarket
 * @property null|ExternalVkParameter $vkAlbum
 * @property null|ExternalVkParameter $vkMarketAlbum
 *
 */
class Tour extends Model
{
    use Statusable;
    use Imageable;
    use Favoriteable;
    use Priceable;
    use Questionable;
    use MetaTagable;
    use ExternalVkParamentable;
    use Previewable;
    use Sourceable;
    use UserActionable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'tours';

    /**
     * @var array
     */
    protected $fillable = [
        'organization_id', 'direction_id', 'name', 'title', 'description', 'preview_description',
        'tag_id',
        //'days', 'places',
        'include', 'not_include',
        'start_at', 'finish_at',
        'published_at',
        'release_at',
        'start_at_split', 'finish_at_split',
        'multi', 'multi_code',
        'status',
        'duplicate_id',
    ];

    /**
     * @var array
     */
    public $dates = [
        'release_at',
        'published_at',
        'start_at',
        'finish_at',
    ];

    /**
     * @return string
     */
    public function textableKey(): string
    {
        return 'description';
    }

    /**
     * @param array $value
     */
    public function setStartAtSplitAttribute(array $value = [])
    {
        $date = $value['date'];
        if (!empty($value['time'])) {
            $date = $date . ' ' . $value['time'];
        }
        $this->attributes['start_at'] = Carbon::parse($date);
    }

    /**
     * @param array $value
     */
    public function setFinishAtSplitAttribute(array $value = [])
    {
        $date = $value['date'];
        if (!empty($value['time'])) {
            $date = $date . ' ' . $value['time'];
        }
        $this->attributes['finish_at'] = Carbon::parse($date);
    }

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('created_at');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderedNearest(Builder $query): Builder
    {
        return $query->orderBy('start_at');
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE && $this->release_at !== null && $this->start_at->startOfDay() >= now()->startOfDay();
    }

    /**
     * @return bool
     */
    public function isMulti(): bool
    {
        return $this->multi === 1;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function direction()
    {
        return $this->belongsTo(Direction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tag()
    {
        return $this->belongsTo(Tag::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function statistic()
    {
        return $this->hasOne(TourStatistic::class, 'tour_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cancellation()
    {
        return $this->hasOne(TourCancellation::class, 'tour_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function announcement()
    {
        return $this->hasOne(TourAnnouncement::class, 'tour_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'tour_category')
            ->whereIn('type', [
                Category::TYPE_DEFAULT,
                Category::TYPE_TOURS,
            ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function entertainments()
    {
        return $this->belongsToMany(Entertainment::class, 'tour_entertainment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timelines()
    {
        return $this->hasMany(TourTimeline::class, 'tour_id')
            //->orderBy('type')
            ->orderBy('start_at');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query
            ->where('status', self::STATUS_ACTIVE)
            ->whereNotNull('release_at')
            ->activeByDirection()
            ->activeByOrganization()
            ->where('start_at', '>=', now()->startOfDay());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActivePassed(Builder $query): Builder
    {
        return $query
            ->where('status', self::STATUS_ACTIVE)
            ->whereNotNull('release_at')
            ->activeByDirection()
            ->activeByOrganization()
            ->where('start_at', '>=', now()->subWeek()->startOfDay());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActiveByDirection(Builder $query): Builder
    {
        return $query->whereHas('direction', function (Builder $query) {
            $query->where('status', Direction::STATUS_ACTIVE);
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActiveByOrganization(Builder $query): Builder
    {
        return $query->whereHas('organization', function (Builder $query) {
            $query->where('status', Organization::STATUS_ACTIVE);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function departures()
    {
        return $this->belongsToMany(Departure::class, 'tour_departure')
            ->withPivot('departure_at', 'departure_at_hourly', 'description');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function outfits()
    {
        return $this
            ->belongsToMany(Outfit::class, 'tour_outfit')
            ->withPivot('rent', 'rent_price', 'rent_description');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function outfitsOrdered()
    {
        return $this
            ->belongsToMany(Outfit::class, 'tour_outfit')
            ->orderBy('priority', 'desc')
            ->withPivot('rent', 'rent_price', 'rent_description');
    }

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = Str::slug($value);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('app.tour', ['id' => $this->id, 'name' => $this->name]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(TourOptionValues::class, 'tour_id')->with('option');
    }

    /**
     * @return string
     */
    public function getParameterVideoYoutubeAttribute(): string
    {
        $item = $this->values
            ->where('option.name', Option::NAME_VIDEO_YOUTUBE)
            //->where('option.purpose', Option::PURPOSE_TOURS)
            ->first();
        return !is_null($item) ? $item->value : '';
    }

    /**
     * @return string
     */
    public function getParameterReservationAttribute(): string
    {
        $item = $this->values
            ->where('option.name', Option::NAME_RESERVATION)
            ->where('option.purpose', Option::PURPOSE_TOURS)
            ->first();
        return !is_null($item) ? $item->value : '';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function duplicates()
    {
        return $this->hasMany(Tour::class, 'duplicate_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function multiples()
    {
        return $this->hasMany(Tour::class, 'multi_code', 'multi_code')
            ->where('id', '<>', $this->id)
            ->orderBy('start_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function multiplesActive()
    {
        return $this->hasMany(Tour::class, 'multi_code', 'multi_code')
            ->where('id', '<>', $this->id)
            ->active()
            ->orderBy('start_at');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parentDuplicate()
    {
        return $this->belongsTo(__CLASS__, 'duplicate_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attractions()
    {
        return $this->belongsToMany(Attraction::class, 'tour_attraction');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attractionsActive()
    {
        return $this->attractions()->active();
    }
}
