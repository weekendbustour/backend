<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Comment\Commentable;
use App\Services\Modelable\Imageable;
use App\Services\Modelable\Locationable;
use App\Services\Modelable\MetaTagable;
use App\Services\Modelable\Rate\Rateable;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\Word\WordDeclensionable;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Direction
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $preview_description
 * @property string $description
 * @property Location|null $location
 * @property int $status
 *
 *
 * @property mixed $tours
 * @property DirectionStatistic|null $statistic
 *
 *
 * @see Imageable::getImageAttribute()
 * @property string $image
 * @property string $image_default
 * @property string $image_card
 *
 * @see Imageable::getGalleryAttribute()
 * @property array $gallery
 *
 * @see Imageable::getImageBannerAttribute()
 * @property array $image_banner
 */
class Direction extends Model
{
    use Statusable;
    use Imageable;
    use Commentable;
    use Rateable;
    use Favoriteable;
    use Locationable;
    use MetaTagable;
    use WordDeclensionable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'directions';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'preview_description', 'description', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     * @return string
     */
    public function textableKey(): string
    {
        return 'description';
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('app.direction', ['id' => $this->id, 'name' => $this->name]);
    }

    /**
     * @return string
     */
    public function getUrlShort()
    {
        return route('app.name', ['name' => $this->name]);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('title');
    }

    /**
     * @return mixed
     */
    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    /**
     * @return mixed
     */
    public function toursActive()
    {
        return $this->hasMany(Tour::class)->active()->where('start_at', '>=', now());
    }

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = Str::slug($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function statistic()
    {
        return $this->hasOne(DirectionStatistic::class, 'direction_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attractions()
    {
        return $this->hasMany(Attraction::class, 'direction_id', 'id');
    }

    /**
     * @return mixed
     */
    public function attractionsActive()
    {
        return $this->attractions()->active();
    }
}
