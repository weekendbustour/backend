<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class WordDeclension
 * @package App\Models
 *
 * @property int $id
 * @property string $type
 * @property int $word_declensionable_id
 * @property string $word_declensionable_type
 * @property string $value
 * @property int $status
 *
 */
class WordDeclension extends Model
{
    use Statusable;
    use Typeable;

    /**
     * @var string
     */
    protected $table = 'word_declensions';

    /**
     * @var array
     */
    protected $fillable = [
        'type', 'word_declensionable_id', 'word_declensionable_type', 'value', 'status',
    ];

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     *
     */
    const TYPE_WHERE = 'where';

    /**
     *
     */
    const TYPE_FROM = 'from';

    /**
     * @return array
     */
    public function types()
    {
        return [
            self::TYPE_WHERE => 'Куда (Тур в/на Адыгею), сохранять вместе с предлогом и начинать с маленькой буквы',
            self::TYPE_FROM => 'Откуда (Тур из Ростова), сохранять вместе с предлогом и начинать с маленькой буквы',
        ];
    }
}
