<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Parentable;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Payments
 * @property int $id
 * @property string $type
 * @property float $amount
 * @property string $code
 * @property string|null $options
 * @property string|null $purpose
 * @property string|null $source
 * @property string|null $external_id
 * @property null|Carbon $opened_at
 * @property null|Carbon $closed_at
 * @property null|int|Payment $parent_id
 * @property int $status
 *
 *
 * @package App\Models
 */
class Payment extends Model
{
    use Statusable;
    use Typeable;
    use Parentable;

    /**
     * @var string
     */
    protected $table = 'payments';

    /**
     * @var array
     */
    protected $fillable = [
        'type', 'amount', 'code', 'options', 'purpose', 'source', 'external_id', 'opened_at', 'closed_at', 'parent_id', 'status',
    ];

    public $dates = [
        'opened_at', 'closed_at',
    ];

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;
    const STATUS_CLOSED = 2;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
        self::STATUS_CLOSED => 'Закрыт',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
        self::STATUS_CLOSED => [
            'class' => 'badge badge-default',
        ],
    ];

    /**
     *
     */
    const TYPE_PAY = 'pay';
    const TYPE_RETURN = 'return';

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $types = [
        self::TYPE_PAY => 'Оплата',
        self::TYPE_RETURN => 'Возврат',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $typesIcons = [
        self::TYPE_PAY => [
            'class' => 'badge badge-success',
        ],
        self::TYPE_RETURN => [
            'class' => 'badge badge-danger',
        ],
    ];
}
