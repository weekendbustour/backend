<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class TourStatistic
 *
 * @property int $id
 * @property int $tour_id
 * @property int $views
 * @property int $comments
 * @property int $redirects
 * @property int $likes
 * @property int $favourites
 */
class TourStatistic extends Model
{
    /**
     * @var string
     */
    protected $table = 'tour_statistics';

    /**
     * @var array
     */
    protected $fillable = [
        'tour_id', 'views', 'comments', 'redirects', 'likes', 'favourites',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }
}
