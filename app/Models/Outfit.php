<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Imageable;
use App\Services\Modelable\Statusable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Outfit
 * @package App\Models
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $preview_description
 * @property string $tooltip
 * @property int $priority
 * @property int $status
 *
 * @property object $pivot
 *
 */
class Outfit extends Model
{
    use Statusable;
    use Imageable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'outfits';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'preview_description', 'tooltip', 'priority', 'status',
    ];

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = Str::slug($value);
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('priority', 'desc');
    }
}
