<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserAction
 * @package App\Models
 *
 * @property int $id
 * @property int $user_id
 * @property string $type
 * @property int $user_actionable_id
 * @property string $user_actionable_type
 * @property string|Carbon|null $start_at
 * @property string|Carbon|null $finish_at
 * @property string $status
 */
class UserAction extends Model
{
    use Statusable;
    use Typeable;

    /**
     * @var string
     */
    protected $table = 'user_actions';

    /**
     * @var array
     */
    protected $fillable = [
        'type', 'user_id', 'user_actionable_id', 'user_actionable_type', 'start_at', 'finish_at', 'status',
    ];

    /**
     * @var array
     */
    public $dates = [
        'start_at', 'finish_at',
    ];

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    const TYPE_CREATED = 'created';
    const TYPE_UPDATED = 'updated';
    const TYPE_DELETED = 'deleted';
    const TYPE_PUBLISHED = 'published';
    const TYPE_UNPUBLISHED = 'unpublished';

    /**
     * @var array
     */
    protected $types = [
        self::TYPE_CREATED => 'Создано',
        self::TYPE_UPDATED => 'Обновлено',
        self::TYPE_DELETED => 'Удалено',
        self::TYPE_PUBLISHED => 'Опубликовано',
        self::TYPE_UNPUBLISHED => 'Снято с публикации',
    ];

    protected $typeIcons = [
        self::TYPE_CREATED => [
            'class' => 'badge badge-success',
        ],
        self::TYPE_UPDATED => [
            'class' => 'badge badge-primary',
        ],
        self::TYPE_DELETED => [
            'class' => 'badge badge-danger',
        ],
        self::TYPE_PUBLISHED => [
            'class' => 'badge badge-success',
        ],
        self::TYPE_UNPUBLISHED => [
            'class' => 'badge badge-danger',
        ],
    ];

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('start_at', 'desc');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
