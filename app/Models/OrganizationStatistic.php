<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationStatistic
 *
 * @property int $id
 * @property int $tour_id
 * @property int $views
 * @property int $comments
 * @property int $redirects
 * @property int $likes
 * @property int $favourites
 */
class OrganizationStatistic extends Model
{
    /**
     * @var string
     */
    protected $table = 'organization_statistics';

    /**
     * @var array
     */
    protected $fillable = [
        'organization_id', 'views', 'comments', 'redirects', 'likes', 'favourites',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
