<?php

declare(strict_types=1);

namespace App\Models;

use App\Models\Traits\OrganizationParametersTrait;
use App\Services\Modelable\Comment\Commentable;
use App\Services\Modelable\Imageable;
use App\Services\Modelable\MetaTagable;
use App\Services\Modelable\Parentable;
use App\Services\Modelable\Rate\Rateable;
use App\Services\Modelable\Socialable;
use App\Services\Modelable\Statusable;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * Class Organization
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $preview_description
 * @property string $signature
 * @property int $status
 *
 *
 * @property null|OrganizationStatistic $statistic
 *
 * @property mixed $departures
 * @property mixed $categories
 * @property mixed|Collection $socials
 * @property mixed|Collection $tours
 *
 * @see Imageable::getImageAttribute()
 * @property string $image
 * @property string $image_default
 * @property string $image_card
 * @property string $image_square
 *
 * @see Imageable::getGalleryAttribute()
 * @property array $gallery
 *
 * @see Imageable::getImageBannerAttribute()
 * @property array $image_banner
 *
 * @property mixed $values
 * @property mixed $toursActive
 *
 * @property string|null $parameter_reservation
 *
 *
 */
class Organization extends Model
{
    use Statusable;
    use Imageable;
    use Parentable;
    use Socialable;
    use Rateable;
    use Commentable;
    use Favoriteable;
    use MetaTagable;
    use OrganizationParametersTrait;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'organizations';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'title', 'description', 'preview_description', 'signature', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     * @return string
     */
    public function textableKey(): string
    {
        return 'description';
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('app.organization', ['id' => $this->id, 'name' => $this->name]);
    }

    /**
     * @return string
     */
    public function getUrlShort()
    {
        return route('app.name', ['name' => $this->name]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tours()
    {
        return $this->hasMany(Tour::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany(Article::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articlesActive()
    {
        return $this->hasMany(Article::class)->active();
    }

    /**
     * @return mixed
     */
    public function toursActive()
    {
        return $this->hasMany(Tour::class)->active()->where('start_at', '>=', now());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'organization_category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function departures()
    {
        return $this->belongsToMany(Departure::class, 'organization_departure');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function statistic()
    {
        return $this->hasOne(OrganizationStatistic::class, 'organization_id', 'id');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('title');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderedRate(Builder $query): Builder
    {
        return $query->withCount(['ratings as average_rating' => function ($query) {
            $query->select(DB::raw('coalesce(avg(value),0)'));
        }])->orderByDesc('average_rating');
    }

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = Str::slug($value);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(OrganizationOptionValues::class, 'organization_id')->with('option');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function questions()
    {
        return $this->morphMany(Question::class, 'questionable')->ordered();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activeQuestions()
    {
        return $this->morphMany(Question::class, 'questionable')->active()->ordered();
    }


//    public function getDeparturesTitleAttribute()
//    {
//        $departures = $this->departures;
//        $str = 'Из ';
//    }
}
