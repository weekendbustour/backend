<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ArticleStatistic
 *
 * @property int $id
 * @property int $attraction_id
 * @property int $views
 * @property int $comments
 * @property int $redirects
 * @property int $likes
 * @property int $favourites
 */
class AttractionStatistic extends Model
{
    /**
     * @var string
     */
    protected $table = 'attraction_statistics';

    /**
     * @var array
     */
    protected $fillable = [
        'attraction_id', 'views', 'comments', 'redirects', 'likes', 'favourites',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function attraction()
    {
        return $this->belongsTo(Attraction::class);
    }
}
