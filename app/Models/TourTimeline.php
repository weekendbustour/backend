<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Imageable;
use App\Services\Modelable\Statusable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class TourTimeline
 *
 * @property int $id
 * @property int $tour_id
 * @property string $name
 * @property string $type
 * @property string $title
 * @property string $description
 * @property string $include
 * @property string $not_include
 * @property null|Carbon $start_at
 * @property bool $start_at_hourly
 * @property null|Carbon $finish_at
 * @property bool $finish_at_hourly
 * @property int $status
 *
 *
 * @property array $gallery
 * @property null|Tour $tour
 */
class TourTimeline extends Model
{
    use Statusable;
    use Imageable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'tour_timelines';

    /**
     * @var array
     */
    protected $fillable = [
        'tour_id', 'name', 'title', 'description',
        'type',
        'include', 'not_include',
        'start_at', 'start_at_hourly',
        'finish_at', 'finish_at_hourly',
        'status',
    ];

    /**
     * @var array
     */
    public $dates = [
        'start_at', 'finish_at',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     *
     */
    const TYPE_DAY_TITLE = 'day_title';

    /**
     *
     */
    const TYPE_PRIMARY_TITLE = 'primary_title';

    /**
     *
     */
    const TYPE_SECONDARY_TITLE = 'secondary_title';

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('start_at');
    }

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = Str::slug($value);
    }
}
