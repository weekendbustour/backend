<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DirectionStatistic
 *
 * @property int $id
 * @property int $direction_id
 * @property int $views
 * @property int $comments
 * @property int $redirects
 * @property int $likes
 * @property int $favourites
 */
class DirectionStatistic extends Model
{
    /**
     * @var string
     */
    protected $table = 'direction_statistics';

    /**
     * @var array
     */
    protected $fillable = [
        'direction_id', 'views', 'comments', 'redirects', 'likes', 'favourites',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function direction()
    {
        return $this->belongsTo(Direction::class);
    }
}
