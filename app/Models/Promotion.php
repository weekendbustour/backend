<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Imageable;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Promotion
 *
 * @property int $id
 * @property int $organization_id
 * @property int $tour_id
 * @property string $type
 * @property string $name
 * @property string $title
 * @property null|string $description
 * @property null|string $url
 * @property null|Carbon $start_at
 * @property null|Carbon $finish_at
 * @property int $status
 *
 * @property mixed $gallery
 *
 * @package App\Models
 */
class Promotion extends Model
{
    use Statusable;
    use Imageable;
    use Typeable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    const TYPE_DEFAULT = 'default';
    const TYPE_TARGET = 'target';

    /**
     * @var string
     */
    protected $table = 'promotions';

    /**
     * @var array
     */
    protected $fillable = [
        'organization_id', 'tour_id', 'type', 'name', 'title', 'description', 'url', 'start_at', 'finish_at', 'status',
        'start_at_split', 'finish_at_split',
    ];

    /**
     * @var array
     */
    public $dates = [
        'start_at', 'finish_at',
    ];

    /**
     * @param array $value
     */
    public function setStartAtSplitAttribute(array $value = [])
    {
        $date = $value['date'];
        if (!empty($value['time'])) {
            $date = $date . ' ' . $value['time'];
        }
        $this->attributes['start_at'] = Carbon::parse($date);
    }

    /**
     * @param array $value
     */
    public function setFinishAtSplitAttribute(array $value = [])
    {
        $date = $value['date'];
        if (!empty($value['time'])) {
            $date = $date . ' ' . $value['time'];
        }
        $this->attributes['finish_at'] = Carbon::parse($date);
    }

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        if ($this->type === self::TYPE_DEFAULT) {
            return $this->status === self::STATUS_ACTIVE;
        }
        if ($this->type === self::TYPE_TARGET) {
            return $this->status === self::STATUS_ACTIVE && $this->start_at->isBefore(now()) && $this->finish_at->isAfter(now());
        }
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('start_at');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query
            ->where('status', self::STATUS_ACTIVE)
            ->where('start_at', '>=', now());
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActiveDefault(Builder $query): Builder
    {
        return $query
            ->where('type', self::TYPE_DEFAULT)
            ->where('status', self::STATUS_ACTIVE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActiveTarget(Builder $query): Builder
    {
        return $query
            ->where('type', self::TYPE_TARGET)
            ->where('status', self::STATUS_ACTIVE)
            ->where('start_at', '<=', now())
            ->where('finish_at', '>=', now());
    }

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = Str::slug($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tour()
    {
        return $this->belongsTo(Tour::class);
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('app.promotion', ['id' => $this->id, 'name' => $this->name]);
    }
}
