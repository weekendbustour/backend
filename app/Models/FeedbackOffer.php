<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Illuminate\Database\Eloquent\Model;

class FeedbackOffer extends Model
{
    use Statusable;
    use Typeable;

    /**
     * @var string
     */
    protected $table = 'feedback_offers';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'description', 'status',
    ];

    const TYPE_ORGANIZATION = 'organization';
    const TYPE_VACANCY = 'vacancy';
    const TYPE_PROMOTION = 'promotion';

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     *
     */
    const STATUS_LATER = 2;
    const STATUS_RESOLVED = 3;
    const STATUS_CANCELED = 4;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
        self::STATUS_LATER => 'Отложена',
        self::STATUS_RESOLVED => 'Принята',
        self::STATUS_CANCELED => 'Отменена',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $types = [
        self::TYPE_ORGANIZATION => 'Организация',
        self::TYPE_VACANCY => 'Вакансия',
        self::TYPE_PROMOTION => 'Реклама',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-info',
        ],
        self::STATUS_RESOLVED => [
            'class' => 'badge badge-success',
        ],
        self::STATUS_LATER => [
            'class' => 'badge badge-success',
        ],
        self::STATUS_CANCELED => [
            'class' => 'badge badge-success',
        ],
    ];
}
