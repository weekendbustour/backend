<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Statusable;
use App\Services\Modelable\Typeable;
use Illuminate\Database\Eloquent\Model;

class FeedbackReport extends Model
{
    use Statusable;
    use Typeable;

    /**
     * @var string
     */
    protected $table = 'feedback_reports';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'description', 'page', 'status',
    ];

    const TYPE_INFO = 'info';
    const TYPE_DANGER = 'danger';
    const TYPE_CRITICAL = 'critical';

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     *
     */
    const STATUS_RESOLVED = 2;

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
        self::STATUS_RESOLVED => 'Решена',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $types = [
        self::TYPE_INFO => 'Информативный',
        self::TYPE_DANGER => 'Опасный',
        self::TYPE_CRITICAL => 'Критичный',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-danger',
        ],
        self::STATUS_RESOLVED => [
            'class' => 'badge badge-success',
        ],
    ];
}
