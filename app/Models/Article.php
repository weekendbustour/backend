<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Comment\Commentable;
use App\Services\Modelable\Imageable;
use App\Services\Modelable\Locationable;
use App\Services\Modelable\Rate\Rateable;
use App\Services\Modelable\Sourceable;
use App\Services\Modelable\Statusable;
use App\Services\Modelable\TitleNameable;
use App\Services\Modelable\Typeable;
use Carbon\Carbon;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Article
 * @property int $id
 * @property string $position
 * @property string $type
 * @property int|null $number
 * @property string $name
 * @property string $title
 * @property string $preview_description
 * @property string $description
 * @property string $source
 * @property null|Carbon $release_at
 * @property null|Carbon $published_at
 * @property int $status
 * @property int|bool $with_banner
 *
 * @property mixed|Collection $categories
 * @property mixed|NewsStatistic $statistic
 *
 *
 * @property Location|null $location
 * @property mixed $locations
 *
 *
 *
 * @property mixed $values
 * @property Organization|null $organization
 *
 * @package App\Models
 */
class Article extends Model
{
    use Statusable;
    use Imageable;
    use Commentable;
    use Rateable;
    use Favoriteable;
    use Locationable;
    use Typeable;
    use TitleNameable;
    use Sourceable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'articles';

    /**
     * @var array
     */
    protected $fillable = [
        'position', 'type', 'organization_id', 'number', 'name', 'title', 'preview_description',
        'description', 'with_banner', 'release_at', 'published_at', 'status',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'release_at', 'published_at',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    const POSITION_DEFAULT = 'default';
    const POSITION_BANNER = 'banner';

    const TYPE_DEFAULT = 'default';
    const TYPE_TOP = 'top';

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $types = [
        self::TYPE_DEFAULT => 'По умолчанию',
        self::TYPE_TOP => 'Топ',
    ];

    /**
     * @return string
     */
    public function textableKey(): string
    {
        return 'description';
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE
            && $this->published_at !== null
            && $this->release_at !== null
            && $this->release_at < now();
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query
            ->where('status', self::STATUS_ACTIVE)
            ->whereNotNull('release_at')
            ->whereNotNull('published_at');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('published_at', 'desc');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrderedReserve(Builder $query): Builder
    {
        return $query->orderBy('published_at', 'asc');
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('app.article', ['id' => $this->id, 'name' => $this->name]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'article_category')
            ->whereIn('type', [
                Category::TYPE_ARTICLES,
            ]);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function statistic()
    {
        return $this->hasOne(ArticleStatistic::class, 'article_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(ArticleOptionValues::class, 'article_id')->with('option');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function attractions()
    {
        return $this->belongsToMany(Attraction::class, 'attraction_article');
    }

    /**
     * @return mixed
     */
    public function attractionsActive()
    {
        return $this->attractions()->active();
    }
}
