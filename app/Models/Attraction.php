<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Comment\Commentable;
use App\Services\Modelable\Imageable;
use App\Services\Modelable\Locationable;
use App\Services\Modelable\MetaTagable;
use App\Services\Modelable\Rate\Rateable;
use App\Services\Modelable\Statusable;
use Carbon\Carbon;
use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * Class Attraction
 * @property int $id
 * @property int $direction_id
 * @property string $name
 * @property string $title
 * @property string $preview_description
 * @property string $description
 * @property int $with_banner
 * @property int $status
 *
 * @property Carbon|null $release_at
 *
 * @property Location|null $location
 * @property AttractionStatistic|null $statistic
 * @property Direction|null $direction
 *
 * @see Imageable::getImageAttribute()
 * @property string $image
 * @property string $image_default
 * @property string $image_card
 *
 * @see Imageable::getGalleryAttribute()
 * @property array $gallery
 *
 * @see Imageable::getImageBannerAttribute()
 * @property array $image_banner
 *
 * @property mixed $values
 *
 * @package App\Models
 */
class Attraction extends Model
{
    use Statusable;
    use Imageable;
    use Commentable;
    use Rateable;
    use Favoriteable;
    use Locationable;
    use MetaTagable;

    /**
     *
     */
    const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    const STATUS_ACTIVE = 1;

    /**
     * @var string
     */
    protected $table = 'attractions';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'title', 'preview_description', 'description', 'with_banner', 'release_at', 'status',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        self::STATUS_NOT_ACTIVE => 'Не активно',
        self::STATUS_ACTIVE => 'Активно',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statusIcons = [
        self::STATUS_NOT_ACTIVE => [
            'class' => 'badge badge-default',
        ],
        self::STATUS_ACTIVE => [
            'class' => 'badge badge-success',
        ],
    ];

    /**
     * @return string
     */
    public function textableKey(): string
    {
        return 'description';
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return route('app.attraction', ['id' => $this->id, 'name' => $this->name]);
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->status === self::STATUS_ACTIVE
            && $this->release_at !== null
            && $this->release_at < now();
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOrdered(Builder $query): Builder
    {
        return $query->orderBy('title');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeActive(Builder $query): Builder
    {
        return $query
            ->where('status', self::STATUS_ACTIVE)
            ->whereNotNull('release_at');
    }

    /**
     * @param string $value
     */
    public function setTitleAttribute(string $value)
    {
        $this->attributes['title'] = $value;
        $this->attributes['name'] = Str::slug($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function direction()
    {
        return $this->belongsTo(Direction::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articles()
    {
        return $this->belongsToMany(Article::class, 'attraction_article');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articlesOrdered()
    {
        return $this->articles()->ordered();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articlesOrderedActive()
    {
        return $this->articles()->active()->ordered();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function statistic()
    {
        return $this->hasOne(AttractionStatistic::class, 'attraction_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function values()
    {
        return $this->hasMany(AttractionOptionValues::class, 'attraction_id')->with('option');
    }
}
