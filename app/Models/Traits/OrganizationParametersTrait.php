<?php

declare(strict_types=1);

namespace App\Models\Traits;

use App\Models\Option;

/**
 * Trait OrganizationParametersTrait
 * @package App\Models\Traits
 *
 * @property string|null $parameterVideoYoutube
 * @property string|null $parameterReservation
 * @property string|null $parameterPrepaymentCancellation
 *
 */
trait OrganizationParametersTrait
{
    /**
     * @return string|null
     */
    public function getParameterVideoYoutubeAttribute(): ?string
    {
        $item = $this->values
            ->where('option.name', Option::NAME_VIDEO_YOUTUBE)
            ->where('option.purpose', Option::PURPOSE_ORGANIZATIONS)
            ->first();
        return !is_null($item) ? $item->value : null;
    }

    /**
     * @return string|null
     */
    public function getParameterReservationAttribute(): ?string
    {
        $item = $this->values
            ->where('option.name', Option::NAME_RESERVATION)
            ->where('option.purpose', Option::PURPOSE_ORGANIZATIONS)
            ->first();
        return !is_null($item) ? $item->value : null;
    }

    /**
     * @return string|null
     */
    public function getParameterPrepaymentCancellationAttribute(): ?string
    {
        $item = $this->values
            ->where('option.name', Option::NAME_PREPAYMENT_CANCELLATION)
            ->where('option.purpose', Option::PURPOSE_ORGANIZATIONS)
            ->first();
        return !is_null($item) ? $item->value : null;
    }
}
