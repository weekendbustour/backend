<?php

declare(strict_types=1);

namespace App\Models;

use App\Services\Modelable\Comment\Commentable;
use App\Services\Modelable\Parentable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 *
 * @property integer $id
 * @property integer $commentable_id
 * @property string $commentable_type
 * @property integer $commented_id
 * @property string $commented_type
 * @property string $text
 * @property float $rate
 * @property boolean $approved
 * @property boolean $anon
 * @property int|null $parent_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property Direction|Organization|null $commentable
 * @property User|null $commented
 *
 * @property mixed $approvedChildren
 */
class Comment extends Model
{
    use Commentable;
    use Parentable;

    /**
     * @var string
     */
    public $table = 'comments';

    /**
     * @var bool
     */
    protected $canBeRated = true;

    /**
     * @var bool
     */
    protected $mustBeApproved = true;

    protected $fillable = [
        'commented_id', 'commented_type', 'commentable_id', 'commentable_type', 'text', 'rate', 'approved', 'anon', 'rating_id', 'parent_id', 'reply_id',
    ];

    protected $casts = [
        'approved' => 'boolean',
        'anon' => 'boolean',
    ];

    /**
     * The status attributes for model
     *
     * @var array
     */
    protected $statuses = [
        0 => 'Не одобрен',
        1 => 'Одобрен',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function commented()
    {
        return $this->morphTo();
    }

    /**
     * @return $this
     */
    public function approve()
    {
        $this->approved = true;
        $this->save();

        return $this;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function loveUsers()
    {
        return $this->BelongsToMany(User::class, 'love_user_comment')
            ->withPivot('like as like', 'dislike as dislike');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reply()
    {
        return $this->belongsTo(Comment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function rating()
    {
        return $this->belongsTo(Rating::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function votes()
    {
        return $this->morphMany(CommentVote::class, 'votable')->orderBy('created_at', 'asc');
    }

    /**
     * Текст для соответствующего типа
     *
     * @return array
     */
    public function getCommentablesText(): array
    {
        return [
            Direction::class => 'Отправление',
            Organization::class => 'Организатор',
            News::class => 'Новость',
        ];
    }
    /**
     * @return array
     */
    public function getCommentables(): array
    {
        return [
            Direction::class,
            Organization::class,
            News::class,
        ];
    }

    /**
     * @return array
     */
    public function getCommentablesTextIcon(): array
    {
        return [
            Direction::class => [
                'text' => 'Отправление',
                'type' => 'success',
            ],
            Organization::class => [
                'text' => 'Организатор',
                'type' => 'success',
            ],
            News::class => [
                'text' => 'Новость',
                'type' => 'info',
            ],
        ];
    }

    /**
     * @return string
     */
    public function getCommentableTypeNameAttribute()
    {
        return $this->getCommentablesTextIcon()[$this->commentable_type]['type'];
    }

    /**
     * @return string
     */
    public function getCommentableTextAttribute()
    {
        return $this->getCommentablesTextIcon()[$this->commentable_type]['text'];
    }

    /**
     * @return string
     */
    public function getCommentableModelNameAttribute()
    {
        switch ($this->commentable_type) {
            case Direction::class:
                return $this->commentable->title ?? '';
            case Organization::class:
                return $this->commentable->title ?? '';
            case News::class:
                return $this->commentable->title ?? '';
            default:
                return '';
        }
    }
}
