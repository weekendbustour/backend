<?php

declare(strict_types=1);

namespace App\Mail\Auth;

use App\Jobs\QueueCommon;
use App\Mail\SendReferToClient;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisteredMail extends Mailable
{
    use SerializesModels;

    /**
     *
     */
    const NAME = 'register';

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = 'Добро пожаловать';
        return $this->view('emails.auth.register')->with([
            'title' => $title,
        ])->subject($title);
    }
}
