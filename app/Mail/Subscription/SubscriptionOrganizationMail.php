<?php

declare(strict_types=1);

namespace App\Mail\Subscription;

use App\Jobs\QueueCommon;
use App\Mail\SendReferToClient;
use App\Models\Organization;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class SubscriptionOrganizationMail extends Mailable
{
    use SerializesModels;

    /**
     *
     */
    const NAME = 'organization';

    /**
     * @var Organization|null
     */
    private $oOrganization = null;

    /**
     * @var array|mixed|Collection|\Illuminate\Database\Eloquent\Collection
     */
    private $oTours = [];

    /**
     * SubscriptionOrganizationMail constructor.
     * @param Organization $oOrganization
     * @param mixed $oTours
     */
    public function __construct(Organization $oOrganization, $oTours)
    {
        $this->oOrganization = $oOrganization;
        $this->oTours = $oTours;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = 'Появились новые туры';
        return $this->view('emails.subscription.organization')->with([
            'title' => $title,
            'oOrganization' => $this->oOrganization,
            'oTours' => $this->oTours,
        ])->subject($title);
    }
}
