<?php

declare(strict_types=1);

namespace App\Mail\Subscription;

use App\Jobs\QueueCommon;
use App\Mail\SendReferToClient;
use App\Models\Direction;
use App\Models\Organization;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class SubscriptionDirectionMail extends Mailable
{
    use SerializesModels;

    /**
     *
     */
    const NAME = 'direction';

    /**
     * @var Direction|null
     */
    private $oDirection = null;

    /**
     * @var array|mixed|Collection|\Illuminate\Database\Eloquent\Collection
     */
    private $oTours = [];

    /**
     * SubscriptionDirectionMail constructor.
     * @param Direction $oDirection
     * @param $oTours
     */
    public function __construct(Direction $oDirection, $oTours)
    {
        $this->oDirection = $oDirection;
        $this->oTours = $oTours;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $title = 'Появились новые туры';
        return $this->view('emails.subscription.direction')->with([
            'title' => $title,
            'oDirection' => $this->oDirection,
            'oTours' => $this->oTours,
        ])->subject($title);
    }
}
