<?php

declare(strict_types=1);

namespace App\Cmf\Project;

use App\Cmf\Project\Tour\TourController;
use App\Models\Tour;

class ModalController
{
    /**
     * Модальное окно, текст под Редактирование
     * для тура - Рафтинг 1 Августа
     *
     *
     * @param string $name
     * @param object|null|Tour $oItem
     * @return string
     */
    public function editTitle(string $name, object $oItem): string
    {
        $title = '#' . $oItem->id . ': ' . $oItem->title;
        switch ($name) {
            case TourController::NAME:
                $title .= ' ' . serviceTransformerTour()->startAtDays($oItem) . ' ' . serviceTransformerTour()->startAtMonth($oItem);
                break;
            default:
                break;
        }
        return $title;
    }
}
