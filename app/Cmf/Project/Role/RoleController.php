<?php

declare(strict_types=1);

namespace App\Cmf\Project\Role;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\FieldParameter;
use App\Cmf\Core\MainController;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class RoleController extends MainController
{
    use RoleSettingsTrait;
    use ImageableTrait;
    use RoleCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Роли';

    /**
     * Имя сущности
     */
    const NAME = 'role';

    /**
     * Иконка
     */
    const ICON = 'icon-mustache';

    /**
     * Модель сущности
     */
    public $class = Role::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'name' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Ключ',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::DELETE_TITLE => 'роль',
            FieldParameter::REQUIRED => true,
        ],
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::REQUIRED => true,
        ],
    ];
}
