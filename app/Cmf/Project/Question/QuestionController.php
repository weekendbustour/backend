<?php

declare(strict_types=1);

namespace App\Cmf\Project\Question;

use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\FieldParameter;
use App\Cmf\Core\MainController;
use App\Models\Question;

class QuestionController extends MainController
{
    use TextableTrait;
    use QuestionSettingsTrait;
    use QuestionCustomTrait;
    use QuestionThisTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Вопросы (FAQ)';

    /**
     * Имя сущности
     */
    const NAME = 'question';

    /**
     * Иконка
     */
    const ICON = 'icon-info';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Question::class;

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [

    ];

    /**
     * Настройки для компонента
     *
     * @var array
     */
    public $indexComponent = [
        'history' => false,
        'image' => false,
        'create' => false,
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => []
        ]
    ];

    /**
     * @var array
     */
    public $roles = [];

    /**
     * Validation rules
     * @var array
     */
    public $rules = [];

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * Сортировка с учетом сессии, например ['column' => 'created_at', 'type' => 'desc']
     *
     * @var array
     */
    protected $aOrderBy = [
        'column' => 'priority',
        'type' => 'desc',
    ];

    /**
     * @var array
     */
    public $fields = [
//        'questionable' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
//            FieldParameter::TITLE => 'Сущность',
//            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
//            FieldParameter::VALUES => \App\Models\Organization::class,
//            FieldParameter::ORDER => [
//                FieldParameter::ORDER_METHOD => 'orderBy',
//                FieldParameter::ORDER_BY => 'title',
//            ],
//            FieldParameter::EMPTY => true,
//        ],
//        'type' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
//            FieldParameter::TITLE => 'Тип',
//            FieldParameter::REQUIRED => true,
//            FieldParameter::VALUES => [
//                Question::TYPE_DEFAULT => 'По умолчанию',
//            ],
//            FieldParameter::IN_TABLE => 1,
//        ],
        'questionable' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CUSTOM,
            FieldParameter::TITLE => 'Сущность',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::HIDDEN => true,
        ],
        'question' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Вопрос',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::REQUIRED => true,
        ],
        'answer' => [
            FieldParameter::TYPE => parent::DATA_TYPE_MARKDOWN,
            FieldParameter::TITLE => 'Ответ',
            FieldParameter::REQUIRED => true,
        ],
        'priority' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'Приоритет',
            FieldParameter::DEFAULT => 0,
            FieldParameter::IN_TABLE => 3,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 4,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
