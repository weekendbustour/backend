<?php

declare(strict_types=1);

namespace App\Cmf\Project\Question;

use App\Cmf\Core\SettingsTrait;

trait QuestionSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => QuestionController::TITLE,
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => QuestionController::NAME,
            ],
        ],
        'icon' => 'fa fa-users',
    ];
}
