<?php

declare(strict_types=1);

namespace App\Cmf\Project\Question;

use App\Cmf\Core\FieldParameter;
use App\Models\Article;
use App\Models\Option;
use App\Models\Question;
use App\Models\Tour;
use App\Models\TourOptionValues;
use App\Services\Image\ImageType;
use App\Services\Image\Upload\ImageUploadBannerService;
use App\Services\Image\Upload\ImageUploadGalleryService;
use App\Services\Image\Upload\ImageUploadModelService;
use Illuminate\Http\Request;

trait QuestionThisTrait
{
    /**
     * @param Question|null $oItem
     */
    public function thisPrepareFieldsValues(?Question $oItem)
    {
        if (is_null($oItem)) {
            // Вкладка Данные
            //$this->fields['questionable'][FieldParameter::GROUP_COL] = 12;
        } else {
            // Вкладка Данные
            //$this->fields['title'][FieldParameter::GROUP_COL] = 6;
        }
    }
}
