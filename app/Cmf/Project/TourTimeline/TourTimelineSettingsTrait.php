<?php

declare(strict_types=1);

namespace App\Cmf\Project\TourTimeline;

trait TourTimelineSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => TourTimelineController::NAME,
        'title' => TourTimelineController::TITLE,
        'description' => null,
        'icon' => TourTimelineController::ICON,
    ];
}
