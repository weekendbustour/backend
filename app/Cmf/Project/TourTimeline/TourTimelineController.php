<?php

declare(strict_types=1);

namespace App\Cmf\Project\TourTimeline;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\MainController;
use App\Models\TourTimeline;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Cmf\Core\FieldParameter;

class TourTimelineController extends MainController
{
    use TourTimelineSettingsTrait;
    use ImageableTrait;
    use TourTimelineCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Расписание туров';

    /**
     * Имя сущности
     */
    const NAME = 'tour_timeline';

    /**
     * Иконка
     */
    const ICON = 'icon-list';

    /**
     * Модель сущности
     */
    public $class = \App\Models\TourTimeline::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
            'tabs.image' => [
                'title' => 'Галлерея',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $image = [
        ImageType::GALLERY => [
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'tour' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тур',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Tour::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => true,
            FieldParameter::IN_TABLE => 1,
            FieldParameter::EMPTY => true,
        ],
        'start_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата и время начала',
            FieldParameter::REQUIRED => true,
            FieldParameter::DATETIME => true,
            FieldParameter::IN_TABLE => 2,
            FieldParameter::GROUP_NAME => 'from_to_term',
            FieldParameter::GROUP_COL => 6,
        ],
        'finish_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата и время окончания',
            FieldParameter::REQUIRED => false,
            FieldParameter::DATETIME => true,
            FieldParameter::IN_TABLE => 3,
            FieldParameter::GROUP_NAME => 'from_to_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => true,
        ],
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 4,
            FieldParameter::DELETE_TITLE => 'направление',
            FieldParameter::REQUIRED => true,
        ],
        'type' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тип',
            FieldParameter::REQUIRED => true,
            FieldParameter::VALUES => [
                TourTimeline::TYPE_SECONDARY_TITLE => 'Обычный заголовок',
                TourTimeline::TYPE_PRIMARY_TITLE => 'Крупный заголовок',
                TourTimeline::TYPE_DAY_TITLE => 'Заголовок дня',
            ],
        ],
        'description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Описание',
        ],
        'include' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Что входит',
            FieldParameter::PLACEHOLDER => 'Входит: &#10; - одно &#10; - второе',
            FieldParameter::GROUP_NAME => 'includes',
            FieldParameter::GROUP_COL => 6,
        ],
        'not_include' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Что не входит',
            FieldParameter::PLACEHOLDER => 'Не входит: &#10; - одно &#10; - второе',
            FieldParameter::GROUP_NAME => 'includes',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => true,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 5,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
