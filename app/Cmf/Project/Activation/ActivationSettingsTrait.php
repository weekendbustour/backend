<?php

declare(strict_types=1);

namespace App\Cmf\Project\Activation;

trait ActivationSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => ActivationController::NAME,
        'title' => ActivationController::TITLE,
        'description' => null,
        'icon' => ActivationController::ICON,
    ];
}
