<?php

declare(strict_types=1);

namespace App\Cmf\Project\Activation;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\FieldParameter;
use App\Cmf\Core\MainController;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ActivationController extends MainController
{
    use ActivationSettingsTrait;
    use ImageableTrait;
    use ActivationCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Активации';

    /**
     * Имя сущности
     */
    const NAME = 'activation';

    /**
     * Иконка
     */
    const ICON = 'icon-key';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Activation::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'code' => 'код',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'user' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Пользователь',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\User::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'first_name',
            ],
            FieldParameter::EMPTY => true,
            FieldParameter::REQUIRED => true,
            FieldParameter::IN_TABLE => 1,
        ],
        'phone' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Телефон',
            FieldParameter::REQUIRED => true,
            FieldParameter::MASK_PHONE => true,
        ],
        'code' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'Код',
            FieldParameter::REQUIRED => true,
            FieldParameter::LENGTH => 6,
            FieldParameter::IN_TABLE => 2,
        ],
        'completed' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Активирован',
            FieldParameter::TITLE_FORM => 'Активирован',
            FieldParameter::DEFAULT => false,
            FieldParameter::IN_TABLE => 3,
        ],
        'completed_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата и время активации',
            FieldParameter::DATETIME => true,
            FieldParameter::IN_TABLE => 4,
            FieldParameter::FORMAT => 'd.m.Y H:i:s',
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 5,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
