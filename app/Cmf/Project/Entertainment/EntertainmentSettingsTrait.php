<?php

declare(strict_types=1);

namespace App\Cmf\Project\Entertainment;

trait EntertainmentSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => EntertainmentController::NAME,
        'title' => EntertainmentController::TITLE,
        'description' => null,
        'icon' => EntertainmentController::ICON,
    ];
}
