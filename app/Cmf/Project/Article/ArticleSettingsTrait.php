<?php

declare(strict_types=1);

namespace App\Cmf\Project\Article;

trait ArticleSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => ArticleController::NAME,
        'title' => ArticleController::TITLE,
        'description' => null,
        'icon' => ArticleController::ICON,
    ];
}
