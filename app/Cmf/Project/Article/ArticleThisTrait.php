<?php

declare(strict_types=1);

namespace App\Cmf\Project\Article;

use App\Cmf\Core\FieldParameter;
use App\Models\Article;
use App\Models\Option;
use App\Models\Tour;
use App\Models\TourOptionValues;
use App\Services\Image\ImageType;
use App\Services\Image\Upload\ImageUploadBannerService;
use App\Services\Image\Upload\ImageUploadGalleryService;
use App\Services\Image\Upload\ImageUploadModelService;
use Illuminate\Http\Request;

trait ArticleThisTrait
{
    /**
     * @param Article $oItem
     * @return array
     */
    public function thisEditDataModal(Article $oItem)
    {
        // вкладка Параметры
        $oOptions = Option::active()->purposeArticles()->ordered()->get();
        $oOptions = $oOptions->reject(function ($item) {
            return in_array($item->name, [
                Option::NAME_VIDEO_YOUTUBE,
            ]);
        });

        return [
            'oValues' => $oItem->values->keyBy('option.name'),
            'oOptions' => $oOptions,
        ];
    }

    /**
     * @param Article|null $oItem
     */
    public function thisPrepareFieldsValues(?Article $oItem)
    {
        if (is_null($oItem)) {
            // Вкладка Данные
            $this->fields['title'][FieldParameter::GROUP_COL] = 12;
            $this->fields['name'][FieldParameter::HIDDEN] = true;
            $this->fields['name'][FieldParameter::DISABLED] = true;
            $this->fields['name'][FieldParameter::REQUIRED] = false;
        } else {
            // Вкладка Данные
            $this->fields['title'][FieldParameter::GROUP_COL] = 6;
            $this->fields['name'][FieldParameter::HIDDEN] = false;
        }
    }

    /**
     * @param Article $oItem
     * @throws \Exception
     */
    public function thisDestroy(Article $oItem): void
    {
        $oImages = $oItem->images;
        foreach ($oImages as $oImage) {
            $oService = null;
            switch ($oImage->type) {
                case ImageType::MODEL:
                    $oService = new ImageUploadModelService();
                    break;
                case ImageType::GALLERY:
                    $oService = new ImageUploadGalleryService();
                    break;
                case ImageType::BANNER:
                    $oService = new ImageUploadBannerService();
                    break;
            }
            $result = $oService->delete($oItem, $oImage, $this->image[$oImage->type]);
        }
        $oItem->delete();
    }
}
