<?php

declare(strict_types=1);

namespace App\Cmf\Project\Article;

use App\Cmf\Core\Defaults\GeoTrait;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\SourceableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\Defaults\TextableUploadTrait;
use App\Cmf\Core\MainController;
use App\Cmf\Core\TabParameter;
use App\Cmf\Project\Organization\OrganizationController;
use App\Models\Category;
use App\Models\Article;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Cmf\Core\FieldParameter;

class ArticleController extends MainController
{
    use ArticleSettingsTrait;
    use ImageableTrait;
    use ArticleCustomTrait;
    use TextableTrait;
    use TextableUploadTrait;
    use ArticleThisTrait;
    use ArticleParametersTrait;
    use GeoTrait;
    use SourceableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Статьи';

    /**
     * Имя сущности
     */
    const NAME = 'article';

    /**
     * Иконка
     */
    const ICON = 'icon-book-open';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Article::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images', 'comments', 'organization'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    public $indexComponent = [
        'image' => true,
        'comments' => true,
        'release_at' => true,
        'private_show' => true,
    ];

    /**
     * @var array
     */
    protected $aOrderBy = [
        'column' => 'published_at',
        'type' => 'desc',
    ];

    /**
     * @var array
     */
    public $image = [
        ImageType::MODEL => [
            'with_main' => true,
            'unique' => false,
            'filters' => [
                ImageSize::XS => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => 36,
                    ],
                ],
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
                ImageSize::CARD_DEFAULT => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 350,
                        'height' => 445,
                    ],
                ],
                ImageSize::CARD_MD => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 700,
                        'height' => 890,
                    ],
                ],
            ],
        ],
        ImageType::GALLERY => [
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ],
    ];

    /**
     * @param object|null $model
     * @param array $data
     * @return array
     */
    public function rules($model = null, array $data = [])
    {
        if (!empty($data) && $data['position'] === Article::POSITION_BANNER) {
            if (!is_null($model)) {
                array_push($this->rules['update']['position'], Rule::unique('articles')->ignore($model->id));
            } else {
                array_push($this->rules['store']['position'], Rule::unique('articles'));
            }
        }
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            'position' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            'position' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png,webp'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'scrolling' => true,
        'edit' => [
            TabParameter::TAB_MAIN => TabParameter::TAB_MAIN_CONTENT,
            TabParameter::TAB_MARKDOWN => TabParameter::TAB_MARKDOWN_CONTENT,
            TabParameter::TAB_PARAMETERS => TabParameter::TAB_PARAMETERS_CONTENT,
            TabParameter::TAB_IMAGES_MODEL => TabParameter::TAB_IMAGES_MODEL_CONTENT,
            'tabs.images.gallery' => [
                'title' => 'Галлерея',
            ],
            'tabs.geo.coordinates' => [
                'title' => 'Координаты',
            ],
            TabParameter::TAB_VIDEO => TabParameter::TAB_VIDEO_CONTENT,
            TabParameter::TAB_SOURCE => TabParameter::TAB_SOURCE_CONTENT,
            'tabs.comments' => [
                'title' => 'Комментарии',
            ],
            TabParameter::TAB_DUPLICATE => TabParameter::TAB_DUPLICATE_CONTENT,
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'organization' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Организатор',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Organization::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => false,
            FieldParameter::IN_TABLE => 1,
            FieldParameter::EMPTY => true,
            FieldParameter::COLORED => true,
            FieldParameter::TABLE_TITLE => '<i class="' . OrganizationController::ICON . '"></i>',
        ],
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::DELETE_TITLE => 'направление',
            FieldParameter::REQUIRED => true,

            FieldParameter::GROUP_NAME => 'title_name_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => false,
        ],
        'name' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Ключ',
            FieldParameter::SHOW_ONLY => true,

            FieldParameter::GROUP_NAME => 'title_name_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => true,
        ],
        'preview_description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Краткое описание',
        ],
        'categories' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Категория',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO_MANY,
            FieldParameter::VALUES => \App\Models\Category::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            'whereIn' => [
                'column' => 'type',
                'value' => [
                    Category::TYPE_ARTICLES,
                ],
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => false,
            FieldParameter::EMPTY => true,
            FieldParameter::MULTIPLE => false,

            FieldParameter::GROUP_NAME => 'category_type_number_term',
            FieldParameter::GROUP_COL => 4,
            FieldParameter::GROUP_HIDE => false,
        ],
        'type' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тип статьи',
            FieldParameter::REQUIRED => true,
            FieldParameter::VALUES => [
                Article::TYPE_DEFAULT => 'По умолчанию',
                Article::TYPE_TOP => 'Топ',
            ],

            FieldParameter::GROUP_NAME => 'category_type_number_term',
            FieldParameter::GROUP_COL => 4,
            FieldParameter::GROUP_HIDE => true,
        ],
        'number' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'Номер',
            FieldParameter::TOOLTIP => 'Если Тип будет Топ',

            FieldParameter::GROUP_NAME => 'category_type_number_term',
            FieldParameter::GROUP_COL => 4,
            FieldParameter::GROUP_HIDE => true,
        ],
        'position' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Расположение',
            FieldParameter::REQUIRED => true,
            FieldParameter::VALUES => [
                Article::POSITION_DEFAULT => 'По умолчанию',
                Article::POSITION_BANNER => 'Баннер',
            ],
        ],
//        'source' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_MARKDOWN,
//            FieldParameter::TITLE => 'Источник',
//            FieldParameter::PLACEHOLDER => 'Название источника {ссылка на статью}',
//        ],
        'with_banner' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Выводить с баннером',
            FieldParameter::TITLE_FORM => 'Выводить с Главное изображение Баннером',
            FieldParameter::DEFAULT => true,
            FieldParameter::TOOLTIP => 'Если Активно, то вверху статьи будет Главное изображение в виде Баннера',
        ],
        'published_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата публикации',
            FieldParameter::REQUIRED => true,
            FieldParameter::DATETIME => true,
            FieldParameter::IN_TABLE => 2,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 3,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
