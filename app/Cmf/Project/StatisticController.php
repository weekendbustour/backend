<?php

declare(strict_types=1);

namespace App\Cmf\Project;

use App\Events\Auth\RegisteredEvent;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\Category;
use App\Models\Direction;
use App\Models\Entertainment;
use App\Models\News;
use App\Models\Organization;
use App\Models\Tag;
use App\Models\Tour;
use App\Models\User;
use App\Services\Notification\Slack\SlackCriticalNotification;
use App\Services\Notification\Slack\SlackDebugNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Monolog\Logger as MonologLogger;

class StatisticController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('cmf');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $oUsers = User::all();
        $oDirections = Direction::all();
        $oTours = Tour::all();
        $oOrganizations = Organization::all();
        $oCategories = Category::all();
        $oEntertainments = Entertainment::all();
        $oNews = News::all();
        $oArticles = Article::all();

        $aDirections = [];
        $aTours = [];
        $aOrganizations = [];

        $aUsers['count'] = count($oUsers);
        $aUsers['active'] = $oUsers->reject(function (User $item) {
            return !$item->isActive();
        })->count();

        $aDirections['count'] = count($oDirections);
        $aDirections['active'] = $oDirections->reject(function (Direction $item) {
            return !$item->isActive();
        })->count();

        $aTours['count'] = count($oTours);
        $aTours['month'] = Tour::active()->whereBetween('start_at', [now()->startOfMonth(), now()->endOfMonth()])->get()->count();
        $aTours['after_month'] = Tour::active()->where('start_at', '>', now()->endOfMonth())->get()->count();
        $aTours['active'] = $oTours->reject(function (Tour $item) {
            return !$item->isActive();
        })->count();

        $aOrganizations['count'] = count($oOrganizations);
        $aOrganizations['active'] = $oOrganizations->reject(function (Organization $item) {
            return !$item->isActive();
        })->count();

        $aEntertainments['count'] = count($oEntertainments);
        $aEntertainments['active'] = $oEntertainments->reject(function (Entertainment $item) {
            return !$item->isActive();
        })->count();

        $aCategories['count'] = count($oCategories);
        $aCategories['active'] = $oCategories->reject(function (Category $item) {
            return !$item->isActive();
        })->count();

        $aNews['count'] = count($oNews);
        $aNews['active'] = $oNews->reject(function (News $item) {
            return !$item->isActive();
        })->count();

        $aArticles['count'] = count($oArticles);
        $aArticles['active'] = $oArticles->reject(function (Article $item) {
            return !$item->isActive();
        })->count();

        return view('cmf.components.statistic.index', [
            'aUsers' => $aUsers,
            'aDirections' => $aDirections,
            'aTours' => $aTours,
            'aOrganizations' => $aOrganizations,
            'aEntertainments' => $aEntertainments,
            'aCategories' => $aCategories,
            'aNews' => $aNews,
            'aArticles' => $aArticles,
        ]);
    }


    /**
     * @param string $name
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function user()
    {
        $oUsers = User::all();
        return view('cmf.components.statistic.user', [
            'oUsers' => $oUsers,
        ]);
    }
}
