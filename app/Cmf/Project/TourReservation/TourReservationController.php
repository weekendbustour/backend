<?php

declare(strict_types=1);

namespace App\Cmf\Project\TourReservation;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\MainController;
use App\Models\TourReservation;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Cmf\Core\FieldParameter;

class TourReservationController extends MainController
{
    use TourReservationSettingsTrait;
    use ImageableTrait;
    use TourReservationCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Бронирования туров';

    /**
     * Имя сущности
     */
    const NAME = 'tour_reservation';

    /**
     * Иконка
     */
    const ICON = 'icon-pin';

    /**
     * Модель сущности
     */
    public $class = \App\Models\TourReservation::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $image = [];

    /**
     * @var array
     */
    public $fields = [
        'tour' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тур',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Tour::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => true,
            FieldParameter::IN_TABLE => 1,
            FieldParameter::EMPTY => true,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 5,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
