<?php

declare(strict_types=1);

namespace App\Cmf\Project\TourReservation;

trait TourReservationSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => TourReservationController::NAME,
        'title' => TourReservationController::TITLE,
        'description' => null,
        'icon' => TourReservationController::ICON,
    ];
}
