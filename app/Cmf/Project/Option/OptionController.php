<?php

declare(strict_types=1);

namespace App\Cmf\Project\Option;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\FieldParameter;
use App\Cmf\Core\MainController;
use App\Models\Option;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class OptionController extends MainController
{
    use OptionSettingsTrait;
    use ImageableTrait;
    use OptionCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Опции';

    /**
     * Имя сущности
     */
    const NAME = 'option';

    /**
     * Иконка
     */
    const ICON = 'icon-options-vertical';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Option::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
    ];

    protected $aOrderBy = [
        'column' => 'priority',
        'type' => 'desc',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            // 'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            // 'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'purpose' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Назначение',
            FieldParameter::REQUIRED => true,
            FieldParameter::VALUES => [
                Option::PURPOSE_DEFAULT => 'По умолчанию',
                Option::PURPOSE_TOURS => 'Туры',
                Option::PURPOSE_ORGANIZATIONS => 'Организаторы',
            ],
            FieldParameter::IN_TABLE => 3,
        ],
        'name' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Ключ',
            FieldParameter::REQUIRED => true,
        ],
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::DELETE_TITLE => 'направление',
            FieldParameter::REQUIRED => true,
        ],
        'type' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тип',
            FieldParameter::REQUIRED => true,
            FieldParameter::VALUES => [
                parent::DATA_TYPE_TEXT => 'Обычное поле',
                parent::DATA_TYPE_NUMBER => 'Число',
                parent::DATA_TYPE_TEXTAREA => 'Текстовое поле',
                parent::DATA_TYPE_MARKDOWN => 'Текстовое поле со стилизацией',
                parent::DATA_TYPE_CHECKBOX => 'Чекбокс',
            ],
            FieldParameter::IN_TABLE => 2,
        ],
        'description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Описание',
        ],
        'placeholder' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Подложка',
        ],
        'tooltip' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Посказка',
        ],
        'priority' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'Приоритет',
            FieldParameter::TOOLTIP => 'Чем выше - тем выше находится',
            FieldParameter::DEFAULT => 0,
            FieldParameter::LENGTH => 4,
            FieldParameter::IN_TABLE => 4,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 5,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
