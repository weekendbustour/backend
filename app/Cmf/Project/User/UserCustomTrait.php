<?php

declare(strict_types=1);

namespace App\Cmf\Project\User;

use App\Events\ChangeCacheEvent;
use App\Services\Image\ImageType;
use App\Services\Image\Upload\ImageUploadModelService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

trait UserCustomTrait
{
    /**
     * @param Request $request
     */
    public function thisCreate(Request $request)
    {
        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $oUser = User::create($data);

        $this->afterChange([], $oUser);
    }

    /**
     * @param Request $request
     * @param object $oUser
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function thisUpdate(Request $request, $oUser)
    {
        $secondValidate = $this->validateByData($request, $oUser);
        if (!$secondValidate['success']) {
            return responseCommon()->jsonError([
                'email' => $secondValidate['message'],
            ]);
        }
        // профиль
        if (isset($request->first_name) && !is_null($request->first_name)) {
            $data = $request->all();
            $birthday = [
                'day' => $request->get('birthday_day'),
                'month' => $request->get('birthday_month'),
                'year' => $request->get('birthday_year'),
            ];
            if ($birthday['day'] !== null && $birthday['month'] !== null && $birthday['year'] !== null) {
                $data['birthday_at'] = Carbon::parse($birthday['day'] . '.' . $birthday['month'] . '.' . $birthday['year']);
            }

            $oUser->update($data);
        }

        $this->afterChange([], $oUser);

        return responseCommon()->success([
            'name' => $oUser->name,
        ]);
    }

    /**
     * @param $oUser
     * @throws \Exception
     */
    public function thisDestroy($oUser)
    {
        $id = $oUser->id;

        $oActivations = $oUser->activations;
        foreach ($oActivations as $oActivation) {
            $oActivation->delete();
        }

        $oUser->favouriteTours()->delete();
        $oUser->favouriteDirections()->delete();
        $oUser->favouriteOrganizations()->delete();

        $oSubscriptions = $oUser->subscriptions;
        foreach ($oSubscriptions as $oSubscription) {
            $oSubscription->delete();
        }

        $oSubscriptions = $oUser->subscriptionsDirections;
        foreach ($oSubscriptions as $oSubscription) {
            $oSubscription->delete();
        }

        $oSubscriptions = $oUser->subscriptionsOrganizations;
        foreach ($oSubscriptions as $oSubscription) {
            $oSubscription->delete();
        }

        $oComments = $oUser->commentsOrganizations;
        foreach ($oComments as $oComment) {
            $oComment->delete();
        }

        $oComments = $oUser->commentsDirections;
        foreach ($oComments as $oComment) {
            $oComment->delete();
        }

        $oComments = $oUser->commentsNews;
        foreach ($oComments as $oComment) {
            $oComment->delete();
        }

        $oVotes = $oUser->votes;
        foreach ($oVotes as $oVote) {
            $oVote->delete();
        }

        $oReservations = $oUser->reservations;
        foreach ($oReservations as $oReservation) {
            $oReservation->delete();
        }

        $oSocials = $oUser->socials;
        foreach ($oSocials as $oSocial) {
            $oSocial->delete();
        }

        $oImages = $oUser->modelImages;
        foreach ($oImages as $oImage) {
            (new ImageUploadModelService())->delete($oUser, $oImage, $this->image[ImageType::MODEL]);
        }

        $oUser->delete();

        $oItem = new User();
        $oItem->id = $id;
        $this->thisAfterChange($oItem);
    }

    /**
     * @param Request $request
     * @param object $oModel
     * @return array
     */
    protected function validateByData(Request $request, $oModel): array
    {
        $oUsers = $this->class::where('email', $request->get('email'))->get();
        foreach ($oUsers as $oUser) {
            if ($oModel->id !== $oUser->id) {
                return responseCommon()->error([
                    'message' => 'Такое значение поля email уже существует.',
                ]);
            }
        }
        return responseCommon()->success();
    }

    /**
     * @param object $oItem
     */
    public function thisAfterChange($oItem): void
    {
        debug($oItem->id);
        event(new ChangeCacheEvent('user_' . $oItem->id, 'members'));
        $this->afterChange($this->cache);
    }

    /**
     * Скрыть/показать сайд бар для адмики, запрос в
     * resources\assets\js\admin\template\app.js
     *
     * @param Request $request
     * @return array
     */
    public function saveSidebarToggle(Request $request): array
    {
        if ($request->exists('toggle') && $request->get('toggle') === 'true') {
            Session::put('sidebar-toggle', $request->get('toggle'));
        } else {
            Session::remove('sidebar-toggle');
        }
        return responseCommon()->success();
    }

    /**
     * Модальное окно для изменений
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function getModalCommand(Request $request): array
    {
        $view = view($this->getCurrentView() . '.components.modals.command', [])->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    /**
     * Модальное окно с Emoji
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function actionModalMarkdownEmoji(Request $request): array
    {
        return responseCommon()->success([
            'view' => view('cmf.content.default.modals.tabs.markdown.dialogs.emoji')->render(),
        ]);
    }

    public function thisEditDataModal(User $oUser)
    {
        //$oActivations = User
        return [];
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionChangePassword(Request $request, int $id)
    {
        $validation = Validator::make($request->all(), [
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        }
        /** @var User $oUser */
        $oUser = User::find($id);

        $oUser->update([
            'password' => bcrypt($request->get('password')),
        ]);

        return responseCommon()->success([], 'Пароль успешно изменен');
    }
}
