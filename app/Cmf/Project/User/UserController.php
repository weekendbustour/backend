<?php

declare(strict_types=1);

namespace App\Cmf\Project\User;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\SocialableTrait;
use App\Cmf\Core\FieldParameter;
use App\Cmf\Core\MainController;
use App\Models\User;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Role;

class UserController extends MainController
{
    use UserSettingsTrait;
    use ImageableTrait;
    use UserCustomTrait;
    use SocialableTrait;
    use UserMaintenanceTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Пользователи';

    /**
     * Имя сущности
     */
    const NAME = 'user';

    /**
     * Иконка
     */
    const ICON = 'icon-people';

    /**
     * Модель сущности
     */
    public $class = \App\Models\User::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    public $indexComponent = [
        'state' => true,
    ];

    /**
     * @var array
     */
    public $image = [
        ImageType::MODEL => [
            'with_main' => true,
            'unique' => true,
            'clear_cache' => true,
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ],
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        if (!is_null($model)) {
            array_push($this->rules['update']['email'], Rule::unique('users')->ignore($model->id));
            array_push($this->rules['update']['login'], Rule::unique('users')->ignore($model->id));
        }
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            'first_name' => ['required', 'max:255'],
            'email' => ['required', 'email', 'unique:users', 'max:255'],
            'login' => ['required', 'unique:users', 'max:255'],
            'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            'first_name' => ['required', 'max:255'],
            'email' => ['required', 'email', 'max:255'],
            'login' => ['required', 'max:255'],
            'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
            'tabs.socials' => [
                'title' => 'Социальные сети',
            ],
            'tabs.password' => [
                'title' => 'Пароль',
                'tabs_attributes' => [
                    'aria-controls' => 'tab-password',
                    'aria-expanded' => 'false',
                    'data-hidden-submit' => 0,
                ],
                'content_attributes' => [
                    'aria-expanded' => 'false',
                ],
            ],
            'tabs.images.model' => [
                'title' => 'Изображение',
            ],
        ],
        'show' => [
            'tabs.main' => [
                'title' => 'Данные8',
            ],
            'tabs.activations' => [
                'title' => 'Активации',
                'tabs_attributes' => [
                    'aria-controls' => 'activations',
                    'aria-expanded' => 'false',
                    'data-hidden-submit' => 1,
                ],
                'content_attributes' => [
                    'aria-expanded' => 'false',
                ],
            ],
        ],
    ];


//    /**
//     * @return array
//     */
//    public function buildFields()
//    {
//        $oField = (new FieldParameter());
//        return [
//            'first_name' => $oField->clear()
//                ->type(parent::DATA_TYPE_TEXT)
//                ->title('Имя')
//                ->in_table(1)
//                ->delete_title('пользователя')
//                ->required()
//                ->group('full_name_term', 4, true)
//                ->build(),
//            'last_name' => $oField->clear()
//                ->type(parent::DATA_TYPE_TEXT)
//                ->title('Фамилия')
//                ->required()
//                ->group('full_name_term', 4)
//                ->build(),
//            'second_name' => $oField->clear()
//                ->type(parent::DATA_TYPE_TEXT)
//                ->title('Отчество')
//                ->required()
//                ->group('full_name_term', 4, true)
//                ->build(),
//            'roles' => $oField->clear()
//                ->type(parent::DATA_TYPE_SELECT)
//                ->title('Организация')
//                ->relationship(parent::RELATIONSHIP_BELONGS_TO_MANY, Role::class)
//                ->order('orderBy', 'title')
//                ->alias('title')
//                ->multiple()
//                ->required()
//                ->build(),
//            'email' => $oField->clear()
//                ->type(parent::DATA_TYPE_TEXT)
//                ->title('Email')
//                ->required()
//                ->build(),
//            'login' => $oField->clear()
//                ->type(parent::DATA_TYPE_TEXT)
//                ->title('Логин')
//                ->required()
//                ->build(),
//            'phone' => $oField->clear()
//                ->type(parent::DATA_TYPE_TEXT)
//                ->title('Телефон')
//                ->required()
//                ->build(),
//            'status' => $oField->clear()
//                ->type(parent::DATA_TYPE_CHECKBOX)
//                ->title('Статус')
//                ->in_table(2)
//                ->default(true)
//                ->build(),
//        ];
//    }

    /**
     * @var array
     */
    public $fields = [
        'first_name' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Имя',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::DELETE_TITLE => 'пользователя',
            FieldParameter::REQUIRED => true,
            FieldParameter::GROUP_TITLE => 'ФИО',
            FieldParameter::GROUP_NAME => 'full_name_term',
            FieldParameter::GROUP_COL => 4,
        ],
        'last_name' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Фамилия',
            FieldParameter::REQUIRED => true,
            FieldParameter::GROUP_NAME => 'full_name_term',
            FieldParameter::GROUP_COL => 4,
            FieldParameter::GROUP_HIDE => true,
        ],
        'second_name' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Отчество',
            FieldParameter::GROUP_NAME => 'full_name_term',
            FieldParameter::GROUP_COL => 4,
            FieldParameter::GROUP_HIDE => true,
        ],
        'roles' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Роли',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO_MANY,
            FieldParameter::VALUES => Role::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => true,
            FieldParameter::MULTIPLE => true,
            FieldParameter::IN_TABLE => 3,
            FieldParameter::ROLES => [
                User::ROLE_SUPER_ADMIN,
            ],
        ],
        'login' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Логин',
            FieldParameter::REQUIRED => true,
        ],
        'email' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Email',
            FieldParameter::REQUIRED => true,
            FieldParameter::GROUP_TITLE => 'Основные контакты',
            FieldParameter::GROUP_NAME => 'email_phone_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::IN_TABLE => 2,
        ],
        'phone' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Телефон',
            FieldParameter::REQUIRED => true,
            FieldParameter::MASK_PHONE => true,
            FieldParameter::GROUP_NAME => 'email_phone_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => true,
            FieldParameter::IN_TABLE => 4,
        ],
        'birthday_day' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'День',
            FieldParameter::LENGTH => 2,
            FieldParameter::PLACEHOLDER => 'xx',
            FieldParameter::GROUP_TITLE => 'Дата рождения',
            FieldParameter::GROUP_NAME => 'birthday_term',
            FieldParameter::GROUP_COL => 4,
            FieldParameter::ALIAS => 'birthday_day',
        ],
        'birthday_month' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'Месяц',
            FieldParameter::PLACEHOLDER => 'xx',
            FieldParameter::LENGTH => 2,
            FieldParameter::GROUP_NAME => 'birthday_term',
            FieldParameter::GROUP_COL => 4,
            FieldParameter::GROUP_HIDE => true,
            FieldParameter::ALIAS => 'birthday_month',
        ],
        'birthday_year' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'Год',
            FieldParameter::PLACEHOLDER => 'xxxx',
            FieldParameter::LENGTH => 4,
            FieldParameter::GROUP_NAME => 'birthday_term',
            FieldParameter::GROUP_COL => 4,
            FieldParameter::GROUP_HIDE => true,
            FieldParameter::ALIAS => 'birthday_year',
        ],
        'gender' => [
            FieldParameter::TYPE => parent::DATA_TYPE_RADIO,
            FieldParameter::TITLE => 'Пол',
            FieldParameter::RADIO_VALUES => [
                0 => 'Женский',
                1 => 'Мужской',
            ],
        ],
        'agree' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Разрешил обработку первоснальных данных',
            FieldParameter::DEFAULT => true,
        ],
        'mailing' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Подписка на новости',
            FieldParameter::DEFAULT => true,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
