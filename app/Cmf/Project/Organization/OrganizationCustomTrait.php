<?php

declare(strict_types=1);

namespace App\Cmf\Project\Organization;

use App\Models\Option;
use App\Models\Organization;
use Illuminate\Http\Request;

trait OrganizationCustomTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionCommentsGetModal(Request $request, int $id)
    {
        $oItem = Organization::find($id);

        $tabs = collect($this->tabs['edit'])->only('tabs.comments')->toArray();

        $view = view('cmf.content.default.modals.container.edit', [
            'oItem' => $oItem,
            'tabs' => $tabs,
            'model' => self::NAME,
        ])->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionVideoSave(Request $request, int $id): array
    {
        $video = $request->get('video');
        if ($video !== '') {
            $data = new Request();
            $data->merge([
                'options' => [
                    Option::NAME_VIDEO_YOUTUBE => $video,
                ],
            ]);
            $this->actionUpdateParameters($data, $id);
        }
        return responseCommon()->success([], 'Данные успешно сохранены');
    }


    /**
     * @param Organization $oOrganization
     * @return array
     */
    protected function metaTagsAutoGenerate(Organization $oOrganization)
    {
        $title = $oOrganization->title;
        $description = $oOrganization->preview_description;

        return [
            'title' => $title,
            'description' => $description,
            'keywords' => null,
        ];
    }
}
