<?php

declare(strict_types=1);

namespace App\Cmf\Project\Organization;

use App\Models\Option;
use App\Models\Organization;

trait OrganizationThisTrait
{
    /**
     * @param Organization $oItem
     * @return array
     */
    public function thisEditDataModal(Organization $oItem)
    {
        // вкладка Параметры
        $oAllOptions = Option::active()->purposeOrganizations()->ordered()->get();
        $oAllOptions = $oAllOptions->reject(function ($item) {
            return in_array($item->name, [
                Option::NAME_VIDEO_YOUTUBE,
                Option::NAME_TIMELINE_NOTE,
            ]);
        });
        // для вкладки параметров
        $oOptions = $oAllOptions->whereNotIn('name', [
            \App\Models\Option::NAME_RESERVATION,
        ]);
        $oReservationOptions = $oAllOptions->whereIn('name', [
            \App\Models\Option::NAME_RESERVATION,
        ]);
        return [
            'oValues' => $oItem->values->keyBy('option.name'),
            'oOptions' => $oOptions,
            'oReservationOptions' => $oReservationOptions,
        ];
    }
}
