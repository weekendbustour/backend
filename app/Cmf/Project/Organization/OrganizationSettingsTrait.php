<?php

declare(strict_types=1);

namespace App\Cmf\Project\Organization;

trait OrganizationSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => OrganizationController::NAME,
        'title' => OrganizationController::TITLE,
        'description' => null,
        'icon' => OrganizationController::ICON,
    ];
}
