<?php

declare(strict_types=1);

namespace App\Cmf\Project\Organization;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\MetaTagableTrait;
use App\Cmf\Core\Defaults\SocialableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\Defaults\TextableUploadTrait;
use App\Cmf\Core\MainController;
use App\Cmf\Core\TabParameter;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Cmf\Core\FieldParameter;

class OrganizationController extends MainController
{
    use OrganizationSettingsTrait;
    use ImageableTrait;
    use OrganizationCustomTrait;
    use OrganizationParametersTrait;
    use OrganizationThisTrait;
    use SocialableTrait;
    use TextableTrait;
    use TextableUploadTrait;
    use MetaTagableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Организаторы';

    /**
     * Имя сущности
     */
    const NAME = 'organization';

    /**
     * Иконка
     */
    const ICON = 'icon-ghost';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Organization::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images', 'comments', 'metaTag', 'toursActive', 'tours'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    public $indexComponent = [
        'image' => false,
        'comments' => true,
        'private_show' => true,
        'state' => true,
    ];

    /**
     * Лимит для пагинации
     *
     * @var int
     */
    protected $tableLimit = 40;

    /**
     * @var array
     */
    protected $aOrderBy = [
        'column' => 'title',
        'type' => 'asc',
    ];

    /**
     * @var array
     */
    public $image = [
        ImageType::MODEL => [
            'with_main' => true,
            'unique' => false,
            'filters' => [
                ImageSize::XS => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => 36,
                    ],
                ],
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
                ImageSize::CARD_DEFAULT => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 540,
                        'height' => 212,
                    ],
                ],
            ],
        ],
        ImageType::BANNER => [
            'with_main' => true,
            'unique' => true,
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::CARD_DEFAULT => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 540,
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ],
        ImageType::GALLERY => [
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ]
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => [FieldParameter::REQUIRED, 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png,webp'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'scrolling' => true,
        'edit' => [
            TabParameter::TAB_MAIN => TabParameter::TAB_MAIN_CONTENT,
            TabParameter::TAB_MARKDOWN => TabParameter::TAB_MARKDOWN_CONTENT,
            TabParameter::TAB_PARAMETERS => TabParameter::TAB_PARAMETERS_CONTENT,
            'tabs.socials' => [
                'title' => 'Социальные сети',
            ],
            TabParameter::TAB_IMAGES_MODEL => TabParameter::TAB_IMAGES_MODEL_CONTENT,
            TabParameter::TAB_IMAGES_BANNER => TabParameter::TAB_IMAGES_BANNER_CONTENT,
            'tabs.images.gallery' => [
                'title' => 'Галлерея',
            ],
            TabParameter::TAB_META_TAGS => TabParameter::TAB_META_TAGS_CONTENT,
            'tabs.comments' => [
                'title' => 'Комментарии',
            ],
            TabParameter::TAB_VIDEO => TabParameter::TAB_VIDEO_CONTENT,
            TabParameter::TAB_RESERVATION => TabParameter::TAB_RESERVATION_CONTENT,
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'user' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Ответственный',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\User::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'first_name',
            ],
            FieldParameter::ALIAS => 'first_name',
            FieldParameter::EMPTY => true,
        ],
        'departures' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Отправления',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO_MANY,
            FieldParameter::VALUES => \App\Models\Departure::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::EMPTY => false,
            FieldParameter::MULTIPLE => true,
        ],
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::DELETE_TITLE => 'организацию',
            FieldParameter::REQUIRED => true,
        ],
        'preview_description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Краткое описание',
        ],
//        'description' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_MARKDOWN,
//            FieldParameter::TITLE => 'Полное описание',
//        ],
        'signature' => [
            FieldParameter::TYPE => parent::DATA_TYPE_MARKDOWN,
            FieldParameter::TITLE => 'Подпись',
        ],
        'tours' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CUSTOM,
            FieldParameter::TITLE => 'Туры',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::HIDDEN => true,
        ],
        'parent' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Родительская организация',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Organization::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::EMPTY => true,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
