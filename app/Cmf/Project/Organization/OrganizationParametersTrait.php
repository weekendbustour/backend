<?php

declare(strict_types=1);

namespace App\Cmf\Project\Organization;

use App\Cmf\Core\MainController;
use App\Models\Option;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\OrganizationOptionValues;
use Illuminate\Http\Request;

trait OrganizationParametersTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionUpdateParameters(Request $request, int $id)
    {
        $oItem = Organization::find($id);
        if ($request->exists('options')) {
            $aOptions = $request->get('options');
            foreach ($aOptions as $key => $value) {
                $oOption = Option::where('name', $key)->where('purpose', Option::PURPOSE_ORGANIZATIONS)->first();
                if (!is_null($oOption)) {
                    $oValue = OrganizationOptionValues::where('organization_id', $oItem->id)
                        ->where('option_id', $oOption->id)
                        ->first();
                    if (isset($value)) {
                        if ($oOption->type === MainController::DATA_TYPE_CHECKBOX && (int)$value === 0 && in_array($key, [
                                Option::NAME_PHOTO_SESSION,
                            ])) {
                            if (!is_null($oValue)) {
                                $oValue->delete();
                            }
                            continue;
                        }
                        if (!is_null($oValue)) {
                            $oValue->update([
                                'value' => $value,
                            ]);
                        } else {
                            $oValue = OrganizationOptionValues::create([
                                'organization_id' => $oItem->id,
                                'option_id' => $oOption->id,
                                'value' => $value,
                            ]);
                        }
                    } else {
                        if (!is_null($oValue)) {
                            $oValue->delete();
                        }
                    }
                }
            }
        }
        return responseCommon()->success([], 'Данные успешно обновлены');
    }
}
