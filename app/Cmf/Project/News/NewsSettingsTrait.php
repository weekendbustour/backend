<?php

declare(strict_types=1);

namespace App\Cmf\Project\News;

trait NewsSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => NewsController::NAME,
        'title' => NewsController::TITLE,
        'description' => null,
        'icon' => NewsController::ICON,
    ];
}
