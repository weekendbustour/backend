<?php

declare(strict_types=1);

namespace App\Cmf\Project\News;

use App\Models\News;
use App\Services\Image\Upload\ImageUploadService;
use App\Services\Seo\SeoSitemapService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

trait NewsCustomTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionCommentsGetModal(Request $request, int $id)
    {
        $oItem = News::find($id);

        $tabs = collect($this->tabs['edit'])->only('tabs.comments')->toArray();

        $view = view('cmf.content.default.modals.container.edit', [
            'oItem' => $oItem,
            'tabs' => $tabs,
            'model' => self::NAME,
        ])->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionRelease(Request $request, int $id)
    {
        $oItem = News::find($id);
        $release = (int)$request->get('release');
        if ($release === 1) {
            if ($oItem->status !== News::STATUS_ACTIVE) {
                return responseCommon()->error([], 'Статус новости должен быть "' . (new News())->statuses()[News::STATUS_ACTIVE] . '"');
            }

            if (is_null($oItem->release_at)) {
                $oItem->update([
                    'release_at' => now(),
                ]);
            }
            (new SeoSitemapService())->dispatch();
            return responseCommon()->success([], 'Новость успешно опубликована');
        } else {
            if (!is_null($oItem->release_at)) {
                $oItem->update([
                    'release_at' => null,
                ]);
            }
            (new SeoSitemapService())->dispatch();
            return responseCommon()->success([], 'Новость успешно снята с публикации');
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionDuplicateCreate(Request $request, int $id)
    {
        $oItem = News::find($id);

        $oNew = $oItem->replicate();
        $oNew->release_at = null;
        $oNew->status = 0;
        $oNew->save();

        // копировать изображения
        foreach ($oItem->images as $oValue) {
            $oNewValue = $oValue->replicate();
            $oNewValue->imageable_id = $oNew->id;
            $oNewValue->save();
        }
        // копировать изображения
        $from = (new ImageUploadService())->getDirectory(self::NAME, $oItem->id);
        $to = (new ImageUploadService())->getDirectory(self::NAME, $oNew->id);
        File::copyDirectory($from, $to);

        return responseCommon()->success([], 'Дубликат успешно создан');
    }
}
