<?php

declare(strict_types=1);

namespace App\Cmf\Project\News;

use App\Cmf\Core\Defaults\GeoTrait;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\SourceableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\Defaults\TextableUploadTrait;
use App\Cmf\Core\MainController;
use App\Cmf\Core\TabParameter;
use App\Models\Category;
use App\Models\News;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Cmf\Core\FieldParameter;

class NewsController extends MainController
{
    use NewsSettingsTrait;
    use ImageableTrait;
    use NewsCustomTrait;
    use TextableTrait;
    use TextableUploadTrait;
    use SourceableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Новости';

    /**
     * Имя сущности
     */
    const NAME = 'news';

    /**
     * Иконка
     */
    const ICON = 'icon-book-open';

    /**
     * Модель сущности
     */
    public $class = \App\Models\News::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images', 'comments'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    public $indexComponent = [
        'image' => true,
        'comments' => true,
        'release_at' => true,
        'private_show' => true,
    ];

    /**
     * @var array
     */
    public $image = [
        ImageType::MODEL => [
            'with_main' => true,
            'unique' => false,
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
                ImageSize::CARD_DEFAULT => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 350,
                        'height' => 445,
                    ],
                ],
                ImageSize::CARD_MD => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 700,
                        'height' => 890,
                    ],
                ],
            ],
        ],
    ];

    /**
     * @param object|null $model
     * @param array $data
     * @return array
     */
    public function rules($model = null, array $data = [])
    {
        if (!empty($data) && $data['position'] === News::POSITION_BANNER) {
            if (!is_null($model)) {
                array_push($this->rules['update']['position'], Rule::unique('news')->ignore($model->id));
            } else {
                array_push($this->rules['store']['position'], Rule::unique('news'));
            }
        }
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            'position' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            'position' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png,webp'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            TabParameter::TAB_MAIN => TabParameter::TAB_MAIN_CONTENT,
            TabParameter::TAB_MARKDOWN => TabParameter::TAB_MARKDOWN_CONTENT,
            TabParameter::TAB_IMAGES_MODEL => TabParameter::TAB_IMAGES_MODEL_CONTENT,
            TabParameter::TAB_SOURCE => TabParameter::TAB_SOURCE_CONTENT,
            'tabs.comments' => [
                'title' => 'Комментарии',
            ],
            TabParameter::TAB_DUPLICATE => TabParameter::TAB_DUPLICATE_CONTENT,
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::DELETE_TITLE => 'направление',
            FieldParameter::REQUIRED => true,
        ],
        'preview_description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Краткое описание',
        ],
        'categories' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Категория',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO_MANY,
            FieldParameter::VALUES => \App\Models\Category::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            'whereIn' => [
                'column' => 'type',
                'value' => [
                    Category::TYPE_NEWS,
                ],
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => false,
            FieldParameter::EMPTY => true,
            FieldParameter::MULTIPLE => false,
        ],
        'position' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Расположение',
            FieldParameter::REQUIRED => true,
            FieldParameter::VALUES => [
                News::POSITION_DEFAULT => 'По умолчанию',
                News::POSITION_BANNER => 'Баннер',
            ],
        ],
//        'source' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_MARKDOWN,
//            FieldParameter::TITLE => 'Источник',
//        ],
        'with_banner' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Выводить с баннером',
            FieldParameter::TITLE_FORM => 'Выводить с баннером',
            FieldParameter::DEFAULT => true,
            FieldParameter::TOOLTIP => 'Если Активно, то вверху новости будет изображение в виде Баннера',
        ],
        'published_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата публикации',
            FieldParameter::REQUIRED => true,
            FieldParameter::DATETIME => true,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 3,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
