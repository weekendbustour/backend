<?php

declare(strict_types=1);

namespace App\Cmf\Project\Payment;

use App\Cmf\Core\SettingsTrait;

trait PaymentSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => PaymentController::TITLE,
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => PaymentController::NAME,
            ],
        ],
        'icon' => 'fa fa-users',
    ];
}
