<?php

declare(strict_types=1);

namespace App\Cmf\Project\Payment;

use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\FieldParameter;
use App\Cmf\Core\MainController;
use App\Models\Payment;
use App\Models\Promotion;
use App\Models\User;

class PaymentController extends MainController
{
    use TextableTrait;
    use PaymentSettingsTrait;
    use PaymentCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Платежи';

    /**
     * Имя сущности
     */
    const NAME = 'payment';

    /**
     * Иконка
     */
    const ICON = 'icon-paypal';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Payment::class;

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [

    ];

    /**
     * Настройки для компонента
     *
     * @var array
     */
    public $indexComponent = [
        'image' => false,
        'create' => false,
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => []
        ]
    ];

    /**
     * @var array
     */
    public $roles = [
        User::ROLE_ADMIN,
    ];

    /**
     * Validation rules
     * @var array
     */
    public $rules = [];

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * @var array
     */
    public $fields = [
        'opened_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата и время Открытия',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::FORMAT => 'd.m.Y H:i:s',
            FieldParameter::DATETIME => true,
        ],
        'closed_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата и время Закрытия',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::FORMAT => 'd.m.Y H:i:s',
            FieldParameter::DATETIME => true,
        ],
        'type' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тип',
            FieldParameter::REQUIRED => true,
            FieldParameter::VALUES => [
                Payment::TYPE_PAY => 'Оплата',
                Payment::TYPE_RETURN => 'Возврат',
            ],
            FieldParameter::IN_TABLE => 3,
        ],
        'amount' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'Сумма',
            FieldParameter::IN_TABLE => 4,
            FieldParameter::LENGTH => 8,
        ],
        'code' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Код',
        ],
        'parent' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Родительский платеж',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Payment::class,
            'order' => [
                'method' => 'orderBy',
                'by' => 'created_at',
            ],
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::VALUES => [
                Payment::STATUS_NOT_ACTIVE => 'Не активно',
                Payment::STATUS_ACTIVE => 'Активно',
                Payment::STATUS_CLOSED => 'Закрыт',
            ],
            FieldParameter::IN_TABLE => 5,
        ],
    ];
}
