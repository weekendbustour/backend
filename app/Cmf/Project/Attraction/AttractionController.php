<?php

declare(strict_types=1);

namespace App\Cmf\Project\Attraction;

use App\Cmf\Core\Defaults\GeoTrait;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\MetaTagableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\Defaults\TextableUploadTrait;
use App\Cmf\Core\FieldParameter;
use App\Cmf\Core\ImageParameter;
use App\Cmf\Core\MainController;
use App\Cmf\Core\TabParameter;
use App\Cmf\Project\Article\ArticleController;
use App\Cmf\Project\Direction\DirectionController;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AttractionController extends MainController
{
    use AttractionSettingsTrait;
    use ImageableTrait;
    use AttractionCustomTrait;
    use AttractionThisTrait;
    use AttractionParametersTrait;
    use TextableTrait;
    use TextableUploadTrait;
    use GeoTrait;
    use MetaTagableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Достопримечательности';

    /**
     * Имя сущности
     */
    const NAME = 'attraction';

    /**
     * Иконка
     */
    const ICON = 'icon-rocket';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Attraction::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images', 'articlesOrdered'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    public $indexComponent = [
        'image' => true,
        'comments' => true,
        'release_at' => true,
        'private_show' => true,
        'state' => true,
    ];

    /**
     * @var array
     */
    protected $aOrderBy = [
        'column' => 'title',
        'type' => 'desc',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png,webp'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        //'scrolling' => true,
        'edit' => [
            TabParameter::TAB_MAIN => TabParameter::TAB_MAIN_CONTENT,
            TabParameter::TAB_MARKDOWN => TabParameter::TAB_MARKDOWN_CONTENT,
            //TabParameter::TAB_PARAMETERS => TabParameter::TAB_PARAMETERS_CONTENT,
            'tabs.parameters.way' => [
                'title' => 'Дорога',
                'tabs_attributes' => [
                    'aria-controls' => 'tab-parameters',
                    'aria-expanded' => 'false',
                    'data-hidden-submit' => 0,
                ],
                'content_attributes' => [
                    'aria-expanded' => 'false',
                ],
            ],
            TabParameter::TAB_IMAGES_MODEL => TabParameter::TAB_IMAGES_MODEL_CONTENT,
            TabParameter::TAB_IMAGES_GALLERY => TabParameter::TAB_IMAGES_GALLERY_CONTENT,
            TabParameter::TAB_GEO_COORDINATES => TabParameter::TAB_GEO_COORDINATES_CONTENT,
            TabParameter::TAB_META_TAGS => TabParameter::TAB_META_TAGS_CONTENT,
            //TabParameter::TAB_COMMENTS => TabParameter::TAB_COMMENTS_CONTENT,
        ],
    ];

    /**
     * @var array
     */
    public $image = [
        ImageType::MODEL => [
            'with_main' => true,
            'filters' => [
                ImageSize::XS => ImageParameter::IMAGE_SIZE_XS_CONTENT,
                ImageSize::SQUARE => ImageParameter::IMAGE_SIZE_SQUARE_CONTENT,
                ImageSize::CARD_DEFAULT => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 630,
                        'height' => 270,
                    ],
                ],
                ImageSize::XL => ImageParameter::IMAGE_SIZE_XL_CONTENT,
            ],
        ],
        ImageType::GALLERY => [
            'filters' => [
                ImageSize::SQUARE => ImageParameter::IMAGE_SIZE_SQUARE_CONTENT,
                ImageSize::XL => ImageParameter::IMAGE_SIZE_XL_CONTENT,
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'direction' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Направление',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Direction::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => false,
            FieldParameter::EMPTY => true,
            FieldParameter::IN_TABLE => 1,
            FieldParameter::TABLE_TITLE => '<i class="' . DirectionController::ICON . '"></i>',
        ],
        'articles' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Статья',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO_MANY,
            FieldParameter::VALUES => \App\Models\Article::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => false,
            FieldParameter::EMPTY => false,
            FieldParameter::MULTIPLE => true,
            FieldParameter::IN_TABLE => 2,
            FieldParameter::TABLE_TITLE => '<i class="' . ArticleController::ICON . '"></i>',
        ],
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 3,
            FieldParameter::DELETE_TITLE => 'достопримечательность',
            FieldParameter::REQUIRED => true,
        ],
        'preview_description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Краткое описание',
        ],
        'with_banner' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Выводить с баннером',
            FieldParameter::TITLE_FORM => 'Выводить с Главное изображение Баннером',
            FieldParameter::DEFAULT => true,
            FieldParameter::TOOLTIP => 'Если Активно, то вверху статьи будет Главное изображение в виде Баннера',
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
