<?php

declare(strict_types=1);

namespace App\Cmf\Project\Attraction;

use App\Cmf\Core\MainController;
use App\Models\Attraction;
use App\Models\AttractionOptionValues;
use App\Models\Option;
use App\Models\TourOptionValues;
use Illuminate\Http\Request;

trait AttractionParametersTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionUpdateParameters(Request $request, int $id)
    {
        $oItem = Attraction::find($id);
        if ($request->exists('options')) {
            $aOptions = $request->get('options');
            foreach ($aOptions as $key => $value) {
                $oOption = Option::where('name', $key)->where('purpose', Option::PURPOSE_ATTRACTION)->first();
                if (!is_null($oOption)) {
                    $oValue = AttractionOptionValues::where('attraction_id', $oItem->id)
                        ->where('option_id', $oOption->id)
                        ->first();
                    if (isset($value)) {
                        if ($oOption->type === MainController::DATA_TYPE_CHECKBOX && (int)$value === 0 && in_array($key, [
                                Option::NAME_PHOTO_SESSION,
                            ])) {
                            if (!is_null($oValue)) {
                                $oValue->delete();
                            }
                            continue;
                        }
                        $this->parameterSaveOptionValue($oItem, $oOption, $value, $oValue);
                    } else {
                        if (!is_null($oValue)) {
                            $oValue->delete();
                        }
                    }
                }
            }
        }
        return responseCommon()->success([], 'Данные успешно обновлены');
    }

    /**
     * @param $oItem
     * @param Option $oOption
     * @param string $value
     * @param AttractionOptionValues|null $oValue
     * @return mixed
     */
    private function parameterSaveOptionValue($oItem, Option $oOption, string $value, ?AttractionOptionValues $oValue)
    {
        if (!is_null($oValue)) {
            $oValue->update([
                'value' => $value,
            ]);
        } else {
            $oValue = AttractionOptionValues::create([
                'attraction_id' => $oItem->id,
                'option_id' => $oOption->id,
                'value' => $value,
            ]);
        }
        return $oValue;
    }
}
