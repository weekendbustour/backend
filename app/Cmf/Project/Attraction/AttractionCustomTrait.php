<?php

declare(strict_types=1);

namespace App\Cmf\Project\Attraction;

use App\Models\Attraction;
use App\Services\Seo\SeoSitemapService;
use Illuminate\Http\Request;

trait AttractionCustomTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionCommentsGetModal(Request $request, int $id)
    {
        $oItem = Attraction::find($id);

        $tabs = collect($this->tabs['edit'])->only('tabs.comments')->toArray();

        $view = view('cmf.content.default.modals.container.edit', [
            'oItem' => $oItem,
            'tabs' => $tabs,
            'model' => self::NAME,
        ])->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionRelease(Request $request, int $id)
    {
        $oItem = Attraction::find($id);
        $release = (int)$request->get('release');
        if ($release === 1) {
            if ($oItem->status !== Attraction::STATUS_ACTIVE) {
                return responseCommon()->error([], 'Статус должен быть "' . (new Attraction())->statuses()[Attraction::STATUS_ACTIVE] . '"');
            }

            if (is_null($oItem->release_at)) {
                $oItem->update([
                    'release_at' => now(),
                ]);
            }
            //(new SeoSitemapService())->dispatch();
            return responseCommon()->success([], 'Достопримечательность успешно опубликована');
        } else {
            if (!is_null($oItem->release_at)) {
                $oItem->update([
                    'release_at' => null,
                ]);
            }
            //(new SeoSitemapService())->dispatch();
            return responseCommon()->success([], 'Достопримечательность успешно снята с публикации');
        }
    }

    /**
     * @param Attraction $oItem
     * @return array
     */
    protected function metaTagsAutoGenerate(Attraction $oItem)
    {
        return [
            'title' => $oItem->title,
            'description' => $oItem->preview_description,
            'keywords' => null,
        ];
    }
}
