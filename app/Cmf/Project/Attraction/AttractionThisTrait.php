<?php

declare(strict_types=1);

namespace App\Cmf\Project\Attraction;

use App\Models\Attraction;
use App\Models\Option;

trait AttractionThisTrait
{
    /**
     * @param Attraction $oItem
     * @return array
     */
    public function thisEditDataModal(Attraction $oItem)
    {
        // вкладка Параметры
        $oAllOptions = Option::active()->purposeAttraction()->ordered()->get();
        $oAllOptions = $oAllOptions->reject(function ($item) {
            return in_array($item->name, [

            ]);
        });
        $oOptions = $oAllOptions->whereNotIn('name', [
            Option::NAME_WAY,
        ]);
        $oWayOptions = $oAllOptions->whereIn('name', [
            \App\Models\Option::NAME_WAY,
        ]);

        return [
            'oValues' => $oItem->values->keyBy('option.name'),
            'oOptions' => $oOptions,
            'oWayOptions' => $oWayOptions,
        ];
    }
}
