<?php

declare(strict_types=1);

namespace App\Cmf\Project\Attraction;

trait AttractionSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => AttractionController::NAME,
        'title' => AttractionController::TITLE,
        'description' => null,
        'icon' => AttractionController::ICON,
    ];
}
