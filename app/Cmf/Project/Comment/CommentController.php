<?php

declare(strict_types=1);

namespace App\Cmf\Project\Comment;

use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\MainController;
use App\Models\User;

class CommentController extends MainController
{
    use TextableTrait;
    use CommentSettingsTrait;
    use CommentCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Комментарии';

    /**
     * Имя сущности
     */
    const NAME = 'comment';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Comment::class;

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [

    ];

    /**
     * Настройки для компонента
     *
     * @var array
     */
    public $indexComponent = [
        'history' => false,
        'image' => false,
        'create' => false,
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => []
        ]
    ];

    /**
     * @var array
     */
    public $roles = [
        User::ROLE_ADMIN,
    ];

    /**
     * Validation rules
     * @var array
     */
    public $rules = [];

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = [];

    /**
     * @var array
     */
    public $fields = [
        'created_at' => [
            'dataType' => parent::DATA_TYPE_DATE,
            'title' => 'Дата и время',
            'in_table' => 1,
            'hidden' => true,
            'format' => 'd.m.Y H:i:s',
            'search' => 1,
            'datetime' => true,
        ],
        'commentableObject' => [
            'dataType' => parent::DATA_TYPE_SELECT,
            'title' => 'Модель',
            'in_table' => 2,
            'relationship' => parent::RELATIONSHIP_BELONGS_TO,
            'values' => \App\Models\Direction::class,
            'required' => true,
            'order' => [
                'method' => 'orderBy',
                'by' => 'created_at',
            ],
            'limit' => 20,
            'hidden' => true,
        ],
        'commentable_type' => [
            'dataType' => parent::DATA_TYPE_SELECT,
            'title' => 'Тип',
            'hidden' => 1,
        ],
        'commented' => [
            'dataType' => parent::DATA_TYPE_SELECT,
            'title' => 'Автор',
            'in_table' => 3,
            'relationship' => parent::RELATIONSHIP_BELONGS_TO,
            'values' => \App\Models\User::class,
            'required' => true,
            'order' => [
                'method' => 'orderBy',
            ],
            'limit' => 20,
            'hidden' => true,
        ],
        'text' => [
            'dataType' => parent::DATA_TYPE_TEXTAREA,
            'title' => 'Текст',
            'in_table' => 4,
        ],
        'reply' => [
            'dataType' => parent::DATA_TYPE_SELECT,
            'title' => 'Ответ на',
            'relationship' => parent::RELATIONSHIP_BELONGS_TO,
            'values' => \App\Models\Comment::class,
            'order' => [
                'method' => 'orderBy',
                'by' => 'created_at',
            ],
            'hidden' => 1,
        ],
        'parent' => [
            'dataType' => parent::DATA_TYPE_SELECT,
            'title' => 'Родительский комментарий',
            'relationship' => parent::RELATIONSHIP_BELONGS_TO,
            'values' => \App\Models\Comment::class,
            'order' => [
                'method' => 'orderBy',
                'by' => 'created_at',
            ],
            'hidden' => 1,
        ],
        'approved' => [
            'dataType' => parent::DATA_TYPE_CHECKBOX,
            'title' => 'Одобрен',
            'checked' => true,
            'in_table' => 8,
            'check_style' => 'icon',
        ],
    ];
}
