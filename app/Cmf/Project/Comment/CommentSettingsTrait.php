<?php

declare(strict_types=1);

namespace App\Cmf\Project\Comment;

use App\Cmf\Core\SettingsTrait;

trait CommentSettingsTrait
{
    use SettingsTrait;

    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'title' => CommentController::TITLE,
        'cruds' => [
            'index' => [
                'title' => 'Все',
                'url' => CommentController::NAME,
            ],
        ],
        'icon' => 'fa fa-users',
    ];
}
