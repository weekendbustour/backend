<?php

declare(strict_types=1);

namespace App\Cmf\Project\Comment;

use Illuminate\Http\Request;
use App\Models\Comment;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

trait CommentCustomTrait
{
    /**
     * @param object|null $oItem
     */
    public function thisPrepareFieldsValues($oItem = null)
    {
        $this->fields['commentable_type']['values'] = (new Comment())->getCommentablesText();
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function getRelationshipFieldModal(Request $request, $id)
    {
        $origField = $request->get('field');

        if ($origField === 'commentableObject') {
            $oModel = Comment::find($id);

            $field = null;
            $origField = $request->get('field');

            $aTypes = $oModel->getCommentables();
            foreach ($aTypes as $type) {
                if ($oModel->commentable_type === $type) {
                    $this->fields[$origField]['values'] = $type;
                    $type = str_replace('App\\Models\\', '', $type);
                    $field = Str::lower($type);
                }
            }
            $request->merge([
                'field' => $origField,
            ]);
        }
        return parent::getRelationshipFieldModal($request, $id);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionSaveComment(Request $request, int $id)
    {
        if (empty($request->get('comment'))) {
            return responseCommon()->jsonError([], 'Комментарий не может быть пустым');
        }
        $type = $request->get('commentable_type');
        $aCommentables = (new Comment())->getCommentables();
        if (empty($type) || !in_array($type, $aCommentables)) {
            return responseCommon()->jsonError([], 'Сущность ' . $type . ' не поддерживает комментарии.');
        }
        $class = $type;
        $oModel = $class::find($id);

        $text = $request->get('comment');

        // почему-то после сохранения выводит с \ в конце
        // т.к. сохраняется свои переносы \n
        $text = str_replace('\\', '', $text);

        Auth::user()->comment($oModel, $text);

        $view = view('cmf.components.comments.comments', [
            'oItem' => $oModel,
            'model' => self::NAME,
        ])->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionDeleteComment(Request $request, int $id)
    {
//        if (!User::hasRole(User::ROLE_SUPER_ADMIN)) {
//            return responseCommon()->error([], 'Вы не администратор');
//        }
        $oComment = Comment::find($id);

        $class = $oComment->commentable_type;
        $oModel = $class::find($oComment->commentable_id);
        if (!is_null($oComment)) {
            $oComment->delete();
        }
        $view = view('cmf.components.comments.comments', [
            'oItem' => $oModel,
            'model' => self::NAME,
        ])->render();
        return responseCommon()->success([
            'view' => $view,
        ]);
    }
}
