<?php

declare(strict_types=1);

namespace App\Cmf\Project\Departure;

trait DepartureSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => DepartureController::NAME,
        'title' => DepartureController::TITLE,
        'description' => null,
        'icon' => DepartureController::ICON,
    ];
}
