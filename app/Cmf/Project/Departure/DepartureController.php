<?php

declare(strict_types=1);

namespace App\Cmf\Project\Departure;

use App\Cmf\Core\Defaults\GeoTrait;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\WordDeclensionableTrait;
use App\Cmf\Core\MainController;
use App\Cmf\Core\TabParameter;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Cmf\Core\FieldParameter;

class DepartureController extends MainController
{
    use DepartureSettingsTrait;
    use ImageableTrait;
    use DepartureCustomTrait;
    use GeoTrait;
    use WordDeclensionableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Отправления';

    /**
     * Имя сущности
     */
    const NAME = 'departure';

    /**
     * Иконка
     */
    const ICON = 'icon-flag';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Departure::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    protected $aOrderBy = [
        'column' => 'title',
        'type' => 'asc',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
            'tabs.images.model' => [
                'title' => 'Изображение',
            ],
            TabParameter::TAB_WORD_DECLENSIONS => TabParameter::TAB_WORD_DECLENSIONS_CONTENT,
            'tabs.geo.coordinates' => [
                'title' => 'Координаты',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::DELETE_TITLE => 'направление',
            FieldParameter::REQUIRED => true,
        ],
        'description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Описание',
        ],
        'location' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CUSTOM,
            FieldParameter::TITLE => 'Геолокация',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::HIDDEN => true,
            'table_icon' => 'icon-location-pin',
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 3,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
