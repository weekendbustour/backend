<?php

declare(strict_types=1);

namespace App\Cmf\Project\Outfit;

trait OutfitSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => OutfitController::NAME,
        'title' => OutfitController::TITLE,
        'description' => null,
        'icon' => OutfitController::ICON,
    ];
}
