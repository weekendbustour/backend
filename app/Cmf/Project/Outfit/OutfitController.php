<?php

declare(strict_types=1);

namespace App\Cmf\Project\Outfit;

use App\Cmf\Core\Defaults\GeoTrait;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\WordDeclensionableTrait;
use App\Cmf\Core\MainController;
use App\Cmf\Core\TabParameter;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Cmf\Core\FieldParameter;

class OutfitController extends MainController
{
    use OutfitSettingsTrait;
    use ImageableTrait;
    use OutfitCustomTrait;
    use GeoTrait;
    use WordDeclensionableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Снаряжения';

    /**
     * Имя сущности
     */
    const NAME = 'outfit';

    /**
     * Иконка
     */
    const ICON = 'icon-umbrella';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Outfit::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    protected $aOrderBy = [
        'column' => 'priority',
        'type' => 'desc',
    ];

    /**
     * Лимит для пагинации
     *
     * @var int
     */
    protected $tableLimit = 40;

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            TabParameter::TAB_MAIN => TabParameter::TAB_MAIN_CONTENT,
            TabParameter::TAB_IMAGES_MODEL => TabParameter::TAB_IMAGES_MODEL_CONTENT,
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::DELETE_TITLE => 'направление',
            FieldParameter::REQUIRED => true,
        ],
        'preview_description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Описание',
            FieldParameter::LIMIT => 255,
        ],
        'tooltip' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Подсказка'
        ],
        'priority' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'Приоритет',
            FieldParameter::DEFAULT => 0,
            FieldParameter::IN_TABLE => 2,
            FieldParameter::LENGTH => 4,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 3,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
