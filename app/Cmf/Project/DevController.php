<?php

declare(strict_types=1);

namespace App\Cmf\Project;

use App\Console\Commands\ScheduleCommon;
use App\Http\Controllers\Controller;
use App\Jobs\QueueCommon;
use App\Mail\Auth\RegisteredMail;
use App\Mail\Auth\ResetPasswordMail;
use App\Mail\Subscription\SubscriptionDirectionMail;
use App\Mail\Subscription\SubscriptionOrganizationMail;
use App\Models\Direction;
use App\Models\Organization;
use App\Models\Tour;
use App\Models\User;
use App\Services\Preview;
use App\Services\Seo\SeoSitemapService;
use App\Services\Toastr\Toastr;
use App\Services\Vk\VkApi;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Spatie\Sitemap\Sitemap;

class DevController extends Controller
{
    /**
     * @param Request $request
     * @param string $name
     * @return \Illuminate\View\View
     */
    public function php(Request $request, $name): \Illuminate\View\View
    {
        $method = 'php' . Str::title($name);
        return method_exists($this, $method) ? $this->{$method}() : abort(500, 'Method ' . $method . ' not found');
    }

    /**
     * @param Request $request
     * @param string $name
     * @return \Illuminate\Http\RedirectResponse
     */
    public function command(Request $request, $name): \Illuminate\Http\RedirectResponse
    {
        $command = $name;

        $aCommand = explode(' ', $command);
        foreach ($aCommand as $key => $option) {
            if (Str::startsWith($option, '--')) {
                unset($aCommand[$key]);
            }
        }
        if (!isset($aCommand[0])) {
            return redirect()->back();
        }

        $this->setCommand($command, $this->commandExists($aCommand[0]));

        return redirect()->back();
    }


    /**
     * @return \Illuminate\View\View
     */
    private function phpInfo()
    {
        return view('cmf.components.dev.phpinfo');//response()->make('', 200);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pages()
    {
        return view('cmf.components.dev.pages');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cards()
    {
        return view('cmf.components.dev.cards');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function cardsTours()
    {
        $oTours = Tour::active()->ordered()->get();
        foreach ($oTours as $oTour) {
            $url = (new Preview())->createTourCard($oTour);
            //break;
            //dd($url);
        }
        //(new Preview())->create();

        $oTour = Tour::find(46);
        return view('cmf.components.dev.cards.pdf.tour', [
            'oTour' => $oTour,
        ]);
        return view('cmf.components.dev.cards.tours');
    }

    /**
     * @return \Illuminate\View\View
     */
    public function sitemap()
    {
        //$oSitemap = (new SeoSitemapService())->getSitemap();
        //dd($oSitemap);
        dd((new VkApi())->run());

        return view('cmf.components.dev.sitemap', [
            //'oTags' => $oSitemap->getTags(),
        ]);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function queue()
    {
        $aCommands = (new QueueCommon())->commands();

        foreach ($aCommands as $key => $command) {
            $aCommands[$key] = [
                'active' => checkQueue($key),
                'command' => config('app.php_alias') . ' artisan ' . $command,
            ];
        }

        return view('cmf.components.dev.queue', [
            'aCommands' => $aCommands,
        ]);
    }


    /**
     * @param string $name
     * @return bool
     */
    private function commandExists($name)
    {
        return Arr::has(Artisan::all(), $name);
    }

    /**
     * @param string $name
     * @param bool $exists
     */
    private function setCommand($name, $exists): void
    {
        if ($exists) {
            Artisan::call($name);
            (new Toastr('Команда php artisan ' . $name . ' успешно выполнена.'))->success(false);
        } else {
            (new Toastr('Команда php artisan ' . $name . ' не найдена.'))->error(false);
        }
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function settings(Request $request)
    {
        $view = view('cmf.settings')->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Illuminate\Http\RedirectResponse
     */
    public function system()
    {
        if (!Auth::user()->hasRole(User::ROLE_SUPER_ADMIN)) {
            return redirect()->to(routeCmf('dashboard.index'));
        }


        $commandsExecute = commandsExecute();

        $oScheduleCommon = (new ScheduleCommon());
        $aSchedule = [
            'active' => $oScheduleCommon->isActive(),
            'time' => $oScheduleCommon->lastTime(),
        ];

        return view('cmf.components.dev.system', [
            'commandsExecute' => $commandsExecute,
            'aSchedule' => $aSchedule,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function mail(Request $request)
    {
        if (!Auth::user()->hasRole(User::ROLE_SUPER_ADMIN)) {
            return redirect()->to(routeCmf('dashboard.index'));
        }

        if ($request->exists('name')) {
            return view('cmf.components.dev.mail', [
                'name' => $request->get('name'),
            ]);
        }
        return view('cmf.components.dev.mail', []);
    }

    /**
     * @param Request $request
     * @param string $name
     * @return string
     * @throws \ReflectionException
     */
    public function mailName(Request $request, string $name)
    {
        if ($name === RegisteredMail::NAME) {
            event(new \App\Events\Auth\RegisteredEvent(Auth::user()));
            return (new RegisteredMail())->render();
        }
        if ($name === ResetPasswordMail::NAME) {
            return (new ResetPasswordMail())->render();
        }
        if ($name === SubscriptionOrganizationMail::NAME) {
            $oOrganization = Organization::find(5);
            $oTours = $oOrganization->tours()->take(1)->get();
            return (new SubscriptionOrganizationMail($oOrganization, $oTours))->render();
        }
        if ($name === SubscriptionDirectionMail::NAME) {
            $oDirection = Direction::find(4);
            $oTours = $oDirection->tours()->take(1)->get();
            return (new SubscriptionDirectionMail($oDirection, $oTours))->render();
        }
        return '';
    }
}
