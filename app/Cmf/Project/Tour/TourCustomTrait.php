<?php

declare(strict_types=1);

namespace App\Cmf\Project\Tour;

use App\Models\Entertainment;
use App\Models\Option;
use App\Models\Tag;
use App\Models\Tour;
use App\Services\Image\ImageType;
use App\Services\ImageService;
use App\Services\Seo\SeoSitemapService;
use App\Services\Subscription\SubscriptionService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

trait TourCustomTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionRelease(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);
        $release = (int)$request->get('release');
        if ($release === 1) {
            if ($oItem->status !== Tour::STATUS_ACTIVE) {
                return responseCommon()->error([], 'Статус тура должен быть "' . (new Tour())->statuses()[Tour::STATUS_ACTIVE] . '"');
            }

            if (is_null($oItem->release_at)) {
                $oItem->update([
                    'release_at' => now(),
                ]);
                userActionService()->tourPublished($oItem);
                (new SubscriptionService(null, $oItem))->mailing();
            }
            (new SeoSitemapService())->dispatch();
            return responseCommon()->success([], 'Тур успешно опубликован');
        } else {
            if (!is_null($oItem->release_at)) {
                $oItem->update([
                    'release_at' => null,
                ]);
                userActionService()->tourUnpublished($oItem);
            }
            (new SeoSitemapService())->dispatch();
            return responseCommon()->success([], 'Тур успешно снят с публикации');
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Exception
     */
    public function actionSaveDepartures(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        $data = $request->get('departure');
        foreach ($data as $id => $value) {
            $departure_at = $value['departure_at'];
            if (!empty($departure_at['time'])) {
                $departure_at = $departure_at['date'] . ' ' . $departure_at['time'];
                $departure_at_hourly = 1;
            } else {
                $departure_at = $departure_at['date'];
                $departure_at_hourly = 0;
            }
            if (!is_null($departure_at)) {
                $date = Carbon::parse($departure_at);

                $oItem->departures()->syncWithoutDetaching([$id => [
                    'departure_at' => $date,
                    'departure_at_hourly' => $departure_at_hourly,
                    'description' => $value['description'],
                ]]);
            } else {
                $oItem->departures()->syncWithoutDetaching([$id => [
                    'departure_at' => null,
                    'departure_at_hourly' => 0,
                    'description' => null,
                ]]);
            }
        }

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionSaveDepartures($request, $oMultiple->id);
            });
        }

        return responseCommon()->success([], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionSaveRentOutfits(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);
        $data = $request->get('outfit');
        $save = [];
        foreach ($data as $id => $value) {
            if (empty($value['rent_price']) && empty($value['rent_description'])) {
                continue;
            }
            $save[$id] = [
                'rent' => 1,
                'rent_price' => $value['rent_price'] ?? null,
                'rent_description' => $value['rent_description'] ?? null,
            ];
        }
        $oItem->outfits()->sync($save);

        if ($request->exists('options')) {
            $requestParameters = clone $request;
            $this->actionUpdateParameters($requestParameters, $oItem->id);
        }

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionSaveRentOutfits($request, $oMultiple->id);
            });
        }

        return responseCommon()->success([], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionVideoSave(Request $request, int $id): array
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);
        $video = $request->get('video');
        if ($video !== '') {
            $data = new Request();
            $data->merge([
                'options' => [
                    Option::NAME_VIDEO_YOUTUBE => $video,
                ],
            ]);
            $this->actionUpdateParameters($data, $id);
        }

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionVideoSave($request, $oMultiple->id);
            });
        }
        return responseCommon()->success([], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionDuplicateCreate(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            // когда мультитур и вводятся даты
            if ($oItem->isMulti()) {
                $data = $request->all();
                $dates = serviceTransformerTour()->mergeMultiDates($data);
                foreach ($dates as $id => $date) {
                    $oNew = $oItem->replicate();
                    $oNew->release_at = null;
                    $oNew->status = 0;
                    $oNew->save();

                    $oNew->update([
                        'start_at_split' => $date['start_at'],
                        'finish_at_split' => $date['finish_at'],
                    ]);
                    serviceTransformerTour()->duplicate($oItem, $oNew);
                    userActionService()->tourCreated($oNew);
                }
                return responseCommon()->success([], 'Новые даты успешно добавлены');
            } else {
                $multiCode = serviceTransformerTour()->getMulticode();
                $oItem->update([
                    'multi' => 1,
                    'multi_code' => $multiCode,
                    'duplicate_id' => null,
                ]);
                $oItem->refresh();
                $oNew = $oItem->replicate();
                $oNew->release_at = null;
                $oNew->status = 0;
                $oNew->duplicate_id = null;
                $oNew->save();
                serviceTransformerTour()->duplicate($oItem, $oNew);
                userActionService()->tourCreated($oNew);
                return responseCommon()->success([], 'Дубликат успешно создан');
            }
        } else {
            $oNew = $oItem->replicate();
            $oNew->release_at = null;
            $oNew->status = 0;
            $oNew->duplicate_id = $oItem->id;
            $oNew->save();
            serviceTransformerTour()->duplicate($oItem, $oNew);
            userActionService()->tourCreated($oNew);
            return responseCommon()->success([], 'Дубликат успешно создан');
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionSaveCancellation(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        $cancellation = $request->get('cancellation');

        $start_at_date = $cancellation['start_at']['date'] ?? null;
        $start_at_time = $cancellation['start_at']['time'] ?? null;
        $start_at_hourly = false;
        if (!empty($start_at_date)) {
            if (!empty($start_at_time)) {
                $start_at_hourly = true;
                $start_at_datetime = Carbon::parse($start_at_date . ' ' . $start_at_time);
            } else {
                $start_at_datetime = Carbon::parse($start_at_date);
            }
        } else {
            $start_at_datetime = null;
        }

        $finish_at_date = $cancellation['finish_at']['date'] ?? null;
        $finish_at_time = $cancellation['finish_at']['time'] ?? null;
        $finish_a_hourly = false;
        if (!empty($finish_at_date)) {
            if (!empty($finish_at_time)) {
                $finish_a_hourly = true;
                $finish_a_datetime = Carbon::parse($finish_at_date . ' ' . $finish_at_time);
            } else {
                $finish_a_datetime = Carbon::parse($finish_at_date);
            }
        } else {
            $finish_a_datetime = null;
        }

        $data = [
            'type' => $cancellation['type'],
            'description' => $cancellation['description'],
            'tooltip' => $cancellation['tooltip'],
            'start_at' => $start_at_datetime,
            'start_at_hourly' => $start_at_hourly,
            'finish_at' => $finish_a_datetime,
            'finish_at_hourly' => $finish_a_hourly,
        ];
        if (is_null($oItem->cancellation)) {
            $oItem->cancellation()->create($data);
        } else {
            $oItem->cancellation()->update($data);
        }

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionSaveCancellation($request, $oMultiple->id);
            });
        }

        $oItem = Tour::find($id);

        return responseCommon()->success([
            'view' => view('cmf.content.tour.modals.tabs.cancellation', [
                'model' => self::NAME,
                'oItem' => $oItem,
            ])->render(),
        ], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionDeleteCancellation(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);
        $oCancellation = $oItem->cancellation;

        if (!is_null($oCancellation)) {
            $oCancellation->delete();
        }

        $oItem = Tour::find($id);

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionDeleteCancellation($request, $oMultiple->id);
            });
        }

        return responseCommon()->success([
            'view' => view('cmf.content.tour.modals.tabs.cancellation', [
                'model' => self::NAME,
                'oItem' => $oItem,
            ])->render(),
        ], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionSaveAnnouncement(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        $announcement = $request->get('announcement');

        $data = [
            'type' => $announcement['type'],
            'description' => $announcement['description'],
            'tooltip' => $announcement['tooltip'],
        ];
        if (is_null($oItem->announcement)) {
            $oItem->announcement()->create($data);
        } else {
            $oItem->announcement()->update($data);
        }

        $oItem = Tour::find($id);

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionSaveAnnouncement($request, $oMultiple->id);
            });
        }

        return responseCommon()->success([
            'view' => view('cmf.content.tour.modals.tabs.announcement', [
                'model' => self::NAME,
                'oItem' => $oItem,
            ])->render(),
        ], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionDeleteAnnouncement(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);
        $oAnnouncement = $oItem->announcement;

        if (!is_null($oAnnouncement)) {
            $oAnnouncement->delete();
        }

        $oItem->refresh();

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionDeleteAnnouncement($request, $oMultiple->id);
            });
        }

        return responseCommon()->success([
            'view' => view('cmf.content.tour.modals.tabs.announcement', [
                'model' => self::NAME,
                'oItem' => $oItem,
            ])->render(),
        ], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionBannerGenerate(Request $request, int $id)
    {
        $oItem = Tour::find($id);
        $oDirection = $oItem->direction;
        if (is_null($oDirection)) {
            return responseCommon()->jsonError([], 'Направление не найдено');
        }
        $oImage = $oDirection->modelImages()->where('is_main', 1)->first();
        if (is_null($oImage)) {
            return responseCommon()->jsonError([], 'Изображение не найдено');
        }
        $file_path = $oDirection->getImageModelOriginalStorage($oImage);
        $oImageService = (new ImageService());
        $type = ImageType::BANNER;
        $options = (new \App\Cmf\Project\Tour\TourController())->image[$type];

        $finfo = new \finfo(FILEINFO_MIME_TYPE);
        $oFile = new UploadedFile($file_path, $oImage->filename, $finfo->file($file_path), filesize($file_path));

        $filename = $oImageService->upload($oItem, [$oFile], $options, $type);

        return responseCommon()->success([
            'view' => view('cmf.content.default.modals.tabs.images.banner', [
                'model' => self::NAME,
                'oItem' => $oItem,
            ])->render(),
        ], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function attractions(Request $request, int $id)
    {
        $oItem = Tour::find($id);

        $fields = $this->fields;

        return view('cmf.content.default.table.index', [
            'fields' => $fields,
            'search_fields' => [],
            'oItem' => $oItem,
            'breadcrumbPage' => [
                'name' => 'attractions',
                'title' => 'Достопримечательности',
            ],
            'oItems' => [$oItem],
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function getEntertainments(Request $request, int $id)
    {
        $oItem = Tour::find($id);
        $oEntertainments = Entertainment::active()->ordered()->get();

        return responseCommon()->success([
            'view' => view('cmf.content.tour.table.modals.entertainments', [
                'oItem' => $oItem,
                'oEntertainments' => $oEntertainments,
                'model' => self::NAME,
            ])->render(),
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function saveEntertainments(Request $request, int $id)
    {
        $oItem = Tour::find($id);
        $oEntertainments = Entertainment::active()->ordered()->get();

        $aEntertainments = [];
        $entertainments = $request->get('entertainment');
        foreach ($oEntertainments as $key => $oEntertainment) {
            if (isset($entertainments[$oEntertainment->id])) {
                $aEntertainments[] = $oEntertainment->id;
            }
        }
        $oItem->entertainments()->sync($aEntertainments);

        return responseCommon()->success([], 'Данные успешно сохранены');
    }

    /**
     * @param Tour $oTour
     * @return array
     */
    protected function metaTagsAutoGenerate(Tour $oTour)
    {
        $oDirection = $oTour->direction;
        $oDepartures = $oTour->departures;
        $oOrganization = $oTour->organization;
        $aDepartures = [];
        foreach ($oDepartures as $oDeparture) {
            $aDepartures[] = $oDeparture->title;
        }
        $departure = natural_language_join($aDepartures);

        $date = $oTour->start_at->translatedFormat('j');
        $date = $date . ' ' . serviceTransformerTour()->startAtMonth($oTour);

        $directionTitle = 'в ' . $oDirection->title;
        if (!is_null($oDirection->declensionWhere)) {
            $directionTitle = $oDirection->declensionWhere->value;
        }
        $title1 = 'Тур ' . $directionTitle . ' ' . $date;
        $title2 = $oTour->title;

        $start = $oTour->start_at->startOfDay()->format('d.m');
        $finish = $oTour->finish_at->startOfDay()->format('d.m');

        $descriptionStart = 'Тур Выходного Дня';
        if ($start !== $finish) {
            $descriptionStart = 'Тур';
        }
        $title3 = $descriptionStart . ' ' . $directionTitle . ' из ' . $departure . ' ' . $date . ' от организатора ' . $oOrganization->title;


        return [
            'title' => $title1,
            'description' => $title3,
            'keywords' => null,
        ];
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionTagsGet(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);
        $oTags = Tag::active()->ordered()->get();

        return responseCommon()->success([
            'view' => view('cmf.content.tour.table.modals.tags', [
                'oItem' => $oItem,
                'oTags' => $oTags,
                'model' => self::NAME,
            ])->render(),
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function saveTags(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);
        $tag_id = null;
        if ($request->exists('tag')) {
            $tags = $request->get('tag');
            $tags = array_keys($tags);
            $tag_id = $tags[0];
        }
        $oItem->update([
            'tag_id' => $tag_id,
        ]);
        return responseCommon()->success([], 'Данные успешно сохранены');
    }

    public function actionUserActionModal(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);
        return responseCommon()->success([
            'view' => view('cmf.content.tour.table.modals.user_actions', [
                'oItem' => $oItem,
                'model' => self::NAME,
            ])->render(),
        ]);
    }
}
