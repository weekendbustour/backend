<?php

declare(strict_types=1);

namespace App\Cmf\Project\Tour;

use App\Cmf\Core\Defaults\ExternalVkParamentableTrait;
use App\Cmf\Core\Defaults\MetaTagableTrait;
use App\Cmf\Core\Defaults\MultiTourTrait;
use App\Cmf\Core\Defaults\PriceableTrait;
use App\Cmf\Core\Defaults\SourceableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\FieldParameter;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\MainController;
use App\Cmf\Core\TabParameter;
use App\Cmf\Project\Direction\DirectionController;
use App\Cmf\Project\Organization\OrganizationController;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class TourController extends MainController
{
    use TourSettingsTrait;
    use ImageableTrait;
    use TourCustomTrait;
    use TourTimelineTrait;
    use TourParametersTrait;
    use PriceableTrait;
    use TourThisTrait;
    use TextableTrait;
    use TourQuestionTrait;
    use MetaTagableTrait;
    use ExternalVkParamentableTrait;
    use MultiTourTrait;
    use SourceableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Туры';

    /**
     * Имя сущности
     */
    const NAME = 'tour';

    /**
     * Иконка
     */
    const ICON = 'icon-graph';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Tour::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images', 'organization', 'direction', 'timelines', 'cancellation', 'announcement', 'metaTag', 'tag', 'userActionsActive'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    protected $aOrderBy = [
        'column' => 'start_at',
        'type' => 'desc',
    ];

    /**
     * @var array
     */
    public $indexComponent = [
        'release_at' => true,
        'private_show' => true,
        'state' => true,
        'user_actions' => true,
    ];

    /**
     * Лимит для пагинации
     *
     * @var int
     */
    protected $tableLimit = 40;

    /**
     * @var array
     */
    public $image = [
        ImageType::MODEL => [
            'with_main' => true,
            'filters' => [
                ImageSize::XS => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => 36,
                    ],
                ],
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => 360,
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
                ImageSize::CARD_DEFAULT => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 350,
                        'height' => 235,
                    ],
                ],
                ImageSize::CARD_MD => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 700,
                        'height' => 470,
                    ],
                ],
            ],
        ],
        ImageType::BANNER => [
            'with_main' => true,
            'unique' => true,
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => 360,
                    ],
                ],
                ImageSize::BANNER_XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ],
        ImageType::GALLERY => [
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ],
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png,webp'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'scrolling' => true,
        'edit' => [
            TabParameter::TAB_MAIN => TabParameter::TAB_MAIN_CONTENT,
            TabParameter::TAB_DEPARTURES => TabParameter::TAB_DEPARTURES_CONTENT,
            TabParameter::TAB_MARKDOWN => TabParameter::TAB_MARKDOWN_CONTENT,
            TabParameter::TAB_PARAMETERS => TabParameter::TAB_PARAMETERS_CONTENT,
            TabParameter::TAB_INCLUDES => TabParameter::TAB_INCLUDES_CONTENT,
            TabParameter::TAB_PRICES => TabParameter::TAB_PRICES_CONTENT,
            TabParameter::TAB_IMAGES_MODEL => TabParameter::TAB_IMAGES_MODEL_CONTENT,
            TabParameter::TAB_IMAGES_BANNER => TabParameter::TAB_IMAGES_BANNER_CONTENT,
            //TabParameter::TAB_IMAGES_GALLERY => TabParameter::TAB_IMAGES_GALLERY_CONTENT,
            TabParameter::TAB_META_TAGS => TabParameter::TAB_META_TAGS_CONTENT,
            TabParameter::TAB_VIDEO => TabParameter::TAB_VIDEO_CONTENT,
            TabParameter::TAB_RESERVATION => TabParameter::TAB_RESERVATION_CONTENT,
            TabParameter::TAB_SOURCE => TabParameter::TAB_SOURCE_CONTENT,
            TabParameter::TAB_CANCELLATION => TabParameter::TAB_CANCELLATION_CONTENT,
            TabParameter::TAB_ANNOUNCEMENT => TabParameter::TAB_ANNOUNCEMENT_CONTENT,
            TabParameter::TAB_QUESTIONS => TabParameter::TAB_QUESTIONS_CONTENT,
            TabParameter::TAB_OUTFITS_RENT => TabParameter::TAB_OUTFITS_RENT_CONTENT,
            TabParameter::TAB_DUPLICATE => TabParameter::TAB_DUPLICATE_CONTENT,
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'departures' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Откуда',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO_MANY,
            FieldParameter::VALUES => \App\Models\Departure::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::EMPTY => true,
            FieldParameter::MULTIPLE => true,
            FieldParameter::REQUIRED => false,

            FieldParameter::GROUP_NAME => 'departure_direction_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => false,
        ],
        'direction' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Направление',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Direction::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => true,
            FieldParameter::IN_TABLE => 2,
            FieldParameter::TABLE_TITLE => '<i class="' . DirectionController::ICON . '"></i>',
            FieldParameter::EMPTY => true,
            FieldParameter::SEARCH => 1,

            FieldParameter::GROUP_NAME => 'departure_direction_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => true,
        ],
        'organization' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Организатор',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Organization::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => true,
            FieldParameter::COLORED => true,
            FieldParameter::IN_TABLE => 1,
            FieldParameter::TABLE_TITLE => '<i class="' . OrganizationController::ICON . '"></i>',
            FieldParameter::EMPTY => true,
            FieldParameter::SEARCH => 2,

            FieldParameter::GROUP_NAME => 'organization_categories_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => false,
        ],
        'categories' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Категория',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO_MANY,
            FieldParameter::VALUES => \App\Models\Category::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::WHERE_IN => [
                FieldParameter::WHERE_IN_COLUMN => 'type',
                FieldParameter::WHERE_IN_VALUE => [
                    \App\Models\Category::TYPE_TOURS,
                ],
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => false,
            FieldParameter::EMPTY => true,
            FieldParameter::MULTIPLE => true,

            FieldParameter::GROUP_NAME => 'organization_categories_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => true,
        ],
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 3,
            FieldParameter::DELETE_TITLE => 'направление',
            FieldParameter::REQUIRED => true,
            'bold' => true,
        ],
        'start_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата и время начала',
            FieldParameter::TITLE_TABLE => 'Начало',
            FieldParameter::SPLIT => true,
            FieldParameter::REQUIRED => true,
            FieldParameter::DATETIME => true,
            FieldParameter::IN_TABLE => 4,
            FieldParameter::GROUP_NAME => 'start_finish_term',
            FieldParameter::GROUP_COL => 6,
            'format' => 'd.m.Y',
        ],
        'finish_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата и время окончания',
            FieldParameter::TITLE_TABLE => 'Конец',
            FieldParameter::SPLIT => true,
            FieldParameter::REQUIRED => true,
            FieldParameter::DATETIME => true,
            FieldParameter::IN_TABLE => 5,
            FieldParameter::GROUP_NAME => 'start_finish_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => true,
            'format' => 'd.m.Y',
        ],
        'multi' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Мультитур',
            FieldParameter::TITLE_FORM => 'Мультитур',
            FieldParameter::TOOLTIP => 'Если Активно, то все изменения будут распространяться и на другие связанные туры',
            FieldParameter::DEFAULT => false,
        ],
        'preview_description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Краткое описание',
            FieldParameter::LIMIT => 240,
        ],
//        'description' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
//            FieldParameter::TITLE => 'Описание',
//        ],
//        'price' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
//            FieldParameter::TITLE => 'Цена',
//            FieldParameter::LENGTH => 6,
//            FieldParameter::GROUP_NAME => 'price_term',
//            FieldParameter::GROUP_COL => 6,
//        ],
//        'price_discount' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
//            FieldParameter::TITLE => 'Цена со скидкой',
//            FieldParameter::LENGTH => 6,
//            FieldParameter::TOOLTIP => 'Старая цена будет перечеркнута',
//            FieldParameter::GROUP_NAME => 'price_term',
//            FieldParameter::GROUP_COL => 6,
//            FieldParameter::GROUP_HIDE => true,
//        ],
        'tag' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тэг',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Tag::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::TOOLTIP => 'Разрешается выбрать только один Тэг, он должен вкратце характеризировать тур. Тэг можно менять.',
            FieldParameter::ALIAS => 'title',
            FieldParameter::EMPTY => true,
            FieldParameter::SEARCH => 3,
        ],
        'attractions' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Достопримечательности',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO_MANY,
            FieldParameter::VALUES => \App\Models\Attraction::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => false,
            FieldParameter::EMPTY => false,
            FieldParameter::MULTIPLE => true,
        ],
//        'include' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
//            FieldParameter::TITLE => 'Что входит',
//            FieldParameter::PLACEHOLDER => '- одно &#10;- второе',
//            FieldParameter::GROUP_NAME => 'includes',
//            FieldParameter::GROUP_COL => 6,
//        ],
//        'not_include' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
//            FieldParameter::TITLE => 'Что не входит',
//            FieldParameter::PLACEHOLDER => '- одно &#10;- второе',
//            FieldParameter::GROUP_NAME => 'includes',
//            FieldParameter::GROUP_COL => 6,
//            FieldParameter::GROUP_HIDE => true,
//        ],
//        'places' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
//            FieldParameter::TITLE => 'Количество мест',
//            FieldParameter::LENGTH => 2,
//            FieldParameter::PLACEHOLDER => 'xx',
//            FieldParameter::GROUP_TITLE => 'Характеристики',
//            FieldParameter::GROUP_NAME => 'days_places',
//            FieldParameter::GROUP_COL => 6,
//        ],
//        'days' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
//            FieldParameter::TITLE => 'Количество дней',
//            FieldParameter::PLACEHOLDER => 'xx',
//            FieldParameter::LENGTH => 2,
//            FieldParameter::GROUP_NAME => 'days_places',
//            FieldParameter::GROUP_COL => 6,
//            FieldParameter::GROUP_HIDE => true,
//        ],
        'published_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата публикации',
            FieldParameter::REQUIRED => true,
        ],
//        'entertainments' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_CUSTOM,
//            FieldParameter::TITLE => 'Развлечения',
//            FieldParameter::IN_TABLE => 5,
//            FieldParameter::HIDDEN => true,
//        ],
//        'tags' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_CUSTOM,
//            FieldParameter::TITLE => 'Тэги',
//            FieldParameter::IN_TABLE => 5,
//            FieldParameter::HIDDEN => true,
//        ],
        'timeline' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CUSTOM,
            FieldParameter::TITLE => 'Расписание',
            FieldParameter::IN_TABLE => 6,
            FieldParameter::HIDDEN => true,
            FieldParameter::TABLE_TITLE => '<i class="fa fa-clock-o"></i>',
        ],
        'vk' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CUSTOM,
            FieldParameter::TITLE => 'ВК параметр',
            FieldParameter::IN_TABLE => 6,
            FieldParameter::HIDDEN => true,
            FieldParameter::TABLE_TITLE => '<i class="fa fa-vk"></i>',
        ],
        'tags' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CUSTOM,
            FieldParameter::TITLE => 'Тэги',
            FieldParameter::IN_TABLE => 7,
            FieldParameter::HIDDEN => true,
            FieldParameter::TABLE_TITLE => '<i class="fa fa-tags"></i>',
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 7,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
