<?php

declare(strict_types=1);

namespace App\Cmf\Project\Tour;

trait TourSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => TourController::NAME,
        'title' => TourController::TITLE,
        'description' => null,
        'icon' => TourController::ICON,
    ];
}
