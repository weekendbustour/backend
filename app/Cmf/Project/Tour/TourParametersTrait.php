<?php

declare(strict_types=1);

namespace App\Cmf\Project\Tour;

use App\Cmf\Core\MainController;
use App\Models\Option;
use App\Models\Tour;
use App\Models\TourOptionValues;
use Illuminate\Http\Request;

trait TourParametersTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionUpdateParameters(Request $request, int $id)
    {
        /** @var Tour $oTour */
        $oTour = Tour::find($id);
        if ($request->exists('options')) {
            $aOptions = $request->get('options');
            foreach ($aOptions as $key => $value) {
                $oOption = Option::where('name', $key)->where('purpose', Option::PURPOSE_TOURS)->first();
                if (!is_null($oOption)) {
                    $oValue = TourOptionValues::where('tour_id', $oTour->id)
                        ->where('option_id', $oOption->id)
                        ->first();
                    if (isset($value)) {
                        if ($oOption->type === MainController::DATA_TYPE_CHECKBOX && (int)$value === 0 && in_array($key, [
                                Option::NAME_PHOTO_SESSION,
                            ])) {
                            if (!is_null($oValue)) {
                                $oValue->delete();
                            }
                            continue;
                        }
                        $this->parameterSaveOptionValue($oTour, $oOption, $value, $oValue);
                    } else {
                        if (!is_null($oValue)) {
                            $oValue->delete();
                        }
                    }
                }
            }
        }

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oTour, function ($request, $oMultiple) {
                $this->actionUpdateParameters($request, $oMultiple->id);
            });
        }
        return responseCommon()->success([], 'Данные успешно обновлены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionParameterReservationAutoGenerate(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = $this->findByClass($this->class, $id);
        $oOption = Option::where('name', Option::NAME_RESERVATION)->where('purpose', Option::PURPOSE_TOURS)->first();
        $oValue = TourOptionValues::where('tour_id', $oItem->id)
            ->where('option_id', $oOption->id)
            ->first();

        $reservation = 'asd';
        $this->parameterSaveOptionValue($oItem, $oOption, $reservation, $oValue);
        $oItem->refresh();

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionParameterReservationAutoGenerate($request, $oMultiple->id);
            });
        }

        ['oValues' => $oValues, 'oReservationOptions' => $oReservationOptions] = $this->thisEditDataModal($oItem);

        return responseCommon()->success([
            'view' => view('cmf.content.tour.modals.tabs.reservation', [
                'oItem' => $oItem,
                'model' => self::NAME,
                'oValues' => $oValues,
                'oReservationOptions' => $oReservationOptions,
            ])->render(),
        ], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionParameterReservationAutoGenerateOrganization(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = $this->findByClass($this->class, $id);
        $oOption = Option::where('name', Option::NAME_RESERVATION)->where('purpose', Option::PURPOSE_TOURS)->first();
        $oValue = TourOptionValues::where('tour_id', $oItem->id)
            ->where('option_id', $oOption->id)
            ->first();

        $reservation = $oItem->organization->parameter_reservation;

        $this->parameterSaveOptionValue($oItem, $oOption, $reservation, $oValue);
        $oItem->refresh();

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionParameterReservationAutoGenerateOrganization($request, $oMultiple->id);
            });
        }

        ['oValues' => $oValues, 'oReservationOptions' => $oReservationOptions] = $this->thisEditDataModal($oItem);
        return responseCommon()->success([
            'view' => view('cmf.content.tour.modals.tabs.reservation', [
                'oItem' => $oItem,
                'model' => self::NAME,
                'oValues' => $oValues,
                'oReservationOptions' => $oReservationOptions,
            ])->render(),
        ], 'Данные успешно сохранены');
    }

    /**
     * @param $oItem
     * @param Option $oOption
     * @param string $value
     * @param TourOptionValues $oValue
     * @return mixed
     */
    private function parameterSaveOptionValue($oItem, Option $oOption, string $value, ?TourOptionValues $oValue)
    {
        if (!is_null($oValue)) {
            $oValue->update([
                'value' => $value,
            ]);
        } else {
            $oValue = TourOptionValues::create([
                'tour_id' => $oItem->id,
                'option_id' => $oOption->id,
                'value' => $value,
            ]);
        }
        return $oValue;
    }
}
