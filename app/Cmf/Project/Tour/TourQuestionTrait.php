<?php

declare(strict_types=1);

namespace App\Cmf\Project\Tour;

use App\Models\Image;
use App\Models\Option;
use App\Models\Question;
use App\Models\Tour;
use App\Models\TourTimeline;
use App\Services\Image\ImageType;
use App\Services\ImageService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait TourQuestionTrait
{

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionQuestionSave(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        $aQuestions = $request->get('question');
        foreach ($aQuestions as $id => $aQuestion) {
            if (!empty($aQuestion['question']) && Str::startsWith($id, 'tmp-')) {
                $oItem->questions()->create([
                    'question' => $aQuestion['question'],
                    'answer' => $aQuestion['answer'],
                    'priority' => $aQuestion['priority'],
                ]);
            } else {
                $oQuestion = Question::find($id);
                if (!is_null($oQuestion)) {
                    $oQuestion->update([
                        'question' => $aQuestion['question'],
                        'answer' => $aQuestion['answer'],
                        'priority' => $aQuestion['priority'],
                    ]);
                }
            }
        }

        $oItem->refresh();
        return responseCommon()->success([
            'view' => view('cmf.content.default.modals.tabs.questions', [
                'model' => self::NAME,
                'oItem' => $oItem,
            ])->render(),
        ], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionQuestionDelete(Request $request, int $id)
    {
        /** @var Question $oQuestion */
        $oQuestion = Question::find($id);
        $oItem = $oQuestion->questionable;

        $oQuestion->delete();

        $oItem->refresh();
        return responseCommon()->success([
            'view' => view('cmf.content.default.modals.tabs.questions', [
                'model' => self::NAME,
                'oItem' => $oItem,
            ])->render(),
        ]);
    }
}
