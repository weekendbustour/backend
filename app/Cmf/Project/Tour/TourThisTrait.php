<?php

declare(strict_types=1);

namespace App\Cmf\Project\Tour;

use App\Models\Option;
use App\Models\Outfit;
use App\Models\Tour;
use App\Models\TourOptionValues;
use App\Services\Image\ImageType;
use App\Services\Image\Upload\ImageUploadBannerService;
use App\Services\Image\Upload\ImageUploadGalleryService;
use App\Services\Image\Upload\ImageUploadModelService;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

trait TourThisTrait
{
    /**
     * @param Tour $oItem
     * @return array
     */
    public function thisEditDataModal(Tour $oItem)
    {
        // вкладка Параметры
        $oAllOptions = Option::active()->purposeTours()->ordered()->get();
        $oAllOptions = $oAllOptions->reject(function ($item) {
            return in_array($item->name, [
                Option::NAME_VIDEO_YOUTUBE,
                Option::NAME_TIMELINE_NOTE,
            ]);
        });
        $oOptions = $oAllOptions->whereNotIn('name', [
            \App\Models\Option::NAME_INCLUDE,
            \App\Models\Option::NAME_NOT_INCLUDE,
            \App\Models\Option::NAME_RESERVATION,
            \App\Models\Option::NAME_MORE_INCLUDE,
            \App\Models\Option::NAME_PREPAYMENT_CANCELLATION,
        ]);
        $oIncludesOptions = $oAllOptions->whereIn('name', [
            \App\Models\Option::NAME_INCLUDE,
            \App\Models\Option::NAME_NOT_INCLUDE,
            \App\Models\Option::NAME_MORE_INCLUDE,
        ]);
        $oReservationOptions = $oAllOptions->whereIn('name', [
            \App\Models\Option::NAME_RESERVATION,
        ]);

        $oPriceOptions = $oAllOptions->whereIn('name', [
            \App\Models\Option::NAME_PREPAYMENT_CANCELLATION,
        ]);
        $oOutfitRentOptions = $oAllOptions->whereIn('name', [
            \App\Models\Option::NAME_OUTFIT_RENT_NOTE,
        ]);

        // вкладка Цены
        $oPrice = $oItem->price;

        //$title = $oItem->title . ' - ' . serviceTransformerTour()->startAtDays($oItem) . ' ' . serviceTransformerTour()->startAtMonth($oItem);

        $oOutfits = Outfit::active()->ordered()->get();
        $aTourOutfits = $oItem->outfits->keyBy('id')->toArray();

        return [
            'oValues' => $oItem->values->keyBy('option.name'),
            'oOptions' => $oOptions,
            'oIncludesOptions' => $oIncludesOptions,
            'oReservationOptions' => $oReservationOptions,
            'oPrice' => $oPrice,
            'oPriceOptions' => $oPriceOptions,
            'oOutfits' => $oOutfits,
            'aTourOutfits' => $aTourOutfits,
            'oOutfitRentOptions' => $oOutfitRentOptions,
        ];
    }

    /**
     * @param Tour $oItem
     * @throws \Exception
     */
    public function thisDestroy(Tour $oItem): void
    {
        $oImages = $oItem->images;
        foreach ($oImages as $oImage) {
            $oService = null;
            switch ($oImage->type) {
                case ImageType::MODEL:
                    $oService = new ImageUploadModelService();
                    break;
                case ImageType::GALLERY:
                    $oService = new ImageUploadGalleryService();
                    break;
                case ImageType::BANNER:
                    $oService = new ImageUploadBannerService();
                    break;
            }
            $result = $oService->delete($oItem, $oImage, $this->image[$oImage->type]);
        }
        $oItem->delete();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function thisCreate(Request $request)
    {
        $data = $request->all();
        $multi = $request->get('multi');
        $oModel = null;
        if ($multi) {
            $multiCode = Str::random(10);
            $request->merge([
                'multi_code' => $multiCode,
            ]);
            // сохранение по родному реквесту
            $oModel = $this->storeModel($request);
            $dates = serviceTransformerTour()->mergeMultiDates($data);
            foreach ($dates as $id => $date) {
                $request->merge([
                    'start_at_split' => $date['start_at'],
                    'finish_at_split' => $date['finish_at'],
                ]);
                // сохранение по измененному реквесту
                $oModel = $this->storeModel($request);
                userActionService()->tourCreated($oModel);
            }
        } else {
            $oModel = $this->storeModel($request);
            userActionService()->tourCreated($oModel);
        }
        return $oModel;
    }

    /**
     * @param Request $request
     * @param Tour $oItem
     * @return array
     */
    public function thisUpdate(Request $request, Tour $oItem)
    {
        $data = $request->all();
        if ($this->isMultiSave($request)) {
            // обновление с которого просмотр
            $oItem->update($request->all());
            $this->saveRelationships($oItem, $request);
            userActionService()->tourUpdated($oItem);

            $dates = serviceTransformerTour()->mergeMultiDates($data);
            foreach ($dates as $id => $date) {
                $oMulti = Tour::find($id);
                if (!is_null($oMulti)) {
                    $request->merge([
                        'start_at_split' => $date['start_at'],
                        'finish_at_split' => $date['finish_at'],
                    ]);
                    // обновление остальных по измененному реквесту
                    $oMulti->update($request->all());
                    $this->saveRelationships($oMulti, $request);
                    userActionService()->tourUpdated($oMulti);
                }
            }
        } else {
            $oItem->update($request->all());
            $this->saveRelationships($oItem, $request);
            userActionService()->tourUpdated($oItem);
        }
        return responseCommon()->success();
    }

    /**
     * @param Tour|null $oItem
     */
    public function thisPrepareFieldsValues(?Tour $oItem)
    {
        if (!is_null($oItem) && $oItem->isMulti()) {
            $this->tabs['edit']['tabs.duplicate']['tabs_attributes']['data-hidden-submit'] = 0;
        }
    }
}
