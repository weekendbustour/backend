<?php

declare(strict_types=1);

namespace App\Cmf\Project\Category;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\FieldParameter;
use App\Cmf\Core\MainController;
use App\Models\Category;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CategoryController extends MainController
{
    use CategorySettingsTrait;
    use ImageableTrait;
    use CategoryCustomTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Категории';

    /**
     * Имя сущности
     */
    const NAME = 'category';

    /**
     * Иконка
     */
    const ICON = 'icon-list';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Category::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png'],
        ],
    ];

    /**
     * @var array
     */
    public $image = [
        ImageType::MODEL => [
            'with_main' => true,
            'unique' => false,
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
                ImageSize::CARD_DEFAULT => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 350,
                        'height' => 445,
                    ],
                ],
                ImageSize::CARD_MD => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 700,
                        'height' => 890,
                    ],
                ],
            ],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
            'tabs.images.model' => [
                'title' => 'Изображение',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'type' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тип',
            FieldParameter::REQUIRED => true,
            FieldParameter::VALUES => [
                Category::TYPE_DEFAULT => 'По умолчанию',
                Category::TYPE_TOURS => 'Туры',
                Category::TYPE_ORGANIZATIONS => 'Организации',
                Category::TYPE_NEWS => 'Новости',
                Category::TYPE_ARTICLES => 'Статьи',
            ],
            FieldParameter::IN_TABLE => 1,
        ],
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::DELETE_TITLE => 'направление',
            FieldParameter::REQUIRED => true,
        ],
        'description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_MARKDOWN,
            FieldParameter::TITLE => 'Описание',
        ],
        'parent' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Родительская категория',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Category::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::EMPTY => true,
        ],
        'color' => [
            FieldParameter::TYPE => parent::DATA_TYPE_COLOR,
            FieldParameter::TITLE => 'Цвет',
            FieldParameter::IN_TABLE => 2,
        ],
        'priority' => [
            FieldParameter::TYPE => parent::DATA_TYPE_NUMBER,
            FieldParameter::TITLE => 'Приоритет',
            FieldParameter::DEFAULT => 0,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
