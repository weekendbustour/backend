<?php

declare(strict_types=1);

namespace App\Cmf\Project\Category;

trait CategorySettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => CategoryController::NAME,
        'title' => CategoryController::TITLE,
        'description' => null,
        'icon' => CategoryController::ICON,
    ];
}
