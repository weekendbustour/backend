<?php

declare(strict_types=1);

namespace App\Cmf\Project\Tag;

trait TagSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => TagController::NAME,
        'title' => TagController::TITLE,
        'description' => null,
        'icon' => TagController::ICON,
    ];
}
