<?php

declare(strict_types=1);

namespace App\Cmf\Project\Direction;

use App\Cmf\Core\Defaults\GeoTrait;
use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\MetaTagableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\Defaults\TextableUploadTrait;
use App\Cmf\Core\Defaults\WordDeclensionableTrait;
use App\Cmf\Core\MainController;
use App\Cmf\Core\TabParameter;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Cmf\Core\FieldParameter;

class DirectionController extends MainController
{
    use DirectionSettingsTrait;
    use ImageableTrait;
    use DirectionCustomTrait;
    use GeoTrait;
    use TextableTrait;
    use TextableUploadTrait;
    use MetaTagableTrait;
    use WordDeclensionableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Направления';

    /**
     * Имя сущности
     */
    const NAME = 'direction';

    /**
     * Иконка
     */
    const ICON = 'icon-directions';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Direction::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images', 'comments', 'metaTag', 'toursActive', 'tours', 'location'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    public $indexComponent = [
        'image' => false,
        'comments' => true,
        'private_show' => true,
        'state' => true,
    ];

    /**
     * Лимит для пагинации
     *
     * @var int
     */
    protected $tableLimit = 40;

    /**
     * @var array
     */
    protected $aOrderBy = [
        'column' => 'title',
        'type' => 'asc',
    ];

    /**
     * @var array
     */
    public $image = [
        ImageType::MODEL => [
            'with_main' => true,
            'unique' => false,
            'filters' => [
                ImageSize::XS => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => 36,
                    ],
                ],
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
                ImageSize::CARD_DEFAULT => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 540,
                        'height' => 220,
                    ],
                ],
                ImageSize::CARD_MD => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'width' => 1080,
                        'height' => 440,
                    ],
                ],
            ],
        ],
//        ImageType::BANNER => [
//            'with_main' => true,
//            'unique' => true,
//            'filters' => [
//                ImageSize::SQUARE => [
//                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
//                    'options' => [
//                        'dimension' => '1:1',
//                        'size' => 360,
//                    ],
//                ],
//                ImageSize::XL => [
//                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
//                    'options' => [
//                        'size' => 1280,
//                    ],
//                ],
//            ],
//        ],
        ImageType::GALLERY => [
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ]
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'scrolling' => true,
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
            'tabs.markdown' => [
                'title' => 'Полное описание',
            ],
            'tabs.images.model' => [
                'title' => 'Изображение',
            ],
//            'tabs.images.banner' => [
//                'title' => 'Баннер',
//            ],
            'tabs.images.gallery' => [
                'title' => 'Галлерея',
            ],
            TabParameter::TAB_META_TAGS => TabParameter::TAB_META_TAGS_CONTENT,
            TabParameter::TAB_WORD_DECLENSIONS => TabParameter::TAB_WORD_DECLENSIONS_CONTENT,
            'tabs.geo.coordinates' => [
                'title' => 'Координаты',
            ],
            'tabs.comments' => [
                'title' => 'Комментарии',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 1,
            FieldParameter::DELETE_TITLE => 'направление',
            FieldParameter::REQUIRED => true,
        ],
        'preview_description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXTAREA,
            FieldParameter::TITLE => 'Краткое описание',
        ],
//        'description' => [
//            FieldParameter::TYPE => parent::DATA_TYPE_MARKDOWN,
//            FieldParameter::TITLE => 'Полное описание',
//        ],
        'tours' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CUSTOM,
            FieldParameter::TITLE => 'Туры',
            FieldParameter::IN_TABLE => 2,
            FieldParameter::HIDDEN => true,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 3,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
