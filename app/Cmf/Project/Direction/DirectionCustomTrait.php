<?php

declare(strict_types=1);

namespace App\Cmf\Project\Direction;

use App\Models\Direction;
use Illuminate\Http\Request;

trait DirectionCustomTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionCommentsGetModal(Request $request, int $id)
    {
        $oItem = Direction::find($id);

        $tabs = collect($this->tabs['edit'])->only('tabs.comments')->toArray();

        $view = view('cmf.content.default.modals.container.edit', [
            'oItem' => $oItem,
            'tabs' => $tabs,
            'model' => self::NAME,
        ])->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    /**
     * @param Direction $oDirection
     * @return array
     */
    protected function metaTagsAutoGenerate(Direction $oDirection)
    {
        $title = $oDirection->title;
        $description = $oDirection->preview_description;

        return [
            'title' => $title,
            'description' => $description,
            'keywords' => null,
        ];
    }
}
