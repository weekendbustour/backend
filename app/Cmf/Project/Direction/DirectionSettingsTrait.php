<?php

declare(strict_types=1);

namespace App\Cmf\Project\Direction;

trait DirectionSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => DirectionController::NAME,
        'title' => DirectionController::TITLE,
        'description' => null,
        'icon' => DirectionController::ICON,
    ];
}
