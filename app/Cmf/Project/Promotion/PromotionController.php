<?php

declare(strict_types=1);

namespace App\Cmf\Project\Promotion;

use App\Cmf\Core\Defaults\ImageableTrait;
use App\Cmf\Core\Defaults\SocialableTrait;
use App\Cmf\Core\Defaults\TextableTrait;
use App\Cmf\Core\MainController;
use App\Cmf\Project\Organization\OrganizationController;
use App\Models\Promotion;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Cmf\Core\FieldParameter;

class PromotionController extends MainController
{
    use PromotionSettingsTrait;
    use ImageableTrait;
    use PromotionCustomTrait;
    use SocialableTrait;
    use TextableTrait;

    /**
     * Заголовок сущности
     */
    const TITLE = 'Реклама';

    /**
     * Имя сущности
     */
    const NAME = 'promotion';

    /**
     * Иконка
     */
    const ICON = 'icon-bulb';

    /**
     * Модель сущности
     */
    public $class = \App\Models\Promotion::class;

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    public $with = ['images'];

    /**
     * Validation name return
     * @var array
     */
    public $attributes = [
        'name' => 'имя',
        'email' => 'email',
        'password' => 'пароль',
        'image' => 'Изображение',
    ];

    /**
     * @var array
     */
    public $indexComponent = [
        'image' => true,
    ];

    /**
     * @var array
     */
    public $image = [
        ImageType::MODEL => [
            'with_main' => true,
            'unique' => false,
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ],
        ImageType::GALLERY => [
            'filters' => [
                ImageSize::SQUARE => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'dimension' => '1:1',
                        'size' => '360',
                    ],
                ],
                ImageSize::XL => [
                    'filter' => \App\Services\Image\Filters\SquareFilter::class,
                    'options' => [
                        'size' => 1280,
                    ],
                ],
            ],
        ],
    ];

    /**
     * @param object|null $model
     * @return array
     */
    public function rules($model = null)
    {
        return $this->rules;
    }

    /**
     * Validation rules
     * @var array
     */
    public $rules = [
        'store' => [
            //'name' => [FieldParameter::REQUIRED, 'max:255'],
            //'password' => ['required', 'confirmed', 'max:255'],
        ],
        'update' => [
            //'name' => ['required', 'max:255'],
            //'password' => ['confirmed', 'max:255'],
        ],
        'upload' => [
            'id' => ['required', 'max:255'],
            'images' => ['required', 'max:5000', 'mimes:jpg,jpeg,gif,png'],
        ],
    ];

    /**
     * @var array
     */
    public $tabs = [
        'edit' => [
            'tabs.main' => [
                'title' => 'Данные',
            ],
            'tabs.images.model' => [
                'title' => 'Изображение',
            ],
            'tabs.images.gallery' => [
                'title' => 'Галлерея',
            ],
        ],
    ];

    /**
     * @var array
     */
    public $fields = [
        'organization' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Организация',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Organization::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => false,
            FieldParameter::EMPTY => true,
            FieldParameter::IN_TABLE => 1,
            FieldParameter::TABLE_TITLE => '<i class="' . OrganizationController::ICON . '"></i>',
        ],
        'tour' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тур',
            FieldParameter::RELATIONSHIP => parent::RELATIONSHIP_BELONGS_TO,
            FieldParameter::VALUES => \App\Models\Tour::class,
            FieldParameter::ORDER => [
                FieldParameter::ORDER_METHOD => 'orderBy',
                FieldParameter::ORDER_BY => 'title',
            ],
            FieldParameter::ALIAS => 'title',
            FieldParameter::REQUIRED => false,
            FieldParameter::EMPTY => true,
            FieldParameter::IN_TABLE => 2,
        ],
        'type' => [
            FieldParameter::TYPE => parent::DATA_TYPE_SELECT,
            FieldParameter::TITLE => 'Тип',
            FieldParameter::REQUIRED => true,
            FieldParameter::VALUES => [
                Promotion::TYPE_DEFAULT => 'Реклама по умолчанию',
                Promotion::TYPE_TARGET => 'Заказная реклама',
            ],
            FieldParameter::IN_TABLE => 3,
        ],
        'title' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Наименование',
            FieldParameter::IN_TABLE => 4,
            FieldParameter::DELETE_TITLE => 'рекламу',
            FieldParameter::REQUIRED => true,
        ],
        'description' => [
            FieldParameter::TYPE => parent::DATA_TYPE_MARKDOWN,
            FieldParameter::TITLE => 'Описание',
        ],
        'url' => [
            FieldParameter::TYPE => parent::DATA_TYPE_TEXT,
            FieldParameter::TITLE => 'Ссылка',
            FieldParameter::REQUIRED => true,
            FieldParameter::IN_TABLE => 5,
        ],
        'start_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата и время начала',
            FieldParameter::TITLE_TABLE => 'Начало',
            FieldParameter::SPLIT => true,
            FieldParameter::REQUIRED => true,
            FieldParameter::DATETIME => true,
            FieldParameter::IN_TABLE => 6,
            FieldParameter::GROUP_NAME => 'start_finish_term',
            FieldParameter::GROUP_COL => 6,
        ],
        'finish_at' => [
            FieldParameter::TYPE => parent::DATA_TYPE_DATE,
            FieldParameter::TITLE => 'Дата и время окончания',
            FieldParameter::TITLE_TABLE => 'Конец',
            FieldParameter::SPLIT => true,
            //FieldParameter::REQUIRED => true,
            FieldParameter::DATETIME => true,
            FieldParameter::IN_TABLE => 7,
            FieldParameter::GROUP_NAME => 'start_finish_term',
            FieldParameter::GROUP_COL => 6,
            FieldParameter::GROUP_HIDE => true,
        ],
        'status' => [
            FieldParameter::TYPE => parent::DATA_TYPE_CHECKBOX,
            FieldParameter::TITLE => 'Статус',
            FieldParameter::TITLE_FORM => 'Активно',
            FieldParameter::IN_TABLE => 8,
            FieldParameter::DEFAULT => true,
        ],
    ];
}
