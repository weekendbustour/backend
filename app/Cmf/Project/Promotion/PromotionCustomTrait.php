<?php

declare(strict_types=1);

namespace App\Cmf\Project\Promotion;

use App\Models\Promotion;
use Illuminate\Http\Request;

trait PromotionCustomTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionCommentsGetModal(Request $request, int $id)
    {
        $oItem = Promotion::find($id);

        $tabs = collect($this->tabs['edit'])->only('tabs.comments')->toArray();

        $view = view('cmf.content.default.modals.container.edit', [
            'oItem' => $oItem,
            'tabs' => $tabs,
            'model' => self::NAME,
        ])->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }
}
