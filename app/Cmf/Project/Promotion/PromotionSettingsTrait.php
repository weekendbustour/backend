<?php

declare(strict_types=1);

namespace App\Cmf\Project\Promotion;

trait PromotionSettingsTrait
{
    /**
     * Visible sidebar menu
     *
     * @var array
     */
    public $menu = [
        'name' => PromotionController::NAME,
        'title' => PromotionController::TITLE,
        'description' => null,
        'icon' => PromotionController::ICON,
    ];
}
