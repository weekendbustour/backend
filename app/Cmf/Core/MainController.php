<?php

declare(strict_types=1);

namespace App\Cmf\Core;

use App\Http\Controllers\Controller;
use App\Models\Image;
use App\Models\Tour;
use App\Services\Image\ImageSize;
use App\Services\Image\ImageType;
use App\Services\ImageService;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;

class MainController extends Controller
{
    use SettingsTrait;
    use ControllerCrmTrait;
    use CmfFieldParameters;

    /**
     * Имя сущности
     */
    const NAME = 'default';

    const RELATIONSHIP_BELONGS_TO_MANY = 1;
    const RELATIONSHIP_BELONGS_TO = 2;
    const RELATIONSHIP_HAS_ONE = 3;
    const RELATIONSHIP_HAS_MANY = 4;

    const DATA_TYPE_TEXT = 1;
    const DATA_TYPE_SELECT = 2;
    const DATA_TYPE_CHECKBOX = 3;
    const DATA_TYPE_DATE = 4;
    const DATA_TYPE_TEXTAREA = 5;
    const DATA_TYPE_NUMBER = 6;
    const DATA_TYPE_FILE = 7;
    const DATA_TYPE_CUSTOM = 8;
    const DATA_TYPE_IMG = 9;
    const DATA_TYPE_JSON = 10;
    const DATA_TYPE_RADIO = 11;
    const DATA_TYPE_COLOR = 12;
    const DATA_TYPE_MARKDOWN = 13;
    const DATA_TYPE_MARKDOWN_IMAGE = 14;

    /**
     * @var string
     */
    protected $theme = 'cmf';

    /**
     * @var array
     */
    public $fields = [];

    /**
     * @var string
     */
    public $class = '';

    /**
     * @var string
     */
    public $session = '';

    /**
     * Реляции по умолчанию
     *
     * @var array
     */
    protected $with = [];

    /**
     * @var string
     */
    public $view = '';

    /**
     * @var array
     */
    public $attributes = [];

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [];

    /**
     * @var array
     */
    public $cache = [];

    /**
     * @var array
     */
    public $tabs = [];

    /**
     * @var array
     */
    public $tabsDefault = [
        'edit' => [
            TabParameter::TAB_MAIN => TabParameter::TAB_MAIN_CONTENT,
            'tabs.image' => [
                'title' => 'Галерея',
                'tabs_attributes' => [
                    'aria-controls' => 'messages',
                    'aria-expanded' => 'false',
                    'data-hidden-submit' => 1,
                ],
                'content_attributes' => [
                    'aria-expanded' => 'false',
                ],
            ],
            TabParameter::TAB_SOCIALS => TabParameter::TAB_SOCIALS_CONTENT,
            TabParameter::TAB_META_TAGS => TabParameter::TAB_META_TAGS_CONTENT,
            TabParameter::TAB_GEO_COORDINATES => TabParameter::TAB_GEO_COORDINATES_CONTENT,
            TabParameter::TAB_COMMENTS => TabParameter::TAB_COMMENTS_CONTENT,
            TabParameter::TAB_MARKDOWN => TabParameter::TAB_MARKDOWN_CONTENT,
        ],
        'show' => [
            'tabs.main' => [
                'title' => 'Данные',
                'tabs_attributes' => [
                    'aria-controls' => 'home',
                    'aria-expanded' => 'true',
                    'data-hidden-submit' => 0,
                ],
                'content_attributes' => [
                    'aria-expanded' => 'true',
                ],
            ],
        ]
    ];

    /**
     * @var array
     */
    public $image = [];


    /**
     * Отбор по умолчанию, например ['status' => 1, ]
     *
     * @var array
     */
    protected $aQuery = [];

    /**
     * Сортировка с учетом сессии, например ['column' => 'created_at', 'type' => 'desc']
     *
     * @var array
     */
    protected $aOrderBy = [
        'column' => 'created_at',
        'type' => 'desc',
    ];

    /**
     * Пути для шаблонов
     *
     * @see \App\Cmf\Core\ControllerCrmTrait::setViews()
     *
     * @var array
     */
    protected $views = [];

    /**
     * @var array
     */
    public $indexComponent = [];

    /**
     * Лимит для пагинации
     *
     * @var int
     */
    protected $tableLimit = 10;

    /**
     * UserBaseController constructor.
     * - распрарсить дефолтный квери
     * - распрарсить дефолтный ордер
     * - взять пути для шаблонов с кэша
     */

    public function __construct()
    {
        $this->updateFields();
        $this->setDefaults();

        /**
         * Пути для шаблонов
         */
        $this->views = $this->setViews();

        View::share('theme', $this->theme);
    }

    /**
     *
     */
    private function setDefaults()
    {
        $this->session = $this::NAME;
        $this->view = $this::NAME;

        /**
         * Если свойства не переопределены, то указываем дефолтное значение значение
         */
        if (empty($this->cache)) {
            $this->cache = [
                $this::NAME,
            ];
        }
//        if (empty($this->rules)) {
//            $this->rules = [
//                'store' => [],
//                'update' => [],
//            ];
//        }


        $this->indexComponent = array_merge([
            'search' => true,
            'create' => true,
            'history' => true,
            'image' => true,
            'title' => $this::TITLE,
            'description' => null,
            'release_at' => false,
        ], $this->indexComponent);

        if (empty($this->image)) {
            $this->image = [
                ImageType::MODEL => [
                    'with_main' => true,
                    'unique' => false,
                    'clear_cache' => true,
                    'filters' => [
                        ImageSize::SQUARE => [
                            'filter' => \App\Services\Image\Filters\SquareFilter::class,
                            'options' => [
                                'dimension' => '1:1',
                                'size' => '360',
                            ],
                        ],
                    ],
                ],
            ];
        }
        if (!empty($this->tabs) && !empty($this->tabs['edit'])) {
            $keys = array_keys($this->tabsDefault['edit']);
            foreach ($keys as $key) {
                if (isset($this->tabs['edit'][$key])) {
                    $this->tabs['edit'][$key] = array_merge($this->tabsDefault['edit'][$key], $this->tabs['edit'][$key]);
                }
            }
        }
        if (!empty($this->tabs) && !empty($this->tabs['show'])) {
            $keys = array_keys($this->tabs['show']);
            foreach ($keys as $key) {
                if (isset($this->tabs['show'][$key])) {
                    if (isset($this->tabs['edit'][$key])) {
                        $this->tabs['show'][$key] = array_merge($this->tabs['edit'][$key], $this->tabs['show'][$key]);
                    }
                    if (isset($this->tabsDefault['show'][$key])) {
                        $this->tabs['show'][$key] = array_merge($this->tabsDefault['show'][$key], $this->tabs['show'][$key]);
                    }
                }
            }
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): \Illuminate\View\View
    {
        if (method_exists($this, 'thisBeforePaginate')) {
            $this->thisBeforePaginate();
        }
        if (!empty($this->indexComponents)) {
            View::share('indexComponents', $this->indexComponents);
        }
        $oItems = $this->paginate($this->class);

        //Session::put($this->session . '.count.get', $oItems->total());
        //Session::put($this->session . '.count.total', $this->class::count());

        $this->prepareFieldsValues();
        $this->searchTableFields();
        $this->sortTableFields();

        if (method_exists($this, 'thisPrepareIndexComponent')) {
            $this->thisPrepareIndexComponent();
        }
        View::share('indexComponent', $this->indexComponent);

        if ($request->exists('id')) {
            $oItem = $this->findByClass($this->class, $request->get('id'));
            View::share('oItem', $oItem);
        }

        return view($this->views[__FUNCTION__], [
            'oItems' => $oItems,
        ]);
    }

    /**
     * Default query items
     *
     * @param string $oModel
     * @param string|null $type
     * @param array $aColumns
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function paginate($oModel, $type = null, $aColumns = []): \Illuminate\Pagination\LengthAwarePaginator
    {
        $oItems = $oModel::with($this->with);

        if (isset($this->aOrderBy['translation'])) {
            $oItems = $oItems->orderByTranslation($this->aOrderBy['column'], $this->aOrderBy['type']);
        } else {
            $oItems = $oItems->orderBy($this->aOrderBy['column'], $this->aOrderBy['type']);
        }
        if (!is_null($type) && $type === 'search') {
            foreach ($aColumns as $key => $value) {
                $oItems = $this->querySearchBy($oItems, $key, $value);
            }
        }
        if (!empty($this->aQuery)) {
            foreach ($this->aQuery as $key => $value) {
                $oItems = $oItems->where($key, $value);
            }
        }

        return $oItems->paginate($this->tableLimit)->withPath(routeCmf($this->view . '.view.post'));
    }

    /**
     * Поиск
     *
     * @param object $oItems
     * @param string $key
     * @param array|string|null $value
     * @return mixed
     */
    private function querySearchBy($oItems, $key, $value)
    {
        // Кастомный метод для фильтрации полей с нестандартными связями. Например для ролей пользователя
        $searchMethod = Str::camel($key) . 'FieldSearch';
        if (method_exists($this, $searchMethod)) {
            if ($value === '0' && !empty($this->fields[$key]['zero_good'])) {
                return $this->$searchMethod($oItems, $value);
            }
            if (empty($value)) {
                return $oItems;
            }
            return $this->$searchMethod($oItems, $value);
        }

        if (is_array($value)) {
            if (isset($value['translation']) && $value['translation'] === 'like') {
                if (property_exists(new $this->class(), 'translatedAttributes')) {
                    if (!is_null($value['value'])) {
                        $oItems = $oItems->where(function ($query) use ($value, $key) {
                            $query->whereTranslationLike($key, '%' . $value['value'] . '%');
                            if (isset($value['support'])) {
                                $query->orWhereTranslationLike($value['support'], '%' . $value['value'] . '%');
                            }
                        });
                    }
                } else {
                    if (isset($value['support'])) {
                        $oItems = $oItems->where(function ($query) use ($value, $key) {
                            $query->where($key, 'like', '%' . $value['value'] . '%')
                                ->orWhere($value['support'], 'like', '%' . $value['value'] . '%');
                        });
                    } else {
                        $oItems = $oItems->where($key, 'like', '%' . $value['value'] . '%');
                    }
                }
            }
            if (isset($value['type']) && $value['type'] === 'like') {
                $oItems = $oItems->where($key, 'like', '%' . $value['value'] . '%');
            }
            if (!empty($value['begin'])) {
                $oItems = $oItems->where($key, '>=', Carbon::parse($value['begin']));
            }
            if (!empty($value['end'])) {
                $oItems = $oItems->where($key, '<', Carbon::parse($value['end'])->addDay());
            }
            if (!empty($value['from'])) {
                $oItems = $oItems->where($key, '>=', $value['from']);
            }
            if (!empty($value['to'])) {
                $oItems = $oItems->where($key, '<', $value['to']);
            }
        } else {
            if (method_exists(new $this->class(), $key) && !empty($value)) {
                // Фильтрация по связанным полям
                $oItems = $oItems->whereHas($key, function ($q) use ($value) {
                    $q->where('id', $value);
                });
            } elseif (!is_null($value)) {
                $oItems = $oItems->where($key, 'like', '%' . $value . '%');
            }
        }
        return $oItems;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(): \Illuminate\View\View
    {
        return view($this->views[__FUNCTION__]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return array | JsonResponse
     */
    public function store(Request $request)
    {
        $this->prepareRequestDataBeforeValidation($request);
        $validation = $this->validation($request, $this->getRules(), $this->getAttributes(), __FUNCTION__);
        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        } else {
            $data = [];
            $this->prepareRequestDataAfterValidation($request);
            if (method_exists($this, 'thisCreate')) {
                $return = $this->thisCreate($request);
                if ($return instanceof JsonResponse) {
                    return $return;
                }
            } else {
                $model = $this->storeModel($request);
                if ($request->exists('edit')) {
                    Session::put('last_create', $model->id);
                }
            }
            return responseCommon()->success($data, $this->toastText[__FUNCTION__]);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function storeModel(Request $request)
    {
        $model = $this->class::create($request->all());
        $this->saveRelationships($model, $request);
        return $model;
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function show($id): \Illuminate\View\View
    {
        $oItem = $this->findByClass($this->class, $id);

        $this->prepareFieldsValues($oItem);

        return view($this->views[__FUNCTION__], [
            'oItem' => $oItem,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\View\View
     */
    public function edit(Request $request, $id): \Illuminate\View\View
    {
        $oItem = $id ? $this->findByClass($this->class, $id) : null;
        $this->prepareFieldsValues($oItem);

        $tabs = $this->tabs['edit'];

        unset($tabs[TabParameter::TAB_VIDEO]);
        unset($tabs[TabParameter::TAB_CANCELLATION]);
        unset($tabs[TabParameter::TAB_RESERVATION]);
        unset($tabs[TabParameter::TAB_ANNOUNCEMENT]);
        unset($tabs[TabParameter::TAB_QUESTIONS]);
        unset($tabs[TabParameter::TAB_OUTFITS_RENT]);
        unset($tabs[TabParameter::TAB_DUPLICATE]);
        //dd($this->tabs['edit']);
        $data = [
            'oItem' => $oItem,
            'tabs' => $tabs,
            'tabsScrolling' => $this->tabs['scrolling'] ?? false,
            'view' => $this->view,
        ];

        if (method_exists($this, 'thisEditDataModal')) {
            $data = array_merge($data, $this->thisEditDataModal($oItem));
        }

        return view($this->views[__FUNCTION__], $data);
        //return $this->index($request);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return array | JsonResponse
     */
    public function update(Request $request, $id)
    {
        $this->prepareRequestDataBeforeValidation($request);
        $validation = $this->validation($request, $this->getRules(), $this->getAttributes(), __FUNCTION__);
        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        }
        $model = $this->class::find($id);
        $validation = $this->validation($request, $this->getRules($model, $request->all()), $this->getAttributes(), __FUNCTION__);
        if ($validation->fails()) {
            return responseCommon()->validationMessages($validation);
        }
        $this->prepareRequestDataAfterValidation($request);
        $returnData = [];
        if (method_exists($this, 'thisUpdate')) {
            $return = $this->thisUpdate($request, $model);
            if ($return instanceof JsonResponse) {
                return $return;
            }
            if (isset($return['success'])) {
                unset($return['success']);
            }
            $returnData = $return;
        } else {
            $model->update($request->all());
        }
        $this->saveRelationships($model, $request);
        return responseCommon()->success($returnData, $this->toastText[__FUNCTION__]);
    }

    /**
     * Изменить статус пользователю
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return array
     */
    public function status(Request $request, $id): array
    {
        $oItem = $this->findByClass($this->class, $id);
        $oItem->update([
            'status' => $request->get('status'),
        ]);
        return responseCommon()->success([], $this->toastText[__FUNCTION__]);
    }

    /**
     * @param int $id
     * @return array
     */
    public function destroy($id): array
    {
        $model = $this->findByClass($this->class, $id);
        if (method_exists($this, 'thisDestroy')) {
            $this->thisDestroy($model);
        } else {
            $model->delete();
        }
        $this->afterChange($this->cache);
        return responseCommon()->success([], $this->toastText[__FUNCTION__]);
    }

    /**
     * Custom action
     *
     * @param Request $request
     * @param string $name
     * @return mixed
     */
    public function action(Request $request, $name)
    {
        $method = Str::camel($name);
        if (!method_exists($this, $method)) {
            abort(500, 'Method not found');
        }
        return $this->{$method}($request);
    }

    /**
     * Генерация после добавление/изменения/удаления
     *
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function view(Request $request): array
    {
        return $this->query($request);
    }

    /**
     * Обновление таблицы после добавление/изменения/удаления
     *
     * @param \Illuminate\Pagination\LengthAwarePaginator $oItems
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    private function table($oItems): array
    {
        $this->prepareFieldsValues();
        $this->sortTableFields();

        View::share('indexComponent', $this->indexComponent);

        $view = view($this->views[__FUNCTION__], [
            'oItems' => $oItems,
        ])->render();

        $data = [
            'view' => $view,
            'count' => $oItems->total(),
        ];
        if (Session::exists('last_create')) {
            $data['id'] = Session::get('last_create');
            Session::forget('last_create');
        }
        return responseCommon()->success($data);
    }

    /**
     * @param Request $request
     * @param string $type
     * @param int|null $id
     * @return array
     * @throws \Throwable
     */
    public function modal(Request $request, $type, $id = null)
    {
        $oItem = $id ? $this->findByClass($this->class, $id) : null;
        $this->prepareFieldsValues($oItem);

        $data = [
            'oItem' => $oItem,
            'tabs' => $this->tabs[$type] ?? '',
            'tabsScrolling' => $this->tabs['scrolling'] ?? false,
            'view' => $this->view,
        ];
        //if ($type === 'create' && $this->class !== Refer::class) {
        if ($type === 'create') {
            $data['fastEdit'] = true;
        }

        if ($type === 'edit' && method_exists($this, 'thisEditDataModal')) {
            $data = array_merge($data, $this->thisEditDataModal($oItem));
        }

        $view = view($this->views['modal'] . $type, $data)->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    /**
     * @param Request $request
     * @return array
     * @throws \Throwable
     */
    public function query(Request $request): array
    {
        $columns = [];
        foreach ($request->all() as $key => $value) {
            switch ($key) {
                case 'first_name':
                    $columns[$key] = [
                        'translation' => 'like',
                        'value' => $value,
                        'support' => 'id',
                    ];
                    break;
                case 'name':
                    $columns[$key] = [
                        'translation' => 'like',
                        'value' => $value,
                        'support' => 'id',
                    ];
                    break;
                case 'promocode':
                    $columns[$key] = [
                        'type' => 'like',
                        'value' => $value,
                    ];
                    break;
                default:
                    $columns[$key] = $value;
                    break;
            }
        }
        if (isset($columns['page'])) {
            unset($columns['page']);
        }
        if (isset($columns['multi_save'])) {
            unset($columns['multi_save']);
        }
        if (isset($columns['phpunit'])) {
            unset($columns['phpunit']);
        }
        $oItems = $this->paginate($this->class, 'search', $columns);
        return $this->table($oItems);
    }

    /**
     *
     */
    private function updateFields(): void
    {
        if (method_exists($this, 'buildFields')) {
            $this->fields = $this->buildFields();
        }
    }

    /**
     * @param object|null $item
     */
    public function prepareFieldsValues($item = null)
    {
        if (method_exists($this, 'thisPrepareFieldsValues')) {
            $this->thisPrepareFieldsValues($item);
        }
        foreach ($this->fields as $key => &$value) {
            if (isset($value['values'])) {
                $value['selected_values'] = [];
                if (isset($item) && !is_null($item->$key)) {
                    if (is_string($item->$key)) {
                        $value['selected_values'][] = $item->$key;
                    } elseif ($item->$key instanceof \Illuminate\Database\Eloquent\Collection) {
                        $value['selected_values'] = $item->$key->pluck('id')->toArray();
                    } elseif (is_int($item->$key)) {
                        $value['selected_values'][] = $item->$key;
                    } else {
                        $value['selected_values'][] = $item->$key->id ?? 'Не выбрано';
                    }
                }
                if (!is_array($value['values']) && $value['dataType'] !== self::DATA_TYPE_CUSTOM) {
                    $method = $value['order']['method'] ?? 'orderBy';
                    $name = $value['order']['by'] ?? 'name';
                    $dir = $value['order']['dir'] ?? 'asc';
                    $class = $value['values'];
                    $value['values'] = $class::count() < 100
                        ? $class::$method($name, $dir)
                        : $class::whereIn((new $class())->getTable() . '.id', $value['selected_values'])->$method($name, $dir);

                    if (isset($value['whereIn'])) {
                        $value['values']->whereIn($value['whereIn']['column'], $value['whereIn']['value']);
                    }
                    $value['values'] = $value['values']->get()->pluck($name, 'id');
                }
            }
        }

        $this->shareToView('fields', $this->fields);
        $this->shareToView('model', $this::NAME);
    }

    public function sortTableFields()
    {
        $this->fields = collect($this->fields)->reject(function ($field) {
            return !isset($field['in_table']);
        })->sortBy('in_table')->toArray();
    }

    /**
     * @param Request $request
     */
    public function prepareRequestDataBeforeValidation(Request &$request)
    {
        foreach ($this->fields as $name => $field) {
            if ($field['dataType'] === self::DATA_TYPE_CHECKBOX && !$request->has($name)) {
                $request->request->add([$name => false]);
            }
            if ($field['dataType'] === self::DATA_TYPE_DATE) {
                $aDates = (new $this->class())->getDates();
                foreach ($aDates as $date) {
                    if ($request->exists($date) && !empty($request->get($date))) {
                        $request->merge([
                            $date => $request->get($date) instanceof \DateTime ? $request->get($date) : Carbon::parse($request->get($date)),
                        ]);
                    }
                }
            }
        }
    }

    /**
     * @param Request $request
     */
    public function prepareRequestDataAfterValidation(Request &$request): void
    {
        foreach ($this->fields as $name => $field) {
            if ($field['dataType'] === self::DATA_TYPE_IMG) {
                if ($request->hasFile($name)) {
                    $file_path = $request->$name->store('public/' . $this->view . '_' . $name);

                    $new_request = new Request();
                    $new_request->replace($request->except($name));
                    $request = $new_request;

                    $request->merge([
                        $name => $file_path,
                    ]);
                } else {
                    $request->offsetUnset($name);
                }
            }
        }
    }

    /**
     * @param object $model
     * @param Request $request
     */
    public function saveRelationships($model, Request $request): void
    {
        foreach ($this->fields as $name => $field) {
            if (!isset($field['relationship'])) {
                continue;
            }
            switch ($field['relationship']) {
                case self::RELATIONSHIP_BELONGS_TO_MANY:
                    $model->$name()->sync($request->get($name));
                    break;
                case self::RELATIONSHIP_BELONGS_TO:
                    $model->$name()->associate($field['values']::find($request->get($name)));
                    $model->save();
                    break;
            }
        }
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function getRelationshipFieldModal(Request $request, $id)
    {
        $field = $request->get('field');
        $key = $request->get('key');
        $type = empty($this->fields[$field]['multiple']) ? 'show' : 'list';
        $oItem = $this->findByClass($this->class, $id);
        $model = self::getModelNameByClass($this->fields[$field]['values']);
        $Controller = self::getControllerByModelName($model);
        $Controller->prepareFieldsValues();

        $data = [
            'title' => $this->fields[$field]['title'],
            'model' => strtolower($model),
        ];

        if ($key) {
            $field = $oItem->$field()->whereId($key)->first();
            $type = 'show';
        } else {
            $field = empty($method) ? $oItem->$field : $oItem->$method();
        }

        $data[in_array(self::getModelNameByClass(get_parent_class($field)), ['Model', 'User']) ? 'oItem' : 'oItems'] = $field;

        /**
         * @see ControllerCrmTrait::setViews()
         */
        $view = view('cmf.content.default.modals.container.' . $type, $data)->render();

        return responseCommon()->success([
            'view' => $view,
        ]);
    }

    /**
     * Фильтры
     *
     * @return array
     */
    public function getImageFilters(): array
    {
        return $this->image['filters'];
    }

    /**
     * @param Request $request
     * @return array
     */
    public function searchRelationshipField(Request $request)
    {
        $field = $request->input('field');

        if (isset($this->fields[$field]['values'])) {
            $search = $request->input('q');
            $selected = !empty($request->input('selected')) ? $selected = explode(',', $request->input('selected')) : [];
            $model = self::getModelNameByClass($this->fields[$field]['values']);
            $Controller = self::getControllerByModelName($model);

            $method = $this->fields[$field]['order']['method'] ?? 'orderBy';
            $name = $this->fields[$field]['order']['by'] ?? 'name';
            $dir = $this->fields[$field]['order']['dir'] ?? 'asc';
            $class = $this->fields[$field]['values'];

            if (!is_array($this->fields[$field]['values']) && $this->fields[$field]['dataType'] !== self::DATA_TYPE_CUSTOM) {
                $selected = $class::whereIn((new $class())->getTable() . '.id', $selected)->$method($name, $dir)->get()->pluck($name, 'id');
            }
            if (!is_array($this->fields[$field]['values']) && $this->fields[$field]['dataType'] !== self::DATA_TYPE_CUSTOM) {
                $search = $class::where($name, 'like', $search . '%')->$method($name, $dir)->limit(20)->get()->pluck($name, 'id');
            }
            return responseCommon()->success([
                'search' => $search,
                'selected' => $selected,
            ]);
        }
        return responseCommon()->error([]);
    }

    /**
     * @param object|null $model
     * @param array $data
     * @return array
     */
    public function getRules($model = null, $data = [])
    {
        if (method_exists($this, 'rules')) {
            return $this->rules($model, $data);
        }
        if (!empty($this->rules)) {
            return $this->rules;
        }
        return [
            'store' => [],
            'update' => [],
        ];
    }

    /**
     * @return array
     */
    public function getAttributes()
    {
        return $this->attributes ?? [];
    }

    /**
     * @return string
     */
    protected function getCurrentView(): string
    {
        return $this->theme . '.content.' . $this->view;
    }

    /**
     *
     */
    public function searchTableFields()
    {
        $search_fields = collect($this->fields)->reject(function ($field) {
            return !isset($field['search']);
        })->sortBy('search')->toArray();
        $this->shareToView('search_fields', $search_fields);
    }

    /**
     * @param Request $request
     * @return bool
     */
    protected function isMultiSave(Request $request): bool
    {
        $multiSave = $request->get('multi_save');
        return !is_null($multiSave) && (int)$multiSave === 1;
    }

    /**
     * @param Request $request
     * @param object|Tour $oItem
     * @param callable $function
     * @param object|null $oSupportItem
     */
    protected function multiSave(Request $request, $oItem, callable $function, $oSupportItem = null)
    {
        $oMultiples = $oItem->multiples;
        $request->merge([
            'multi_save' => 0,
        ]);
        foreach ($oMultiples as $oMultiple) {
            $function($request, $oMultiple, $oSupportItem);
        }
    }

    /**
     * @param Request $request
     * @param object|Tour $oItem
     * @param Image $oImage
     * @param callable $function
     */
    protected function multiSaveImage(Request $request, $oItem, Image $oImage, callable $function)
    {
        $oMultiples = $oItem->multiples;
        $request->merge([
            'multi_save' => 0,
        ]);
        $oImageService = (new ImageService());
        foreach ($oMultiples as $oMultiple) {
            /** @var Image $oMultipleImage */
            $oMultipleImage = null;
            if (!is_null($oImage->number)) {
                $oImages = $oImageService->getOrderedImagesByType($oMultiple, $oImage->type);
                $oMultipleImage = $oImages->where('number', $oImage->number)->first();
            }
            if (!is_null($oMultipleImage)) {
                $function($request, $oMultiple, $oMultipleImage);
            }
        }
    }
}
