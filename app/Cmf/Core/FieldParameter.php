<?php

declare(strict_types=1);

namespace App\Cmf\Core;

use App\Cmf\Project\Tour\TourController;
use Illuminate\Support\Str;

class FieldParameter
{
    const TYPE = 'dataType';
    const TITLE = 'title';
    const TITLE_TABLE = 'table_title';
    const TITLE_FORM = 'title_form';
    const TABLE_TITLE = 'table_title';
    const RELATIONSHIP = 'relationship';
    const VALUES = 'values';
    const ORDER = 'order';
    const ORDER_METHOD = 'method';
    const ORDER_BY = 'by';
    const ALIAS = 'alias';
    const REQUIRED = 'required';
    const IN_TABLE = 'in_table';
    const ROLES = 'roles';
    const FORMAT = 'format';
    const EMPTY = 'empty';
    const SEARCH = 'search';
    const DELETE_TITLE = 'delete_title';
    const DEFAULT = 'default';
    const DATETIME = 'datetime';
    const GROUP_NAME = 'group';
    const GROUP_TITLE = 'group-title';
    const GROUP_COL = 'group-col';
    const GROUP_HIDE = 'group-hide';
    const LENGTH = 'length';
    const TOOLTIP = 'tooltip';
    const PLACEHOLDER = 'placeholder';
    const MULTIPLE = 'multiple';
    const MASK_PHONE = 'mask_phone';
    const RADIO_VALUES = 'radio_values';
    const HIDDEN = 'hidden';
    const DISABLED = 'disabled';
    const READONLY = 'readOnly';
    const SHOW_ONLY = 'show_only';
    const SELECTED_VALUES = 'selected_values';
    const SPLIT = 'split';
    const WHERE_IN = 'whereIn';
    const WHERE_IN_COLUMN = 'column';
    const WHERE_IN_VALUE = 'value';
    const COLORED = 'colored';
    const LIMIT = 'limit';

    /**
     * @var string|null
     */
    private $name = null;

    /**
     * @var string|null
     */
    private $type = null;
    private $title = null;
    private $title_form = null;
    private $relationship = null;
    private $values = null;
    private $order = null;
    private $order_method = null;
    private $order_by = null;
    private $alias = null;
    private $required = null;
    private $default = null;
    private $empty = null;
    private $in_table = null;
    private $delete_title = null;
    private $datetime = null;
    private $group_name = null;
    private $group_col = null;
    private $group_hide = null;
    private $length = null;
    private $tooltip = null;
    private $placeholder = null;
    private $multiple = null;
    private $hidden = null;

    /**
     * @param string $type
     * @return $this
     */
    public function type($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function title($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function titleForm($title)
    {
        $this->title_form = $title;
        return $this;
    }

    /**
     * @param string $relationship
     * @param string $values
     * @return $this
     */
    public function relationship($relationship, $values)
    {
        $this->relationship = $relationship;
        $this->values = $values;
        return $this;
    }

    /**
     * @param string $method
     * @param string $by
     * @return $this
     */
    public function order($method, $by)
    {
        $this->order = [
            'method' => $method,
            'by' => $by,
        ];
        return $this;
    }

    /**
     * @param string $alias
     * @return $this
     */
    public function alias($alias)
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return $this
     */
    public function required()
    {
        $this->required = true;
        return $this;
    }

    /**
     * @param string|bool|int $value
     * @return $this
     */
    public function default($value)
    {
        $this->default = $value;
        return $this;
    }

    /**
     * @return $this
     */
    public function empty()
    {
        $this->empty = true;
        return $this;
    }

    /**
     * @param int $in_table
     * @return $this
     */
    public function inTable(int $in_table)
    {
        $this->in_table = $in_table;
        return $this;
    }

    /**
     * @param int $length
     * @return $this
     */
    public function length(int $length)
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @param string $placeholder
     * @return $this
     */
    public function placeholder(string $placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * @param string $tooltip
     * @return $this
     */
    public function tooltip(string $tooltip)
    {
        $this->tooltip = $tooltip;
        return $this;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function deleteTitle(string $title)
    {
        $this->delete_title = $title;
        return $this;
    }

    /**
     * @return $this
     */
    public function datetime()
    {
        $this->datetime = true;
        return $this;
    }

    /**
     * @param string $name
     * @param int $col
     * @param bool $isFirst
     * @return $this
     */
    public function group(string $name, int $col = 6, bool $isFirst = false)
    {
        $this->group_name = $name;
        $this->group_col = $col;
        $this->group_hide = !$isFirst;
        return $this;
    }

    /**
     * @return $this
     */
    public function multiple()
    {
        $this->multiple = true;
        return $this;
    }

    /**
     * @return $this
     */
    public function hidden()
    {
        $this->hidden = true;
        return $this;
    }

    /**
     * @return array
     */
    public function build(): array
    {
        $array = [];
        if (!is_null($this->type)) {
            $array[self::TYPE] = $this->type;
        }
        if (!is_null($this->title)) {
            $array[self::TITLE] = $this->title;
        }
        if (!is_null($this->title_form)) {
            $array[self::TITLE_FORM] = $this->title_form;
        }
        if (!is_null($this->relationship)) {
            $array[self::RELATIONSHIP] = $this->relationship;
        }
        if (!is_null($this->order)) {
//            $array[self::ORDER] = [
//                self::ORDER_METHOD => $this->order_method,
//                self::ORDER_BY => $this->order_by,
//            ];
            $array[self::ORDER] = $this->order;
        }
        if (!is_null($this->alias)) {
            $array[self::ALIAS] = $this->alias;
        }
        if (!is_null($this->required)) {
            $array[self::REQUIRED] = $this->required;
        }
        if (!is_null($this->empty)) {
            $array[self::EMPTY] = $this->empty;
        }
        if (!is_null($this->in_table)) {
            $array[self::IN_TABLE] = $this->in_table;
        }
        if (!is_null($this->delete_title)) {
            $array[self::DELETE_TITLE] = $this->delete_title;
        }
        if (!is_null($this->default)) {
            $array[self::DEFAULT] = $this->default;
        }
        if (!is_null($this->datetime)) {
            $array[self::DATETIME] = $this->datetime;
        }
        if (!is_null($this->group_name)) {
            $array[self::GROUP_NAME] = $this->group_name;
        }
        if (!is_null($this->group_col)) {
            $array[self::GROUP_COL] = $this->group_col;
        }
        if (!is_null($this->group_hide)) {
            $array[self::GROUP_HIDE] = $this->group_hide;
        }
        if (!is_null($this->length)) {
            $array[self::LENGTH] = $this->length;
        }
        if (!is_null($this->tooltip)) {
            $array[self::TOOLTIP] = $this->tooltip;
        }
        if (!is_null($this->placeholder)) {
            $array[self::PLACEHOLDER] = $this->placeholder;
        }
        if (!is_null($this->multiple)) {
            $array[self::MULTIPLE] = $this->multiple;
        }
        if (!is_null($this->hidden)) {
            $array[self::HIDDEN] = $this->hidden;
        }
        return $array;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function field(string $name)
    {
        $this->clear();
        $this->name = $name;
        return $this;
    }

    /**
     * @return $this
     */
    public function clear()
    {
        $this->type = null;
        $this->title = null;
        $this->title_form = null;
        $this->relationship = null;
        $this->order = null;
        $this->alias = null;
        $this->required = null;
        $this->empty = null;
        $this->in_table = null;
        $this->delete_title = null;
        $this->default = null;
        $this->datetime = null;
        $this->group_name = null;
        $this->group_col = null;
        $this->group_hide = null;
        $this->length = null;
        $this->tooltip = null;
        $this->placeholder = null;
        $this->multiple = null;
        $this->hidden = null;

        return $this;
    }

    /**
     * @param string $model
     * @param string $field
     * @return mixed|null
     */
    public function getField(string $model, string $field)
    {
        $sPath = Str::ucfirst($model);
        $sClass = $sPath . Str::studly('_controller');
        $sClass = 'App\Cmf\Project\\' . $sPath . '\\' . $sClass;
        if (class_exists($sClass)) {
            /** @var TourController $oClass */
            $oClass = new $sClass();
            $model = $oClass::NAME;
            $oClass->prepareFieldsValues();
            return $oClass->fields[$field];
        }
        return null;
    }
}
