<?php

declare(strict_types=1);

namespace App\Cmf\Core\Defaults;

use App\Models\Direction;
use App\Models\Organization;
use App\Models\Tour;
use Illuminate\Http\Request;

trait MetaTagableTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionMetaTagsSave(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = $this->findByClass($this->class, $id);
        $data = $request->get('meta_tag');

        $oMetaTag = $oItem->metaTag;
        if (is_null($oMetaTag)) {
            $oItem->metaTag()->create([
                'title' => $data['title'] ?? null,
                'description' => $data['description'] ?? null,
                'keywords' => $data['keywords'] ?? null,
            ]);
        } else {
            $oMetaTag->update([
                'title' => $data['title'] ?? null,
                'description' => $data['description'] ?? null,
                'keywords' => $data['keywords'] ?? null,
            ]);
        }

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionMetaTagsSave($request, $oMultiple->id);
            });
        }

        return responseCommon()->success([], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionMetaTagsAutoGenerate(Request $request, int $id)
    {
        /** @var Tour|Direction|Organization $oItem */
        $oItem = $this->findByClass($this->class, $id);
        $oMetaTag = $oItem->metaTag;

        list('title' => $title, 'description' => $description, 'keywords' => $keywords) = $this->metaTagsAutoGenerate($oItem);

        if (is_null($oMetaTag)) {
            $oItem->metaTag()->create([
                'title' => $title,
                'description' => $description,
                'keywords' => $keywords,
            ]);
        } else {
            $oMetaTag->update([
                'title' => $title,
                'description' => $description,
                'keywords' => $keywords,
            ]);
        }
        $oItem->refresh();

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionMetaTagsAutoGenerate($request, $oMultiple->id);
            });
        }

        return responseCommon()->success([
            'view' => view('cmf.content.default.modals.tabs.meta_tags', [
                'oItem' => $oItem,
                'model' => self::NAME,
            ])->render(),
        ], 'Данные успешно сохранены');
    }
}
