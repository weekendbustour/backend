<?php

declare(strict_types=1);

namespace App\Cmf\Core\Defaults;

use App\Models\Direction;
use App\Models\WordDeclension;
use Illuminate\Http\Request;

trait WordDeclensionableTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionWordDeclensionsSave(Request $request, int $id)
    {
        /** @var Direction $oItem */
        $oItem = $this->findByClass($this->class, $id);
        $data = $request->get('word_declension');

        foreach ($data as $key => $value) {
            $oDeclension = $oItem->getDeclensionItemByType($key);
            if (is_null($oDeclension)) {
                $oItem->getDeclensionMorphByType($key)->create([
                    'type' => $key,
                    'value' => $value,
                ]);
            } else {
                $oDeclension->update([
                    'value' => $value,
                ]);
            }
        }
        return responseCommon()->success([], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionWordDeclensionsAutoGenerate(Request $request, int $id)
    {
        /** @var Direction $oItem */
        $oItem = $this->findByClass($this->class, $id);

        $oItem->refresh();
        return responseCommon()->success([
            'view' => view('cmf.content.default.modals.tabs.word_declensions', [
                'oItem' => $oItem,
                'model' => self::NAME,
            ])->render(),
        ], 'Данные успешно сохранены');
    }
}
