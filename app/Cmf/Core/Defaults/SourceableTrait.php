<?php

declare(strict_types=1);

namespace App\Cmf\Core\Defaults;

use App\Models\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;

trait SourceableTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionSourceSave(Request $request, int $id)
    {
        /** @var Article $oItem */
        $oItem = $this->findByClass($this->class, $id);
        $data = $request->get('source');

        $oSource = $oItem->source;
        if (is_null($oSource)) {
            $oItem->source()->create([
                'organization_id' => $data['organization_id'] ?? null,
                'url' => $data['url'] ?? null,
                'description' => $data['description'] ?? null,
                'published_at' => !is_null($data['published_at']) ? Carbon::parse($data['published_at']) : null,
            ]);
        } else {
            $oSource->update([
                'organization_id' => $data['organization_id'] ?? null,
                'url' => $data['url'] ?? null,
                'description' => $data['description'] ?? null,
                'published_at' => !is_null($data['published_at']) ? Carbon::parse($data['published_at']) : null,
            ]);
        }

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionSourceSave($request, $oMultiple->id);
            });
        }
        return responseCommon()->success([], 'Данные успешно сохранены');
    }
}
