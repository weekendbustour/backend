<?php

declare(strict_types=1);

namespace App\Cmf\Core\Defaults;

use App\Models\Direction;
use App\Models\Tour;
use App\Models\WordDeclension;
use Illuminate\Http\Request;

trait MultiTourTrait
{
    /**
     * @param Request $request
     * @param int|null $id
     * @return array
     * @throws \Throwable
     */
    public function actionMultiAddDate(Request $request, ?int $id = null)
    {
        $oItem = null;
        if (!is_null($id)) {
            //$oItem = Tour::find($id);
        }
        $view = view('cmf.content.tour.modals.create.components.date', [
            'oItem' => $oItem,
            'model' => self::NAME,
        ])->render();
        return responseCommon()->success([
            'view' => $view,
        ]);
    }
}
