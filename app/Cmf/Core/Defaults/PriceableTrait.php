<?php

declare(strict_types=1);

namespace App\Cmf\Core\Defaults;

use App\Models\Organization;
use App\Models\Tour;
use Carbon\Carbon;
use Illuminate\Http\Request;

trait PriceableTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function actionPriceSave(Request $request, int $id): array
    {
        /** @var Tour $oItem */
        $oItem = $this->findByClass($this->class, $id);
        $data = $request->get('price');

        $oPrice = $oItem->price;
        if (!is_null($oPrice)) {
            $oPrice->update([
                'value' => $data['value'],
                'value_description' => $data['value_description'],
                'discount' => $data['discount'],
                'discount_before_at' => !empty($data['discount_before_at']) ? Carbon::parse($data['discount_before_at']) : null,
                'discount_description' => $data['discount_description'],
                'prepayment' => $data['prepayment'],
                'prepayment_description' => $data['prepayment_description'],
            ]);
        } else {
            $oItem->price()->create([
                'value' => $data['value'],
                'value_description' => $data['value_description'],
                'discount' => $data['discount'],
                'discount_before_at' => !empty($data['discount_before_at']) ? Carbon::parse($data['discount_before_at']) : null,
                'discount_description' => $data['discount_description'],
                'prepayment' => $data['prepayment'],
                'prepayment_description' => $data['prepayment_description'],
            ]);
        }

        if ($request->exists('options')) {
            $requestParameters = clone $request;
            $this->actionUpdateParameters($requestParameters, $oItem->id);
        }

        // мульти сохранение
        if ($this->isMultiSave($request)) {
            $this->multiSave($request, $oItem, function ($request, $oMultiple) {
                $this->actionPriceSave($request, $oMultiple->id);
            });
        }

        return responseCommon()->success([], 'Данные успешно сохранены');
    }
}
