<?php

declare(strict_types=1);

namespace App\Cmf\Core\Defaults;

use App\Models\Location;
use App\Models\Organization;
use Illuminate\Http\Request;

trait GeoTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function coordinatesSave(Request $request, int $id)
    {
        $oItem = $this->findByClass($this->class, $id);
        $aLocations = $request->get('location');
        foreach ($aLocations as $id => $aLocation) {
            $oLocation = $oItem->locations()->where('id', $id)->first();
            if (!is_null($oLocation)) {
                $oLocation->update([
                    'latitude' => $aLocation['latitude'],
                    'longitude' => $aLocation['longitude'],
                    'zoom' => $aLocation['zoom'],
                    'title' => !empty($aLocation['title']) ? $aLocation['title'] : null,
                    'type' => !empty($aLocation['type']) ? $aLocation['type'] : Location::TYPE_DEFAULT,
                    'description' => !empty($aLocation['description']) ? $aLocation['description'] : null,
                    'placeholder' => !empty($aLocation['placeholder']) ? $aLocation['placeholder'] : null,
                ]);
            }
        }
        return responseCommon()->success([
            'view' => view('cmf.content.default.modals.tabs.geo.coordinates', [
                'model' => self::NAME,
                'oItem' => $oItem,
            ])->render(),
        ], 'Координаты успешно сохранены');
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function coordinatesClear(Request $request, int $id)
    {
        $oItem = $this->findByClass($this->class, $id);
        $oLocation = $oItem->locations()->where('id', $request->get('location_id'))->first();
        if (!is_null($oLocation)) {
            $oLocation->delete();
        }
        return responseCommon()->success([
            'view' => view('cmf.content.default.modals.tabs.geo.coordinates', [
                'model' => self::NAME,
                'oItem' => $oItem,
                'lastAdded' => true,
            ])->render(),
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function coordinatesAddTab(Request $request, int $id)
    {
        $oItem = $this->findByClass($this->class, $id);
        $oItem->location()->create([]);
        $oItem->refresh();
        return responseCommon()->success([
            'view' => view('cmf.content.default.modals.tabs.geo.coordinates', [
                'model' => self::NAME,
                'oItem' => $oItem,
                'lastAdded' => true,
            ])->render(),
        ]);
    }
}
