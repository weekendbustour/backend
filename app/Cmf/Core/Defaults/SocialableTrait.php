<?php

declare(strict_types=1);

namespace App\Cmf\Core\Defaults;

use App\Models\Organization;
use Illuminate\Http\Request;

trait SocialableTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function socialsSave(Request $request, int $id)
    {
        $oItem = $this->findByClass($this->class, $id);
        $oSocial = new \App\Models\Social();

        foreach ($oSocial->types as $key => $title) {
            $oSocial = $oItem->socials()->where('type', $key)->first();
            $url = $request->get($key);
            $description = $request->get($key . '-description');

            if (is_null($oSocial)) {
                if ($this->socialIsCorrect($request, $key)) {
                    $oItem->socials()->create([
                        'type' => $key,
                        'url' => $url,
                        'description' => $description,
                    ]);
                }
            } else {
                if ($this->socialIsCorrect($request, $key)) {
                    $oSocial->update([
                        'url' => $url,
                        'description' => $description,
                    ]);
                } else {
                    $oSocial->delete();
                }
            }
        }
        return responseCommon()->success([], 'Данные успешно сохранены');
    }

    /**
     * @param Request $request
     * @param string $key
     * @return bool
     */
    private function socialIsCorrect(Request $request, string $key): bool
    {
        return $request->has($key) && !is_null($request->get($key)) && $request->get($key) !== '@';
    }
}
