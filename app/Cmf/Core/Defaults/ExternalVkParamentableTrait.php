<?php

declare(strict_types=1);

namespace App\Cmf\Core\Defaults;

use App\Models\Tour;
use App\Services\Vk\VkMarket;
use App\Services\Vk\VkWall;
use Illuminate\Http\Request;

trait ExternalVkParamentableTrait
{
    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionVkParametersGet(Request $request, int $id)
    {
        $oItem = Tour::find($id);
        return responseCommon()->success([
            'view' => view('cmf.content.tour.table.modals.vkParameters', [
                'oItem' => $oItem,
                'model' => self::NAME,
            ])->render(),
        ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionVkParametersPostCreate(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        (new VkWall($oItem))
            ->post($oItem->vkPostTextTour())
            ->attachUrl($oItem->getUrl())
            ->attachPhoto($oItem->getImageModelOriginalStorage($oItem->modelImages()->where('is_main', 1)->first()))
            ->send();

        $oItem->refresh();

        return $this->actionVkParametersGet($request, $oItem->id);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionVkParametersPostMarketCreate(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        $market = (new VkMarket($oItem));
        $market = $market->product($oItem->metaTag->title, $oItem->vkMarketTextTour(), 800, $oItem->price->value);

        if (!is_null($oItem->price->discount)) {
            $market = $market->attachDiscount($oItem->price->discount);
        }
        $market
            ->attachUrl($oItem->getUrl())
            ->attachPhoto($oItem->previewCardCreatePath())
            //->attachAlbum('Архыз', public_path('img/about.jpg'))
            //->attachAlbum('Название организатора')
            ->send();

        $oItem->refresh();

        (new VkWall($oItem))
            ->post($oItem->vkPostTextTour())
            //->attachPhoto($oItem->getImageModelOriginalStorage($oItem->modelImages()->where('is_main', 1)->first()))
            ->attachMarket($oItem->vkMarket->item_id)
            ->attachUrl($oItem->getUrl())
            ->send();

        $oItem->refresh();

        return $this->actionVkParametersGet($request, $oItem->id);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     */
    public function actionVkParametersMarketCreate(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        $market = (new VkMarket($oItem));
        $market = $market->product($oItem->metaTag->title, $oItem->vkMarketTextTour(), 800, $oItem->price->value);

        if (!is_null($oItem->price->discount)) {
            $market = $market->attachDiscount($oItem->price->discount);
        }
        $market
            ->attachUrl($oItem->getUrl())
            ->attachPhoto($oItem->previewCardCreatePath())
            //->attachPhoto($oItem->getImageModelOriginalStorage($oItem->modelImages()->where('is_main', 1)->first()))
            //->attachAlbum('Архыз', public_path('img/about.jpg'))
            //->attachAlbum('Название организатора')
            ->send();

        $oItem->refresh();

        return $this->actionVkParametersGet($request, $oItem->id);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function actionVkParametersMarketDelete(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        if (!is_null($oItem->vkMarket)) {
            if (!is_null($oItem->vkMarket->item_id)) {
                $success = (new VkMarket($oItem))->delete();
                if (!$success) {
                    return responseCommon()->error([], 'Ошибка удаления');
                }
            }
            $oItem->vkMarket->delete();
        }

        return $this->actionVkParametersGet($request, $oItem->id);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return array
     * @throws \Throwable
     * @throws \VK\Exceptions\VKApiException
     * @throws \VK\Exceptions\VKClientException
     */
    public function actionVkParametersPostDelete(Request $request, int $id)
    {
        /** @var Tour $oItem */
        $oItem = Tour::find($id);

        if (!is_null($oItem->vkPost)) {
            if (!is_null($oItem->vkPost->item_id)) {
                $success = (new VkWall($oItem))->delete();
                if (!$success) {
                    return responseCommon()->error([], 'Ошибка удаления');
                }
            }
            $oItem->vkPost->delete();
        }

        return $this->actionVkParametersGet($request, $oItem->id);
    }
}
