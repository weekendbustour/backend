<?php

declare(strict_types=1);

namespace App\Cmf\Core\Defaults;

use App\Services\Image\ImageSize;
use App\Services\Image\Upload\ImageUploadTextService;
use App\Services\Image\Upload\ImageUploadTmpService;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use finfo;

trait TextableUploadTrait
{
    /**
     * Фильтры для изображений, загруженные с визивига
     * size = 1280 - максимальная ширина
     * @var array
     */
    private $upload = [
        'filters' => [
            ImageSize::XL => [
                'filter' => \App\Services\Image\Filters\SquareFilter::class,
                'options' => [
                    'size' => 1280,
                ],
            ],
        ],
    ];

    /**
     * Тип загрузки в images.type
     * Директория для записи, будет public/news/text/...
     *
     * @var string
     */
    private $uploadType = 'news/text';

    /**
     * @var string
     */
    private $uploadPrefix = 'text';

    /**
     * Загрузка изображения в визивиге
     *
     * @param Request $request
     * @param int $id
     * @return array
     */
    public function textUpload(Request $request, $id)
    {
        $oItem = $this->findByClass($this->class, $id);
        if ($request->hasFile('file')) {
            $url = $this->tmpTextImageUploadCache($request, $oItem);
            return responseCommon()->success([
                'file' => $url,
            ], 'Данные успешно сохранены');
        }
        return responseCommon()->error();
    }

    /**
     * Загрузка файла в tmp директорию и сохранение в кэш
     *
     * @param Request $request
     * @param object $oItem
     * @return string
     */
    private function tmpTextImageUploadCache(Request $request, $oItem)
    {
        $uid = $request->get('uid');
        $oService = (new ImageUploadTmpService());
        $filename = $oService->upload($request->file('file'), $oItem, []);
        $id = Str::random(32);

        $url = $oService->getAssetToSize($oItem, 'original') . '/' . $filename;
        $this->cacheTextImageMerge($uid, [
            $id => [
                'url' => $url,
                'name' => $filename,
                'path' => '/' . $oService->getPathToSize($oItem, 'original') . '/' . $filename,
            ],
        ]);
        return $url;
    }

    /**
     * Замена текстов в изображениях
     *
     * @param object $oItem
     * @param string $uid
     * @param string $text
     * @return mixed
     */
    public function clearTextImages($oItem, $uid, $text)
    {
        $aData = $this->cacheTextImageGet($uid);

        if (empty($aData) || !$uid) {
            return $text;
        }
        $oService = new ImageUploadTextService();
        foreach ($aData as $k => $value) {
            $fileName = $value['name'];
            $fileUrl = $value['url'];
            $filePath = $value['path'];
            $filePath = public_path($filePath);
            if (File::exists($filePath)) {
                // если это изображение не было удалено из редактора
                // т.е. найдено в тексте
                $pos = strpos($text, $fileUrl);
                if ($pos !== false) {
                    $text = $this->copyFromTmp($oService, $oItem, $filePath, $fileName, $text, $fileUrl);
                }
                /*
                try {

                } catch (\Exception $e) {
                    // ничего пока не исключаем
                    $success = false;
                }
                */
            }
            // обновляем значение в кеше
            unset($aData[$k]);
            $this->cacheTextImageSet($uid, $aData);
        }
        // if $success
        $this->cacheTextImageDelete($uid);
        $this->removeTmpDirectory($oItem);
        return $text;
    }

    /**
     * Проверить изображения по тексту, если их нет в тексте, то удалить Image
     *
     * @param object $oItem
     * @param string $text
     * @throws \Exception
     */
    protected function checkTextImages($oItem, $text): void
    {
        $oImages = $oItem->textImages;
        $oService = new ImageUploadTextService();
        foreach ($oImages as $oImage) {
            $fileUrl = $oService->getAssetToSize($oItem, ImageSize::XL) . '/' . $oImage->filename;
            // если изображение не найдено в тексте, то удалить его файлы и из базы
            $pos = strpos($text, $fileUrl);
            if ($pos === false) {
                $oService->delete($oItem, $oImage, $this->upload);
                $oImage->destroy($oImage->id);
            }
        }
    }

    /**
     * Удаление временной директории
     *
     * @param object $oItem
     */
    public function removeTmpDirectory($oItem)
    {
        $oService = new ImageUploadTmpService();
        $dir = public_path($oService->getPathToSize($oItem));
        File::deleteDirectory($dir);
    }

    /**
     * Копировать файл из временого хранилища
     * Сохранить файл в базу
     * Удалить файл из временого хранилища
     * Заменить ссылку в тексте на актуальный файл
     *
     * @param ImageUploadTextService $oService
     * @param object $oItem
     * @param string $filePath
     * @param string $fileName
     * @param string $text
     * @param string $fileUrl
     * @return string
     */
    private function copyFromTmp(ImageUploadTextService $oService, $oItem, $filePath, $fileName, $text, $fileUrl)
    {
        // проверяем, что файл является картинкой
        if (exif_imagetype($filePath) === false) {
            // Если не получили имя файла - возвращаем текст без изменения
            return $text;
        }
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $oFile = new UploadedFile($filePath, $fileName, $finfo->file($filePath), filesize($filePath));

        $newFileName = $oService->upload($oFile, $oItem, $this->upload);
        $newFileUrl = $oService->getAssetToSize($oItem, ImageSize::XL) . '/' . $newFileName;

        $text = str_replace($fileUrl, $newFileUrl, $text);
        return $text;
    }

    /**
     * Вытащить из кэша
     *
     * @param string $uid
     * @return mixed
     */
    private function cacheTextImageGet($uid)
    {
        $key = $this->cacheTextImageKey($uid);
        return Cache::get($key);
    }

    /**
     * Занести данные
     *
     * @param string $uid
     * @param array $aData
     */
    private function cacheTextImageSet($uid, $aData = [])
    {
        $key = $this->cacheTextImageKey($uid);
        Cache::put($key, $aData, null);
    }

    /**
     * @param string $uid
     */
    private function cacheTextImageDelete($uid)
    {
        $key = $this->cacheTextImageKey($uid);
        Cache::forget($key);
    }

    /**
     * Смержить данные из кэша
     *
     * @param string $uid
     * @param array $aData
     */
    private function cacheTextImageMerge($uid, $aData = [])
    {
        $aOld = $this->cacheTextImageGet($uid);
        if (is_null($aOld)) {
            $aOld = [];
        }
        $aData = array_merge($aOld, $aData);
        $this->cacheTextImageSet($uid, $aData);
    }

    /**
     * Идентификатор сессии, пример 201906211848-9f3RbwxfJAWBW409
     *
     * @param string $uid
     * @return string
     */
    private function cacheTextImageKey(string $uid)
    {
        return 'upload:tmp:' . $uid;
    }
}
