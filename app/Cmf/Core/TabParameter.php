<?php

declare(strict_types=1);

namespace App\Cmf\Core;

class TabParameter
{
    /**
     *
     */
    const TAB_MAIN = 'tabs.main';
    const TAB_MAIN_CONTENT = [
        'title' => 'Данные',
        'tabs_attributes' => [
            'aria-controls' => 'tab-main',
            'aria-expanded' => 'true',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'true',
        ],
    ];

    /**
     *
     */
    const TAB_DEPARTURES = 'tabs.departures';
    const TAB_DEPARTURES_CONTENT = [
        'title' => 'Отправления',
        'tabs_attributes' => [
            'aria-controls' => 'tab-departures',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_MARKDOWN = 'tabs.markdown';
    const TAB_MARKDOWN_CONTENT = [
        'title' => 'Полное описание',
        'tabs_attributes' => [
            'aria-controls' => 'tab-markdown',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_PARAMETERS = 'tabs.parameters';
    const TAB_PARAMETERS_CONTENT = [
        'title' => 'Параметры',
        'tabs_attributes' => [
            'aria-controls' => 'tab-parameters',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_INCLUDES = 'tabs.includes';
    const TAB_INCLUDES_CONTENT = [
        'title' => 'Что входит',
        'tabs_attributes' => [
            'aria-controls' => 'tab-includes',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_PRICES = 'tabs.prices';
    const TAB_PRICES_CONTENT = [
        'title' => 'Цена',
        'tabs_attributes' => [
            'aria-controls' => 'tab-prices',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_VIDEO = 'tabs.video';
    const TAB_VIDEO_CONTENT = [
        'title' => 'Видео',
        'tabs_attributes' => [
            'aria-controls' => 'tab-video',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_RESERVATION = 'tabs.reservation';
    const TAB_RESERVATION_CONTENT = [
        'title' => 'Как забронировать',
        'tabs_attributes' => [
            'aria-controls' => 'tab-reservation',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_CANCELLATION = 'tabs.cancellation';
    const TAB_CANCELLATION_CONTENT = [
        'title' => 'Отмена тура',
        'tabs_attributes' => [
            'aria-controls' => 'tab-cancellation',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 1,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_ANNOUNCEMENT = 'tabs.announcement';
    const TAB_ANNOUNCEMENT_CONTENT = [
        'title' => 'Анонс тура',
        'tabs_attributes' => [
            'aria-controls' => 'tab-announcement',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 1,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_QUESTIONS = 'tabs.questions';
    const TAB_QUESTIONS_CONTENT = [
        'title' => 'Вопросы (FAQ)',
        'tabs_attributes' => [
            'aria-controls' => 'tab-questions',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_DUPLICATE = 'tabs.duplicate';
    const TAB_DUPLICATE_CONTENT = [
        'title' => 'Дубликаты',
        'tabs_attributes' => [
            'aria-controls' => 'tab-duplicate',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 1,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_META_TAGS = 'tabs.meta_tags';
    const TAB_META_TAGS_CONTENT = [
        'title' => 'Мета Тэги',
        'tabs_attributes' => [
            'aria-controls' => 'tab-meta_tags',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_WORD_DECLENSIONS = 'tabs.word_declensions';
    const TAB_WORD_DECLENSIONS_CONTENT = [
        'title' => 'Склонения',
        'tabs_attributes' => [
            'aria-controls' => 'tab-word_declensions',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_GEO_COORDINATES = 'tabs.geo.coordinates';
    const TAB_GEO_COORDINATES_CONTENT = [
        'title' => 'Координаты',
        'tabs_attributes' => [
            'aria-controls' => 'tab-coordinates',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_COMMENTS = 'tabs.comments';
    const TAB_COMMENTS_CONTENT = [
        'title' => 'Комментарии',
        'tabs_attributes' => [
            'aria-controls' => 'tab-comments',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 1,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_SOCIALS = 'tabs.socials';
    const TAB_SOCIALS_CONTENT = [
        'title' => 'Социальные сети',
        'tabs_attributes' => [
            'aria-controls' => 'tab-socials',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_SOURCE = 'tabs.source';
    const TAB_SOURCE_CONTENT = [
        'title' => 'Источник',
        'tabs_attributes' => [
            'aria-controls' => 'tab-source',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];

    /**
     *
     */
    const TAB_IMAGES_MODEL = 'tabs.images.model';
    const TAB_IMAGES_MODEL_CONTENT = [
        'title' => 'Изображение',
    ];

    /**
     *
     */
    const TAB_IMAGES_BANNER = 'tabs.images.banner';
    const TAB_IMAGES_BANNER_CONTENT = [
        'title' => 'Баннер',
    ];

    /**
     *
     */
    const TAB_IMAGES_GALLERY = 'tabs.images.gallery';
    const TAB_IMAGES_GALLERY_CONTENT = [
        'title' => 'Галлерея',
    ];

    /**
     *
     */
    const TAB_OUTFITS_RENT = 'tabs.outfits';
    const TAB_OUTFITS_RENT_CONTENT = [
        'title' => 'Аренда снаряжения',
        'tabs_attributes' => [
            'aria-controls' => 'tab-outfits',
            'aria-expanded' => 'false',
            'data-hidden-submit' => 0,
        ],
        'content_attributes' => [
            'aria-expanded' => 'false',
        ],
    ];
}
