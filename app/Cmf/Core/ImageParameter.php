<?php

declare(strict_types=1);

namespace App\Cmf\Core;

class ImageParameter
{
    /**
     *
     */
    const IMAGE_SIZE_XS_CONTENT = [
        'filter' => \App\Services\Image\Filters\SquareFilter::class,
        'options' => [
            'dimension' => '1:1',
            'size' => 36,
        ],
    ];

    /**
     *
     */
    const IMAGE_SIZE_SQUARE_CONTENT = [
        'filter' => \App\Services\Image\Filters\SquareFilter::class,
        'options' => [
            'dimension' => '1:1',
            'size' => 360,
        ],
    ];

    /**
     *
     */
    const IMAGE_SIZE_XL_CONTENT = [
        'filter' => \App\Services\Image\Filters\SquareFilter::class,
        'options' => [
            'size' => 1280,
        ],
    ];
}
