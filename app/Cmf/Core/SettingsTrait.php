<?php

declare(strict_types=1);

namespace App\Cmf\Core;

trait SettingsTrait
{
    /**
     * Сообщения об успехе
     *
     * @var array
     */
    public $toastText = [
        'store' => 'Данные успешно добавлены.',
        'update' => 'Данные успешно изменены.',
        'destroy' => 'Данные успешно удалены.',
        'status' => 'Статус успешно изменен.',
    ];
}
