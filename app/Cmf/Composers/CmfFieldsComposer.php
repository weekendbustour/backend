<?php

declare(strict_types=1);

namespace App\Cmf\Composers;

use App\Http\Composers\CommonComposersTrait;
use Illuminate\Contracts\View\View;

class CmfFieldsComposer
{
    use CommonComposersTrait;

    private $fields;

    public function __construct()
    {
        $this->fields = config('cmf.cache.fields')
            ? $this->getFieldsCache()
            : $this->getFields(app_path('Cmf/Project'));
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('aComposerFields', $this->fields);
    }
}
