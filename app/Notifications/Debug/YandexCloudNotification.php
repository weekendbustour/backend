<?php

declare(strict_types=1);

namespace App\Notifications\Debug;

use App\Jobs\QueueCommon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\AnonymousNotifiable;

class YandexCloudNotification extends DebugCommonNotification implements ShouldQueue
{
    use Queueable;

    /**
     *
     */
    const TYPE_ERROR = 'error';

    /**
     *
     */
    const TYPE_DEFAULT = 'default';

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var string
     */
    private $type = null;

    /**
     * DebugNotification constructor.
     * @param string $message
     * @param string $type
     */
    public function __construct(string $message, string $type = self::TYPE_DEFAULT)
    {
        parent::__construct();
        $this->message = $message;
        $this->type = $type;
    }

    /**
     * @param AnonymousNotifiable $notifiable
     * @return array
     */
    public function via(AnonymousNotifiable $notifiable)
    {
        return ['slack'];
    }

    /**
     * @return array
     */
    public function tags()
    {
        return ['notification', 'notification:yandex-cloud'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param AnonymousNotifiable $notifiable
     * @return SlackMessage
     */
    public function toSlack(AnonymousNotifiable $notifiable)
    {
        if ($this->type === self::TYPE_ERROR) {
            return (new SlackMessage())->from($this->host)->error()->content($this->message);
        } else {
            return (new SlackMessage())->from($this->host)->content($this->message);
        }
    }
}
