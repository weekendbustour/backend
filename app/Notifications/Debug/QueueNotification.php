<?php

declare(strict_types=1);

namespace App\Notifications\Debug;

use App\Jobs\QueueCommon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\AnonymousNotifiable;

class QueueNotification extends DebugCommonNotification implements ShouldQueue
{
    use Queueable;

    private $message = '';
    private $tag = '';

    /**
     * DebugNotification constructor.
     * @param string $message
     * @param string $tag
     */
    public function __construct(string $message, string $tag)
    {
        parent::__construct();
        $this->message = $message;
        $this->tag = $tag;
    }

    /**
     * @param AnonymousNotifiable $notifiable
     * @return array
     */
    public function via(AnonymousNotifiable $notifiable)
    {
        return ['slack'];
    }

    /**
     * @return array
     */
    public function tags()
    {
        return ['notification', 'notification:queue', $this->tag];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param AnonymousNotifiable $notifiable
     * @return SlackMessage
     */
    public function toSlack(AnonymousNotifiable $notifiable)
    {
        return (new SlackMessage())->from($this->host)->content($this->message);
    }
}
