<?php

declare(strict_types=1);

namespace App\Notifications\Debug;

use App\Jobs\QueueCommon;
use App\Services\Environment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\SlackAttachment;
use Illuminate\Notifications\Messages\SlackMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\AnonymousNotifiable;

class DebugNotification extends DebugCommonNotification implements ShouldQueue
{
    use Queueable;

    /**
     * @var string
     */
    private $message = '';

    /**
     * @var string
     */
    private $type = 'default';

    /**
     * DebugNotification constructor.
     * @param string $message
     * @param string $type
     */
    public function __construct(string $message, string $type = 'default')
    {
        parent::__construct();
        $this->message = $message;
        $this->type = $type;
    }

    /**
     * Determine which queues should be used for each notification channel.
     *
     * @return array
     */
    public function viaQueues()
    {
        return [
            'slack' => QueueCommon::QUEUE_NAME_NOTIFICATION,
        ];
    }

    /**
     * @param AnonymousNotifiable $notifiable
     * @return array
     */
    public function via(AnonymousNotifiable $notifiable)
    {
        return ['slack'];
    }

    /**
     * @return array
     */
    public function tags()
    {
        return ['notification', 'notification:debug'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param AnonymousNotifiable $notifiable
     * @return SlackMessage
     */
    public function toSlack(AnonymousNotifiable $notifiable)
    {
        return (new SlackMessage())->from($this->host)->content($this->message);
//
//        return $slack->from($env)->attachment(function (SlackAttachment $attachment) use ($env) {
//            //$attachment->title($env . ':' . config('app.url'));
//            $attachment->pretext($this->message);
//            //$attachment->content($this->message);
//            $attachment->footer($env);
//        });
    }
}
