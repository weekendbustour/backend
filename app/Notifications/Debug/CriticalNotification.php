<?php

declare(strict_types=1);

namespace App\Notifications\Debug;

use App\Jobs\QueueCommon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\AnonymousNotifiable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\SlackMessage;

class CriticalNotification extends DebugCommonNotification implements ShouldQueue
{
    use Queueable;

    private $message = '';

    /**
     * DebugNotification constructor.
     * @param string $message
     */
    public function __construct(string $message)
    {
        parent::__construct();
        $this->message = $message;
    }

    public function via(AnonymousNotifiable $notifiable)
    {
        return ['slack'];
    }

    /**
     * @return array
     */
    public function tags()
    {
        return ['notification', 'notification:critical'];
    }

    /**
     * Get the Slack representation of the notification.
     *
     * @param AnonymousNotifiable $notifiable
     * @return SlackMessage
     */
    public function toSlack(AnonymousNotifiable $notifiable)
    {
        return (new SlackMessage())->from($this->host)->content($this->message);
    }
}
