<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// @codingStandardsIgnoreLine
class CreateCommentsTable extends Migration
{
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('commentable_id')->nullable();
            $table->string('commentable_type')->nullable();
            $table->integer('commented_id')->nullable();
            $table->string('commented_type')->nullable();
            $table->longText('text');
            $table->boolean('approved')->default(1);
            $table->integer('rate')->nullable();
            $table->boolean('anon')->default(0);
            $table->bigInteger('rating_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('reply_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('parent_id')->unsigned()->nullable()->default(null);
            $table->timestamps();

            $table->foreign('rating_id')->references('id')->on('ratings')->onDelete('cascade');
            $table->foreign('reply_id')->references('id')->on('comments')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('comments')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropForeign(['rating_id']);
            $table->dropForeign(['reply_id']);
            $table->dropForeign(['parent_id']);
        });
        Schema::drop('comments');
    }
}
