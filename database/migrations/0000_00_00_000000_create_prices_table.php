<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// @codingStandardsIgnoreLine
class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->nullable()->default(null);
            $table->integer('priceable_id');
            $table->string('priceable_type');

            $table->integer('value')->unsigned()->nullable()->default(null);
            $table->string('value_description')->nullable()->default(null);

            $table->integer('discount')->unsigned()->nullable()->default(null);
            $table->string('discount_description')->nullable()->default(null);
            $table->timestamp('discount_before_at')->nullable()->default(null);

            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
