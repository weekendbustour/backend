<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateDeparturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('organization_id')->unsigned()->nullable()->default(null);
            $table->string('name');
            $table->string('title');
            $table->longText('description')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('set null');
        });

        Schema::create('tour_departure', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tour_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('departure_id')->unsigned()->nullable()->default(null);
            $table->timestamp('departure_at')->nullable()->default(null);
            $table->boolean('departure_at_hourly')->unsigned()->default(0);
            $table->string('description')->nullable()->default(null); // Адрес сбора г.Шахты:площадь Ленина,1 ( напротив главного входа в «ШАДИ»)
            $table->bigInteger('location_id')->unsigned()->nullable()->default(null); // координаты по карте
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
            $table->foreign('departure_id')->references('id')->on('departures')->onDelete('cascade');
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
        });

        Schema::create('organization_departure', function (Blueprint $table) {
            $table->unsignedBigInteger('organization_id');
            $table->unsignedBigInteger('departure_id');

            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
            $table->foreign('departure_id')->references('id')->on('departures')->onDelete('cascade');

            $table->primary(['organization_id', 'departure_id'], 'organization_departure_organization_id_departure_id_primary');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tour_departure');
        Schema::enableForeignKeyConstraints();

        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('organization_departure');
        Schema::enableForeignKeyConstraints();

        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('departures');
        Schema::enableForeignKeyConstraints();
    }
}
