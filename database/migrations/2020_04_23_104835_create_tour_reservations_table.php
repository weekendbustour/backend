<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateTourReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_reservations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tour_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('user_id')->unsigned()->nullable()->default(null);
            $table->string('phone', 20)->nullable()->default(null);
            $table->decimal('amount', 8, 2)->default(0);
            $table->string('code')->nullable()->default(null);
            $table->bigInteger('payment_id')->unsigned()->nullable()->default(null);
            $table->timestamp('payment_at')->nullable()->default(null);
            $table->string('source')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('payment_id')->references('id')->on('payments')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tour_reservations', function (Blueprint $table) {
            $table->dropForeign('tour_reservations_tour_id_foreign');
        });
        Schema::table('tour_reservations', function (Blueprint $table) {
            $table->dropForeign('tour_reservations_user_id_foreign');
        });
        Schema::table('tour_reservations', function (Blueprint $table) {
            $table->dropForeign('tour_reservations_payment_id_foreign');
        });
        Schema::dropIfExists('tour_reservations');
    }
}
