<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateQuestionsTable extends Migration
{
    use \App\Services\Database\Migration\MigrationFieldTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->default(\App\Models\Question::TYPE_DEFAULT);
            $table->bigInteger('questionable_id')->nullable()->default(null);
            $table->string('questionable_type')->nullable()->default(null);
            $table->string('question')->nullable()->default(null);
            $table->text('answer')->nullable()->default(null);
            $this->priority($table);
            $this->status($table);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
