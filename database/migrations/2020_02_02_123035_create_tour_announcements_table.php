<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateTourAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tour_announcements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tour_id')->unsigned()->nullable()->default(null);
            $table->string('type');
            $table->string('tooltip')->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);

            $table->timestamp('start_at')->nullable()->default(null);
            $table->boolean('start_at_hourly')->unsigned()->default(0);

            $table->timestamp('finish_at')->nullable()->default(null);
            $table->boolean('finish_at_hourly')->unsigned()->default(0);

            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tour_announcements');
        Schema::enableForeignKeyConstraints();
    }
}
