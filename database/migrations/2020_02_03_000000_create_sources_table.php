<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// @codingStandardsIgnoreLine
class CreateSourcesTable extends Migration
{
    use \App\Services\Database\Migration\MigrationFieldTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->nullable()->default(\App\Models\Source::TYPE_DEFAULT);
            $table->bigInteger('organization_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('sourceable_id');
            $table->string('sourceable_type');
            $table->string('url')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->timestamp('published_at')->nullable()->default(null);
            $this->status($table);
            $table->timestamps();

            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sources', function (Blueprint $table) {
            $table->dropForeign('sources_organization_id_foreign');
        });
        Schema::dropIfExists('sources');
    }
}
