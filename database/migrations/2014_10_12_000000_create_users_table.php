<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('login')->unique();
            $table->string('phone')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('first_name')->nullable();
            $table->string('second_name')->nullable()->default(null);
            $table->string('last_name')->nullable()->default(null);
            $table->string('password');
            $table->rememberToken();
            $table->date('birthday_at')->nullable()->default(null);
            $table->boolean('gender')->unsigned()->nullable()->default(null);
            $table->boolean('agree')->unsigned()->default(0);
            $table->boolean('mailing')->unsigned()->default(0);
            $table->boolean('is_online')->unsigned()->default(0);
            $table->bigInteger('parent_id')->unsigned()->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_parent_id_foreign');
        });
        Schema::dropIfExists('users');
    }
}
