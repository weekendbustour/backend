<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateWeathersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weathers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('location_id')->unsigned();
            $table->string('icon')->nullable()->default(null); // тип
            $table->string('type')->nullable()->default(null); // тип
            $table->string('condition')->nullable()->default(null); // температура
            $table->tinyInteger('temp')->nullable()->default(null); // температура
            $table->tinyInteger('feels_like')->nullable()->default(null); // ощущается как
            $table->string('precipitation')->nullable()->default(null); // осадки
            $table->string('pressure')->nullable()->default(null); // давление
            $table->string('humidity')->nullable()->default(null); // влажность
            $table->string('wind')->nullable()->default(null); // ветер
            $table->string('cloudness')->nullable()->default(null); // ветер
            $table->text('options')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('weathers');
        Schema::enableForeignKeyConstraints();
    }
}
