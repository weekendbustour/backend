<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->nullable()->default(null);
            $table->decimal('amount', 8, 2)->default(0);
            $table->string('code')->nullable()->default(null);
            $table->text('options')->nullable()->default(null);
            $table->text('purpose')->nullable()->default(null);
            $table->string('source')->nullable()->default(null);
            $table->bigInteger('external_id')->nullable()->default(null);
            $table->timestamp('opened_at')->nullable()->default(null);
            $table->timestamp('closed_at')->nullable()->default(null);
            $table->bigInteger('parent_id')->unsigned()->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('payments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('payments_parent_id_foreign');
        });
        Schema::dropIfExists('payments');
    }
}
