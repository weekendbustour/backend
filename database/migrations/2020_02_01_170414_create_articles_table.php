<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('position')->nullable()->default(\App\Models\Article::POSITION_DEFAULT);
            $table->string('name')->nullable()->default(null);
            $table->string('title');
            $table->text('preview_description')->nullable()->default(null);
            $table->longText('description')->nullable()->default(null);
            $table->text('tmp_source')->nullable()->default(null);
            $table->timestamp('release_at')->nullable()->default(null);
            $table->timestamp('published_at')->nullable()->default(null);

            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::drop('articles');
        Schema::enableForeignKeyConstraints();
    }
}
