<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalVkParametersTable extends Migration
{
    use \App\Services\Database\Migration\MigrationFieldTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_vk_parameters', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->bigInteger('external_vk_parametrable_id');
            $table->string('external_vk_parametrable_type');
            $table->bigInteger('item_id')->nullable()->default(null);
            $table->bigInteger('photo_id')->nullable()->default(null);
            $table->longText('images')->nullable()->default(null);
            $table->bigInteger('album_id')->nullable()->default(null);
            $table->bigInteger('category_id')->nullable()->default(null);
            $table->timestamp('published_at')->nullable()->default(null);
            $this->status($table);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_vk_parameters');
    }
}
