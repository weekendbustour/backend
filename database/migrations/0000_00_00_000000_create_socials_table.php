<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// @codingStandardsIgnoreLine
class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->nullable()->default(null);
            $table->integer('socialable_id');
            $table->string('socialable_type');
            $table->text('options')->nullable()->default(null);
            $table->string('url')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->boolean('is_main')->unsigned()->default(0);
            $table->tinyInteger('priority')->unsigned()->unsigned()->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('socials');
    }
}
