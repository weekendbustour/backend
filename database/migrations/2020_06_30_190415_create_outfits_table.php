<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOutfitsTable extends Migration
{
    use \App\Services\Database\Migration\MigrationFieldTrait;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outfits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('title');
            $table->text('preview_description')->nullable()->default(null);
            $table->string('tooltip')->nullable()->default(null);
            $this->priority($table);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();
        });

        Schema::create('tour_outfit', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tour_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('outfit_id')->unsigned()->nullable()->default(null);
            $table->boolean('rent')->unsigned()->default(0);
            $table->string('rent_price')->nullable()->default(null);
            $table->string('rent_description')->nullable()->default(null);
            $table->timestamps();

            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
            $table->foreign('outfit_id')->references('id')->on('outfits')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tour_outfit');
        Schema::enableForeignKeyConstraints();

        Schema::dropIfExists('outfits');
    }
}
