<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateToursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('organization_id')->unsigned()->nullable()->default(null);
            $table->bigInteger('direction_id')->unsigned()->nullable()->default(null);
            $table->string('name');
            $table->string('title');
            $table->longText('description')->nullable()->default(null);
            $table->string('preview_description')->nullable()->default(null);

            $table->bigInteger('tag_id')->unsigned()->nullable()->default(null);
            //$table->integer('days')->unsigned()->default(1);
            //$table->integer('places')->unsigned()->nullable()->default(null);

            //$table->text('include')->nullable()->default(null);
            //$table->text('not_include')->nullable()->default(null);

            //$table->integer('price')->unsigned()->nullable()->default(null);
            //$table->integer('price_discount')->unsigned()->nullable()->default(null);
            //$table->string('price_description')->nullable()->default(null);

            $table->timestamp('start_at')->nullable()->default(null);
            $table->timestamp('finish_at')->nullable()->default(null);
            $table->timestamp('published_at')->nullable()->default(null);
            $table->timestamp('release_at')->nullable()->default(null);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('set null');
            $table->foreign('direction_id')->references('id')->on('directions')->onDelete('set null');
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tours', function (Blueprint $table) {
            $table->dropForeign('tours_organization_id_foreign');
            $table->dropForeign('tours_direction_id_foreign');
            $table->dropForeign('tours_tag_id_foreign');
        });
        Schema::dropIfExists('tours');
    }
}
