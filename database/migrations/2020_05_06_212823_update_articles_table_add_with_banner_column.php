<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class UpdateArticlesTableAddWithBannerColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->boolean('with_banner')->unsigned()->default(1)->after('tmp_source');
        });
        Schema::table('news', function (Blueprint $table) {
            $table->boolean('with_banner')->unsigned()->default(1)->after('source');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('articles', function (Blueprint $table) {
            $table->dropColumn('with_banner');
        });
        Schema::table('news', function (Blueprint $table) {
            $table->dropColumn('with_banner');
        });
    }
}
