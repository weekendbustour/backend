<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

// @codingStandardsIgnoreLine
class CreateDirectionStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('direction_statistics', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('direction_id')->unsigned();
            $table->integer('views')->unsigned()->default(0);
            $table->integer('comments')->unsigned()->default(0);
            $table->integer('redirects')->unsigned()->default(0);
            $table->integer('likes')->unsigned()->default(0);
            $table->integer('favourites')->unsigned()->default(0);
            $table->timestamps();

            $table->foreign('direction_id')->references('id')->on('directions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('direction_statistics');
        Schema::enableForeignKeyConstraints();
    }
}
