<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

// @codingStandardsIgnoreLine
class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tags', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('title');
            $table->text('description')->nullable()->default(null);
            $table->bigInteger('parent_id')->unsigned()->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->integer('priority')->unsigned()->default(0);
            $table->tinyInteger('status')->default(1);
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('tags')->onDelete('cascade');
        });
//
//        Schema::create('tour_tag', function (Blueprint $table) {
//            $table->unsignedBigInteger('tour_id');
//            $table->unsignedBigInteger('tag_id');
//
//            $table->foreign('tour_id')->references('id')->on('tours')->onDelete('cascade');
//            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
//
//            $table->primary(['tour_id', 'tag_id'], 'tour_tag_tour_id_tag_id_primary');
//        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::disableForeignKeyConstraints();
//        Schema::dropIfExists('tour_tag');
//        Schema::enableForeignKeyConstraints();

        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('tags');
        Schema::enableForeignKeyConstraints();
    }
}
