<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$fakerRU = \Faker\Factory::create('ru_RU');

$factory->define(\App\Models\Comment::class, function (Faker $faker) use ($fakerRU) {
    return [
        'commentable_id' => 1,
        'commentable_type' => 1,
        'text' => $fakerRU->text(1000),
        'approved' => 1,
        'rate' => $faker->numberBetween(-200, 200),
        'created_at' => $faker->date(),
    ];
});
