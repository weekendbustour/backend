<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$fakerRU = \Faker\Factory::create('ru_RU');

$factory->define(\App\Models\User::class, function (Faker $faker) use ($fakerRU) {
    return [
        'parent_id' => null,
        'login' => $faker->text(63),
        'phone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => $faker->dateTime,
        'first_name' => $fakerRU->firstName,
        'second_name' => $fakerRU->name,
        'last_name' => $fakerRU->lastName,
        'password' => '$2y$10$uTDnsRa0h7wLppc8/vB9C.YqsrAZwhjCgLWjcmpbndTmyo1k5tbRC',
        'remember_token' => $faker->sha1,
        'birthday_at' => $faker->dateTime,
        'gender' => 1,
        'agree' => 1,
        'mailing' => 1,
        'is_online' => 0,
        'status' => 1,
    ];
});
