<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$factory->define(\App\Models\Client::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(0, 4294967295),
        'first_name' => $faker->firstName,
        'second_name' => $faker->text(63),
        'last_name' => $faker->lastName,
        'status' => $faker->boolean(50),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime,
    ];
});
