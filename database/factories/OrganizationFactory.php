<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$fakerRU = \Faker\Factory::create('ru_RU');

$factory->define(\App\Models\Organization::class, function (Faker $faker) use ($fakerRU) {
    return [
        'user_id' => null,
        'parent_id' => null,
        'name' => $fakerRU->slug,
        'title' => $fakerRU->sentence(rand(1, 3)),
        'description' => $fakerRU->text(63),
        'status' => 1,
    ];
});
