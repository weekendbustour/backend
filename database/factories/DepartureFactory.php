<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$fakerRU = \Faker\Factory::create('ru_RU');

$factory->define(\App\Models\Departure::class, function (Faker $faker) use ($fakerRU) {
    return [
        'organization_id' => factory(\App\Models\Organization::class)->create()->id,
        'name' => $fakerRU->slug,
        'title' => $fakerRU->sentence(3),
        'description' => $fakerRU->text(63),
        'status' => 1,
    ];
});
