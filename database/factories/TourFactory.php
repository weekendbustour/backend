<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$fakerRU = \Faker\Factory::create('ru_RU');

$factory->define(\App\Models\Tour::class, function (Faker $faker) use ($fakerRU) {
    return [
        'organization_id' => factory(\App\Models\Organization::class)->create()->id,
        'direction_id' => factory(\App\Models\Direction::class)->create()->id,
        'name' => $fakerRU->slug,
        'title' => $fakerRU->sentence(rand(1, 6)),
        'description' => $fakerRU->text(64),
        //'days' => $faker->numberBetween(1, 7),
        //'places' => $faker->numberBetween(5, 20),
        //'include' => rand(0, 1) ? $fakerRU->text(10) : null,
        //'not_include' => rand(0, 1) ? $fakerRU->text(10) : null,
        //'price' => $faker->numberBetween(100, 5000),
        //'price_discount' => null,
        //'publication_at' => now(),
        'published_at' => now(),
        'start_at' => now()->addDays($faker->numberBetween(1, 30)),
        'finish_at' => now()->addDays(11),
        'status' => 1,
    ];
});
