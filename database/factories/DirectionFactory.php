<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use Faker\Generator as Faker;

$fakerRU = \Faker\Factory::create('ru_RU');

$factory->define(\App\Models\Direction::class, function (Faker $faker) use ($fakerRU) {
    return [
        'name' => $fakerRU->slug,
        'title' => $fakerRU->sentence(1),
        'description' => $fakerRU->text(63),
        'status' => 1,
    ];
});
