<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CategoriesTableSeeder extends Seeder
{
    private $data = [
        [
            'title' => 'Экстрим',
        ], [
            'title' => 'Поход',
        ], [
            'title' => 'Экскурсия',
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->data as $data) {
            factory(\App\Models\Category::class)->create([
                'title' => $data['title'],
            ]);
        }
    }
}
