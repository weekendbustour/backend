<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;

// @codingStandardsIgnoreLine
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(OptionsParametersTableSeeder::class);
        $this->call(OutfitsTableSeeder::class);
        $this->call(EntertainmentsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        //$this->call(CategoriesTableSeeder::class);
        //$this->call(ToursTableSeeder::class);
    }
}
