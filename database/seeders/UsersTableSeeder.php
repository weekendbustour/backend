<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // супер админ
        $admin = User::create([
            'first_name' => 'Админ',
            'login' => 'admin',
            'email' => 'admin@admin.com',
            'phone' => '79889494718',
            'password' => Hash::make('1234567890'),
        ]);
        $admin->assignRole(User::ROLE_SUPER_ADMIN);
    }
}
