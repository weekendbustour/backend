<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Entertainment;

class OutfitsTableSeeder extends Seeder
{
    use CommonDatabaseSeeder;

    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Палатка',
        ], [
            'title' => 'Спальник',
        ], [
            'title' => 'Коврик',
            'tooltip' => 'Каремат',
        ], [
            'title' => 'Дождевик',
        ], [
            'title' => 'Рюкзак',
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->before(\App\Models\Outfit::class);

        $total = count($this->default);
        foreach ($this->default as $key => $value) {
            $oItem = \App\Models\Outfit::create([
                'title' => $value['title'],
                'preview_description' => isset($value['preview_description']) ? $value['preview_description'] : null,
                'tooltip' => isset($value['tooltip']) ? $value['tooltip'] : null,
                'priority' => $this->priority($key, $total),
                'status' => $value['status'] ?? 1,
            ]);
        }
        $this->after();
    }
}
