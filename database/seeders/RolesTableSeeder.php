<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        // Просмотр
//        $view = \Spatie\Permission\Models\Permission::findByName(User::PERMISSION_ADMIN_VIEW);
//        if (is_null($view)) {
//            $view = factory(\Spatie\Permission\Models\Permission::class)->create([
//                'name' => User::PERMISSION_ADMIN_VIEW,
//                'title' => 'Возможность просмотра',
//            ]);
//        }
//        // Редактирование
//        $edit = \Spatie\Permission\Models\Permission::findByName(User::PERMISSION_ADMIN_EDIT);
//        if (is_null($edit)) {
//            $edit = factory(\Spatie\Permission\Models\Permission::class)->create([
//                'name' => User::PERMISSION_ADMIN_EDIT,
//                'title' => 'Возможность редактирования',
//            ]);
//        }
//        // Удаление
//        $delete = \Spatie\Permission\Models\Permission::findByName(User::PERMISSION_ADMIN_DELETE);
//        if (is_null($delete)) {
//            $delete = factory(\Spatie\Permission\Models\Permission::class)->create([
//                'name' => User::PERMISSION_ADMIN_DELETE,
//                'title' => 'Возможность удалять',
//            ]);
//        }

        // Супер-Администратор
        $sSuperAdmin = \Spatie\Permission\Models\Role::where('name', User::ROLE_SUPER_ADMIN)->first();
        if (is_null($sSuperAdmin)) {
            $sSuperAdmin = factory(\Spatie\Permission\Models\Role::class)->create([
                'name' => User::ROLE_SUPER_ADMIN,
                'title' => 'Супер Администратор',
            ]);
            //$sSuperAdmin->givePermissionTo($view);
            //$sSuperAdmin->givePermissionTo($edit);
            //$sSuperAdmin->givePermissionTo($delete);
        }

        // Администратор
        $sAdmin = \Spatie\Permission\Models\Role::where('name', User::ROLE_ADMIN)->first();
        if (is_null($sAdmin)) {
            $sAdmin = factory(\Spatie\Permission\Models\Role::class)->create([
                'name' => User::ROLE_ADMIN,
                'title' => 'Администратор',
            ]);
            //$sAdmin->givePermissionTo($view);
            //$sAdmin->givePermissionTo($edit);
        }

        // Пользователь
        $sUser = \Spatie\Permission\Models\Role::where('name', User::ROLE_USER)->first();
        if (is_null($sUser)) {
            $sUser = factory(\Spatie\Permission\Models\Role::class)->create([
                'name' => User::ROLE_USER,
                'title' => 'Пользователь',
            ]);
        }
    }
}
