<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class ToursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $oOrganization = factory(\App\Models\Organization::class)->create();
        $oDeparture = factory(\App\Models\Departure::class)->create([
            'organization_id' => $oOrganization->id,
        ]);
        $oDirection = factory(\App\Models\Direction::class)->create();

        $oTour = factory(\App\Models\Tour::class)->create([
            'organization_id' => $oOrganization->id,
            'direction_id' => $oDirection->id,
        ]);
    }
}
