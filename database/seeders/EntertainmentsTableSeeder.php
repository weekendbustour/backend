<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Entertainment;

class EntertainmentsTableSeeder extends Seeder
{
    use CommonDatabaseSeeder;

    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Полёт на параплане',
            'preview_description' => 'Парапла́н (от слов: парашют планирующий) — сверхлёгкий летательный аппарат (СЛА), созданный на базе планирующего парашюта. Несмотря на внешнее сходство и родство с парашютом (в основе обоих - мягкое крыло, не оборудованное каркасом), они принципиально отличаются тем, что параплан предназначен для горизонтального полёта, а парашют - для вертикального спуска.',
            'priority' => 100,
        ], [
            'title' => 'Роуп-джампинг',
            'preview_description' => 'Роупджампинг (от англ. ropejumping, дословно «верёвочные прыжки») - прыжки на альпинистских веревках с различных высоких объектов (скал, многоэтажных домов, кранов, мостов, труб, различных вышек)',
            'priority' => 100,
        ], [
            'title' => 'Банджи-джампинг',
            'preview_description' => 'Банджи-джа́мпинг (англ. bungee jumping) — аттракцион, часто называемый в России «тарзанка», хотя и имеющий с русской тарзанкой мало общего. На этом аттракционе участников привязывают к длинному резиновому канату, на котором они совершают прыжок вниз.',
            'priority' => 100,
        ], [
            'title' => 'Прогулки на лошадях',
            'preview_description' => 'Прогулки на лошадях - это незабываемый и насыщенный вид отдыха, ощущения после которого вы обязательно захотите испытать снова и снова.',
            'priority' => 100,
        ], [
            'title' => 'Термальные источники',
            'preview_description' => 'Купание в термальных источниках.',
            'priority' => 100,
        ], [
            'title' => 'Джипинг по горам',
            'preview_description' => 'Джипинг на внедорожниках.',
            'priority' => 100,
        ], [
            'title' => 'Водопады',
            'preview_description' => 'Поход к водопадам.',
            'priority' => 100,
        ], [
            'title' => 'Спать в палатках',
            'preview_description' => 'Можно будет спать в палатках.',
            'priority' => 100,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->before(\App\Models\Entertainment::class);

        foreach ($this->default as $key => $value) {
            $oItem = Entertainment::create([
                'title' => $value['title'],
                'preview_description' => isset($value['preview_description']) ? $value['preview_description'] : null,
                'description' => isset($value['description']) ? $value['description'] : null,
                //'name' => $value['name'],
                'status' => $value['status'] ?? 1,
                'priority' => $value['priority'] ?? 1,
            ]);
        }
        $this->after();
    }
}
