<?php

declare(strict_types=1);

namespace Database\Seeders\Options;

use Database\Seeders\OptionsParametersTableSeeder;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Option;
use App\Models\OptionParameter;

class OptionMoreIncludeSeeder extends OptionsParametersTableSeeder
{
    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Дополнительные расходы',
            'name' => Option::NAME_MORE_INCLUDE,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
            'placeholder' => '- одно &#10;- второе',
            'priority' => 0,
            'purpose' => Option::PURPOSE_TOURS,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->default as $key => $value) {
            $this->createOption($value);
        }
    }
}
