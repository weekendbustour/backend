<?php

declare(strict_types=1);

namespace Database\Seeders\Options;

use Database\Seeders\OptionsParametersTableSeeder;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Option;
use App\Models\OptionParameter;

class OptionReservationSeeder extends OptionsParametersTableSeeder
{
    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Как забронировать?',
            'name' => Option::NAME_RESERVATION,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_MARKDOWN,
            'placeholder' => 'Инструкция как забронировать тур',
            'priority' => 0,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Как забронировать?',
            'name' => Option::NAME_RESERVATION,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_MARKDOWN,
            'placeholder' => 'Инструкция как забронировать тур',
            'priority' => 0,
            'purpose' => Option::PURPOSE_ORGANIZATIONS,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->default as $key => $value) {
            $this->createOption($value);
        }
    }
}
