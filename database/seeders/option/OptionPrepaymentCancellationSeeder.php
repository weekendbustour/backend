<?php

declare(strict_types=1);

namespace Database\Seeders\Options;

use App\Models\Option;
use Database\Seeders\OptionsParametersTableSeeder;

class OptionPrepaymentCancellationSeeder extends OptionsParametersTableSeeder
{
    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Условия отмены бронирования',
            'name' => Option::NAME_PREPAYMENT_CANCELLATION,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_MARKDOWN,
            'placeholder' => 'Например: Предоплата возвращается в полном объёме',
            'priority' => 0,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Условия отмены бронирования',
            'name' => Option::NAME_PREPAYMENT_CANCELLATION,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_MARKDOWN,
            'placeholder' => 'Например: Предоплата возвращается в полном объёме',
            'priority' => 0,
            'purpose' => Option::PURPOSE_ORGANIZATIONS,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->default as $key => $value) {
            $this->createOption($value);
        }
    }
}
