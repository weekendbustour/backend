<?php

declare(strict_types=1);

namespace Database\Seeders\Options;

use Database\Seeders\OptionsParametersTableSeeder;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Option;
use App\Models\OptionParameter;

class OptionArticleDistanceSeeder extends OptionsParametersTableSeeder
{
    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'от г. Краснодар',
            'name' => Option::NAME_DISTANCE_FROM_KRASNODAR,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXT,
            'placeholder' => '155 км',
            'priority' => 100,
            'purpose' => Option::PURPOSE_ARTICLES,
        ], [
            'title' => 'от г. Ростов-на-Дону',
            'name' => Option::NAME_DISTANCE_FROM_ROSTOV,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXT,
            'placeholder' => '155 км',
            'priority' => 90,
            'purpose' => Option::PURPOSE_ARTICLES,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->default as $key => $value) {
            $this->createOption($value);
        }
    }
}
