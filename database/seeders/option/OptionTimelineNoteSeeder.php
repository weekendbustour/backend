<?php

declare(strict_types=1);

namespace Database\Seeders\Options;

use Database\Seeders\OptionsParametersTableSeeder;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Option;
use App\Models\OptionParameter;

class OptionTimelineNoteSeeder extends OptionsParametersTableSeeder
{
    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Примечание для Тайминга',
            'name' => Option::NAME_TIMELINE_NOTE,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
            'placeholder' => 'Например: Тайминг путешествия и экскурсии могут меняться в зависимости от погодных условий и иных обстоятельств не зависящих от организаторов.',
            'priority' => 0,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->default as $key => $value) {
            $this->createOption($value);
        }
    }
}
