<?php

declare(strict_types=1);

namespace Database\Seeders\Options;

use Database\Seeders\OptionsParametersTableSeeder;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Option;
use App\Models\OptionParameter;

class OptionArticleYouTubeVideoSeeder extends OptionsParametersTableSeeder
{
    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Видео с Youtube',
            'description' => 'Ссылка на видео',
            'name' => Option::NAME_VIDEO_YOUTUBE,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXT,
            'purpose' => Option::PURPOSE_ARTICLES,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->default as $key => $value) {
            $this->createOption($value);
        }
    }
}
