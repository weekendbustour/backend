<?php

declare(strict_types=1);

namespace Database\Seeders\Options;

use Database\Seeders\OptionsParametersTableSeeder;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Option;
use App\Models\OptionParameter;

class OptionOutfitRentSeeder extends OptionsParametersTableSeeder
{
    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Примечание для Аренды снаряжения',
            'name' => Option::NAME_OUTFIT_RENT_NOTE,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
            'placeholder' => 'Например: Снаряжение арендуется на все дни похода.',
            'purpose' => Option::PURPOSE_TOURS,
            'priority' => 0,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->default as $key => $value) {
            $this->createOption($value);
        }
    }
}
