<?php

declare(strict_types=1);

namespace Database\Seeders\Options;

use Database\Seeders\OptionsParametersTableSeeder;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Option;
use App\Models\OptionParameter;

class OptionAttractionWaySeeder extends OptionsParametersTableSeeder
{
    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Дорога (как добраться)',
            'name' => Option::NAME_WAY,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_MARKDOWN,
            'placeholder' => 'Как добраться',
            'priority' => 100,
            'purpose' => Option::PURPOSE_ATTRACTION,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->default as $key => $value) {
            $this->createOption($value);
        }
    }
}
