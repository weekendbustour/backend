<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent;
use Illuminate\Support\Str;

trait CommonDatabaseSeeder
{
    private function before($model)
    {
        \Eloquent::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        $model::truncate();
    }

    private function after()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

    private function priority($key, $total)
    {
        if ($total < 10) {
            $total = $total * 100;
        } else {
            $total = 100;
        }
        if ($key === 0) {
            $key = 0.7;
        }
        return intval($total / $key);
    }

    private function children($class, $data, $parent = null)
    {
        $oItem = $this->create($class, $data, $parent);
        if (isset($data['children'])) {
            foreach ($data['children'] as $children) {
                $this->children($class, $children, $oItem->id);
            }
        }
    }

    private function create($class, $data, $parent = null)
    {
        if (is_array($data)) {
            if (!is_null($parent)) {
                $data['parent_id'] = $parent;
            }
            $oClass = new $class();
            $aFillable = $oClass->getFillable();
            $a = [];
            foreach ($data as $key => $value) {
                if (in_array($key, $aFillable)) {
                    $a[$key] = $value;
                }
            }
            if (in_array('url', $aFillable) && isset($a['title'])) {
                $a['url'] = Str::slug($a['title']);
            }
            return $class::create($a);
        }
        return null;
    }

    /**
     *
     */
    private function clear()
    {
        Schema::disableForeignKeyConstraints();
        foreach (DB::select('SHOW TABLES') as $k => $v) {
            $table = array_values((array)$v)[0];
            if ($table === 'migrations') {
                continue;
            }
            DB::statement('TRUNCATE TABLE `' . $table . '`');
        }
        Schema::enableForeignKeyConstraints();
    }
}
