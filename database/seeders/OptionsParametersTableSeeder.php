<?php

declare(strict_types=1);

namespace Database\Seeders;

use Database\Seeders\Options\OptionArticleDistanceSeeder;
use Database\Seeders\Options\OptionArticleYouTubeVideoSeeder;
use Database\Seeders\Options\OptionAttractionWaySeeder;
use Database\Seeders\Options\OptionPrepaymentCancellationSeeder;
use Database\Seeders\Options\OptionReservationSeeder;
use Illuminate\Database\Seeder;
use App\Models\Tag;
use App\Models\Option;
use App\Models\OptionParameter;

class OptionsParametersTableSeeder extends Seeder
{
    use CommonDatabaseSeeder;

    /**
     * @var array
     */
    private $default = [
        [
            'title' => 'Количество дней',
            'name' => Option::NAME_DAYS,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_NUMBER,
            'priority' => 100,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Количество мест',
            'name' => Option::NAME_PLACES,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_NUMBER,
            'priority' => 90,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Телефон для справок',
            'name' => Option::NAME_PHONE_INFO,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXT,
            'priority' => 70,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Что входит',
            'placeholder' => '- одно &#10;- второе',
            'name' => Option::NAME_INCLUDE,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Что не входит',
            'placeholder' => '- одно &#10;- второе',
            'name' => Option::NAME_NOT_INCLUDE,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Гид',
            'description' => 'ФИО',
            'name' => Option::NAME_GID,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXT,
            'priority' => 20,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Видео с Youtube',
            'description' => 'Ссылка на видео',
            'name' => Option::NAME_VIDEO_YOUTUBE,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXT,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Видео с Youtube',
            'description' => 'Ссылка на видео',
            'name' => Option::NAME_VIDEO_YOUTUBE,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXT,
            'purpose' => Option::PURPOSE_ORGANIZATIONS,
        ], [
            'title' => 'Время в дороге',
            'description' => 'Примерное',
            'name' => Option::NAME_TAKE_WITH_YOU,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
            'priority' => 80,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Необходимо с собой взять',
            'description' => 'Список предметов',
            'name' => Option::NAME_TIME_ROAD,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
            'priority' => 60,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Что взять с собой в горы',
            'description' => 'Список предметов',
            'name' => Option::NAME_TAKE_WITH_YOU_IN_MOUNT,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
            'priority' => 60,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Фотоотчет',
            'tooltip' => 'Если выбрано, то будет отображаться, если не выбрано, то не будет',
            'name' => Option::NAME_PHOTO_SESSION,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_CHECKBOX,
            'priority' => 60,
            'purpose' => Option::PURPOSE_TOURS,
        ], [
            'title' => 'Примечание для Тайминга',
            'name' => Option::NAME_TIMELINE_NOTE,
            'type' => \App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
            'priority' => 0,
            'purpose' => Option::PURPOSE_TOURS,
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->before(Option::class);
        $this->before(OptionParameter::class);


        foreach ($this->default as $key => $value) {
            $this->createOption($value);
        }

        $this->call(OptionReservationSeeder::class);
        $this->call(OptionPrepaymentCancellationSeeder::class);
        $this->call(OptionAttractionWaySeeder::class);
        $this->call(OptionArticleDistanceSeeder::class);
        $this->call(OptionArticleYouTubeVideoSeeder::class);

        $this->after();
    }

    /**
     * @param array $value
     */
    protected function createOption(array $value)
    {
        $oOption = Option::create([
            'title' => $value['title'],
            'description' => isset($value['description']) ? $value['description'] : null,
            'tooltip' => isset($value['tooltip']) ? $value['tooltip'] : null,
            'placeholder' => isset($value['placeholder']) ? $value['placeholder'] : null,
            'purpose' => isset($value['purpose']) ? $value['purpose'] : Option::PURPOSE_TOURS,
            'name' => $value['name'],
            'type' => $value['type'] ?? \App\Cmf\Core\MainController::DATA_TYPE_TEXT,
            'status' => $value['status'] ?? 1,
        ]);
        if (isset($value['parameters'])) {
            $total = count($value['parameters']);
            foreach ($value['parameters'] as $sKey => $parameter) {
                OptionParameter::create([
                    'option_id' => $oOption->id,
                    'quantity' => $parameter['quantity'],
                    'priority' => $this->priority($sKey, $total)
                ]);
            }
        }
    }
}
