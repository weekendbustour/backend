
### Тестирование
`./vendor/bin/phpcs -n -s -p --report-full --colors --standard=PSR1,PSR2,PSR12 --exclude=Generic.Files.LineEndings ./app || true`

`./vendor/bin/phpstan analyse`

### Фронтенд
`cd public/frontend`
`node_modules/.bin/eslint src/`
