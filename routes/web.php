<?php

use Illuminate\Support\Facades\Route;
use App\Cmf\Project\DevController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\OrganizationController;
use App\Http\Controllers\DirectionController;
use App\Http\Controllers\TourController;
use App\Http\Controllers\LkController;
use App\Http\Controllers\PromotionController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ArticleController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\AttractionController;

Route::group([
    'domain' => env('APP_URL'),
    'middleware' => [],
    'as' => 'app.',
], function () {

    Route::get('/', ['uses' => '\\' . IndexController::class . '@index', 'as' => 'index']);
    Route::get('/search', ['uses' => '\\' . IndexController::class . '@search', 'as' => 'search']);
    Route::get('/about', ['uses' => '\\' . IndexController::class . '@about', 'as' => 'about']);
    Route::get('/contacts', ['uses' => '\\' . IndexController::class . '@contacts', 'as' => 'contacts']);
    Route::get('/help', ['uses' => '\\' . IndexController::class . '@help', 'as' => 'help']);
    Route::get('/terms', ['uses' => '\\' . IndexController::class . '@terms', 'as' => 'terms']);
    Route::get('/privacy', ['uses' => '\\' . IndexController::class . '@privacy', 'as' => 'privacy']);

    Route::get('/tours', ['uses' => '\\' . TourController::class . '@tours', 'as' => 'tours']);
    Route::get('/tour/{id}/{name?}', ['uses' => '\\' . TourController::class . '@tour', 'as' => 'tour']);

    Route::get('/news', ['uses' => '\\' . NewsController::class . '@news', 'as' => 'news']);
    Route::get('/news/{id}/{name?}', ['uses' => '\\' . NewsController::class . '@newsItem', 'as' => 'news.item']);

    Route::get('/directions', ['uses' => '\\' . DirectionController::class . '@directions', 'as' => 'directions']);
    Route::get('/direction/{id}/attractions', ['uses' => '\\' . DirectionController::class . '@directionAttractions', 'as' => 'direction.attractions']);
    Route::get('/direction/{id}/{name?}', ['uses' => '\\' . DirectionController::class . '@direction', 'as' => 'direction']);

    Route::get('/articles', ['uses' => '\\' . ArticleController::class . '@articles', 'as' => 'articles']);
    Route::get('/article/{id}/{name?}', ['uses' => '\\' . ArticleController::class . '@article', 'as' => 'article']);

    Route::get('/attractions', ['uses' => '\\' . AttractionController::class . '@attractions', 'as' => 'attractions']);
    Route::get('/attraction/{id}/{name?}', ['uses' => '\\' . AttractionController::class . '@attraction', 'as' => 'attraction']);

    Route::get('/organizations', ['uses' => '\\' . OrganizationController::class . '@organizations', 'as' => 'organizations']);
    Route::get('/organization/{id}/articles', ['uses' => '\\' . OrganizationController::class . '@organizationArticles', 'as' => 'organization.articles']);
    Route::get('/organization/{id}/{name?}', ['uses' => '\\' . OrganizationController::class . '@organization', 'as' => 'organization']);

    Route::get('/promotion/{id}/{name?}', ['uses' => '\\' . PromotionController::class . '@promotion', 'as' => 'promotion']);

    Route::get('/category/{id}/{name?}', ['uses' => '\\' . CategoryController::class . '@category', 'as' => 'category']);

    Route::get('/auth/login', ['uses' => '\\' . AuthController::class . '@login', 'as' => 'auth.login']);
    Route::get('/auth/register', ['uses' => '\\' . AuthController::class . '@register', 'as' => 'auth.register']);
    Route::get('/auth/logout', ['uses' => '\\' . AuthController::class . '@logout', 'as' => 'auth.logout']);


    Route::get('/lk/account', ['uses' => '\\' . LkController::class . '@account', 'as' => 'lk.account']);
    Route::get('/lk/favourites', ['uses' => '\\' . LkController::class . '@favourites', 'as' => 'lk.favourites']);
    Route::get('/lk/subscriptions', ['uses' => '\\' . LkController::class . '@subscriptions', 'as' => 'lk.subscriptions']);

    Route::get('/{name?}', ['uses' => '\\' . IndexController::class . '@name', 'as' => 'name'])->where('name', '^(?!admin).*$');
});
