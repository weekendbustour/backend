<?php

use Dingo\Api\Routing\Router;
use Illuminate\Http\Request;

use App\Http\Controllers\Api\IndexController;
use App\Http\Controllers\Api\TourController;
use App\Http\Controllers\Api\DirectionController;
use App\Http\Controllers\Api\ArticleController;
use App\Http\Controllers\Api\NewsController;
use App\Http\Controllers\Api\OrganizationController;
use App\Http\Controllers\Api\HelpController;
//use App\Http\Controllers\Api\ApiDirectionController;
use App\Http\Controllers\Api\SearchController;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\User\UserFavouritesController;
use App\Http\Controllers\Api\User\UserSubscriptionsController;
use App\Http\Controllers\Api\User\UserCommentsController;
use App\Http\Controllers\Api\User\UserReservationsController;
use App\Http\Controllers\Api\User\UserAccountController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\AttractionController;
use App\Http\Controllers\Api\AuthController;
use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Auth\ForgotPasswordController;
use Laravel\Sanctum\Http\Controllers\CsrfCookieController;
use App\Http\Controllers\Socialite\SocialAuthVkontakteController;
use App\Http\Controllers\Api\Articles;
use App\Http\Controllers\Api\Attractions;
use App\Http\Controllers\Api\Category;
use App\Http\Controllers\Api\Directions;
use App\Http\Controllers\Api\Help;
use App\Http\Controllers\Api\Index;
use App\Http\Controllers\Api\News;
use App\Http\Controllers\Api\Organizations;
use App\Http\Controllers\Api\Promotion;
use App\Http\Controllers\Api\Tours;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

Route::group([
    'domain' => env('API_URL'),
    'as' => 'api',
], function () use ($api) {
    $api->version('v1', function (Router $api) {
        $api->group(['prefix' => config('sanctum.prefix', 'sanctum')], function (Router $api) {
            $api->get('/csrf-cookie', ['as' => 'csrf', 'uses' => '\\' . CsrfCookieController::class . '@show']);
        });

        $api->group([
            'middleware' => ['api'],
            'as' => 'api',
        ], function (Router $api) {

            /**
             * Общие
             */
            $api->any('/', ['as' => 'index', 'uses' => Index\Index::class]);
            $api->any('/data', ['as' => 'data', 'uses' => Index\Data\Index::class]);
            $api->any('/data/update', ['as' => 'data', 'uses' => Index\Data\Update::class]);
            $api->any('/search', ['as' => 'search', 'uses' => Index\Search\Index::class]);

            /**
             *
             */
            $api->group(['prefix' => 'help', 'as' => 'help'], function (Router $api) {
                $api->any('/report', ['as' => 'report', 'uses' => Help\Report::class]);
                $api->any('/offer', ['as' => 'offer', 'uses' => Help\Offer::class]);
                $api->any('/rating', ['as' => 'rating', 'uses' => Help\Rating::class]);
            });

            /**
             * Туры
             */
            $api->any('/tours', ['as' => 'tours', 'uses' => Tours\Index::class]);

            $api->group(['prefix' => 'tour', 'as' => 'tour'], function (Router $api) {
                $api->any('/{id}/timelines', ['as' => 'timelines', 'uses' =>  Tours\Timelines::class]);
                $api->any('/{id}/{name?}', ['as' => 'index', 'uses' =>  Tours\Show::class]);
            });

            /**
             * Организации
             */
            $api->any('/organizations', ['as' => 'organizations', 'uses' => Organizations\Index::class]);

            $api->group(['prefix' => 'organization', 'as' => 'organization'], function (Router $api) {
                $api->any('/{id}/comments', ['as' => 'comments', 'uses' => Organizations\Comments::class]);
                $api->any('/{id}/tours', ['as' => 'tours', 'uses' => Organizations\Tours::class]);
                $api->any('/{id}/{name?}', ['as' => 'index', 'uses' => Organizations\Show::class]);
            });

            /**
             * Направления
             */
            $api->any('/directions', ['as' => 'directions', 'uses' => Directions\Index::class]);

            $api->group(['prefix' => 'direction', 'as' => 'direction'], function (Router $api) {
                $api->any('/{id}/comments', ['as' => 'comments', 'uses' => Directions\Comments::class]);
                $api->any('/{id}/tours', ['as' => 'tours', 'uses' => Directions\Tours::class]);
                $api->any('/{id}/{name?}', ['as' => 'index', 'uses' => Directions\Show::class]);
            });

            /**
             * Новости
             */
            $api->any('/news', ['as' => 'news', 'uses' => News\Index::class]);

            $api->group(['prefix' => 'news', 'as' => 'news.page'], function (Router $api) {
                $api->any('/{id}/comments', ['as' => 'comments', 'uses' => News\Comments::class]);
                $api->any('/{id}/{name?}', ['as' => 'index', 'uses' => News\Show::class]);
            });

            /**
             * Статьи
             */
            $api->any('/articles', ['as' => 'articles', 'uses' => Articles\Index::class]);

            $api->group(['prefix' => 'article', 'as' => 'article'], function (Router $api) {
                $api->any('/{id}/comments', ['as' => 'comments', 'uses' => Articles\Comments::class]);
                $api->any('/{id}/{name?}', ['as' => 'index', 'uses' => Articles\Show::class]);
            });

            /**
             * Досто
             */
            $api->any('/attractions', ['as' => 'attractions', 'uses' => Attractions\Index::class]);

            $api->group(['prefix' => 'attraction', 'as' => 'attraction'], function (Router $api) {
                $api->any('/{id}/comments', ['as' => 'comments', 'uses' => Attractions\Comments::class]);
                $api->any('/{id}/{name?}', ['as' => 'index', 'uses' => Attractions\Show::class]);
            });

            /**
             * Категории
             */
            $api->group(['prefix' => 'category', 'as' => 'category'], function (Router $api) {
                $api->any('/{id}/{name?}', ['as' => 'index', 'uses' => Category\Show::class]);
            });

            /**
             * Реклама
             */
            $api->group(['prefix' => 'promotion', 'as' => 'promotion'], function (Router $api) {
                $api->any('/{id}/{name?}', ['as' => 'index', 'uses' => Promotion\Show::class]);
            });

            /**
             * Пользователь
             */
            $api->group(['prefix' => 'user', 'as' => 'user', 'middleware' => ['auth:sanctum']], function (Router $api) {
                $api->any('/', ['as' => 'index', 'uses' => '\\' . UserController::class . '@user']);

                $api->group(['prefix' => 'account', 'as' => 'account'], function (Router $api) {
                    $api->any('/profile', ['as' => 'profile', 'uses' => '\\' . UserAccountController::class . '@accountProfile']);
                    $api->any('/image', ['as' => 'image', 'uses' => '\\' . UserAccountController::class . '@accountImage']);
                    $api->any('/image/delete', ['as' => 'image.delete', 'uses' => '\\' . UserAccountController::class . '@accountImageDelete']);
                });


                $api->post('/favourite', ['as' => 'favourite', 'uses' => '\\' . UserFavouritesController::class . '@userFavourite']);
                $api->get('/favourites', ['as' => 'favourites', 'uses' => '\\' . UserFavouritesController::class . '@userFavourites']);

                $api->post('/comment', ['as' => 'comment', 'uses' => '\\' . UserCommentsController::class . '@userComment']);
                $api->get('/comments', ['as' => 'comments', 'uses' => '\\' . UserCommentsController::class . '@userComments']);
                $api->post('/comment/{id}/vote', ['as' => 'comment.vote', 'uses' => '\\' . UserCommentsController::class . '@userCommentVote']);
                $api->get('/votes', ['as' => 'votes', 'uses' => '\\' . UserCommentsController::class . '@userVotes']);

                $api->post('/subscribe', ['as' => 'subscribe', 'uses' => '\\' . UserSubscriptionsController::class . '@userSubscribe']);
                $api->post('/subscription', ['as' => 'subscription', 'uses' => '\\' . UserSubscriptionsController::class . '@userSubscription']);
                $api->get('/subscriptions', ['as' => 'subscriptions', 'uses' => '\\' . UserSubscriptionsController::class . '@userSubscriptions']);

                $api->post('/reservation', ['as' => 'reservation', 'uses' => '\\' . UserReservationsController::class . '@userReservation']);
                $api->post('/reservation/cancel', ['as' => 'reservation.cancel', 'uses' => '\\' . UserReservationsController::class . '@userReservationCancel']);
                $api->get('/reservations', ['as' => 'reservations', 'uses' => '\\' . UserReservationsController::class . '@userReservations']);
            });

            /**
             * Авторизация
             */
            $api->group(['prefix' => 'auth', 'as' => 'auth'], function (Router $api) {
                $api->post('/login', ['as' => 'login', 'uses' => '\\' . LoginController::class . '@login']);
                $api->post('/register', ['as' => 'register', 'uses' => '\\' . RegisterController::class . '@register']);
                $api->post('/password/email', ['as' => 'password.email.post', 'uses' => '\\' . ForgotPasswordController::class . '@sendResetLinkEmail']);

                $api->post('/vkontakte/callback', ['as' => 'vkontakte.callback.post', 'uses' => '\\' . SocialAuthVkontakteController::class . '@handleProviderCallbackPost']);
            });

            $api->any('/vk/callback', ['as' => 'vk.callback', 'uses' => '\\' . \App\Http\Controllers\VkApiCallbackController::class . '@execute']);
        });
    });
});

