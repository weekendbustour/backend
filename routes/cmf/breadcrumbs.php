<?php

declare(strict_types=1);

use DJStarCOM\Breadcrumbs\BreadcrumbsGenerator;
use DJStarCOM\Breadcrumbs\Facades\Breadcrumbs;
use App\Cmf\Project\User\UserController;
use App\Cmf\Project\Organization\OrganizationController;
use App\Cmf\Project\Direction\DirectionController;
use App\Cmf\Project\Departure\DepartureController;
use App\Cmf\Project\Category\CategoryController;
use App\Cmf\Project\Tag\TagController;
use App\Cmf\Project\Tour\TourController;
use App\Cmf\Project\TourTimeline\TourTimelineController;
use App\Cmf\Project\Role\RoleController;
use App\Cmf\Project\Activation\ActivationController;
use App\Cmf\Project\Option\OptionController;
use App\Cmf\Project\Comment\CommentController;
use App\Cmf\Project\Entertainment\EntertainmentController;
use App\Cmf\Project\Promotion\PromotionController;
use App\Cmf\Project\News\NewsController;
use App\Cmf\Project\Payment\PaymentController;
use App\Cmf\Project\TourReservation\TourReservationController;
use App\Cmf\Project\Attraction\AttractionController;
use App\Cmf\Project\Article\ArticleController;
use App\Cmf\Project\Question\QuestionController;
use App\Cmf\Project\Outfit\OutfitController;

/**
 * --------------------------------------------------
 * APP
 * --------------------------------------------------
 */
Breadcrumbs::register('app.dashboard', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->push(__('Главная'), route('index'));
});
/**
 * --------------------------------------------------
 * ADMIN
 * --------------------------------------------------
 */
Breadcrumbs::register('dashboard', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->push(__('Главная'), routeCmf('dashboard.index'));
});
Breadcrumbs::register('index', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->push(__('Главная'), routeCmf('dashboard.index'));
});
Breadcrumbs::register('statistic', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->push(__('Главная'), routeCmf('dashboard.index'));
    $breadcrumbs->push('Статистика', routeCmf('statistic.index'));
});
Breadcrumbs::register('home', function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->push('Главная', routeCmf('dashboard.index'));
});
Breadcrumbs::register(UserController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(UserController::TITLE, routeCmf(UserController::NAME.'.index'));
});
Breadcrumbs::register(OrganizationController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(OrganizationController::TITLE, routeCmf(OrganizationController::NAME.'.index'));
});
Breadcrumbs::register(DirectionController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(DirectionController::TITLE, routeCmf(DirectionController::NAME.'.index'));
});
Breadcrumbs::register(DepartureController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(DepartureController::TITLE, routeCmf(DepartureController::NAME.'.index'));
});
Breadcrumbs::register(CategoryController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(CategoryController::TITLE, routeCmf(CategoryController::NAME.'.index'));
});
Breadcrumbs::register(TagController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(TagController::TITLE, routeCmf(TagController::NAME.'.index'));
});
Breadcrumbs::register(TourController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(TourController::TITLE, routeCmf(TourController::NAME.'.index'));
});
Breadcrumbs::register(TourController::NAME . '.item', function (BreadcrumbsGenerator $breadcrumbs, $oItem) {
    $breadcrumbs->parent(TourController::NAME);
    $breadcrumbs->push($oItem->title, routeCmf(TourController::NAME.'.action.item', ['id' => $oItem->id]));
});
Breadcrumbs::register(TourController::NAME . '.item.page', function (BreadcrumbsGenerator $breadcrumbs, $oItem, $page) {
    $breadcrumbs->parent(TourController::NAME . '.item', $oItem);
    $breadcrumbs->push($page['title'], routeCmf(TourController::NAME.'.action.item', ['id' => $oItem->id, 'name' => $page['name']]));
});
Breadcrumbs::register(TourTimelineController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(TourTimelineController::TITLE, routeCmf(TourTimelineController::NAME.'.index'));
});
Breadcrumbs::register(RoleController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(RoleController::TITLE, routeCmf(RoleController::NAME.'.index'));
});
Breadcrumbs::register(ActivationController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(ActivationController::TITLE, routeCmf(ActivationController::NAME.'.index'));
});
Breadcrumbs::register(CommentController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(CommentController::TITLE, routeCmf(CommentController::NAME.'.index'));
});
Breadcrumbs::register(OptionController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(OptionController::TITLE, routeCmf(OptionController::NAME.'.index'));
});
Breadcrumbs::register(EntertainmentController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(EntertainmentController::TITLE, routeCmf(EntertainmentController::NAME.'.index'));
});
Breadcrumbs::register(PromotionController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(PromotionController::TITLE, routeCmf(PromotionController::NAME.'.index'));
});
Breadcrumbs::register(NewsController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(NewsController::TITLE, routeCmf(NewsController::NAME.'.index'));
});
Breadcrumbs::register(PaymentController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(PaymentController::TITLE, routeCmf(PaymentController::NAME.'.index'));
});
Breadcrumbs::register(TourReservationController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(TourReservationController::TITLE, routeCmf(TourReservationController::NAME.'.index'));
});
Breadcrumbs::register(AttractionController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(AttractionController::TITLE, routeCmf(AttractionController::NAME.'.index'));
});
Breadcrumbs::register(ArticleController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(ArticleController::TITLE, routeCmf(ArticleController::NAME.'.index'));
});
Breadcrumbs::register(QuestionController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(QuestionController::TITLE, routeCmf(QuestionController::NAME.'.index'));
});
Breadcrumbs::register(OutfitController::NAME, function (BreadcrumbsGenerator $breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push(OutfitController::TITLE, routeCmf(OutfitController::NAME.'.index'));
});
/**
 * --------------------------------------------------
 * ADMIN DEV
 * --------------------------------------------------
 */
Breadcrumbs::register('dev.pages', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', routeCmf('dashboard.index'));
    $breadcrumbs->push('Системные страницы', routeCmf('dev.pages.index'));
});
Breadcrumbs::register('dev.cards', function ($breadcrumbs) {
    $breadcrumbs->parent('dev.pages');
    $breadcrumbs->push('Карточки', routeCmf('dev.cards.index'));
});
Breadcrumbs::register('dev.cards.tours', function ($breadcrumbs) {
    $breadcrumbs->parent('dev.cards');
    $breadcrumbs->push('Туры', routeCmf('dev.cards.tours.index'));
});
Breadcrumbs::register('dev.sitemap', function ($breadcrumbs) {
    $breadcrumbs->parent('dev.pages');
    $breadcrumbs->push('Sitemap', routeCmf('dev.sitemap.index'));
});
Breadcrumbs::register('dev.system', function ($breadcrumbs) {
    $breadcrumbs->parent('dev.pages');
    $breadcrumbs->push('Система', routeCmf('dev.system.index'));
});
Breadcrumbs::register('dev.queue', function ($breadcrumbs) {
    $breadcrumbs->parent('dev.pages');
    $breadcrumbs->push('Очереди', routeCmf('dev.queue.index'));
});
Breadcrumbs::register('dev.mail', function ($breadcrumbs) {
    $breadcrumbs->parent('dev.pages');
    $breadcrumbs->push('Электронные письма', routeCmf('dev.mail.index'));
});
/**
 * --------------------------------------------------
 * ADMIN STATISTIC
 * --------------------------------------------------
 */
Breadcrumbs::register('statistic.index', function ($breadcrumbs) {
    $breadcrumbs->push('Главная', routeCmf('dashboard.index'));
    $breadcrumbs->push('Статистика', routeCmf('statistic.index'));
});
Breadcrumbs::register('statistic.user', function ($breadcrumbs) {
    $breadcrumbs->parent('statistic.index');
    $breadcrumbs->push('Пользователи', routeCmf('statistic.user'));
});
