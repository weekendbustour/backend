<?php

declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| Cmf Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Cmf\Core\RouteCmf;
use App\Cmf\Project\HomeController;
use App\Cmf\Project\StatisticController;
use App\Http\Controllers\Auth\LoginController;
use App\Cmf\Project\User\UserController;
use App\Cmf\Project\Organization\OrganizationController;
use App\Cmf\Project\Direction\DirectionController;
use App\Cmf\Project\Departure\DepartureController;
use App\Cmf\Project\Category\CategoryController;
use App\Cmf\Project\Tag\TagController;
use App\Cmf\Project\Tour\TourController;
use App\Cmf\Project\TourTimeline\TourTimelineController;
use App\Cmf\Project\Role\RoleController;
use App\Cmf\Project\Activation\ActivationController;
use App\Cmf\Project\Comment\CommentController;
use App\Cmf\Project\Option\OptionController;
use App\Cmf\Project\Entertainment\EntertainmentController;
use App\Cmf\Project\Promotion\PromotionController;
use App\Cmf\Project\News\NewsController;
use App\Cmf\Project\Payment\PaymentController;
use App\Cmf\Project\TourReservation\TourReservationController;
use App\Cmf\Project\Attraction\AttractionController;
use App\Cmf\Project\Article\ArticleController;
use App\Cmf\Project\Question\QuestionController;
use App\Cmf\Project\Outfit\OutfitController;

Route::group([
    'domain' => cmfHelper()->getUrl(),
    'as' => cmfHelper()->getAs(),
    'prefix' => cmfHelper()->getPrefix(),
    'middleware' => ['cmf', 'member'],
], function () {
    Route::get('/', ['uses' => '\\' . HomeController::class . '@index', 'as' => 'index']);
    Route::get('/home', ['uses' => '\\' . HomeController::class . '@index', 'as' => 'home.index']);
    Route::get('/dashboard', ['uses' => '\\' . HomeController::class . '@index', 'as' => 'dashboard.index']);

    Route::get('/statistic', ['uses' => '\\' . StatisticController::class . '@index', 'as' => 'statistic.index']);
    Route::get('/statistic/user', ['uses' => '\\' . StatisticController::class . '@user', 'as' => 'statistic.user']);

    Route::post('/logout', ['uses' => '\\' . LoginController::class . '@logoutAdmin', 'as' => 'logout.post']);

    RouteCmf::resource(UserController::NAME);
    RouteCmf::resource(OrganizationController::NAME);
    RouteCmf::resource(DirectionController::NAME);
    RouteCmf::resource(DepartureController::NAME);
    RouteCmf::resource(CategoryController::NAME);
    RouteCmf::resource(TagController::NAME);
    RouteCmf::resource(TourController::NAME);
    RouteCmf::resource(TourTimelineController::NAME);
    RouteCmf::resource(RoleController::NAME);
    RouteCmf::resource(ActivationController::NAME);
    RouteCmf::resource(CommentController::NAME);
    RouteCmf::resource(OptionController::NAME);
    RouteCmf::resource(EntertainmentController::NAME);
    RouteCmf::resource(PromotionController::NAME);
    RouteCmf::resource(NewsController::NAME);
    RouteCmf::resource(PaymentController::NAME);
    RouteCmf::resource(TourReservationController::NAME);
    RouteCmf::resource(AttractionController::NAME);
    RouteCmf::resource(ArticleController::NAME);
    RouteCmf::resource(QuestionController::NAME);
    RouteCmf::resource(OutfitController::NAME);

    Route::get('/{name}', ['uses' => '\\' . HomeController::class . '@unknown', 'as' => 'unknown']);
});
