<?php

declare(strict_types=1);

/*
|--------------------------------------------------------------------------
| Cmf Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

use App\Cmf\Project\DevController;

Route::group([
    'domain' => cmfHelper()->getUrl(),
    'as' => cmfHelper()->getAs(),
    'prefix' => cmfHelper()->getPrefix(),
    'middleware' => ['auth', 'member', 'cmf'],
], function () {

    Route::get('/dev/command/{name}', ['uses' => '\\' . DevController::class . '@command', 'as' => 'dev.command.index']);
    Route::get('/dev/php/{name}', ['uses' => '\\' . DevController::class . '@php', 'as' => 'dev.php.index']);
    Route::get('/dev/email', ['uses' => '\\' . DevController::class . '@email', 'as' => 'dev.email.index']);

    Route::get('/dev/pages', ['uses' => '\\' . DevController::class . '@pages', 'as' => 'dev.pages.index']);
    Route::get('/dev/cards', ['uses' => '\\' . DevController::class . '@cards', 'as' => 'dev.cards.index']);
    Route::get('/dev/cards/tours', ['uses' => '\\' . DevController::class . '@cardsTours', 'as' => 'dev.cards.tours.index']);
    Route::get('/dev/system', ['uses' => '\\' . DevController::class . '@system', 'as' => 'dev.system.index']);
    Route::get('/dev/queue', ['uses' => '\\' . DevController::class . '@queue', 'as' => 'dev.queue.index']);
    Route::get('/dev/sitemap', ['uses' => '\\' . DevController::class . '@sitemap', 'as' => 'dev.sitemap.index']);
    Route::get('/dev/mail', ['uses' => '\\' . DevController::class . '@mail', 'as' => 'dev.mail.index']);
    Route::get('/dev/mail/{name}', ['uses' => '\\' . DevController::class . '@mailName', 'as' => 'dev.mail.name.index']);

    Route::post('/dev/settings', ['uses' => '\\' . DevController::class . '@settings', 'as' => 'dev.settings']);
    Route::post('/dev/settings/save', ['uses' => '\\' . DevController::class . '@saveSettings', 'as' => 'dev.settings.save']);
});
