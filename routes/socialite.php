<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Socialite\SocialAuthGoogleController;
use App\Http\Controllers\Socialite\SocialAuthVkontakteController;
use App\Http\Controllers\Socialite\SocialAuthYandexController;
use App\Http\Controllers\Socialite\SocialAuthFacebookController;
use App\Http\Controllers\Socialite\SocialAuthOdnoklassnikiController;

Route::group([
    'domain' => env('APP_URL'),
    'middleware' => [],
    'as' => 'socialite.',
], function () {

    /**
     * @see SocialAuthGoogleController
     */
    Route::get('/auth/google/redirect', ['uses' => '\\' . SocialAuthGoogleController::class . '@redirect', 'as' => 'google.redirect']);
    Route::get('/auth/google/callback', ['uses' => '\\' . SocialAuthGoogleController::class . '@handleProviderCallback', 'as' => 'google.callback']);

    Route::get('/auth/vkontakte/redirect', ['uses' => '\\' . SocialAuthVkontakteController::class . '@redirectToProvider', 'as' => 'vkontakte.redirect']);
    Route::get('/auth/vkontakte/callback', ['uses' => '\\' . SocialAuthVkontakteController::class . '@handleProviderCallback', 'as' => 'vkontakte.callback']);
    Route::get('/auth/vkontakte/code', ['uses' => '\\' . SocialAuthVkontakteController::class . '@code', 'as' => 'vkontakte.code']);
    Route::get('/auth/vkontakte/wall', ['uses' => '\\' . SocialAuthVkontakteController::class . '@wall', 'as' => 'vkontakte.wall']);

    Route::get('/auth/yandex/redirect', ['uses' => '\\' . SocialAuthYandexController::class . '@redirect', 'as' => 'yandex.redirect']);
    Route::get('/auth/yandex/callback', ['uses' => '\\' . SocialAuthYandexController::class . '@handleProviderCallback', 'as' => 'yandex.callback']);

    Route::get('/auth/facebook/redirect', ['uses' => '\\' . SocialAuthFacebookController::class . '@redirect', 'as' => 'facebook.redirect']);
    Route::get('/auth/facebook/callback', ['uses' => '\\' . SocialAuthFacebookController::class . '@callback', 'as' => 'facebook.callback']);

    Route::get('/auth/odnoklassniki/redirect', ['uses' => '\\' . SocialAuthOdnoklassnikiController::class . '@redirect', 'as' => 'odnoklassniki.redirect']);
    Route::get('/auth/odnoklassniki/callback', ['uses' => '\\' . SocialAuthOdnoklassnikiController::class . '@callback', 'as' => 'odnoklassniki.callback']);

});
