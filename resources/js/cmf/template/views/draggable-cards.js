$(function(){
    var element = ".drag-block";
    var handle = ".card-header";
    var connect = ".drag-block";

    $(element).sortable({
        handle: handle,
        connectWith: connect,
        tolerance: 'pointer',
        forcePlaceholderSize: true,
        opacity: 0.8,
        placeholder: 'card-placeholder',
        update: function( event, ui ) {
            var $element = $(this);
            if ($element.data('save-action')) {

                document.delay(function() {
                    var $elements = $('[data-name="' + $element.data('name') + '"] form');
                    var id = '';
                    $elements.each(function() {
                        id += $(this).data('id') + ';';
                    });
                    $.ajax({
                        url: $element.data('save-action'),
                        type: "POST",
                        data: {positions: id},
                        success: function (result) {
                            if (result.toastr) {
                                if (result.success) {
                                    toastr.info(result.toastr.text, result.toastr.title, result.toastr.options);
                                } else {
                                    toastr.error(result.toastr.text, result.toastr.title, result.toastr.options);
                                }
                            }
                        },
                        error: function(data, status, headers, config) {
                            console.log(data);
                        }
                    });
                    //$('#post-positions').val(id);
                }, 500);
            }
        }
    });
    //.disableSelection();
  
});


