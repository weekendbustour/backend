

require('jquery-ui/ui/widgets/sortable.js');
require('jquery-ui/ui/widgets/draggable.js');

$(document).ready(function () {
    //window['drag-image']();
});

/**
 * @see ./blueimp.js
 */
window['drag-image'] = function () {
    let element = ".is-gallery-row"; // data-save-action=url
    let handle = ".drag-pointer";
    let connect = ".drag-image";
    let empty = '.--empty-placeholder';
    let placeholder = 'col-' + $(element).data('col') + ' card-placeholder';
    initDragImage(element, handle, connect, placeholder, empty);
};

let initDragImage = function (element, handle, connect, placeholder, empty) {
    $(element).sortable({
        handle: handle,
        connectWith: connect,
        tolerance: 'pointer',
        forcePlaceholderSize: true,
        opacity: 0.8,
        placeholder: placeholder,
        update: function (event, ui) {
            console.log('drag');
            let $element = $(this);
            let $parent = $(element);
            let $empty = $(empty);
            if (!$empty.is(':last-child')) {
                $empty.clone().appendTo(element);
                $parent.find(empty).first().remove();
            }
            if ($element.data('save-action')) {
                document.delay(function () {
                    let $elements = $('[data-name="' + $element.data('name') + '"] .drag-pointer');
                    let id = '';
                    $elements.each(function () {
                        id += $(this).data('id') + ';';
                    });
                    $.ajax({
                        url: $element.data('save-action'),
                        type: "POST",
                        data: {positions: id, id: $element.data('item_id'), type: $element.data('type')},
                        success: function (result) {
                            if (result.toastr) {
                                if (result.success) {
                                    toastr.info(result.toastr.text, result.toastr.title, result.toastr.options);
                                } else {
                                    toastr.error(result.toastr.text, result.toastr.title, result.toastr.options);
                                }
                            }
                        },
                        error: function (data, status, headers, config) {
                            console.log(data);
                        }
                    });
                    //$('#post-positions').val(id);
                }, 500);
            }
        }
    });
};

/*$('body').on('mouseenter','#dropzone',function(){
    $(this).addClass('dragable');
})

$('body').on('mouseleave','#dropzone',function(){
    $(this).removeClass('dragable');
})*/
