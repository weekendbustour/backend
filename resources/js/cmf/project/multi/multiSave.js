$(document).ready(function () {
    const $body = $('body');
    $body.on('change', '.switch-multi-save-dialog input', function () {
        const $switch = $(this);
        if ($switch.prop('checked')) {
            multiSave.enableMulti();
        } else {
            multiSave.disableMulti();
        }
    });
    $body.on('click', '.remove-multi-date', function () {
        const $target = $(this).closest('.row');
        $target.remove();
    });
});


export const multiSave = {

    /**
     *
     */
    container: '#modal--edit-container',

    /**
     *
     */
    closestContainer: '.dialog__content',

    /**
     *
     */
    className: '--save-multi',

    /**
     * @returns bool
     */
    isMulti() {
        const self = this;
        return $(self.container).hasClass(self.className);
    },

    /**
     *
     * @param $form
     * @returns bool
     */
    isMultiByForm($form) {
        const self = this;
        const $dialog__content = $form.closest('.dialog__content');
        return $dialog__content && $dialog__content.hasClass(self.className);
    },

    /**
     *
     */
    enableMulti() {
        const self = this;
        $(self.container).addClass(self.className);
    },

    /**
     *
     */
    disableMulti() {
        const self = this;
        $(self.container).removeClass(self.className);
    }
};
