

/**
 * -------------------------------------------
 * Summernote
 * -------------------------------------------
 *
 */
require('codemirror/lib/codemirror.js');
require('summernote/dist/summernote.js');
require('summernote/lang/summernote-ru-RU.js');

//require('summernote-webpack-fix/dist/summernote.min.js');
//require('summernote-webpack-fix/dist/lang/summernote-ru-RU.min.js');
require('codemirror');
require('codemirror/mode/htmlembedded/htmlembedded');

$(function () {
    window['initSummernote']();
});

function progressHandlingFunction (e) {
    if (e.lengthComputable) {
        $('progress').attr({value: e.loaded, max: e.total});
        // reset progress on complete
        if (e.loaded == e.total) {
            $('progress').attr('value', '0.0');
        }
    }
}


window['initSummernote'] = function () {
    let $element;
    $element = $('.modal .note-video-btn').closest('.modal');
    if ($element.length) {
        $element.remove();
    }
    $element = $('.modal .note-image-btn').closest('.modal');
    if ($element.length) {
        $element.remove();
    }
    $element = $('.link-dialog');
    if ($element.length) {
        $element.remove();
    }
    $element = $('.note-link-popover');
    if ($element.length) {
        $element.remove();
    }
    $element = $('.note-image-popover');
    if ($element.length) {
        $element.remove();
    }
    $element = $('a[href="http://summernote.org/"]').closest('.modal');
    if ($element.length) {
        $element.remove();
    }
    /**
     * <div id="summernote"
     *  data-text-area="summernote-textarea" // куда будет сохраняться текст
     *  data-text-block="textblock" // где он будет выводиться, только для быстрого просмотра
     * >
     *
     * <div>
     */
    $('.summernote-editor').each(function () {
        initSummernote($('#' + $(this).attr('id')));
    });


};

let initSummernote = function ($container) {
    let $textArea = $('#' + $container.data('text-area'));
    let textArea = $textArea.attr('id');
    let $textBlock = $('#' + $container.data('text-block'));
    let textBlock = $textBlock.attr('id');
    let editor = '.note-editable';
    let $editor = $('.note-editable');
    $container.summernote({
        lang: 'ru-RU',
        height: 500,                 // set editor height

        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor

        focus: true,                // set focus to editable area after initializing summernote
        dialogsInBody: true,
        // https://codemirror.net/doc/manual.html
        codemirror: {
            mode: 'text/html',
            htmlMode: true,
            lineNumbers: true,
            lineWrapping: true,
            spellcheck: true,
            autocorrect: true,
            autocapitalize: true,
            theme: 'default',
        },
        colors: [
            window.colors,
        ],
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'strikethrough', 'clear', 'hr']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            //['height', ['height']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['view', [
                'codeview',
                'picture',
                'link',
                'video',
                'fullscreen',
            ]],
        ],
        popover: {
            image: [
                ['image', ['resizeFull', 'resizeHalf', 'resizeQuarter', 'resizeNone']],
                ['float', ['floatLeft', 'floatRight', 'floatNone']],
                ['remove', ['removeMedia']]
            ],
            link: [
                ['link', ['linkDialogShow', 'unlink']]
            ],
            air: [
                ['color', ['color']],
                ['font', ['bold', 'underline', 'clear']],
                ['para', ['ul', 'paragraph']],
                ['table', ['table']],
                ['insert', ['link', 'picture']]
            ]
        },
        callbacks: {
            onInit: function () {
                let parent = document.getElementById(textArea);
                if (parent) {
                    $container.summernote('code', parent.value);
                }

                $('.note-video-url').closest('.modal').addClass('summernote-video-modal');
                $('.note-image-btn').closest('.modal').addClass('summernote-image-modal');

                // в модальном окне загрузки изображения разрешить input только один файл
                let $element = $('.note-image-input');
                if ($element.length) {
                    $element.removeAttr('multiple');
                }
            },
            onChange: function (contents, $editable) {
                let parent = document.getElementById(textArea);
                if (parent) {
                    parent.value = contents;
                }
                let visual = document.getElementById(textBlock);
                if (visual) {
                    visual.innerHTML = contents;
                }
            },
            onBlur: function () {
                /*
                let parent = document.getElementById('parent');
                if (parent) {
                    parent.value = $('#summernote').val();
                }
                let visual = document.getElementById('textblock');
                if (visual) {
                    visual.innerHTML = $('#summernote').val();
                }
                */
            },
            onImageUpload: function (files, editor, welEditable) {
                summernoteEvents.sendFile(files[0], $container);
                //sendFile(files[0], $container);
                console.log(files);
                console.log(editor);
                console.log(welEditable);
                // upload image to server and create imgNode...
                //$('#summernote').summernote('insertNode', '<img src="https://dummyimage.com/mediumrectangle/222222/eeeeee">');
            },
            onPaste: function (e) {
                let bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');

                e.preventDefault();

                // Firefox fix
                setTimeout(function () {
                    document.execCommand('insertText', false, bufferText);
                }, 10);
            }
        }
    });
};

let summernoteEvents = {
    sendFile(file, editor, welEditable) {
        const self = this;
        let data = new FormData();
        let $container = editor;
        let id = $container.attr('id');
        data.append('file', file);
        data.append('uid', $('#' + id + '-uid').val());
        let url = $container.data('upload-url');
        $.ajax({
            data: data,
            type: 'POST',
            xhr: function () {
                let myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) myXhr.upload.addEventListener('progress', progressHandlingFunction, false);
                return myXhr;
            },
            url: url,
            cache: false,
            contentType: false,
            processData: false,
            success: function (response) {
                if (response.success) {
                    //$('.note-image-url').val(response.file.url);
                    //let input = '<input type="hidden" name="uid[]" value="' + response.file.id + '">';
                    //$('#' + id + '-upload-image-inputs').append(input);
                }
                let url = response.file;
                let name = self.getFilenameFromUrl(url);
                console.log(url, name);
                $container.summernote('insertImage', url, name);
                console.log(response);
                //editor.insertImage(welEditable, url);
            },
            error: function (message) {
                console.log(message);
            }
        });
    },
    getFilenameFromUrl(url) {
        const index = url.lastIndexOf('/');
        return (-1 !== index ? url.substring(index + 1) : url);
    }
};

/**
 * После сохранения обновить данные в summernote
 * Причины:
 * - после сохранения меняются url для загруженных изображений
 *
 * @param result
 * @param $target
 */
window['updateSummernoteAfterSubmit'] = function (result, $target) {
    let $container = $target.find('.summernote-editor');
    if (result.success && result.text) {
        $container.summernote('code', result.text);
        $('.note-editable').trigger('input');
    }
};
