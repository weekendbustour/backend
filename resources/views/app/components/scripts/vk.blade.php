<script src="https://vk.com/js/api/openapi.js?168" type="text/javascript"></script>
<div id="vk_auth"></div>

<script type="text/javascript">
    window.onload = function () {
        VK.init({
            apiId: '{{config('services.vkontakte.app_id')}}',
        });
        VK.Widgets.Auth('vk_auth', {
            "authUrl": '{{config('services.vkontakte.redirect')}}'
        });
    }
</script>
