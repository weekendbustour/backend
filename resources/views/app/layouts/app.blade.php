<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="{{ asset('favicon.png') }}" type="image/png">
    <link rel="shortcut icon" href="{{ asset('favicon.png') }}" type="image/png">
    <link rel="apple-touch-icon" sizes="64x64" href="{{ asset('favicon64x64.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('favicon120x120.png') }}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon152x152.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('favicon152x152.png') }}">
    @include('app.components.meta')
    <link rel="manifest" href="{{ frontendCommon()->manifest() }}"/>
    <link rel="preload" as="font" href="{{ frontendCommon()->font('GTEestiProDisplay-Medium.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ frontendCommon()->font('GTEestiProDisplay-Regular.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ frontendCommon()->font('GTEestiProDisplay-Bold.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ frontendCommon()->font('fa-solid-900.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ frontendCommon()->font('fa-regular-400.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link href="{{ frontendCommon()->asset('css/vendors~main.chunk.css') }}" rel="stylesheet">
    <link href="{{ frontendCommon()->asset('css/main.chunk.css') }}" rel="stylesheet">
    <script>
        window.project = {!! $aComposerData !!};
        @if(!is_null($aComposerUser))
            window.user = {!! $aComposerUser !!};
        @endif
        @if(isset($aPage))
            window.page = {!! json_encode($aPage) !!};
        @endif
        @if(!\Illuminate\Support\Facades\Auth::guest() && (\Illuminate\Support\Facades\Auth::user()->hasRole(\App\Models\User::ROLE_SUPER_ADMIN) || \Illuminate\Support\Facades\Auth::user()->hasRole(\App\Models\User::ROLE_ADMIN)))
            window.isAdmin = true;
        @endif
    </script>
</head>
<body>
    @yield('content')

    <div id="scripts">
        <script src="{{ frontendCommon()->asset('js/bundle.js') }}"></script>
        <script src="{{ frontendCommon()->asset('js/vendors~main.chunk.js') }}"></script>
        <script src="{{ frontendCommon()->asset('js/main.chunk.js') }}"></script>
        @if(checkEnv(\App\Services\Environment::PRODUCTION) && config('services.google_analytics.enabled') && !isset($isPrivateShow))
            @include('app.components.scripts.google_analytics')
        @endif
        @if(checkEnv(\App\Services\Environment::PRODUCTION) && config('services.google_tag_manager.enabled') && !isset($isPrivateShow))
            @include('app.components.scripts.google_tag_manager')
        @endif
        @if(checkEnv(\App\Services\Environment::PRODUCTION) && config('services.vkontakte.enabled'))
            @include('app.components.scripts.vk')
        @endif
        @if(checkEnv(\App\Services\Environment::PRODUCTION) && config('services.yandex_metrika.enabled') && !isset($isPrivateShow))
            @include('app.components.scripts.yandex_metrika')
        @endif
    </div>
</body>
</html>
