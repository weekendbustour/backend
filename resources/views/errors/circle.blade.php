<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon152x152.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('favicon152x152.pn') }}">
    <title>@yield('code') @yield('message')</title>
    <style>
        @font-face {
            font-family: "GT Eesti Pro Display";
            src: url(/fonts/GTEestiProDisplay-Regular.eot);
            src:
                local("GT Eesti Pro Display Regular"),
                local("GTEestiProDisplay-Regular"),
                url(/fonts/GTEestiProDisplay-Regular.eot) format("embedded-opentype"),
                url(/fonts/GTEestiProDisplay-Regular.woff2) format("woff2"),
                url(/fonts/GTEestiProDisplay-Regular.woff) format("woff"),
                url(/fonts/GTEestiProDisplay-Regular.ttf) format("truetype");
            font-weight: 400;
            font-style: normal
        }

        html, body {
            width: 100%;
            height: 100%;
            padding: 0;
            margin: 0;
        }

        body {
            margin: 0;
            font-family: "GT Eesti Pro Display","Segoe UI",sans-serif;
            font-size: .875rem;
            font-weight: 400;
            line-height: 1.5;
            color: #333;
            text-align: left;
            background-color: #fff;
        }

        .wrapper {
            display: inline-block;
            width: 100%;
            height: 100%;
            background-image: url('{{ asset('img/trees.jpg') }}');
            background-size: cover;
            -webkit-filter: blur(0px);
            filter: blur(0px);
            overflow: hidden;
        }

        .wrapper .name_container {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            width: 80vmin;
            height: 80vmin;
            background: rgba(255, 255, 255, 0.1);
            border-radius: 100%;
            box-shadow: inset 0 0 30px 30px rgba(200, 200, 200, 0.1);
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }

        .wrapper .name_container > div {
            color: rgba(255, 255, 255, 0.7);
        }

        .wrapper .name_container .name {
            font-size: 5.5vmax;
        }

        .wrapper .name_container .designation {
            margin-top: 10px;
            font-size: 2vmax;
        }
        a {
            font-size: 16px;
            color: rgba(255, 255, 255, 0.7);
            text-decoration: none;
        }
        a:hover {
            color: rgba(255, 255, 255, 0.9);
        }
        .mt-3 {
            margin-top: 1rem;
        }
        .mb-3 {
            margin-bottom: 1rem;
        }
    </style>
</head>
<body>
<div class="wrapper">
    <div class="name_container">
        <div class="name">
            @yield('code')
        </div>
        <div class="designation">
            @yield('message')
        </div>
        <div class="flex-center link">
            @yield('link')
        </div>
    </div>
</div>
</body>
</html>
