@extends('errors.circle')

@section('title', __('Not Found'))
@section('code', '404')
@section('message', __('Not Found'))
@section('link')
    <div class="mt-3 mb-3">
        <a href="/" class="btn btn-light">
            Вернуться на главную
        </a>
    </div>
@endsection
