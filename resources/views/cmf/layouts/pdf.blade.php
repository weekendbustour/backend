<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon152x152.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('favicon152x152.pn') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $oComposerSite->app->title ?? 'app.title'}}</title>
    <meta name="description" content="{{ $oComposerSite->app->description ?? 'app.description' }}">
    <meta name="keywords" content="{{ $oComposerSite->app->keywords ?? 'app.keywords' }}">
    <link rel="manifest" href="{{ frontendCommon()->manifest() }}"/>
    <link rel="preload" as="font" href="{{ frontendCommon()->font('GTEestiProDisplay-Medium.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ frontendCommon()->font('GTEestiProDisplay-Regular.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ frontendCommon()->font('GTEestiProDisplay-Bold.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ frontendCommon()->font('fa-solid-900.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link rel="preload" as="font" href="{{ frontendCommon()->font('fa-regular-400.woff2') }}" type="font/woff2" crossorigin="anonymous">
    <link href="{{ frontendCommon()->asset('css/vendors~main.chunk.css') }}" rel="stylesheet">
    <link href="{{ frontendCommon()->asset('css/main.chunk.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>
    @yield('content')
</body>
</html>
