<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon152x152.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('favicon152x152.pn') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $oComposerSite->app->title ?? 'app.title'}}</title>
    <meta name="description" content="{{ $oComposerSite->app->description ?? 'app.description'}}">
    <meta name="keywords" content="{{ $oComposerSite->app->keywords ?? 'app.keywords'}}">
{{--    <link rel="shortcut icon" href="{{ $oComposerSite->app->favicon['main'] ?? '/img/favicon.png' }}">--}}

    <link rel="stylesheet" href="{{ asset('cmf/css/cmf.css') }}?v={{ $sComposerVersion ?? '' }}">

    @stack('styles')

</head>
<body class="app flex-row align-items-center">
@hasSection('body')
    @yield('body')
@else
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @yield('content')
            </div>
        </div>
    </div>
@endif

<div id="modals">
    @yield('modals')
</div>

<div id="scripts">
    @include('cmf.components.toastr')
    <script src="{{ asset('cmf/js/cmf.js') }}?v={{ $sComposerVersion ?? '' }}"></script>
    @stack('scripts')
</div>
</body>
</html>
