<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('favicon152x152.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="152x152" href="{{ asset('favicon152x152.pn') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ $oComposerSite->app->title ?? 'app.title'}}</title>
    <meta name="description" content="{{ $oComposerSite->app->description ?? 'app.description' }}">
    <meta name="keywords" content="{{ $oComposerSite->app->keywords ?? 'app.keywords' }}">
{{--    <link rel="shortcut icon" href="{{ $oComposerSite->app->favicon['main'] ?? '/img/favicon.png' }}">--}}

    <link rel="stylesheet" href="{{ asset('cmf/css/cmf.css') }}?v={{ $sComposerVersion ?? '' }}">

    @if(!Auth::guest())
        <script>
            window.user = @json([
                'id' => Auth::user()->id
            ]);
        </script>
    @endif
    @stack('styles')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden {{ $sBodyClass ?? '' }} {{ Session::has('sidebar-toggle') ? 'sidebar-hidden' : '' }}"
        {{-- app flex-row align-items-center --}}
>
@hasSection('body')
    @yield('body')
@else
    <div id="header">
        @include('cmf.layouts.components.header')
    </div>

    <div id="app" class="app-body" style="min-height: 100vh;margin-top: 55px;min-width: 1150px;">

        @include('cmf.layouts.components.sidebar')

        <main class="main">
            <div style="position: relative;">
                @include('cmf.components.breadcrumbs')
                <div style="position: absolute;right: 5px;top: 5px;">
                    @if(!isset($indexComponents) || (isset($indexComponents) && $indexComponents['create']))
                        @hasSection('breadcrumb-right')
                            @yield('breadcrumb-right')
                        @endif
                    @endif
                </div>
            </div>

            <div class="container-fluid">
                @if(!isset($indexComponents) || (isset($indexComponents) && $indexComponents['search']))
                    @hasSection('breadcrumb-search')
                        @yield('breadcrumb-search')
                    @endif
                @endif

                <div class="animated fadeIn">
                    @yield('content')
                </div>
            </div>
        </main>
        @include('cmf.layouts.components.aside')
    </div>

    <div id="footer">
        @include('cmf.layouts.components.footer')
    </div>
@endif

<div id="modals">
    @include('cmf.components.dialogs.settings.command')
    @include('cmf.components.dialogs.ajax')
    @include('cmf.components.dialogs.confirm')
    {{--@include('admin.components.dialogs.form')--}}
    @include('cmf.components.dialogs.info')
    @include('cmf.components.dialogs.bar')

    @yield('modals')
</div>


<div id="scripts">
    @include('cmf.components.toastr')
    @if(config('services.yandex_map.enabled'))
        <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=6b6f7c87-df20-4884-ba74-84d522360cac" type="text/javascript"></script>
    @endif
    <script src="{{ asset('cmf/js/cmf.js') }}?v={{ $sComposerVersion ?? '' }}"></script>
    @stack('scripts')
</div>
</body>
</html>
