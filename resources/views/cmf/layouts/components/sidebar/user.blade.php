<img src="{{ $oComposerMember->get('user.image') }}"
     data-user-image
     class="img-avatar"
     alt="Avatar"
>
<div class="sidebar-name">
    <strong data-user-name>
        {{ $oComposerMember->get('user.first_name') }}
    </strong>
</div>
<div class="text-muted">
    <small>{{ $oComposerMember->get('user.role.title') }}</small>
</div>
<div class="btn-group" role="group" aria-label="Button group with nested dropdown">
    @if($oComposerMember->get('user.role.name') === 'super-admin')
        <button type="button" class="btn btn-link trigger" data-dialog="#user-settings-command-edit-modal" data-modal>
            <i class="icon-settings"></i>
        </button>
    @else
        <button type="button" class="btn btn-link">
            <i class="icon-settings"></i>
        </button>
    @endif
    <button type="button" class="btn btn-link">
        <i class="icon-speech"></i>
    </button>
    <button type="button" class="btn btn-link">
        <i class="icon-user"></i>
    </button>
</div>
