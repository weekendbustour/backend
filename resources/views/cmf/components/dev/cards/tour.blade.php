{{--{{ dd($item) }}--}}
<div class="card__tale">
    <div class="card__tale--container">
        <div class="card__tale--up"></div>
        <div class="card_tale--main">
            <div class="card__tale--gallery-container {{ count($item['image']['images']) === 1 ? '__no-dots' : ''}}">
                <div class="carousel flickity-enabled" tabindex="0">
                    <div class="flickity-viewport" style="touch-action: pan-y;">
                        <div class="flickity-slider" style="left: 0px; transform: translateX(0%);">
                            <div class="card__tale--img is-gallery is-selected" style="background-image: url({{ $item['image']['images'][0] }}); position: absolute; left: 0%;"></div>
                        </div>
                    </div>
                    <button class="flickity-button flickity-prev-next-button previous" type="button" disabled="" aria-label="Previous">
                        <svg class="flickity-button-icon" viewBox="0 0 100 100">
                            <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow"></path>
                        </svg>
                    </button>
                    <button class="flickity-button flickity-prev-next-button next" type="button" disabled="" aria-label="Next">
                        <svg class="flickity-button-icon" viewBox="0 0 100 100">
                            <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" class="arrow" transform="translate(100, 100) rotate(180) "></path>
                        </svg>
                    </button>
                    <ol class="flickity-page-dots">
                        @foreach($item['image']['images'] as $image)
                            <li class="dot {{ $loop->first ? 'is-selected' : '' }}" aria-label="Page dot 1" aria-current="step"></li>
                        @endforeach
                    </ol>

                </div>
            </div>
            <div class="card__tale--date day-month">
                <div><span>{{ $item['start_at_days'] }}</span><span>{{ $item['start_at_month'] }}</span></div>
            </div>
            <div class="card__tale--socials">
                <ul class="list-group list-group-horizontal">
                    <li class="list-group-item">
                        <div class="" data-tooltipped="" aria-describedby="tippy-tooltip-1" data-original-title="Добавить в Избранное" style="display: inline;">
                            <button aria-label="Добавить в Избранное" type="button" class="  btn btn-light btn-sm">
                                <i class="fa fa-heart-o"></i></button>
                        </div>
                    </li>
                </ul>
            </div>
            @if($item['tag'] !== null)
                <div class="card__tale--tag">
                    <span class="colored badge badge-default" style="background-color: {{ $item['tag']['color'] }}; border-color: {{ $item['tag']['color'] }};">{{ $item['tag']['title'] }}</span>
                </div>
            @endif
        </div>
        <div class="card__tale--info">
            <div class="card__tale--logo">
                <div class="" data-tooltipped="" aria-describedby="tippy-tooltip-2" data-original-title="Перейти на страницу Организатора" style="display: inline;">
                    <a href="{{ $oItem->organization->getUrl() }}"><img src="{{ $item['organization']['image']['logo'] }}" alt=""></a>
                </div>
            </div>
            <div class="card__tale--direction">
                <div class="card__tale--direction--item">
                    <div class="" data-tooltipped="" aria-describedby="tippy-tooltip-3" data-original-title="Подробнее о Направлении" style="display: inline;">
                        <a class="tooltip-text text-grey" href="{{ $oItem->direction->getUrl() }}"><span>{{ $item['direction']['title'] }}</span></a>
                    </div>
                </div>
            </div>
            <h5 class="card__tale--title">{{ $oItem->title }}</h5>
            <div class="card__tale--description text-muted">
                <div class="display-text-group">
                    <span class="displayed-text">
                        {{ \Illuminate\Support\Str::limit($oItem->preview_description, 35) }}
                        @if(mb_strlen($oItem->preview_description) > 35)
                            <div class="read-more-button">Читать полностью</div>
                        @endif
                    </span>
                </div>
            </div>
            <div class="card__tale--price">
                <div class="__price">
                    <div>
                        <span>
                            @if(!is_null($item['price']['discount']))
                                {{ $item['price']['discount'] }}р.
                                <span class="card__discount">
                                    <span>{{ $item['price']['value'] }}р.</span>
                                </span>
                            @else
                                {{ $item['price']['value'] }}р.
                            @endif
                        </span>
                    </div>
                </div>
                <div>
                    <a class="btn btn-sm btn-light-dark" href="{{ $oItem->getUrl() }}">Подробнее</a>
                </div>
            </div>
        </div>
    </div>
</div>
