@extends('cmf.layouts.cmf', [
    'breadcrumb' => 'dev.cards.tours'
])

@section('content.title')
    @include('cmf.components.pages.title', [
        'title' => 'Туры',
        'description' => '',
    ])
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="mb-3 col-lg-4 col-md-6 col-sm-12 col-12 ---cmf-tour-card">
                @include('cmf.components.dev.cards.tour', [
                    'oItem' => \App\Models\Tour::find(56),
                    'item' => (new \App\Http\Transformers\TourTransformer())->transform(\App\Models\Tour::find(56)),
                ])
            </div>
            <div class="mb-3 col-lg-4 col-md-6 col-sm-12 col-12 ---cmf-tour-card">
                @include('cmf.components.dev.cards.tour', [
                    'oItem' => \App\Models\Tour::find(22),
                    'item' => (new \App\Http\Transformers\TourTransformer())->transform(\App\Models\Tour::find(22)),
                ])
            </div>
            <div class="mb-3 col-lg-4 col-md-6 col-sm-12 col-12 ---cmf-tour-card">
                @include('cmf.components.dev.cards.tour', [
                    'oItem' => \App\Models\Tour::find(56),
                    'item' => (new \App\Http\Transformers\TourTransformer())->transform(\App\Models\Tour::find(56)),
                ])
            </div>
        </div>
    </div>
@endsection
