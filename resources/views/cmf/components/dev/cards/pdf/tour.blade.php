@extends('cmf.layouts.pdf')

@push('styles')
{{--    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">--}}
{{--    <style>--}}
{{--        body {--}}
{{--            font-family: 'Roboto', sans-serif;--}}
{{--        }--}}
{{--    </style>--}}
<style>
    body {
        background-color: #ccc;
    }
    .card__tale--price div a {
        visibility: hidden;
    }
    .card__tale--date.day-month > div {
        position: absolute;
        top: 13px;
        width: 100%;
    }
    .card__tale--logo div img {
        width: 50px;
        height: 50px;
        border-radius: 100%;
        overflow: hidden;
    }
    .card__discount::after{
        content: '';
        border-bottom: 2px solid #fb5f4f;
        position: absolute;
        left: 0;
        top: 50%;
        width: 100%;
    }
    .card__tale--info {
        background-color: #fbfbfb !important;
    }
    /*.card__discount:after {*/
    /*    content:"";    !* required property *!*/
    /*    position: absolute;*/
    /*    top: 8px;*/
    /*    left: -1px;*/
    /*    border-top: 2px solid #fb5f4f;*/
    /*    height: 2px;*/
    /*    width: 19px;*/
    /*}*/
    /*.card__discount--left {*/
    /*    width: calc(100% + 10px);*/
    /*    height: 1px;*/
    /*    background: #fb5f4f;*/
    /*    position: absolute;*/
    /*    bottom: 8px;*/
    /*    left: -5px;*/
    /*    transform: rotate(172deg);*/
    /*}*/
    /*.card__discount--right {*/
    /*    width: calc(100% + 10px);*/
    /*    height: 1px;*/
    /*    background: #fb5f4f;*/
    /*    position: absolute;*/
    /*    bottom: 8px;*/
    /*    left: -5px;*/
    /*    transform: rotate(8deg);*/
    /*}*/
    /**
     * --------------------------------
     * УВЕЛИЧЕНИЕ В ДВА РАЗА
     * --------------------------------
     */
    body {
        font-size: 1.75rem;
    }
    .card__tale--tag {
        top: 20px !important;
        left: 20px !important;
    }
    .card__tale .card__tale--tag .badge-default {
        font-size: 1.75rem !important;
        padding: 10px 20px;
        border: 2px solid rgba(0,0,0,.05);
        margin-right: 10px;
    }
    .card_tale--main {
        /*height: calc(235px * 2) !important;*/
        height: 470px !important;
    }
    .card__tale .card__tale--gallery-container {
        /*height: calc(235px * 2) !important;*/
        height: 470px !important;
    }
    .card__tale .card__tale--gallery-container .carousel {
        /*height: calc(235px * 2) !important;*/
        height: 470px !important;
    }
    .card__tale .card__tale--img {
        /*height: calc(235px * 2) !important;*/
        height: 470px !important;
    }
    .card__tale .card__tale--date.day-month {
        width: 110px;
        height: 110px;
    }
    .card__tale .card__tale--date {
        top: 40px;
        right: 40px;
    }
    .card__tale .card__tale--date span {
        font-size: 28px;
    }
    .card__tale--date.day-month > div {
        top: 26px;
    }
    .card__tale .card__tale--info {
        padding: 22px 48px 32px;
    }
    .card__tale .card__tale--logo {
        width: 100px;
        height: 100px;
        left: 48px;
        top: -48px;
        border: 4px solid #fff;
    }
    .h5, h5 {
        font-size: 2.1875rem;
    }
    .card__tale .card__tale--description {
        font-size: 28px;
    }
    .card__tale .card__tale--price {
        font-size: 40px;
    }
    .card__tale .card__tale--price .card__discount {
        font-size: 28px;
        margin-left: 16px;
    }
    .card__discount:after {
        border-bottom: 4px solid #fb5f4f;
    }
    .card__discount:before {
        content: none !important;
    }
    .read-more-button {
        font-size: 24px;
    }
</style>
@endpush

@section('content')
    <div style="position: absolute; top: 0;left: 0;">
        <div style="width: 800px;transform: scale(1)">
            @include('cmf.components.dev.cards.tour', [
                'oItem' => $oTour,
                'item' => (new \App\Http\Transformers\TourTransformer())->transform($oTour),
            ])
        </div>
    </div>

@endsection
