@extends('cmf.layouts.cmf', [
    'breadcrumb' => 'dev.pages'
])

@section('content.title')
    @include('cmf.components.pages.title', [
        'title' => 'Системные страницы',
        'description' => '',
    ])
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body" style="padding: 1rem;">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item flex-column align-items-start">
                            <div>
                                <b>Система</b>
                            </div>
                            <p class="mb-1 text-muted">
                                Состояние Broadcasting, Очередей и Крона
                            </p>
                            <p class="mb-0">
                                <a href="{{ routeCmf('dev.system.index') }}">Перейти</a>
                            </p>
                        </li>
                        <li class="list-group-item flex-column align-items-start">
                            <div>
                                <b>Очереди</b>
                            </div>
                            <p class="mb-1 text-muted">
                                Очистка очередей
                            </p>
                            <p class="mb-0">
                                <a href="{{ routeCmf('dev.queue.index') }}">Перейти</a>
                            </p>
                        </li>
                        <li class="list-group-item flex-column align-items-start">
                            <div>
                                <b>Sitemap</b>
                            </div>
                            <p class="mb-1 text-muted">
                                Данные которые попадают в <code>sitemap.xml</code>
                            </p>
                            <p class="mb-0">
                                <a href="{{ routeCmf('dev.sitemap.index') }}">Перейти</a>
                            </p>
                        </li>
                        <li class="list-group-item flex-column align-items-start">
                            <div>
                                <b>Электронные письма</b>
                            </div>
                            <p class="mb-1 text-muted">
                                Посмотреть шаблон
                            </p>
                            <p class="mb-0">
                                <a href="{{ routeCmf('dev.mail.index') }}">Перейти</a>
                            </p>
                        </li>
                        <li class="list-group-item flex-column align-items-start">
                            <div>
                                <b>Карточки</b>
                            </div>
                            <p class="mb-1 text-muted">
                                Карточки туров для перевода в изображение
                            </p>
                            <p class="mb-0">
                                <a href="{{ routeCmf('dev.cards.index') }}">Перейти</a>
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
