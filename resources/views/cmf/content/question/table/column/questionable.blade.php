<div class="alert alert-{{ $oItem->questionable_object_data['type'] }} alert-sm text-center" role="alert">
    {{ $oItem->questionable_object_data['title'] }}
</div>
