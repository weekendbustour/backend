<form class="row ajax-form" action="{{ routeCmf($model.'.action.item.post', ['name' => 'actionChangePassword', 'id' => $oItem->id]) }}">
    <div class="col-12">
        <div class="form-group">
            <label>Новый Пароль</label>
            <input type="password" class="form-control" name="password" placeholder="Новый Пароль" value="">
        </div>
    </div>
    <div class="col-12">
        <div class="form-group">
            <label>Повторить пароль</label>
            <input type="password" class="form-control" name="password_confirmation" placeholder="Повторить пароль" value="">
        </div>
    </div>
</form>
