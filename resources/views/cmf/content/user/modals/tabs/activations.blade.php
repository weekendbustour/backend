<table class="table table-borderless table-sm table-hover">
    <thead>
    <tr>
        <th>Дата создания</th>
        <th>Код</th>
        <th>Активирован</th>
        <th>Активен</th>
        <th>Дата активации</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th>Дата создания</th>
        <th>Код</th>
        <th>Активирован</th>
        <th>Активен</th>
        <th>Дата активации</th>
    </tr>
    </tfoot>
    <tbody>
    @foreach($oItem->activations()->ordered()->get() as $oActivation)
        <tr>
            <td>
                {{ $oActivation->created_at->format('d.m.Y H:i:s') }}
            </td>
            <td>{{ $oActivation->code }}</td>
            <td>
                @include('cmf.content.default.table.column.default', [
                    'hide_title' => true,
                    'item' => $oActivation,
                    'name' => 'completed',
                    'value' => $aComposerFields[\App\Cmf\Project\Activation\ActivationController::NAME]['completed'],
                ])
            </td>
            <td>
                @include('cmf.content.default.table.column.default', [
                    'hide_title' => true,
                    'item' => $oActivation,
                    'name' => 'status',
                    'value' => $aComposerFields[\App\Cmf\Project\Activation\ActivationController::NAME]['status'],
                ])
            </td>
            <td>
                @include('cmf.content.default.table.column.default', [
                    'hide_title' => true,
                    'item' => $oActivation,
                    'name' => 'completed_at',
                    'value' => $aComposerFields[\App\Cmf\Project\Activation\ActivationController::NAME]['completed_at'],
                ])
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
