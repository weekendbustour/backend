<div class="col-8">
    <div class="form-group">
        <label>Название или ID</label>
        <input class="form-control" type="text" placeholder="Название или ID" name="first_name" value="{{ Request()->first_name ?? '' }}">
    </div>
</div>
<div class="col-4">
    <div class="form-group">
        <label>Статус</label>
        <select class="form-control" name="status">
            <option value="">Ничего не выбрано</option>
            <option value="0" {{ Request()->status === '0' ? 'selected' : '' }}>Не активно</option>
            <option value="1" {{ Request()->status === '1' ? 'selected' : '' }}>Активно</option>
        </select>
    </div>
</div>
