@if(!is_null($oItem->socialAccount) && $oItem->socialAccount->provider === 'vkontakte')
    <a class="btn btn-sm text-info" role="button"
       data-tippy-popover
       data-tippy-content="Пользователь зарегистрирован через ВК"
    >
        <i class="fa fa-vk" aria-hidden="true"></i>
    </a>
@endif
@if(!is_null($oItem->socialAccount) && $oItem->socialAccount->provider === 'yandex')
    <a class="btn btn-sm text-danger" role="button"
       data-tippy-popover
       data-tippy-content="Пользователь зарегистрирован через Yandex"
    >
        <i class="fa fa-yahoo" aria-hidden="true"></i>
    </a>
@endif
@if(!is_null($oItem->socialAccount) && $oItem->socialAccount->provider === 'google')
    <a class="btn btn-sm text-success" role="button"
       data-tippy-popover
       data-tippy-content="Пользователь зарегистрирован через Google +"
    >
        <i class="fa fa-google-plus" aria-hidden="true"></i>
    </a>
@endif
@if(is_null($oItem->socialAccount))
    <a class="btn btn-sm text-default" role="button"
       data-tippy-popover
       data-tippy-content="Пользователь зарегистрирован сайт"
    >
        <i class="fa fa-user" aria-hidden="true"></i>
    </a>
@endif
