@if($item->roles()->count() > 1)
    @foreach($item->roles as $role)
        <span class="text-nowrap">{{ $role->title }}</span><br>
    @endforeach
@else
    {{ $item->roleName }}
@endif
