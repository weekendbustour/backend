<div class="alert alert-primary alert-sm text-center" role="alert"
     data-tippy-popover
     data-tippy-content="Активных / Всего"
>
    <span style="font-size: 12px;">{{ $oItem->toursActive->count() }} / {{ $oItem->tours->count() }}</span>
</div>


