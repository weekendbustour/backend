@if(is_null($oItem->metaTag))
    <a class="btn btn-sm text-danger" role="button"
       data-tippy-popover
       data-tippy-content="Нет Мета Тэгов"
    >
        <i class="fa fa-globe" aria-hidden="true"></i>
    </a>
@endif
