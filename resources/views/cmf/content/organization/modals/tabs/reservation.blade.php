<form class="row ajax-form --tab-parameter-reservation-form" action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionUpdateParameters']) }}">
    @foreach($oReservationOptions as $oOption)
        <div class="col-12">
            <div class="form-group">
                @include('cmf.content.default.form.default', [
                    'item' => null,
                    'name' => 'options[' . $oOption->name . ']',
                    'field' => [
                        \App\Cmf\Core\FieldParameter::TYPE => $oOption->type,
                        \App\Cmf\Core\FieldParameter::TITLE => $oOption->title,
                        \App\Cmf\Core\FieldParameter::PLACEHOLDER => !is_null($oOption->placeholder) ? $oOption->placeholder : $oOption->title,
                        \App\Cmf\Core\FieldParameter::EMPTY => true,
                        \App\Cmf\Core\FieldParameter::DEFAULT => isset($oValues[$oOption->name]) ? $oValues[$oOption->name]->value : '',
                        \App\Cmf\Core\FieldParameter::TOOLTIP => $oOption->tooltip,
                        'rows' => 10,
                        //'title_caption' => $oOption->description,
                    ]
                ])
            </div>
        </div>
    @endforeach
</form>
