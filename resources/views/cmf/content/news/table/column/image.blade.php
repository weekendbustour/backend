<?php
$model = isset($model) ? $model : $sComposerRouteView;
?>
<img src="{{ $oItem->image_square }}" width="30" class="{{ $oItem->position === \App\Models\News::POSITION_BANNER ? 'bordered-success' : '' }}"
     data-fancybox="gallery-post-{{ $model }}"
     href="{{ $oItem->image }}"
     style="cursor: pointer"
>
