<div class="row">
    <div class="col-12">
        <button role="button" class="btn btn-primary ajax-link"
                action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionDuplicateCreate']) }}"
                data-list=".admin-table"
                data-list-action="{{ routeCmf($model.'.view.post') }}"
                data-callback="closeModalAfterSubmit, refreshAfterSubmit, editForm{{ ucfirst($model) }}"
                data-loading="1"
        >
            Создать дубликат
        </button>
    </div>
</div>
