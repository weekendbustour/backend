@include('cmf.content.default.modals.tabs.parameters', [
    'model' => $model,
    'oItem' => $oItem,
    'oOptions' => $oOptions,
    'oValues' => $oValues,
    'alert' => 'Параметры <b>не обязательны</b> к заполнению, если параметр заполнен, то он будет отображаться на странице тура в блоке "Характеристики"'
])
