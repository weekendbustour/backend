@if(!is_null($oItem->organization))
    <a class="avatar" href="{{ $oItem->organization->getUrl() }}">
        <img class="img-avatar" src="{{ $oItem->organization->image_xs }}" width="30"
             data-tippy-popover
             data-tippy-content="{{ $oItem->organization->title }}"
        >
        <span class="avatar-status {{ $oItem->organization->isActive() ? 'badge-success' : 'badge-danger' }}"></span>
    </a>
@else
    <span class="text-muted">-</span>
@endif


