<a class="btn btn-sm text-default d-flex align-items-center justify-content-center trigger" role="button"
   data-dialog="#custom-edit-modal"
   data-ajax
   data-action="{{ routeCmf($model . '.action.item.post', ['id' => $oItem->id, 'name' => 'actionVkParametersGet']) }}"
>
    <span class="{{ !is_null($oItem->vkPost) ? 'text-success' : 'text-danger' }}" style="font-size: 10px;margin: 2px;"
          data-tippy-popover
          data-tippy-content="{{ !is_null($oItem->vkPost) ? 'Есть пост в ВК' : 'Нет поста в ВК' }}"
    >
        <i class="fa fa-circle" aria-hidden="true"></i>
    </span>
    <span class="{{ !is_null($oItem->vkMarket) ? 'text-success' : 'text-danger' }}" style="font-size: 10px;margin: 2px;"
          data-tippy-popover
          data-tippy-content="{{ !is_null($oItem->vkMarket) ? 'Есть страница товара в ВК' : 'Нет страницы товара в ВК' }}"
    >
        <i class="fa fa-circle" aria-hidden="true"></i>
    </span>
</a>



