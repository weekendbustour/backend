<a class="btn btn-sm text-default d-flex align-items-center justify-content-center trigger" role="button"
   data-dialog="#custom-edit-modal"
   data-ajax
   data-action="{{ routeCmf($model . '.action.item.post', ['id' => $oItem->id, 'name' => 'actionTagsGet']) }}"
>
    <span class=""
          style="font-size: 10px;margin: 2px;color: {{ !is_null($oItem->tag) ? $oItem->tag->color : '#626262' }}"
          data-tippy-popover
          data-tippy-content="{{ !is_null($oItem->tag) ? 'Тэг: ' . $oItem->tag->title  : 'Тег не проставлен' }}"
    >
        <i class="fa fa-circle" aria-hidden="true"></i>
    </span>
</a>



