<?php
    $count = $oItem->timelines()->count();
?>
<button class="btn btn-sm trigger {{ $count !== 0 ? 'btn-success' : 'btn-default'}}"
     role="button"
    data-dialog="#custom-edit-modal"
    data-ajax
    data-action="{{ routeCmf($model . '.action.item.post', ['id' => $oItem->id, 'name' => 'actionTimelineGet']) }}"
    data-ajax-init="uploader, {{ $init ?? '' }}"
    style="width: 100%;"
>
    @if($count !== 0)
        <small>{{ $count }}</small>
    @else
        <small>0</small>
    @endif
</button>
