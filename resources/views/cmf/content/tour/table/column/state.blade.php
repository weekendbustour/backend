@if(!is_null($oItem->cancellation))
    <a class="btn btn-sm text-danger" role="button"
       data-tippy-popover
       data-tippy-content="Тур отменен"
    >
        <i class="fa fa-ban" aria-hidden="true"></i>
    </a>
@endif
@if(!is_null($oItem->announcement))
    <a class="btn btn-sm text-default" role="button"
       data-tippy-popover
       data-tippy-content="Анонс тура"
    >
        <i class="fa fa-eye-slash" aria-hidden="true"></i>
    </a>
@endif
@if(!is_null($oItem->duplicate_id))
    <a class="btn btn-sm text-default" role="button"
       data-tippy-popover
       data-tippy-content="Дубликат тура #{{ $oItem->duplicate_id }}"
    >
        <i class="fa fa-link" aria-hidden="true"></i>
    </a>
@endif
@if($oItem->multi)
    <a class="btn btn-sm text-primary" role="button"
       data-tippy-popover
       data-tippy-content="Мультитур"
    >
        <i class="fa fa-clone" aria-hidden="true"></i>
    </a>
@endif
@if(is_null($oItem->metaTag))
    <a class="btn btn-sm text-danger" role="button"
       data-tippy-popover
       data-tippy-content="Нет Мета Тэгов"
    >
        <i class="fa fa-globe" aria-hidden="true"></i>
    </a>
@endif
