<?php
$count = $oItem->entertainments()->count();
?>
<button class="btn btn-sm trigger {{ $count !== 0 ? 'btn-success' : 'btn-default'}}"
        role="button"
        data-dialog="#custom-edit-modal"
        data-ajax
        data-action="{{ routeCmf($model . '.action.item.post', ['id' => $oItem->id, 'name' => 'getEntertainments']) }}"
        data-ajax-init="{{ $init ?? '' }}"
        style="padding-left: 12px;padding-right: 12px;"
>
    @if($count !== 0)
        <small>{{ $count }}</small>
    @else
        <small>0</small>
    @endif
</button>
