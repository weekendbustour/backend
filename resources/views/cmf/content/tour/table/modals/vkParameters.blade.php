<div class="modal-content dialog__content dialog__content--vkParameters">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ (new \App\Cmf\Project\ModalController())->editTitle($model, $oItem) }}
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12 mb-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#user-edit-tab-0" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Состояние</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#user-edit-tab-1" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Пост</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#user-edit-tab-2" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Товар</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane tab-submit active " id="user-edit-tab-0" role="tabpanel" aria-expanded="true">
                        <div class="row">
                            <div class="hr-label">
                                <label>Пост</label>
                                <hr>
                            </div>
                            <div class="col-12">
                                <button role="button" class="btn btn-success ajax-link"
                                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionVkParametersPostCreate']) }}"
                                        data-list=".admin-table"
                                        data-list-action="{{ routeCmf($model.'.view.post') }}"
                                        data-view=".dialog__content--vkParameters"
                                        data-callback="replaceView, refreshAfterSubmit"
                                        data-loading="1"
                                >
                                    Создать
                                </button>
                                <button role="button" class="btn btn-success ajax-link"
                                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionVkParametersPostMarketCreate']) }}"
                                        data-list=".admin-table"
                                        data-list-action="{{ routeCmf($model.'.view.post') }}"
                                        data-view=".dialog__content--vkParameters"
                                        data-callback="replaceView, refreshAfterSubmit"
                                        data-loading="1"
                                >
                                    Создать с товаром
                                </button>
                                <button role="button" class="btn btn-danger ajax-link {{ !is_null($oItem->vkPost) ? '' : 'disabled' }}"
                                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionVkParametersPostDelete']) }}"
                                        data-list=".admin-table"
                                        data-list-action="{{ routeCmf($model.'.view.post') }}"
                                        data-view=".dialog__content--vkParameters"
                                        data-callback="replaceView, refreshAfterSubmit"
                                        data-loading="1"
                                >
                                    Удалить
                                </button>
                            </div>
                            @if(!is_null($oItem->vkPost))
                                <div class="col-12 mt-1">
                                    @include('cmf.content.default.table.column.default_row_text', [
                                       'name' => 'item_id',
                                       'value' => $oItem->vkPost->item_id ?? 'Пусто',
                                   ])
                                    @include('cmf.content.default.table.column.default_row_text', [
                                        'name' => 'photo_id',
                                        'value' => $oItem->vkPost->photo_id ?? 'Пусто',
                                    ])
                                    @include('cmf.content.default.table.column.default_row_text', [
                                        'name' => 'Дата публикации',
                                        'value' => $oItem->vkPost->published_at ?? 'Пусто',
                                    ])
                                </div>
                            @endif
                        </div>
                        <div class="row mt-1">
                            <div class="hr-label">
                                <label>Товар</label>
                                <hr>
                            </div>
                            <div class="col-12">
                                <button role="button" class="btn btn-success ajax-link"
                                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionVkParametersMarketCreate']) }}"
                                        data-list=".admin-table"
                                        data-list-action="{{ routeCmf($model.'.view.post') }}"
                                        data-view=".dialog__content--vkParameters"
                                        data-callback="replaceView, refreshAfterSubmit"
                                        data-loading="1"
                                >
                                    {{ !is_null($oItem->vkMarket) ? 'Обновить' : 'Создать' }}
                                </button>
                                <button role="button" class="btn btn-danger ajax-link {{ !is_null($oItem->vkMarket) ? '' : 'disabled' }}"
                                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionVkParametersMarketDelete']) }}"
                                        data-list=".admin-table"
                                        data-list-action="{{ routeCmf($model.'.view.post') }}"
                                        data-view=".dialog__content--vkParameters"
                                        data-callback="replaceView, refreshAfterSubmit"
                                        data-loading="1"
                                >
                                    Удалить
                                </button>
                            </div>
                            @if(!is_null($oItem->vkMarket))
                                <div class="col-12 mt-1">
                                    @include('cmf.content.default.table.column.default_row_text', [
                                        'name' => 'item_id',
                                        'value' => $oItem->vkMarket->item_id ?? 'Пусто',
                                    ])
                                    @include('cmf.content.default.table.column.default_row_text', [
                                        'name' => 'photo_id',
                                        'value' => $oItem->vkMarket->photo_id ?? 'Пусто',
                                    ])
                                    @include('cmf.content.default.table.column.default_row_text', [
                                        'name' => 'Дата публикации',
                                        'value' => $oItem->vkMarket->published_at ?? 'Пусто',
                                    ])
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane tab-submit" id="user-edit-tab-1" role="tabpanel" aria-expanded="true">
                        <div class="row">
                            <div class="hr-label">
                                <label>Текст</label>
                                <hr>
                            </div>
                            <div class="col-12 --vk-parameters-text-container">
                                {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($oItem->vkPostTextTour()) !!}
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane tab-submit" id="user-edit-tab-2" role="tabpanel" aria-expanded="true">
                        <div class="row">
                            <div class="hr-label">
                                <label>Заголовок</label>
                                <hr>
                            </div>
                            <div class="col-12">
                                <b>{{ $oItem->metaTag->title ?? '' }}</b>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="hr-label">
                                <label>Текст</label>
                                <hr>
                            </div>
                            <div class="col-12 --vk-parameters-text-container">
                                {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($oItem->vkMarketTextTour()) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
    </div>
</div>
