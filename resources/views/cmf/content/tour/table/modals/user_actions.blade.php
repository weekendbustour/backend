<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Просмотр</h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ (new \App\Cmf\Project\ModalController())->editTitle($model, $oItem) }}
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12 mb-12">
                @if(count($oItem->userActionsActive) !== 0)
                    <div class="list-group">
                        @foreach($oItem->userActionsActive as $oAction)
                            <div class="list-group-item flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between">
                                    <div>
                                        <b>{{ $oAction->start_at->format('d.m.Y H:i:s') }}</b>
                                    </div>
                                    <div>
                                        <span class="{{ $oAction->type_icon['class'] }}">{{ $oAction->type_text }}</span>
                                    </div>
                                </div>
                                <p class="mb-0 text-muted">
                                    {{ $oAction->user->full_name }}
                                </p>
                            </div>
                        @endforeach
                    </div>
                @else
                    <div class="text-muted">
                        Данных нет
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
    </div>
</div>



