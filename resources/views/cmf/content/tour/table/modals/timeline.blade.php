<div class="modal-header">
    <h4 class="modal-title">Редактирование</h4>
    <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
        <div class="col-12">
            {{ (new \App\Cmf\Project\ModalController())->editTitle($model, $oItem) }}
        </div>
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-12 mb-12">
            @if(count($oTimelines) !== 0)
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#timeline-date-text" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">
                            Текст
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#timeline-date-images" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="1">
                            Изображения
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#timeline-date-info" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">
                            Информация
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane tab-submit active" id="timeline-date-text" role="tabpanel" aria-expanded="true">
                        @include('cmf.content.tour.table.modals.timeline.tabs.text', [
                            'model' => $model,
                            'oItem' => $oItem,
                            'oGroupedTimelines' => $oGroupedTimelines,
                            'oTimelines' => $oTimelines,
                            'activeDate' => $activeDate ?? null,
                        ])
                    </div>
                    <div class="tab-pane tab-submit" id="timeline-date-images" role="tabpanel" aria-expanded="true">
                        @include('cmf.content.tour.table.modals.timeline.tabs.images', [
                            'model' => $model,
                            'oItem' => $oItem,
                            'oGroupedTimelines' => $oGroupedTimelines,
                            'oTimelines' => $oTimelines,
                            'activeDate' => $activeDate ?? null,
                        ])
                    </div>
                    <div class="tab-pane tab-submit" id="timeline-date-info" role="tabpanel" aria-expanded="true">
                        @include('cmf.content.tour.table.modals.timeline.tabs.info', [
                            'oItem' => $oItem,
                            'oTimelineOptions' => $oTimelineOptions,
                            'oValues' => $oValues,
                        ])
                    </div>
                </div>
            @else
                <button role="button" class="btn btn-primary mb-1 ajax-link"
                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionTimelineSave', 'auto' => 1]) }}"
                        data-view="#custom-edit-modal .modal-content.--inner"
                        data-callback="updateView"
                        data-ajax-init="datepicker, multiselect, uploader"
                        data-loading="1"
                >
                    Сгенерировать каркас
                </button>
            @endif
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
    <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">
        Сохранить
    </button>
</div>




