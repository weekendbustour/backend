<div class="modal-content dialog__content">
    <div class="modal-header">
        <h4 class="modal-title">Редактирование</h4>
        <div class="text-muted page-desc" style="position: absolute;width: calc(100% - 30px);text-align: right;">
            <div class="col-12">
                {{ (new \App\Cmf\Project\ModalController())->editTitle($model, $oItem) }}
            </div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-md-12 mb-12">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item ">
                        <a class="nav-link active " data-toggle="tab" href="#user-edit-tab-0" role="tab" aria-controls="home" aria-expanded="true" data-hidden-submit="0">Тэги</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane tab-submit active " id="user-edit-tab-0" role="tabpanel" aria-expanded="true">
                        <form class="row ajax-form"
                              action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'saveTags']) }}"
                              data-list=".admin-table"
                              data-list-action="{{ routeCmf($model.'.view.post') }}"
                              data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                        >
                            <div class="col-12">
                                <ul class="list-group list-group-flush abc-checkbox-container">
                                    @foreach($oTags as $oTag)
                                        <li class="list-group-item">
                                            <div class="abc-checkbox is-radio">
                                                <input type="checkbox" id="checkbox-tag-{{ $oTag->id }}" class="styled" name="tag[{{ $oTag->id }}]" value="1"
                                                    {{ isset($oItem) && $oItem->tag_id === $oTag->id ? 'checked' : '' }}
                                                >
                                                <label for="checkbox-tag-{{ $oTag->id }}">{{ $oTag->title }}</label>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
        <button type="button" class="btn btn-primary ajax-link" data-submit-active-tab=".tab-submit">Сохранить</button>
    </div>
</div>



