<div class="row">
    <div class="col-md-12 mb-12">
        <ul class="nav nav-tabs" role="tablist">
            @foreach($oGroupedTimelines as $date => $oGroup)
                <li class="nav-item">
                    <a class="nav-link {{ isset($activeDate) && $activeDate === $date ? 'active' : '' }} {{ !isset($activeDate) && $loop->first ? 'active' : '' }}"
                       data-toggle="tab"
                       href="#timeline-date-{{ $loop->index }}-images"
                       role="tab"
                       aria-controls="home"
                       aria-expanded="true"
                    >
                        {{ $date }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content">
            @foreach($oGroupedTimelines as $date => $oGroup)
                <div class="tab-pane tab-submit {{ isset($activeDate) && $activeDate === $date ? 'active' : '' }} {{ !isset($activeDate) && $loop->first ? 'active' : '' }}" id="timeline-date-{{ $loop->index }}-images" role="tabpanel" aria-expanded="true">
                    <?php
                    $oDayTimeline = $oGroup->where('title', $date)->first();
                    ?>
                    @if(!is_null($oDayTimeline))
                        @include('cmf.content.default.modals.tabs.images.gallery', [
                            'model' => 'tour_timeline',
                            'oItem' => $oDayTimeline,
                        ])
                    @else
                        <div class="alert alert-info" role="alert">
                            Загрузить изображения можно только если есть текстовая запись во вкладке <b>Текст</b> с данными: <br>
                            <b>Заголовок:</b> {{ $date }}<br>
                            <b>Дата:</b> {{ $date }}<br>
                            <b>Тип:</b> Заголовок дня<br>
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</div>
