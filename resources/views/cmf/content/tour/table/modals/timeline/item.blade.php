<?php
    $idDate = isset($date) ? $date : \Illuminate\Support\Str::random(10);
    $id = 'tmp-' . $idDate;
?>
@if(isset($oTimeline))
    <div class="is-row-group row">
        <div class="form-group col-6 {{ $oTimeline->type === \App\Models\TourTimeline::TYPE_DAY_TITLE ? 'is-disabled' : '' }}">
            <label>Заголовок</label>
            <input class="form-control" type="text" name="timeline[{{$oTimeline->id}}][title]" placeholder="Заголовок" value="{{ $oTimeline->title }}">
        </div>
        <div class="form-group col-6">
            @include('cmf.content.default.form.default', [
                'name' => 'timeline[' . $oTimeline->id . '][type]',
                'field' => [
                    \App\Cmf\Core\FieldParameter::TYPE => App\Cmf\Core\MainController::DATA_TYPE_SELECT,
                    \App\Cmf\Core\FieldParameter::TITLE => 'Тип',
                    \App\Cmf\Core\FieldParameter::EMPTY => true,
                    \App\Cmf\Core\FieldParameter::REQUIRED => true,
                    \App\Cmf\Core\FieldParameter::VALUES => [
                        \App\Models\TourTimeline::TYPE_SECONDARY_TITLE => 'Обычный заголовок',
                        \App\Models\TourTimeline::TYPE_PRIMARY_TITLE => 'Крупный заголовок',
                        \App\Models\TourTimeline::TYPE_DAY_TITLE => 'Заголовок дня',
                    ],
                    \App\Cmf\Core\FieldParameter::SELECTED_VALUES => [$oTimeline->type],
                ]
            ])
        </div>
    </div>
    <div class="is-row-group row">
        <div class="form-group col-6 d-flex">
            <div>
                @include('cmf.content.default.form.default', [
                    'name' => 'timeline[' . $oTimeline->id . '][start_at][date]',
                    'field' => [
                        'title' => 'Начало в',
                        'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                        'date' => true,
                        'required' => true,
                        'default' => $oTimeline->start_at ? $oTimeline->start_at->format('d.m.Y') : '',
                        'placeholder' => 'XX.XX.XXXX',
                    ]
                ])
            </div>
            <div style="margin-left: -1px;width: 100px;">
                @include('cmf.content.default.form.default', [
                    'name' => 'timeline[' .  $oTimeline->id . '][start_at][time]',
                    'field' => [
                        'title' => 'Время',
                        'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                        'time' => true,
                        'placeholder' => '00:00',
                        'default' => $oTimeline->start_at_hourly && $oTimeline->start_at ? $oTimeline->start_at->format('H:i') : '',
                        'noIcon' => true,
                    ]
                ])
            </div>
        </div>
        <div class="form-group col-6 d-flex">
            <div>
                @include('cmf.content.default.form.default', [
                    'name' => 'timeline[' . $oTimeline->id . '][finish_at][date]',
                    'field' => [
                        'title' => 'Конец в',
                        'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                        'date' => true,
                        'default' => $oTimeline->finish_at ? $oTimeline->finish_at->format('d.m.Y') : '',
                        'placeholder' => 'XX.XX.XXXX',
                    ]
                ])
            </div>
            <div style="margin-left: -1px;width: 100px;">
                @include('cmf.content.default.form.default', [
                    'name' => 'timeline[' .  $oTimeline->id . '][finish_at][time]',
                    'field' => [
                        'title' => 'Время',
                        'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                        'time' => true,
                        'default' => $oTimeline->finish_at_hourly && $oTimeline->finish_at ? $oTimeline->finish_at->format('H:i') : '',
                        'noIcon' => true,
                    ]
                ])
            </div>
        </div>
    </div>
    <div class="form-group">
        <textarea class="form-control markdown drop-area" cols="15" rows="5" placeholder="Описание" name="timeline[{{$oTimeline->id}}][description]">{{ $oTimeline->description }}</textarea>
    </div>
@else
    <div class="is-row-group row">
        <div class="form-group col-6">
            <label>Заголовок</label>
            <input class="form-control" type="text" name="timeline[{{$id}}][title]" placeholder="Заголовок" value="">
        </div>
        <div class="form-group col-6">
            @include('cmf.content.default.form.default', [
                'name' => 'timeline[' . $id . '][type]',
                'field' => [
                    \App\Cmf\Core\FieldParameter::TYPE => App\Cmf\Core\MainController::DATA_TYPE_SELECT,
                    \App\Cmf\Core\FieldParameter::TITLE => 'Тип',
                    \App\Cmf\Core\FieldParameter::EMPTY => true,
                    \App\Cmf\Core\FieldParameter::VALUES => [
                        \App\Models\TourTimeline::TYPE_SECONDARY_TITLE => 'Обычный заголовок',
                        \App\Models\TourTimeline::TYPE_PRIMARY_TITLE => 'Крупный заголовок',
                        \App\Models\TourTimeline::TYPE_DAY_TITLE => 'Заголовок дня',
                    ],
                    \App\Cmf\Core\FieldParameter::SELECTED_VALUES => [
                        \App\Models\TourTimeline::TYPE_SECONDARY_TITLE,
                    ],
                    'size' => 'sm',
                ]
            ])
        </div>
    </div>
    <div class="is-row-group row">
        <div class="form-group col-6 d-flex">
            <div>
                @include('cmf.content.default.form.default', [
                    'name' => 'timeline[' . $id . '][start_at][date]',
                    'field' => [
                        'title' => 'Дата',
                        'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                        'date' => true,
                        'placeholder' => 'XX.XX.XXXX',
                        'default' => isset($date) && $date !== null
                        ? $date
                        : ($oItem->start_at
                            ? $oItem->start_at->format('d.m.Y')
                            : ''),
                    ]
                ])
            </div>
            <div style="margin-left: -1px;width: 100px;">
                @include('cmf.content.default.form.default', [
                    'name' => 'timeline[' . $id . '][start_at][time]',
                    'field' => [
                        'title' => 'Время',
                        'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                        'time' => true,
                        'placeholder' => '00:00',
                        'noIcon' => true,
                    ]
                ])
            </div>
        </div>
        <div class="form-group col-6 d-flex">
            <div>
                @include('cmf.content.default.form.default', [
                    'name' => 'timeline[' . $id . '][finish_at][date]',
                    'field' => [
                        'title' => 'Дата',
                        'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                        'date' => true,
                        'placeholder' => 'XX.XX.XXXX',
                    ]
                ])
            </div>
            <div style="margin-left: -1px;width: 100px;">
                @include('cmf.content.default.form.default', [
                    'name' => 'timeline[' . $id . '][finish_at][time]',
                    'field' => [
                        'title' => 'Время',
                        'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                        'time' => true,
                        'placeholder' => '00:00',
                        'noIcon' => true,
                    ]
                ])
            </div>
        </div>
    </div>
    <div class="form-group">
        <textarea class="form-control markdown drop-area" cols="15" rows="5" placeholder="Описание" name="timeline[{{$id}}][description]"></textarea>
    </div>
@endif
