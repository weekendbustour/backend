<form class="row ajax-form"
      id="timeline-form"
      action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionTimelineSave']) }}"
      data-view="#custom-edit-modal .modal-content.--inner"
      data-callback="updateView"
      data-ajax-init="datepicker, multiselect, uploader"
>
    <div class="col-md-12 mb-12">
        <ul class="nav nav-tabs" role="tablist">
            @foreach($oGroupedTimelines as $date => $oGroup)
                <li class="nav-item">
                    <a class="nav-link {{ isset($activeDate) && $activeDate === $date ? 'active' : '' }} {{ !isset($activeDate) && $loop->first ? 'active' : '' }}"
                       data-toggle="tab"
                       href="#timeline-date-{{ $loop->index }}-text"
                       role="tab"
                       aria-controls="home"
                       aria-expanded="true"
                    >
                        {{ $date }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content">
            @foreach($oGroupedTimelines as $date => $oGroup)
                <div class="tab-pane tab-submit {{ isset($activeDate) && $activeDate === $date ? 'active' : '' }} {{ !isset($activeDate) && $loop->first ? 'active' : '' }}" id="timeline-date-{{ $loop->index }}-text" role="tabpanel" aria-expanded="true">
                    <div class="row">
                        <div class="col-12">
                            <?php
                            $oDayTimeline = $oGroup->where('type', \App\Models\TourTimeline::TYPE_DAY_TITLE)->first();
                            ?>
                            @if(count($oGroup) !== 0)
                                <ul class="list-group list-group-flush">
                                    <li id="timeline-template" class="list-group-item" style="flex-direction: column;align-items: normal;">
                                        @include('cmf.content.tour.table.modals.timeline.group-item', [
                                            'oTimeline' => null,
                                            'oItem' => $oItem,
                                            'date' => $date,
                                        ])
                                    </li>
                                    @foreach($oGroup as $oTimeline)
                                        <li class="list-group-item" style="flex-direction: column;align-items: normal;">
                                            @include('cmf.content.tour.table.modals.timeline.group-item', [
                                                'oTimeline' => $oTimeline,
                                                'date' => $date,
                                            ])
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                            @if(is_null($oDayTimeline))
                                <button role="button" class="btn btn-primary mb-1 ajax-link mt-1"
                                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionTimelineSave']) }}"
                                        data-view="#custom-edit-modal .modal-content.--inner"
                                        data-callback="updateView"
                                        data-ajax-init="datepicker, multiselect, uploader"
                                        data-loading="1"
                                        data-date="{{ $date }}"
                                >
                                    Сгенерировать Заголовок Дня
                                </button>
                            @endif
                        </div>
                    </div>
                    @if(count($oGroup) !== 0)
                        <div class="row">
                            <div class="col-12 text-right mt-1">
                                <button role="button" class="btn btn-danger ajax-link"
                                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionTimelineDeleteDay']) }}"
                                        data-view="#custom-edit-modal .modal-content.--inner"
                                        data-callback="updateView"
                                        data-ajax-init="datepicker, multiselect, uploader"
                                        data-loading="1"
                                        data-date="{{ $date }}"
                                >
                                    Удалить данные этого дня
                                </button>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
</form>
