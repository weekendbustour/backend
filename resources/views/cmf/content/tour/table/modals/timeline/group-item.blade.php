@if(!is_null($oTimeline))
    <div class="row">
        <div class="col-12" style="display: flex;justify-content: space-between;">
            <div>
                @if($oTimeline->start_at_hourly)
                    <b>{{ $oTimeline->start_at->format('d.m H:i') }}</b>
                @else
                    <b>{{ $oTimeline->start_at->format('d.m') }}</b>
                @endif
                @if(!is_null($oTimeline->finish_at))
                    @if($oTimeline->finish_at_hourly)
                        - <b>{{ $oTimeline->finish_at->format('d.m H:i') }}</b>
                    @else
                        - <b>{{ $oTimeline->finish_at->format('d.m') }}</b>
                    @endif
                @endif
                {{ $oTimeline->title }}
                @if($oTimeline->type === \App\Models\TourTimeline::TYPE_PRIMARY_TITLE)
                    - <i class="fa fa-header" aria-hidden="true"></i>
                @endif
                @if($oTimeline->type === \App\Models\TourTimeline::TYPE_DAY_TITLE)
                    - <i class="fa fa-font" aria-hidden="true"></i>
                @endif
            </div>
            <div>
                <button class="btn btn-link btn-sm" type="button" data-toggle="collapse" data-target="#collapseOne{{ $oTimeline->id }}" aria-expanded="true" aria-controls="collapseOne{{ $oTimeline->id }}">
                    <small>Редактировать</small>
                </button>
                <button class="btn btn-link btn-sm text-danger ajax-link" type="button"
                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oTimeline->id, 'name' => 'actionTimelineDelete']) }}"
                        data-view="#custom-edit-modal .modal-content.--inner"
                        data-callback="updateView"
                        data-ajax-init="datepicker, multiselect, uploader"
                        data-loading="1"
                >
                    <small>Удалить</small>
                </button>
            </div>
        </div>
    </div>
@else
{{--    <div class="row">--}}
{{--        <div class="col-12" style="display: flex;justify-content: space-between;">--}}
{{--            <div>--}}

{{--            </div>--}}
{{--            <div>--}}
{{--                <button class="btn btn-link btn-sm" type="button" data-toggle="collapse" data-target="#collapseOneTmp" aria-expanded="true" aria-controls="collapseOneTmp">--}}
{{--                    <small>Добавить</small>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
@endif
<div id="collapseOne{{ $oTimeline->id ?? 'Tmp' }}" class="row collapse {{ is_null($oTimeline) ? 'show' : '' }}">
    <div class="col-12 pt-1 pb-1">
        @include('cmf.content.tour.table.modals.timeline.item', [
            'oTimeline' => $oTimeline,
            'date' => is_null($oTimeline) && isset($date) ? $date : null,
        ])
    </div>
    @if(is_null($oTimeline))
        <div class="col-12 mb-1">
            <button type="button" class="btn btn-primary ajax-link" data-form="#timeline-form">Сохранить</button>
        </div>
    @endif
</div>
