<form class="row ajax-form --tab-parameter-reservation-form" action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionUpdateParameters']) }}">
    <div class="col-12">
        <div class="alert alert-info" role="alert">
            По умолчанию в Секцию <b>"Как забронировать"</b> будет поставлена информация от Организатора, которая сохранена во вкладке <b>"Как забронировать"</b>
        </div>
    </div>
    @if(!is_null($oItem->organization->parameter_reservation))
        <div class="col-12">
            <div class="alert alert-success" role="alert">
                <p class="mb-0"><b>У Организатора заполнено:</b></p>
                {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($oItem->organization->parameter_reservation) !!}
            </div>
        </div>
        <div class="col-12 text-right">
            <button role="button" class="btn btn-primary ajax-link mb-1 {{ !is_null($oItem->organization->parameter_reservation) ? '' : 'disabled' }}"
                    action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionParameterReservationAutoGenerateOrganization']) }}"
                    data-view=".--tab-parameter-reservation-form"
                    data-callback="replaceView"
                    data-loading="1"
            >
                Добавить и Изменить что есть у Организатора
            </button>
        </div>
    @endif
{{--    <div class="col-12 text-right">--}}
{{--        <button role="button" class="btn btn-secondary ajax-link mb-2"--}}
{{--                action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionParameterReservationAutoGenerate']) }}"--}}
{{--                data-view=".--tab-parameter-reservation-form"--}}
{{--                data-callback="replaceView"--}}
{{--                data-loading="1"--}}
{{--        >--}}
{{--            Сгенерировать от Системы--}}
{{--        </button>--}}
{{--    </div>--}}
    @foreach($oReservationOptions as $oOption)
        <div class="col-12">
            <div class="form-group">
                @include('cmf.content.default.form.default', [
                    'item' => null,
                    'name' => 'options[' . $oOption->name . ']',
                    'field' => [
                        \App\Cmf\Core\FieldParameter::TYPE => $oOption->type,
                        \App\Cmf\Core\FieldParameter::TITLE => $oOption->title,
                        \App\Cmf\Core\FieldParameter::PLACEHOLDER => !is_null($oOption->placeholder) ? $oOption->placeholder : $oOption->title,
                        \App\Cmf\Core\FieldParameter::EMPTY => true,
                        \App\Cmf\Core\FieldParameter::DEFAULT => isset($oValues[$oOption->name]) ? $oValues[$oOption->name]->value : '',
                        \App\Cmf\Core\FieldParameter::TOOLTIP => $oOption->tooltip,
                        'rows' => 10,
                        //'title_caption' => $oOption->description,
                    ]
                ])
            </div>
        </div>
    @endforeach
</form>
