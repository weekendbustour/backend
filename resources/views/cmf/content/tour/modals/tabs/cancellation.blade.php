<div class="--tab-cancellation-container">
    <div class="row">
        <div class="col-12 mb-1">
            Статус:&#160;
            @if(!is_null($oItem->cancellation))
                <span class="badge badge badge-success">Активно</span>
            @else
                <span class="badge badge badge-default">Не активно</span>
            @endif
        </div>
    </div>
    <form class="row ajax-form"
          action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionSaveCancellation']) }}"
          data-view=".--tab-cancellation-container"
          data-ajax-init="multiselect, datepicker"
          data-list=".admin-table"
          data-callback="replaceView, refreshAfterSubmit"
          data-list-action="{{ routeCmf($model.'.view.post') }}"
    >
        <div class="form-group col-12">
            @include('cmf.content.default.form.default', [
                'name' => 'cancellation[type]',
                'field' => [
                    \App\Cmf\Core\FieldParameter::TYPE => App\Cmf\Core\MainController::DATA_TYPE_SELECT,
                    \App\Cmf\Core\FieldParameter::TITLE => 'Тип',
                    \App\Cmf\Core\FieldParameter::VALUES => (new \App\Models\TourCancellation())->types(),
                    \App\Cmf\Core\FieldParameter::SELECTED_VALUES => [
                        $oItem->cancellation->type ?? \App\Models\TourCancellation::TYPE_QUARANTINE,
                    ],
                ]
            ])
        </div>
        <div class="form-group col-12">
            @include('cmf.content.default.form.default', [
                'name' => 'cancellation[tooltip]',
                'field' => [
                    \App\Cmf\Core\FieldParameter::TYPE => App\Cmf\Core\MainController::DATA_TYPE_TEXT,
                    \App\Cmf\Core\FieldParameter::TITLE => 'Подсказка',
                    \App\Cmf\Core\FieldParameter::DEFAULT => $oItem->cancellation->tooltip ?? '',
                ]
            ])
        </div>
        <div class="form-group col-12">
            @include('cmf.content.default.form.default', [
                'name' => 'cancellation[description]',
                'field' => [
                    \App\Cmf\Core\FieldParameter::TYPE => App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
                    \App\Cmf\Core\FieldParameter::TITLE => 'Описание',
                    \App\Cmf\Core\FieldParameter::DEFAULT => $oItem->cancellation->description ?? '',
                ]
            ])
        </div>
        <div class="col-12">
            <div class="is-row-group row">
                <div class="form-group col-6 d-flex">
                    <div>
                        @include('cmf.content.default.form.default', [
                            'name' => 'cancellation[start_at][date]',
                            'field' => [
                                'title' => 'Заблокировано с',
                                'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                                'date' => true,
                                'placeholder' => 'XX.XX.XXXX',
                                \App\Cmf\Core\FieldParameter::DEFAULT => !is_null($oItem->cancellation) && !is_null($oItem->cancellation->start_at)
                                    ? $oItem->cancellation->start_at->format('d.m.Y')
                                    : '',
                            ]
                        ])
                    </div>
                    <div style="margin-left: -1px;width: 100px;">
                        @include('cmf.content.default.form.default', [
                            'name' => 'cancellation[start_at][time]',
                            'field' => [
                                'title' => 'Время',
                                'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                                'time' => true,
                                'placeholder' => '00:00',
                                'noIcon' => true,
                                \App\Cmf\Core\FieldParameter::DEFAULT => !is_null($oItem->cancellation) && !is_null($oItem->cancellation->start_at) && $oItem->cancellation->start_at_hourly
                                    ? $oItem->cancellation->start_at->format('H:i')
                                    : '',
                            ]
                        ])
                    </div>
                </div>
                <div class="form-group col-6 d-flex">
                    <div>
                        @include('cmf.content.default.form.default', [
                            'name' => 'cancellation[finish_at][date]',
                            'field' => [
                                'title' => 'Заблокировано до',
                                'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                                'date' => true,
                                'placeholder' => 'XX.XX.XXXX',
                                \App\Cmf\Core\FieldParameter::DEFAULT => !is_null($oItem->cancellation) && !is_null($oItem->cancellation->finish_at)
                                    ? $oItem->cancellation->finish_at->format('d.m.Y')
                                    : '',
                            ]
                        ])
                    </div>
                    <div style="margin-left: -1px;width: 100px;">
                        @include('cmf.content.default.form.default', [
                            'name' => 'cancellation[finish_at][time]',
                            'field' => [
                                'title' => 'Время',
                                'dataType' => App\Cmf\Core\MainController::DATA_TYPE_DATE,
                                'time' => true,
                                'placeholder' => '00:00',
                                'noIcon' => true,
                                \App\Cmf\Core\FieldParameter::DEFAULT => !is_null($oItem->cancellation) && !is_null($oItem->cancellation->finish_at) && $oItem->cancellation->finish_at_hourly
                                    ? $oItem->cancellation->finish_at->format('H:i')
                                    : '',
                            ]
                        ])
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <button class="btn btn-outline-danger ajax-link" type="button"
                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionDeleteCancellation']) }}"
                        data-view=".--tab-cancellation-container"
                        data-ajax-init="multiselect, datepicker"
                        data-list=".admin-table"
                        data-callback="replaceView, refreshAfterSubmit"
                        data-list-action="{{ routeCmf($model.'.view.post') }}"
                        data-loading="1"
                >
                    Удалить
                </button>
                <button class="btn btn-primary inner-form-submit" type="submit">
                    Сохранить Данные
                </button>
            </div>
        </div>
    </form>
</div>
