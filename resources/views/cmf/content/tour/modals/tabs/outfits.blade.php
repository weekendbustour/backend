<form class="row ajax-form"
      action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionSaveRentOutfits']) }}"
>
    <div class="col-12">
        <div class="alert alert-info" role="alert">
            Подразумевается Аренда от Организатора, будет отображено в блоке <b>"Аренда снаряжения"</b>
        </div>
    </div>
    <div class="col-12">
        <table class="table">
            <tbody>
            @foreach($oOutfits as $oOutfit)
                <tr>
                    <td>
                        <b>{{ $oOutfit->title }}</b>
                        @if(!is_null($oOutfit->tooltip))
                            @include('cmf.components.tooltip.question', [
                                'title' => $oOutfit->tooltip,
                            ])
                        @endif
                    </td>
                    <td>
                        @include('cmf.content.default.form.default', [
                            'no_title' => true,
                            'name' => 'outfit[' . $oOutfit->id . '][rent_price]',
                            'field' => [
                                \App\Cmf\Core\FieldParameter::TYPE => \App\Cmf\Core\MainController::DATA_TYPE_NUMBER,
                                \App\Cmf\Core\FieldParameter::TITLE => 'Цена',
                                \App\Cmf\Core\FieldParameter::LENGTH => 5,
                                \App\Cmf\Core\FieldParameter::DEFAULT => $aTourOutfits[$oOutfit->id]['pivot']['rent_price'] ?? '',
                            ]
                        ])
                    </td>
                    <td>
                        @include('cmf.content.default.form.default', [
                            'no_title' => true,
                            'name' => 'outfit[' . $oOutfit->id . '][rent_description]',
                            'field' => [
                                \App\Cmf\Core\FieldParameter::TYPE => \App\Cmf\Core\MainController::DATA_TYPE_TEXT,
                                \App\Cmf\Core\FieldParameter::TITLE => 'Примечание',
                                \App\Cmf\Core\FieldParameter::DEFAULT => $aTourOutfits[$oOutfit->id]['pivot']['rent_description'] ?? '',
                            ]
                        ])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-12">
        <div class="row">
            @foreach($oOutfitRentOptions as $oOption)
                @include('cmf.content.default.modals.tabs.parameters.field', [
                    'oOption' => $oOption,
                    'oValues' => $oValues,
                ])
            @endforeach
        </div>
    </div>
</form>
