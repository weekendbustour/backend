<div>
    <div class="alert alert-info" role="alert">
        По умолчанию в Секцию <b>"Условия отмены Бронирования"</b> будет поставлена информация от Организатора, которая сохранена во вкладке <b>"Параметры"</b>
    </div>
    @if(!is_null($oItem->organization->parameterPrepaymentCancellation))
        <div class="alert alert-success" role="alert">
            <p class="mb-0"><b>У Организатора заполнено:</b></p>
            {!! GrahamCampbell\Markdown\Facades\Markdown::convertToHtml($oItem->organization->parameterPrepaymentCancellation) !!}
        </div>
    @endif
</div>
<div class="form-group">
    @include('cmf.content.default.form.default', [
        'name' => 'options[' . $oOption->name . ']',
        'field' => $field
    ])
</div>
