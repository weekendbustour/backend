<form class="row ajax-form"
      action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionSaveDepartures']) }}"
      data-counter=".admin-table-counter"
      data-list=".admin-table"
      data-list-action="{{ routeCmf($model.'.view.post') }}"
      data-callback="refreshAfterSubmit, editForm{{ ucfirst($model) }}"
>
    <div class="col-12">
        <div class="is-row-group row">
            @foreach($oItem->departures as $oDeparture)
                <div class="form-group col-6">
                    <label>
                        Дата и время отправления из {{ $oDeparture->title }}
                    </label>
                    <div class="input-group">
                        <div class="input-group-prepend bg-default">
                        <span class="input-group-text">
                            <i class="fa fa-calendar"></i>
                        </span>
                        </div>
                        <input type="text" class="form-control datetimepicker" name="departure[{{ $oDeparture->id }}][departure_at][date]"
                               data-format="date"
                               data-role="js-mask-datetime"
                               value="{{ !is_null($oDeparture->pivot->departure_at) ? \Carbon\Carbon::parse($oDeparture->pivot->departure_at)->format('d.m.Y') : '' }}"
                               placeholder="Дата"
                        >
                        <input type="text" class="form-control datetimepicker" name="departure[{{ $oDeparture->id }}][departure_at][time]"
                               data-format="time"
                               data-role="js-mask-datetime"
                               value="{{ !is_null($oDeparture->pivot->departure_at) && $oDeparture->pivot->departure_at_hourly ? \Carbon\Carbon::parse($oDeparture->pivot->departure_at)->format('H:i') : '' }}"
                               placeholder="00:00"
                               style="margin-left: -1px;"
                        >
                    </div>
                </div>
                <div class="form-group col-6">
                    @include('cmf.content.default.form.default', [
                        'name' => 'departure[' . $oDeparture->id . '][description]',
                        'field' => [
                            'title' => 'Подробности',
                            'dataType' => App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
                            'default' => $oDeparture->pivot->description,
                        ]
                    ])
                </div>
            @endforeach
        </div>
    </div>
</form>
