<div class="--tab-announcement-container">
    <div class="row">
        <div class="col-12 mb-1">
            Статус:&#160;
            @if(!is_null($oItem->announcement))
                <span class="badge badge badge-success">Активно</span>
            @else
                <span class="badge badge badge-default">Не активно</span>
            @endif
        </div>
    </div>
    <form class="row ajax-form"
          action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionSaveAnnouncement']) }}"
          data-view=".--tab-announcement-container"
          data-callback="replaceView, refreshAfterSubmit"
          data-ajax-init="multiselect"
          data-list=".admin-table"
          data-list-action="{{ routeCmf($model.'.view.post') }}"
    >
        <div class="form-group col-12">
            @include('cmf.content.default.form.default', [
                'name' => 'announcement[type]',
                'field' => [
                    \App\Cmf\Core\FieldParameter::TYPE => App\Cmf\Core\MainController::DATA_TYPE_SELECT,
                    \App\Cmf\Core\FieldParameter::TITLE => 'Тип',
                    \App\Cmf\Core\FieldParameter::VALUES => (new \App\Models\TourAnnouncement())->types(),
                    \App\Cmf\Core\FieldParameter::SELECTED_VALUES => [
                        \App\Models\TourAnnouncement::TYPE_SIMPLE,
                    ],
                    \App\Cmf\Core\FieldParameter::DEFAULT => $oItem->announcement->type ?? \App\Models\TourAnnouncement::TYPE_SIMPLE,
                ]
            ])
        </div>
        <div class="form-group col-12">
            @include('cmf.content.default.form.default', [
                'name' => 'announcement[tooltip]',
                'field' => [
                    \App\Cmf\Core\FieldParameter::TYPE => App\Cmf\Core\MainController::DATA_TYPE_TEXT,
                    \App\Cmf\Core\FieldParameter::TITLE => 'Подсказка',
                    \App\Cmf\Core\FieldParameter::DEFAULT => $oItem->announcement->tooltip ?? '',
                ]
            ])
        </div>
        <div class="form-group col-12">
            @include('cmf.content.default.form.default', [
                'name' => 'announcement[description]',
                'field' => [
                    \App\Cmf\Core\FieldParameter::TYPE => App\Cmf\Core\MainController::DATA_TYPE_TEXTAREA,
                    \App\Cmf\Core\FieldParameter::TITLE => 'Описание',
                    \App\Cmf\Core\FieldParameter::DEFAULT => $oItem->announcement->description ?? '',
                ]
            ])
        </div>
        <div class="col-12">
            <div class="d-flex justify-content-between">
                <button class="btn btn-outline-danger ajax-link" type="button"
                        action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionDeleteAnnouncement']) }}"
                        data-view=".--tab-announcement-container"
                        data-list=".admin-table"
                        data-callback="replaceView, refreshAfterSubmit"
                        data-list-action="{{ routeCmf($model.'.view.post') }}"
                        data-ajax-init="multiselect"
                        data-loading="1"
                >
                    Удалить
                </button>
                <button class="btn btn-primary inner-form-submit" type="submit">
                    Сохранить Анонс
                </button>
            </div>
        </div>
    </form>
</div>

