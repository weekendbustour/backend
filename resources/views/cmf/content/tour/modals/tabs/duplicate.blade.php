<div class="row">
    @if($oItem->multi === 1)
        <div class="hr-label">
            <label>Добавить</label>
            <hr>
        </div>
        <form class="ajax-form"
              action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionDuplicateCreate']) }}"
              data-list=".admin-table"
              data-list-action="{{ routeCmf($model.'.view.post') }}"
              data-callback="closeModalAfterSubmit, refreshAfterSubmit"
        >
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        @include('cmf.content.tour.modals.create.components.date', [
                            'oItem' => null,
                        ])
                    </div>
                    <div class="col-12 --multi-col-for-rows">

                    </div>
                    <div class="col-12">
                        <button role="button" class="btn btn-primary ajax-link"
                                action="{{ routeCmf($model.'.action.post', ['name' => 'actionMultiAddDate']) }}"
                                data-view=".--multi-col-for-rows"
                                data-callback="appendView"
                                data-ajax-init="datepicker"
                                data-loading="1"
                        >
                            Добавить дату
                        </button>
                    </div>
                </div>
            </div>
        </form>
    @else
        <div class="hr-label">
            <label>Создать</label>
            <hr>
        </div>
        <div class="col-12">
            <button role="button" class="btn btn-primary ajax-link"
                    action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionDuplicateCreate']) }}"
                    data-list=".admin-table"
                    data-list-action="{{ routeCmf($model.'.view.post') }}"
                    data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    data-loading="1"
            >
                Дубликат
            </button>
        </div>
        <div class="col-12 mt-1">
            <button role="button" class="btn btn-primary ajax-link"
                    action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionDuplicateCreate']) }}"
                    data-list=".admin-table"
                    data-list-action="{{ routeCmf($model.'.view.post') }}"
                    data-callback="closeModalAfterSubmit, refreshAfterSubmit"
                    data-loading="1"
                    data-multi_save="1"
            >
                Дубликат Мульти-тур
            </button>
        </div>
    @endif
</div>
