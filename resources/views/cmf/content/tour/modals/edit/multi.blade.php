<div>
    <div class="abc-checkbox">
        <input type="checkbox" id="checkbox-{{ $name }}" class="styled" name="{{ $name }}" value="1"
            {{ isset($item) && $item->$name ? 'checked' : '' }}
            {{ !isset($item) && !empty($field['default']) ? 'checked' : '' }}
            {{ !empty($field['required']) ? 'required' : '' }}
            {{ !empty($field['unchecked']) ? 'data-unchecked' : '' }}
            data-toggle="collapse" data-target="#collapseMulti" aria-expanded="true" aria-controls="collapseMulti"
        >
        <label for="checkbox-{{ $name }}">{{ $field['title_form'] ?? $field['title'] }}{!! !empty($field['required']) ? '<i class="r">*</i>' : '' !!}</label>
        @if(isset($field['tooltip']))
            @include('cmf.components.tooltip.question', [
                'title' => $field['tooltip'],
            ])
        @endif
    </div>
    <div class="collapse {{ $item->multi ? 'show' : '' }}" id="collapseMulti">
        <div class="row mt-1">
            @foreach($item->multiples as $oMulti)
                <div class="col-12">
                    @include('cmf.content.tour.modals.create.components.date', [
                        'oItem' => $oMulti,
                    ])
                </div>
            @endforeach
        </div>
    </div>
</div>

