<div class="row">
    <?php
    if (!isset($oItem)) {
        $oItem = null;
    }
    $id = is_null($oItem) ? 'tmp-' . \Illuminate\Support\Str::random(10) : $oItem->id;
    ?>
    <div class="{{ !is_null($oItem) ? 'col-6' : 'col-5' }}">
        <div class="form-group">
            @include('cmf.content.default.form.default', [
                'name' => 'multi_start_at_' . $id,
                'item' => $oItem,
                'field' => array_merge(serviceFieldTour()->get('start_at'), [
                    'required' => is_null($oItem) ? false : true,
                    'default' => is_null($oItem) ? false : [
                        'date' => $oItem->start_at->format('d.m.Y'),
                        'time' => $oItem->start_at->format('H:i') !== '00:00' ? $oItem->start_at->format('H:i') : '',
                    ],
                ]),
            ])
        </div>
    </div>
    <div class="{{ !is_null($oItem) ? 'col-6' : 'col-5' }}">
        <div class="form-group">
            @include('cmf.content.default.form.default', [
                'name' => 'multi_finish_at_' . $id,
                'item' => $oItem,
                'field' => array_merge(serviceFieldTour()->get('finish_at'), [
                    'required' => is_null($oItem) ? false : true,
                    'default' => is_null($oItem) ? false : [
                        'date' => $oItem->finish_at->format('d.m.Y'),
                        'time' => $oItem->finish_at->format('H:i') !== '00:00' ? $oItem->finish_at->format('H:i') : '',
                    ],
                ]),
            ])
        </div>
    </div>
    @if(is_null($oItem))
        <div class="col-2">
            <div class="form-group">
                <label for=""> </label>
                <div>
                    <button role="button" class="btn btn-danger remove-multi-date">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        </div>
    @endif
</div>
