<div>
    <div class="abc-checkbox">
        <input type="checkbox" id="checkbox-{{ $name }}" class="styled" name="{{ $name }}" value="1"
            {{ isset($item) && $item->$name ? 'checked' : '' }}
            {{ !isset($item) && !empty($field['default']) ? 'checked' : '' }}
            {{ !empty($field['required']) ? 'required' : '' }}
            {{ !empty($field['unchecked']) ? 'data-unchecked' : '' }}
            data-toggle="collapse" data-target="#collapseMulti" aria-expanded="true" aria-controls="collapseMulti"
        >
        <label for="checkbox-{{ $name }}">{{ $field['title_form'] ?? $field['title'] }}{!! !empty($field['required']) ? '<i class="r">*</i>' : '' !!}</label>
        @if(isset($field['tooltip']))
            @include('cmf.components.tooltip.question', [
                'title' => $field['tooltip'],
            ])
        @endif
    </div>
    <div class="collapse" id="collapseMulti">
        <div class="row mt-1">
            <div class="col-12">
                @include('cmf.content.tour.modals.create.components.date')
            </div>
            <div class="col-12 --multi-col-for-rows">

            </div>
            <div class="col-12">
                <button role="button" class="btn btn-primary ajax-link"
                        action="{{ routeCmf($model.'.action.post', ['name' => 'actionMultiAddDate']) }}"
                        data-view=".--multi-col-for-rows"
                        data-callback="appendView"
                        data-ajax-init="datepicker"
                        data-loading="1"
                >
                    Добавить дату
                </button>
            </div>
        </div>
    </div>
</div>

