@extends('cmf.layouts.cmf')

@php
    $hasSearch = false;
@endphp
@foreach($fields as $key => $field)
    @if(!is_null(Request()->{$key}))
        @php
            $hasSearch = true;
        @endphp
    @endif
@endforeach

@section('content.title')
    @include('cmf.content.default.table.title')
@endsection

@section('content')
    @component('cmf.components.table.index')
        <div id="modal--edit-container" class="modal-content dialog__content {{ $oItem->multi ? '--save-multi' : '' }}">
            <div class="row">
                <div class="col-12">
                    @if($oItem->multi)
                        @include('cmf.content.default.modals.container.components.multiSave')
                    @endif
                    <button type="button" class="btn btn-primary ajax-link" data-forms="#forms-tour">Сохранить</button>
                </div>
            </div>
            <div class="row" id="forms-tour">
                @foreach($tabs as $key => $tab)
                    <div class="col-12">
                        <hr>
                        <h3>{{ $tab['title'] }}</h3>
                        <hr>
                    </div>
                    <div class="col-12">
                        @if(View::exists('cmf.content.' . $sComposerRouteView . '.modals.'.$key))
                            @include('cmf.content.' . $sComposerRouteView . '.modals.'.$key)
                        @else
                            @include('cmf.content.default.modals.'.$key)
                        @endif
                    </div>
                @endforeach
            </div>
        </div>
    @endcomponent
@endsection

@push('scripts')
    <script>
        window['uploader']();
    </script>
@endpush
