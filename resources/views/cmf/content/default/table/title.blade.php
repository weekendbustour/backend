@if(isset($aComposerCmf[$sComposerRouteView]['title']))
    <section class="section">
        <h1 class="title">{{ $aComposerCmf[$sComposerRouteView]['title'] }}</h1>
        {{--<hr>--}}
        @if(!empty($aComposerCmf[$sComposerRouteView]['description']))
            <div class="text-muted page-desc">{{ $aComposerCmf[$sComposerRouteView]['description'] }}</div>
        @endif
    </section>
@endif
