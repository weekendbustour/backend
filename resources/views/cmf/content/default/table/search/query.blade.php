<div class="form-group float-right">
    <select class="form-control ajax-select" name="{{ $name }}" style="height: 35px;width: 200px;margin-right: 5px;"
            action="{{ routeCmf($sComposerRouteView.'.query') }}"
            data-list=".admin-table"
            data-list-action="{{ routeCmf($sComposerRouteView.'.view.post') }}"
            data-callback="refreshAfterSubmit"
            data-loading-container=".admin-table"
            data-loading-container=".admin-table"
            data-null-value="{{ isset($nullValue) ? $nullValue : 0 }}"
    >
        <option value="{{ isset($nullValue) ? $nullValue : 0 }}">Все @if(isset($title)){{ mb_strtolower($title) }}@endif</option>
        <?php
        $keyTitle = isset($keyTitle) ? $keyTitle : 'title';
        ?>
        @foreach($values as $key => $oValue)
            @if(isset($oValue->id) && isset($oValue->{$keyTitle}))
                <option value="{{ $oValue->id }}"
                        @if(Session::exists($sComposerRouteView) && Session::has($sComposerRouteView.'.query') &&
                            isset(Session::get($sComposerRouteView.'.query')[$name]) &&
                            intval(Session::get($sComposerRouteView.'.query')[$name]) === $oValue->id)
                        selected
                        @endif
                >{{ $oValue->{$keyTitle} }}</option>
            @else
                <option value="{{ $key }}"
                        @if(Session::exists($sComposerRouteView) && Session::has($sComposerRouteView.'.query') &&
                            isset(Session::get($sComposerRouteView.'.query')[$name]) &&
                            intval(Session::get($sComposerRouteView.'.query')[$name]) === intval($key))
                        selected
                        @endif
                >{{ isset($oValue['title']) ? $oValue['title'] : $oValue }}</option>
            @endif
        @endforeach
    </select>
</div>
