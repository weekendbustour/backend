@include('cmf.components.gallery.gallery', [
    'model' => $model,
    'oItem' => $oItem,
    'type' => \App\Services\Image\ImageType::GALLERY,
    'images' => $oItem->galleryImages,
])
