<div class="tab-container is-image-{{ \App\Services\Image\ImageType::BANNER }}-type">
    @if($model === \App\Cmf\Project\Tour\TourController::NAME)
        <p class="text-muted text-center">
            Загрузка баннера не обязательна, по умолчанию Баннером будет Главное изображение Тура.
        </p>
    @endif
    @if($model === \App\Cmf\Project\Tour\TourController::NAME && count($oItem->bannerImages) === 0)
        <div class="mb-1">
            <button class="btn btn-primary btn-block ajax-link"
                    action="{{ routeCmf($model.'.action.item.post', ['id' => $oItem->id, 'name' => 'actionBannerGenerate']) }}"
                    data-view=".tab-container.is-image-{{ \App\Services\Image\ImageType::BANNER }}-type"
                    data-callback="updateView"
                    data-ajax-init="uploader"
                    data-loading="1"
            >
                Загрузить баннер Направления
            </button>
        </div>
    @endif
    @include('cmf.components.gallery.gallery', [
        'model' => $model,
        'oItem' => $oItem,
        'type' => \App\Services\Image\ImageType::BANNER,
        'images' => $oItem->bannerImages,
    ])
</div>
