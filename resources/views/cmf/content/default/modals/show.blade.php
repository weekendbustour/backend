{{--<table class="table table-hover table-bordered table-sm">--}}
{{--    <tbody>--}}
{{--    <tr>--}}
{{--        <td style="border-top: none;">--}}
{{--            <b>ID</b>--}}
{{--        </td>--}}
{{--        <td style="border-top: none;">--}}
{{--            {{ $oItem->id }}--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--    @foreach($fields as $field => $value)--}}
{{--        <tr>--}}
{{--            @include('cmf.content.default.table.column.default_table', [--}}
{{--                'name' => $field,--}}
{{--                'item' => $oItem,--}}
{{--                'value' => $value--}}
{{--            ])--}}
{{--        </tr>--}}
{{--    @endforeach--}}
{{--    </tbody>--}}
{{--</table>--}}


<div class="row" style="border-bottom: 1px solid #eee;">
    <div class="col-md-3 col-sm-12 mbt-5">
        <b>ID</b>
    </div>
    <div class="col-md-9 col-sm-12 mbt-5">
        {{ $oItem->id }}
    </div>
</div>
@foreach($fields as $field => $value)
    <div class="row" style="border-bottom: 1px solid #eee;">
        @include('cmf.content.default.table.column.default_row', [
            'name' => $field,
            'item' => $oItem,
            'value' => $value
        ])
    </div>
@endforeach

{{--<div class="form-group">--}}
{{--    <label class="text-muted m-0">ID:</label>--}}
{{--    <br>--}}
{{--    {{ $oItem->id }}--}}
{{--</div>--}}

