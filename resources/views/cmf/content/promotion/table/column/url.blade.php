<a class="btn btn-sm text-primary" href="{{ $oItem->url }}"
   data-tippy-popover
   data-tippy-content="{{ $oItem->url }}"
   target="_blank"
>
    <i class="fa fa-link" aria-hidden="true"></i>
</a>
