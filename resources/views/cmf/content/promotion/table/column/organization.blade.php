@if(!is_null($oItem->organization))
    <div class="avatar"
         data-tippy-popover
         data-tippy-content="{{ $oItem->organization->title }}"
    >
        <img class="img-avatar" src="{{ $oItem->organization->image_square }}" width="30">
        <span class="avatar-status {{ $oItem->organization->isActive() ? 'badge-success' : 'badge-danger' }}"></span>
    </div>
@endif
