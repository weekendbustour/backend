@if(count($oItem->articlesOrdered) !== 0)
    <div class="avatar-combine">
        @foreach($oItem->articlesOrdered as $oArticle)
            <a class="avatar" href="{{ $oArticle->getUrl() }}">
                <img class="img-avatar" src="{{ $oArticle->image_xs }}" width="30"
                     data-tippy-popover
                     data-tippy-content="{{ $oArticle->title }}"
                >
                <span class="avatar-status {{ $oArticle->isActive() ? 'badge-success' : 'badge-danger' }}"></span>
            </a>
        @endforeach
    </div>
@else
    -
@endif

