@if(is_null($oItem->metaTag))
    <a class="btn btn-sm text-danger" role="button"
       data-tippy-popover
       data-tippy-content="Нет Мета Тэгов"
    >
        <i class="fa fa-globe" aria-hidden="true"></i>
    </a>
@endif
@if(is_null($oItem->location))
    <a class="btn btn-sm text-danger" role="button"
       data-tippy-popover
       data-tippy-content="Геолокация не установлена"
    >
        <i class="fa fa-map-marker" aria-hidden="true"></i>
    </a>
@endif
@if(is_null($oItem->description))
    <a class="btn btn-sm text-danger" role="button"
       data-tippy-popover
       data-tippy-content="Текст пустой"
    >
        <i class="fa fa-align-justify" aria-hidden="true"></i>
    </a>
@endif
