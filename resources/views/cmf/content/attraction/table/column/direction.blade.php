@if(!is_null($oItem->direction))
    <a class="avatar" href="{{ $oItem->direction->getUrl() }}">
        <img class="img-avatar" src="{{ $oItem->direction->image_xs }}" width="30"
             data-tippy-popover
             data-tippy-content="{{ $oItem->direction->title }}"
        >
        <span class="avatar-status {{ $oItem->direction->isActive() ? 'badge-success' : 'badge-danger' }}"></span>
    </a>
@else
    -
@endif
