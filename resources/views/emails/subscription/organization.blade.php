@extends('emails.layouts.email')

@section('content')
    @component('emails.components.container')
        <h2 class="mb-3">Новые туры от организатора "{{ $oOrganization->title }}"</h2>
        <p>
            Организатор: <a href="{{ $oOrganization->getUrl() }}" target="_blank">{{ $oOrganization->title }}</a>
        </p>
        <div>
            @foreach($oTours as $oTour)
                <div class="mb-3" style="border: 1px solid #eee; box-shadow: 0 0 4px 0 #eee; border-radius: 2px; padding: 1rem;">
                    <p class="mb-0">{{ $oTour->start_at->format('d.m.Y') }}</p>
                    <p class="mb-0"><b>{{ $oTour->direction->title }}</b></p>
                    <h3 class="mb-0">
                        <a href="{{ $oTour->getUrl() }}" target="_blank">
                            <b>{{ $oTour->title }}</b>
                        </a>
                    </h3>
                    <p class="mb-0">{{ $oTour->preview_description }}</p>
                    <p class="mb-0">Цена:
                        @if(!is_null($oTour->price))
                            @if(!is_null($oTour->price->discount))
                                <b>{{ $oTour->price->discount }}</b>
                            @else
                                <b>{{ $oTour->price->value }}</b>
                            @endif
                        @else
                            <b>-</b>
                        @endif
                    </p>
                    <div class="btn" style="margin-top: 10px;">
                        <a href="{{ $oTour->getUrl() }}" target="_blank">
                            Перейти
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        <p>
            Рассылка происходит изходя из ваших Подписок на сайте. Чтобы отписаться от этого Организатора - перейдите на страницу Подписки и отпишитесь от Организатора.
        </p>
    @endcomponent
@endsection
