@extends('emails.layouts.email')

@section('content')
    @component('emails.components.container')
        <h2 class="mb-3">Привет, Дмитрий</h2>
        <p>
            Вы успешно сбросили свой пароль, чтобы создать новый перейдите по ссылке
        </p>
    @endcomponent
@endsection
