@extends('emails.layouts.email')

@section('content')
    @component('emails.components.container')
        <h2 class="mb-3">Привет, Дмитрий</h2>
        <p>
            Мы рады, что вы присоединились к нам! Уверены, что вы по достоинству
            оцените удобвства поиска туров на нашей платформе. Мы для вас будет собираться всегда
            актуальную информацию в одном месте.
        </p>
    @endcomponent
@endsection
