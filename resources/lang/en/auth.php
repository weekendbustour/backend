<?php 

return [
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'enter' => 'entrance',
    'activate' => 'Activate',
    'login' => 'To come in',
    'registration' => 'check in',
    'register_account' => 'Register an account',
    'forgot_password' => 'Forgot your password?',
    'cancel' => 'Cancel',
    'rest_password' => 'Reset the password',
    'send_reset_link' => 'Send access code',
];