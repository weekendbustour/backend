#!/usr/bin/env make

.PHONY : help build-npm cache-clear build-php cache-update build maintenance-down maintenance-up

.DEFAULT_GOAL := help
SRC ?= ./app
TESTS ?= ./tests
DBS ?= ./database
ROUTES ?= ./routes
FRONTEND ?= ./public/frontend
OS := $(shell uname -s)
COMPOSER := $(shell which composer)
PHP := php

help: ## Показать эту подсказку
	@echo "Сборка. Платформы weekendbustour.ru"

cache-update: ## Обновить кэш главной страницы
	${PHP} ./artisan cache:data-update

phpcs: ## Проверка psr проекта
	./vendor/bin/phpcs -n -s -p --standard=ruleset.xml --report-full --colors ./app ./database

phpcbf: ## Проверка psr проекта
	./vendor/bin/phpcbf --standard=ruleset.xml --extensions=php --tab-width=4 -sp ./app

cache-clear: ## Очистить кэш
	${PHP} ./artisan cache:clear
	${PHP} ./artisan config:clear
	${PHP} ./artisan route:clear
	${PHP} ./artisan view:clear
	${PHP} ${COMPOSER} clearcache
	${PHP} ${COMPOSER} dumpautoload --optimize

build-npm: build-npm-backend build-npm-frontend ## Собрать скрипты

build-npm-backend: ## Собрать скрипты для бэка
	yarn install
	yarn cmf-prod

build-npm-frontend: ## Собрать скрипты для фронта
	yarn --cwd ${FRONTEND} install
	yarn --cwd ${FRONTEND} run build:prod
	#${PHP} ./artisan version:git

optimize: ## Очистка роутов
	${PHP} ./artisan optimize
	${PHP} ./artisan api:cache

build-npm-frontend-pull: ## Собрать скрипты для фронта с пулом фронтенда
	git -C ${FRONTEND} pull
	build-npm-frontend

build-npm-frontend-only: maintenance-down build-npm-frontend maintenance-up

build-php: ## Собрать php модули проекта
	${PHP} ${COMPOSER} install
	${PHP} ${COMPOSER} dumpautoload --optimize

build: maintenance-down build-php build-npm cache-clear maintenance-up cache-update

maintenance-down: ## Перевести проект в режим обслуживания
	${PHP} ./artisan down --allow=admin

maintenance-up: ## Вывести проект из режима обслуживания
	${PHP} ./artisan up
---------------: ## ---------------
